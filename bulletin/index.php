<?php
include("../config/config.php");
$postSortTypes = array('about_sq', 'todays_pick', 'special_feature');
$postArray = array();

if (isset($_REQUEST['sort_by']) AND in_array($_REQUEST['sort_by'], $postSortTypes)) {
    $sort_by = trim($_REQUEST['sort_by']);
    if ($sort_by == 'todays_pick') {
        $where = "WHERE post_status='active' AND DATE(NOW()) BETWEEN post_show_from_date AND  post_show_to_date AND CURDATE()=DATE(`post_created`)";
    } else {
        $where = "WHERE post_status='active' AND post_" . mysqli_real_escape_string($con, $sort_by) . "= 'yes' AND DATE(NOW()) BETWEEN post_show_from_date AND  post_show_to_date ";
    }
} else {
    $where = "WHERE post_status='active' AND post_all_news='yes' AND post_special_feature='no' AND DATE(NOW()) BETWEEN post_show_from_date AND  post_show_to_date ";
}

/* Start post query */
 $postSql = "SELECT * FROM post $where ORDER BY post_priority DESC LIMIT 48";
$postResult = mysqli_query($con, $postSql);
if ($postResult) {
    while ($postResultRowObj = mysqli_fetch_object($postResult)) {
        $postArray[] = $postResultRowObj;
    }
    mysqli_free_result($postResult);
} else {
    if (DEBUG) {
        echo "postResultRowObj Error" . mysqli_error($con);
    } else {
        echo "Query Failed";
    }
}

/* End post query */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>SQ GROUP | Bulletin</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="SQ-Group,KNITWEAR,LINGERIE,SHIRT,RETAIL,Biriqina">
            <meta name="author" content="SQ-Group">
                <meta name="description" content="SQ visions to build an organization that would thrive on its tribal culture. After all, we all have a common story – SQ. The story of SQ, its attitude and the way it conducts its business are part of this segment. It's all about what we are, not what we have.">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
include("bulletin_header.php");
?>


                        </head>
                        <body class="bulletinBody">
                            <div id="topBarContainer"> </div>
                            <div class="mobilemenu" style="display:none">
                                <div class="rmm">
<?php
include("bulletin_left_navigation.php");
?>
                                </div>
                            </div>
                            <div id="wrapper">
                                <div id="leftPanel">
                                    <div class="shadowIe"></div>
                                    <div class="logo">
                                        <p align="center"><a title="Sq" href="<?php echo baseUrl(); ?>"> <img width="100" src="<?php echo baseUrl(); ?>images/logo.png" /></a><br />
                                            <span class="sus"><a title="Bulletin" class="sasLogoLink" href="<?php echo baseUrl('bulletin'); ?>"> Bulletin </a></span><br />
                                            <span class="logoSubTitle">Apparels Only</span>

                                        </p>
                                    </div>

                                    <div class="leftMenu">
<?php
include("bulletin_left_navigation.php");
?>
                                    </div>

                                    <div class="hrcenter"></div>
                                    <div class="slogan"><p align="center">
                                            <img width="140" src="<?php echo baseUrl(); ?>images/slogan.png" />
                                        </p>


                                    </div>

                                    <div class="hrcenter"></div>

                                </div>
                                <div id="rightPanel" >

                                    <div id="container">


                                        <div id="bulletinwrapper">
                                            <!--bulletinHeader-->
                                            <div id="content" class="container clearfix">
<?php
$postArrayCount = count($postArray);
if ($postArrayCount > 0) {
    for ($i = 0; $i < $postArrayCount; $i++) {
        ?>
                                                        <div class="item">

                                                        <?php
                                                        if ($postArray[$i]->post_image != '') {
                                                            ?>
                                                                <a class="sizeXmal" href="<?php echo baseUrl("bulletin/post_details.php?id=" . $postArray[$i]->post_id . "&title=" . clean($postArray[$i]->post_title)); ?>"><img src="<?php echo baseUrl('upload/post_image/' . $postArray[$i]->post_image); ?>" alt="" /></a>
                                                            <?php } ?>
                                                            <div class="itemInner">
                                                                <a class="bTitle" href="<?php echo baseUrl("bulletin/post_details.php?id=" . $postArray[$i]->post_id . "&title=" . clean($postArray[$i]->post_title)); ?>"><?php echo $postArray[$i]->post_title; ?></a>  
                                                                <p> <a class="" href="<?php echo baseUrl("bulletin/post_details.php?id=" . $postArray[$i]->post_id . "&title=" . clean($postArray[$i]->post_title)); ?>"><?php echo $postArray[$i]->post_short_description; ?>
                                                                    </a>
                                                                </p>

                                                            </div>
                                                        </div>
        <?php
    }
} else {
    ?>

                                                    <div class="item">

                                                        <div class="itemInner">
                                                            <a class="bTitle"></a>  
                                                            <p> <a  class="">
                                                                    <?php
                                                                        switch ($sort_by) {
                                                                            case 'todays_pick':
                                                                                echo 'No picks for today.';
                                                                                break;
                                                                            case 'special_feature':
                                                                                echo 'No special feature found.';
                                                                                break;
                                                                            case 'about_sq':
                                                                                echo 'No data found about SQ.';
                                                                                break;

                                                                            default:
                                                                                echo 'No data found.';
                                                                                break;
                                                                        }
                                                                    ?>
                                                                                                                                   </a>
                                                            </p>

                                                        </div>
                                                    </div>
<?php } ?>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div style="clear:both"></div>

                                <!-- Container --> 
                            </div>
                            </div>

                            <script src="<?php echo baseUrl(); ?>js/jquery.masonry.min.js"></script>
                            <script src="<?php echo baseUrl(); ?>js/script.js"></script>

                        </body>
                        </html>