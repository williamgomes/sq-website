<?php
include '../config/config.php';
$post_id = $_REQUEST["id"];

$postShortTitle = '';
$postTitle = '';
$postImage = '';
$postShortDescription = '';
$postContent = '';
$postSourceUrl = '';
$postSourceTitle = '';
$postDate = '';

$postArray = array();
$recentPostArray = array();
$postSql = "SELECT * FROM post WHERE post_id=" . intval($post_id);
$postResult = mysqli_query($con, $postSql);
if ($postResult) {
    if (mysqli_num_rows($postResult) > 0) {
        $postResultRowObj = mysqli_fetch_object($postResult);
        $postShortTitle = $postResultRowObj->post_short_title;
        $postTitle = $postResultRowObj->post_title;
        $postImage = $postResultRowObj->post_image;
        $postShortDescription = $postResultRowObj->post_short_description;
        $postContent = $postResultRowObj->post_content;
        $postSourceUrl = $postResultRowObj->post_source_url;
        $postSourceTitle = $postResultRowObj->post_source_title;
        $postDate = $postResultRowObj->post_show_from_date;
    }


    mysqli_free_result($postResult);
} else {
    if (DEBUG) {
        echo "postResultRowObj Error" . mysqli_error($con);
    } else {
        echo "Query Failed";
    }
}



/* Start recent post query */
$recentPostSql = "SELECT post_id, post_title, post_show_from_date, post_show_to_date FROM post WHERE post_status = 'active' AND DATE(NOW()) BETWEEN post_show_from_date AND  post_show_to_date AND  post_id != " . intval($post_id) . " ORDER BY  post_show_from_date DESC LIMIT 5";
$recentPostSqlResult = mysqli_query($con, $recentPostSql);
if ($recentPostSqlResult) {
    while ($recentPostSqlResultRowObj = mysqli_fetch_object($recentPostSqlResult)) {
        $recentPostArray[] = $recentPostSqlResultRowObj;
    }
    mysqli_free_result($recentPostSqlResult);
} else {
    if (DEBUG) {
        echo "recentPostSqlResult Error" . mysqli_error($con);
    } else {
        echo "recentPostSqlResult Query Failed";
    }
}

/* End recent post query */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>SQ GROUP | Bulletin : <?php echo $postTitle; ?></title>

            <meta name="keywords" content="SQ group, Bulletin, Garments, News, Post" />
            <meta name="description" content="<?php echo $postShortDescription; ?>"/>
            <meta name="author" content="SQ-Group" />


            <link rel="stylesheet"  href="<?php echo baseUrl(); ?>css/style.css" type="text/css">
                <link rel="shortcut icon" href="<?php echo baseUrl(); ?>favicon.ico" type="image/x-icon" />
                <link rel="apple-touch-icon" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon.png" />
                <link rel="apple-touch-icon" sizes="57x57" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-57x57.png" />
                <link rel="apple-touch-icon" sizes="60x60" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-60x60.png" />
                <link rel="apple-touch-icon" sizes="72x72" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-72x72.png" />
                <link rel="apple-touch-icon" sizes="76x76" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-76x76.png" />
                <link rel="apple-touch-icon" sizes="114x114" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-114x114.png" />
                <link rel="apple-touch-icon" sizes="120x120" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-120x120.png" />
                <link rel="apple-touch-icon" sizes="144x144" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-144x144.png" />
                <link rel="apple-touch-icon" sizes="152x152" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-152x152.png" />
                <!-- Custom scrollbars CSS -->
                <link href="<?php echo baseUrl(); ?>css/jquery.mCustomScrollbar.css" rel="stylesheet" />


                <!--[if IE 6]>
                        <link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 7]>
                        <link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 8]>
                        <link href="<?php echo baseUrl(); ?>css/ie" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 9]>
                        <link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 10]>
                        <link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 8]>
                        <link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
                <![endif]-->

                <script src="<?php echo baseUrl(); ?>js/jquery.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(sizeContent);
                    //Every resize of window
                    $(window).resize(sizeContent);
                    //Dynamically assign height
                    function sizeContent() {
                        var newWidth = $("html").width() - $("#leftPanel").width() + "px";
                        $("#rightPanel").css("width", newWidth);
                    }
                </script>

                <script type="text/javascript">
                    if ($(window).width() < 699) {
                        //small screen, load other JS files
                        $.getScript('../js/device.js', function() {
                            //the script has been added to the DOM, you can now use it's code
                        });
                    }


                    window.onorientationchange = function()
                    {
                        window.location.reload();
                    }


                </script>

                <link rel="stylesheet"  href="<?php echo baseUrl(); ?>css/bulletin.css" type="text/css">
                    <link rel="stylesheet" href="<?php echo baseUrl(); ?>css/responsivemobilemenu.css" type="text/css"/>
                    <script type="text/javascript" src="<?php echo baseUrl(); ?>js/responsivemobilemenu.js"></script>

                    <script>
                    (function(i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function() {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                    ga('create', 'UA-43689390-1', 'sqgc.com');
                    ga('send', 'pageview');

                    </script>
                    <script type="text/javascript">

                        var _gaq = _gaq || [];
                        _gaq.push(['_setAccount', 'UA-43689390-2']);
                        _gaq.push(['_trackPageview']);

                        (function() {
                            var ga = document.createElement('script');
                            ga.type = 'text/javascript';
                            ga.async = true;
                            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                            var s = document.getElementsByTagName('script')[0];
                            s.parentNode.insertBefore(ga, s);
                        })();

                    </script>

                    </head>
                    <body class="bulletinBody">
                        <div id="topBarContainer"> </div>
                        <div class="mobilemenu" style="display:none">
                            <div class="rmm">
                                <?php
                                include("bulletin_left_navigation.php");
                                ?>
                            </div>
                        </div>
                        <div id="wrapper">
                            <div id="leftPanel">
                                <div class="shadowIe"></div>
                                <div class="logo">
                                    <p align="center"><a title="SQ Home" href="<?php echo baseUrl('index.php'); ?>"> <img width="100" src="<?php echo baseUrl(); ?>images/logo.png" /></a><br />
                                        <span class="sus"><a title="Bulletin" class="sasLogoLink" href="<?php echo baseUrl('bulletin'); ?>"> Bulletin </a></span><br />
                                        <span class="logoSubTitle">Apparels Only</span> </p>
                                </div>

                                <div class="leftMenu">
                                    <?php
                                    include("bulletin_left_navigation.php");
                                    ?>
                                </div>

                                <div class="hrcenter"></div>
                                <div class="slogan"><p align="center">
                                        <img width="140" src="<?php echo baseUrl(); ?>images/slogan.png" />
                                    </p>


                                </div>

                                <div class="hrcenter"></div>

                            </div>
                            <div id="rightPanel" >

                                <div id="container">


                                    <div id="bulletinwrapper">
                                        <div class="bulletinInner">

                                            <div class="bulletinColumn bulletinColumnPost">
                                                <div class="newsBox">
                                                    <div class="newsBoxInner">
                                                        <?php if (isset($postImage) AND $postImage != ''): ?>
                                                            <img alt="<?php echo $postShortTitle; ?>" src="<?php echo baseUrl('upload/post_image/' . $postImage); ?>" width="100%" />
                                                        <?php endif; /* (isset($postImage)) */ ?>


                                                        <a class="bulletinTitle">
                                                            <?php if ($postShortTitle != '') { ?>
                                                                <strong>
                                                                    <span style="font-size:16px;"><?php echo $postShortTitle; ?></span>
                                                                </strong><br />
                                                                <?php
                                                            }
                                                            ?>
                                                            <?php echo $postTitle; ?> </a>
                                                        <p class="NewsDate"><?php echo date('F j, Y', strtotime($postDate)); ?></p>    
                                                        <p>
                                                            <?php
                                                            echo html_entity_decode($postContent);
                                                            ?>
                                                        </p>
                                                        <?php
                                                        if ($postSourceUrl != '') {
                                                            ?>
                                                            <p>Source: <a style="display:inline; overflow:hidden" target="_blank" href="<?php echo $postSourceUrl; ?>"><?php echo $postSourceTitle; ?></a></p>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>

                                                </div><!--newsBox-->


                                            </div><!--bulletinColumn-->
                                            <?php $recentPostArrayCounter = count($recentPostArray); ?>
                                            <?php if ($recentPostArrayCounter > 0): ?>
                                                <div class="bulletinColumn noMarginRt">

                                                    <div class="newsBox newsBoxColor rightBullrtinNews">

                                                        <h3> Recent News </h3>
                                                        <?php for ($i = 0; $i < $recentPostArrayCounter; $i++): ?>
                                                            <a href="<?php echo baseUrl('bulletin/post_details.php?id=' . $recentPostArray[$i]->post_id . '&title=' . clean($recentPostArray[$i]->post_title)); ?>" title="<?php echo $recentPostArray[$i]->post_title; ?>"><?php echo $recentPostArray[$i]->post_title; ?></a>
                                                        <?php endfor; /* ($i=0; $i < $recentPostArrayCounter; $i++) */ ?>


                                                    </div><!--newsBox-->

                                                    <!--newsBox-->


                                                </div><!--bulletinColumn-->
                                            <?php endif; /* ($recentPostArrayCounter > 0) */ ?>

                                            <!--bulletinColumnRt-->
                                        </div><!--bulletinInner-->
                                    </div>




                                </div>
                                <div style="clear:both"></div>



                                <div style="clear:both"></div>


                                <div style="clear:both"></div>



                            </div>

                            <div style="clear:both"></div>



                            <!-- Container --> 
                        </div>
                        </div>

                    </body>
                    </html>