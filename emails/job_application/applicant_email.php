<?php
include ('../../config/config.php');

if(isset($_GET['applicant_id']) AND $_GET['applicant_id'] != ''){
	
	$fname = '';
	$lname = '';
	$ApplicantID = $_GET['applicant_id'];
	$sqlApplicantInfo = "SELECT * FROM candidate_db WHERE candidate_id=$ApplicantID";
	$executeApplicantInfo = mysqli_query($con,$sqlApplicantInfo);
	if($executeApplicantInfo){
		$getApplicantInfo = mysqli_fetch_object($executeApplicantInfo);
		if(isset($getApplicantInfo->candidate_id)){
			$fname = $getApplicantInfo->candidate_fname;
			$lname = $getApplicantInfo->candidate_lname;
		}
	} else {
		if(DEBUG){
			echo 'executeApplicantInfo error: ' . mysqli_error($con);
		}
	}
	
	
	echo '<html>
	'.$fname.' '.$lname.',<br><br>
	Thank you for submitting your resume. Our management team is reviewing your qualifications and will contact you if there is a match with our current requirements for the position you applied.<br><br>
	We appreciate your interest in SQ Group and wish you the best of luck in your job search.<br><br>
	Thank you.<br><br>
	SQ Group
	</html>';      //HTML Body
	
} else {
	
	echo 'Incorrect information.';
	
}
?>
