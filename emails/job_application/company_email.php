<?php
include ('../../config/config.php');

$position = '';
$target = '';

if(!isset($_GET['applicant_id']) AND $_GET['applicant_id'] == 0){
	echo 'Incorrect Applicant ID.';
} elseif(!isset($_GET['position']) AND $_GET['position'] == ''){
	echo 'Incorrect Position Info.';
} elseif(!isset($_GET['target']) AND $_GET['target'] == ''){
	echo 'Incorrect Target Info.';
} else {
	
	$fname = '';
	$lname = '';
	
	//initializing all get values
	$ApplicantID = $_GET['applicant_id'];
	$position = base64_decode($_GET['position']);
	$target = base64_decode($_GET['target']);
	
	//running sql query in candidate_db to get candidate information
	$sqlApplicantInfo = "SELECT * FROM candidate_db WHERE candidate_id=$ApplicantID";
	$executeApplicantInfo = mysqli_query($con,$sqlApplicantInfo);
	if($executeApplicantInfo){
		$getApplicantInfo = mysqli_fetch_object($executeApplicantInfo);
		if(isset($getApplicantInfo->candidate_id)){
			$fname = $getApplicantInfo->candidate_fname;
			$lname = $getApplicantInfo->candidate_lname;
		}
	} else {
		if(DEBUG){
			echo 'executeApplicantInfo error: ' . mysqli_error($con);
		}
	}
	
	$body='';
	
	$body .= '<html>';
    $body .= 'Hello,<br><br>';       
	$body .= 'We have received new CV from <strong>'.$fname.' '.$lname.'</strong> for the post <strong>'.$position.'</strong>.<br>';
	$body .= 'Please <a href="'.baseUrl($target).'">Click Here</a> to preview cv or copy below link and paste to your browser address bar:<br>';
	$body .= baseUrl($target);
	$body .= '<br><br>Thank you.<br>';
	$body .= 'SQ Group';	    
	$body .= '</html>';      //HTML Body
	echo $body;
}
?>
