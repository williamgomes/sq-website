<?php
include 'config/config.php';
/** Start: query for contact information * */
$contactArray = array();

$contactArray['PHONE_CALL'] = '';
$contactArray['EMAIL'] = '';
$contactArray['VISIT'] = '';
$contactArray['LINKEDIN'] = '';
$contactArray['TWITTER'] = '';
$contactArray['FACEBOOK'] = '';
$contactArray['SKYPE'] = '';

$contact_sql = mysqli_query($con, "SELECT * FROM contact");
if ($contact_sql) {
    while ($getContact = mysqli_fetch_object($contact_sql)) {
        $contactArray[$getContact->contact_option] = $getContact->contact_value;
    }
}

/** End: query for contact information * */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet"  href="css/style.css" type="text/css">
                <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
                <link rel="apple-touch-icon" href="apple-touch-icon.png" />
                <link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png" />
                <link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png" />
                <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png" />
                <link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png" />
                <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png" />
                <link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png" />
                <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png" />
                <link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png" />
                <!-- Custom scrollbars CSS -->
                <title>SQ GROUP</title>
                <!-- Google CDN jQuery with fallback to local -->
                <script src="js/jquery.min.js"></script>
                <link rel="stylesheet" href="css/responsivemobilemenu.css" type="text/css"/>
                <script type="text/javascript" src="js/responsivemobilemenu.js"></script>
                <!-- Load ScrollTo -->
                <script src="js/jquery.scrollTo-1.4.2-min.js"></script>

                <!--[if IE 6]>
                      <link href="css/ie.css" rel="stylesheet" type="text/css" />
                      <![endif]-->

                <!--[if IE 7]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 8]>
                        <link href="css/ie" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 9]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 10]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 8]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                <![endif]--></head>
                <body class="ContactPage">
                    <div id="topBarContainer"> </div>
                    <div class="mobilemenu" style="display:none">
                        <div class="rmm">
                            <?php
                            include("side_menu_home.php");
                            ?>
                        </div>
                    </div>
                    <div id="wrapper">

                        <div id="leftPanel">
                            <div class="shadowIe"></div>
                            <div class="logo">
                                <p align="center">
                                    <a href="index.html"><img width="100" src="images/logo.png" /></a>
                                    <br />

                                    <span class="sus">
                                        <a class="sasLogoLink sasLogoLinkExtra " href="#contact.php">  
                                            Contact us   </a>
                                    </span>
                                </p>
                            </div>

                            <div class="leftMenu">
                                <?php
                                include("side_menu_home.php");
                                ?>
                            </div>
                            <div class="hrcenter"></div>
                            <div class="slogan"><p align="center">
                                    <img width="140" src="images/slogan.png" />
                                </p>


                            </div>

                            <div class="hrcenter"></div>

                        </div>

                        <div id="rightPanel" class="rightInner ">

                            <div class="contactContainer">


                                <div class="contactBanner">
                                    <img src="images/contactbg.jpg" width="100%" />
                                </div>

                                <div style="clear:both"></div>

                                <div class="contactInner">
                                    <div class="">


                                        <h3>Let's talk</h3>
                                        <p class="pDot"></p>
                                        <div class="contactAddress">
                                            <img src="images/phone.png" width="42" height="42" alt="phone" />
                                            <h6>Call</h6>
                                            <p><?php echo $contactArray['PHONE_CALL']; ?></p>
                                        </div><!--contactAddress-->

                                        <div class="contactAddress">
                                            <img src="images/social_email.png" width="42" height="42" alt="phone" />
                                            <h6>Email</h6>
                                            <p><?php echo $contactArray['EMAIL']; ?></p>
                                        </div><!--contactAddress-->


                                        <div class="contactAddress">
                                            <img src="images/skype.png"  height="42" alt="phone" />
                                            <a href="www.skype.com/sqgroupbd">Skype</a>
                                            <p><?php echo $contactArray['SKYPE']; ?></p>
                                        </div><!--contactAddress-->

                                        <div class="contactAddress visit">
                                            <img src="images/home.png" width="42" height="42" alt="phone" />
                                            <h6>Visit</h6>

                                            <p><?php echo $contactArray['VISIT']; ?></p>
                                        </div><!--contactAddress-->

                                        <!--contactAddress-->


                                    </div><!--padding-->
                                </div><!--contactInner-->

                            </div>






                            <!-- Container --> 
                        </div>
                    </div>




                </body>
                </html>