<?php

/* change when upload to different domain 
 * setting site hosting  data 
 */
$host = $_SERVER['HTTP_HOST'];

$domain = str_replace('www.', '', str_replace('http://', '', $host));

if ($domain == 'testserver.bscheme.com') {
    $config['SITE_NAME'] = 'SQ- Group';
    $config['BASE_URL'] = 'http://testserver.bscheme.com/sqdynamic/';
    $config['DB_TYPE'] = 'mysql';
    $config['DB_HOST'] = 'localhost';
    $config['DB_NAME'] = 'bluetest_sq_final';
    $config['DB_USER'] = 'bluetest_sq';
    $config['DB_PASSWORD'] = "[!o[#L(OTl;R";
}elseif($domain == 'sqgc.com' OR $domain == 'sq-group.com'){
    
    $config['SITE_NAME'] = 'SQ- Group';
    $config['BASE_URL'] = 'http://'.$domain.'/';
    $config['DB_TYPE'] = 'mysql';
    $config['DB_HOST'] = 'localhost';
    $config['DB_NAME'] = 'sqgccom_final';
    $config['DB_USER'] = 'sqgccom_blue2';
    $config['DB_PASSWORD'] = "q1ZqOdOrL~gE";  
}elseif($domain == 'brazil2014.sqgc.com'){
    
    $config['SITE_NAME'] = 'SQ- Group';
    $config['BASE_URL'] = 'http://'.$domain.'/';
    $config['DB_TYPE'] = 'mysql';
    $config['DB_HOST'] = 'localhost';
    $config['DB_NAME'] = 'sqgccom_final';
    $config['DB_USER'] = 'sqgccom_blue2';
    $config['DB_PASSWORD'] = "q1ZqOdOrL~gE";  
} else {
    $config['SITE_NAME'] = 'SQ- Group';
    $config['BASE_URL'] = 'http://localhost/sq-website/';
    $config['DB_TYPE'] = 'mysql';
    $config['DB_HOST'] = 'localhost';
    $config['DB_NAME'] = 'sq';
    $config['DB_USER'] = 'root';
    $config['DB_PASSWORD'] = '';
}

    date_default_timezone_set('Asia/Dhaka');
    $config['MASTER_ADMIN_EMAIL'] = "faruk@bscheme.com"; /* Developer*/
    $config['PASSWORD_KEY'] = "#s1q1g1r1o1u1p1*"; /* If u want to change PASSWORD_KEY value first of all make the admin table empty */
    $config['ADMIN_PASSWORD_LENGTH_MAX'] = 15; /* Max password length for admin user  */
    $config['ADMIN_PASSWORD_LENGTH_MIN'] = 5; /* Min password length for admin user  */
    $config['ADMIN_COOKIE_EXPIRE_DURATION'] = (60 * 60 * 24 * 30); /* Min password length for admin user  */
    
    $config['ITEMS_PER_PAGE'] = 20; /* Pagination */
    
    $config['ALLOWED_IMAGE'] = array('jpeg','jpg','png','gif'); /* image allow to upload */
    $config['ALLOWED_FILE'] = array('jpeg','jpg','png','gif','doc','docx','pdf','txt'); /* file allow to upload */
    $config['IMAGE_PATH'] = $config['BASE_DIR'].'/images'; /* system image path */
    $config['IMAGE_URL'] = $config['BASE_URL'].'images'; /* Upload system path */
    $config['IMAGE_UPLOAD_PATH'] = $config['BASE_DIR'].'/upload'; /* Upload files go here */
    $config['IMAGE_UPLOAD_URL'] = $config['BASE_URL'].'upload'; /* Upload link with this */
    $config['IMAGE_UPLOAD_MAX_WIDTH'] = 1200; /* User cann't uplod image greater than $config['IMAGE_UPLOAD_MAX_WIDTH'] value And it will be checked at image upload */
    
    /* Start of magic quote remover function
   This function is used for removing magic quote, Thats means using this function no slash will add automatically before quotations*/
if (get_magic_quotes_gpc()) {
    $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    while (list($key, $val) = each($process)) {
        foreach ($val as $k => $v) {
            unset($process[$key][$k]);
            if (is_array($v)) {
                $process[$key][stripslashes($k)] = $v;
                $process[] = &$process[$key][stripslashes($k)];
            } else {
                $process[$key][stripslashes($k)] = stripslashes($v);
            }
        }
    }
    unset($process);
}

/* End of magic quote remover function*/