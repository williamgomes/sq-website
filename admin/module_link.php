        <!-- Header -->
        <div id="header" class="wrapper">
            <div class="logo"><a href="<?php echo baseUrl('admin/dashboard.php');?>" title=""><img src="<?php echo baseUrl('admin/images/loginLogo.png');?>"  width="186" alt="logo" /></a></div>
            <div class="middleNav">
                <ul>
                    <li class="home"><a href="<?php echo baseUrl('admin/home/index.php');?>" title="Home"><span>Home</span></a></li>
                    <li class="about"><a href="<?php echo baseUrl('admin/about/index.php');?>" title="About"><span>About</span></a></li>
                    <li class="business"><a href="<?php echo baseUrl('admin/business/index.php');?>" title="Business"><span>Business</span></a></li>
                    <li class="industry"><a href="<?php echo baseUrl('admin/supporting_industry/index.php');?>" title="Supporting Industry"><span>Supporting Industry</span></a></li>
                    <li class="sustain"><a href="<?php echo baseUrl('admin/sustainability/index.php');?>" title="Contact"><span>Sustainability</span></a></li>
                    <li class="job"><a href="<?php echo baseUrl('admin/circular/index.php');?>" title="Circular"><span>Circular</span></a></li>
                    <li class="bulleting"><a href="<?php echo baseUrl('admin/bulletin/index.php?status='.  base64_encode('active'));?>" title="Bulletin"><span>Bulletin</span></a></li>
                    
                </ul>
            </div>
            <div class="fix"></div>
        </div>