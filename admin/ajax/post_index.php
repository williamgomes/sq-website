<?php

include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
$err = 0;

if (isset($_POST["post_id"])) {
    $post_id = $_POST["post_id"];
    $coloum_name = $_POST["coloum_name"];
    if (isset($_POST["checked"])) {
        $checkStatus = 'yes';
    } else {
        $checkStatus = 'no';
    }
    $updateRowSql = "UPDATE post SET " . mysqli_real_escape_string($con, $coloum_name) . "='" . mysqli_real_escape_string($con, $checkStatus) . "' WHERE post_id=" . intval($post_id);
    $updateRowResult = mysqli_query($con, $updateRowSql);
    if ($updateRowResult) {
        $err = 0; //No error
    } else {
        if (DEBUG) {
            $err = "updateRowResult error" . mysqli_error($con); //Mysql query failed
        } else {
            $err = 1; //Mysql query failed
        }
    }
    print $err;
}
?>
