<?php
include ('../../config/config.php');
if (isset($_POST["post_id"])) {
    $post_id = $_POST["post_id"];
}
if (isset($_POST["new_priority"])) {
    $new_priority = $_POST["new_priority"];
}
if (isset($post_id) && isset($new_priority)) {
    $priorityUpdateSql = "UPDATE post SET post_priority = $new_priority WHERE post_id=$post_id";
    $priorityUpdateResult = mysqli_query($con, $priorityUpdateSql);
    if ($priorityUpdateResult) {
        $err = 0; //No error
    } else {
        if (DEBUG) {
            $err = "updateRowResult error" . mysqli_error($con); //Mysql query failed
        } else {
            $err = 1; //Mysql query failed
        }
    }
    print $err;
}
?>
