<?php
include ('../../config/config.php');
include basePath('lib/Zebra_Image.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
//saving image module in database

$aid = $_SESSION["admin_id"];
$responses = array();
$getset = mysqli_query($con, "SELECT * FROM config_settings");
if (mysqli_num_rows($getset) > 0) {

    while ($row = mysqli_fetch_object($getset)) {
        $responses[$row->CS_option] = $row->CS_value;
    }
}

$a_image_width = '';
$b_image_width = '';

if (isset($_POST['update'])) {
    extract($_POST);
    if ($a_top_image_width == "") {
        $err = "A Top Image Width filed is required.";
    }elseif ($a_bottom_image_width == "") {
        $err = "A Bottom Image Width filed is required.";
    }elseif ($b_image_width == "") {
        $err = "B Image Width filed is required.";
    }

    if ($err == "") {
        $setupdate = mysqli_query($con, "UPDATE `config_settings` SET `CS_value` = CASE `CS_option`
                                                                                WHEN 'HOME_A_TOP_IMAGE_WIDTH' THEN '$a_top_image_width'
                                                                                WHEN 'HOME_A_BOTTOM_IMAGE_WIDTH' THEN '$a_bottom_image_width'
                                                                                WHEN 'HOME_B_IMAGE_WIDTH' THEN '$b_image_width'
										ELSE `CS_value`
										END,
                                                                                CS_updated_by= CASE CS_option
                                                                                WHEN 'HOME_A_TOP_IMAGE_WIDTH' THEN '$aid'
                                                                                WHEN 'HOME_A_BOTTOM_IMAGE_WIDTH' THEN '$aid'
                                                                                WHEN 'HOME_B_IMAGE_WIDTH' THEN '$aid'
                                                                                ELSE CS_updated_by
                                                                                END");

        if ($setupdate) {
            $msg = "Image Width updated successfully";
        } else {
            if (DEBUG) {
                echo 'setupdate Error' . mysqli_error($con);
            }
        }
    }
}


$responses = array();
$getset = mysqli_query($con, "SELECT * FROM config_settings");
if (mysqli_num_rows($getset) > 0) {

    while ($row = mysqli_fetch_object($getset)) {
        $responses[$row->CS_option] = $row->CS_value;
    }
}

$a_top_image_width = $responses['HOME_A_TOP_IMAGE_WIDTH'];
$a_bottom_image_width = $responses['HOME_A_BOTTOM_IMAGE_WIDTH'];
$b_image_width = $responses['HOME_B_IMAGE_WIDTH'];
//printDie($responses);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Setting Home</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
       <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               
       
    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include basePath('admin/settings/settings_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Settings Module</h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>

                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Website Settings</h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="<?php echo baseUrl('admin/settings/settings_home.php'); ?>" method="post" class="mainForm" enctype="multipart/form-data">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">Home Settings</h5></div>
                                        <div class="rowElem noborder"><label>A Top Image Width:</label><div class="formRight onlyNums"><input name="a_top_image_width" style="width:200px" class="rightDir" title="You cann't upload image less than this width" type="text" value="<?php echo $a_top_image_width; ?>"/> &nbsp;<b>px</b></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>A Bottom Image Width:</label><div class="formRight onlyNums"><input name="a_bottom_image_width" style="width:200px" class="rightDir" title="You cann't upload image less than this width" type="text" value="<?php echo $a_bottom_image_width; ?>"/> &nbsp;<b>px</b></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>B Image Width:</label><div class="formRight onlyNums"><input name="b_image_width" style="width:200px" class="rightDir" title="You cann't upload image less than this width" type="text" value="<?php echo $b_image_width; ?>"/> &nbsp;<b>px</b></div><div class="fix"></div></div>
                                        <input type="submit" name="update" value="Update Image Setting" class="greyishBtn submitForm" />
                                        <div class="fix"></div>

                                    </div>
                                </fieldset>

                            </form>		


                        </div>

                    </div>
                </div>

            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>
