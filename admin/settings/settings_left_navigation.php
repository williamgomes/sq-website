<div class="leftNav">
                <ul id="menu">
                	<li class="dash"><a href="#" title="" class="exp"><span>Settings Module Menu</span><!--<span class="numberLeft">6</span>--></a>
                        <ul class="sub">
                            <li><a href="<?php echo baseUrl('admin/settings/index.php');?>" title="">General</a></li>
                            <li><a href="<?php echo baseUrl('admin/settings/settings_meta.php');?>" title="">Meta</a></li>
			    <li><a href="<?php echo baseUrl('admin/settings/settings_home.php');?>" title="">Home</a></li>
                           <?php if($_SESSION["admin_email"] == $config['MASTER_ADMIN_EMAIL']) { ?>
                           <li><a href="<?php echo baseUrl('admin/settings/settings_option.php');?>" title="">Options Create</a></li> <?php } ?>
                           <li><a href="<?php echo baseUrl('admin/settings/settings_option_list.php');?>" title="">Options List</a></li>
			    <li><a href="<?php echo baseUrl('admin/settings/settings_album.php');?>" title="">Album</a></li>
			    <li><a href="<?php echo baseUrl('admin/settings/settings_sustainability.php');?>" title="">Sustainability</a></li>
			    <li><a href="<?php echo baseUrl('admin/settings/settings_about.php');?>" title="">About</a></li>
			    <li><a href="<?php echo baseUrl('admin/settings/settings_business.php');?>" title="">Business</a></li>
			    <li><a href="<?php echo baseUrl('admin/settings/settings_employee.php');?>" title="">Employee</a></li>
			    <li><a href="<?php echo baseUrl('admin/settings/settings_company.php');?>" title="">Company</a></li>
                <li><a href="<?php echo baseUrl('admin/settings/phpmailer_setting.php');?>" title="">PHPMailer Settings</a></li>
                        </ul>
                    </li>
                </ul>

                <div class="leftCol">
                    <div class="title">
                        <h5>Note</h5>

                    </div>
                    <div class="leftColInner">
                       This is admin module. You can create, update and see the list of admin in this module.
                    </div>
                </div>

            </div>