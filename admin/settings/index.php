<?php
include ('../../config/config.php');
include basePath('lib/Zebra_Image.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
//saving tags in database

$aid = $_SESSION['admin_id']; //getting loggedin admin id
$name = get_option('SITE_NAME');
$url = get_option('SITE_URL');

if (isset($_POST['update'])) {
    extract($_POST);
    if ($name == "") {
        $err = "Website Name filed is required.";
        } elseif ($url == "") {
        $err = "Website URL filed is required.";
        }
        else {
        if ($_FILES['logo']['size'] > 0 || !empty($_FILES['logo']['tmp_name'])) {   //uploading logo if given
            /* if image select for logo */
            $image = basename($_FILES['logo']['name']);
            $info = pathinfo($image, PATHINFO_EXTENSION);
            $image_name = "logo." . $info;
            $image_source = $_FILES["logo"]["tmp_name"];
            $image_target_path = $config['IMAGE_PATH'] . '/' . $image_name;
            if (move_uploaded_file($image_source, $image_target_path)) {
                $logoupdate = mysqli_query($con, "UPDATE `config_settings` SET `CS_value` = CASE `CS_option`
										WHEN 'SITE_LOGO' THEN '$image_name'
										ELSE `CS_value`
										END");
            }
        }

        if ($_FILES['favicon']['size'] > 0 || !empty($_FILES['favicon']['tmp_name'])) {  //uploading favicon if given
            /* if image select for favicon */
            $image = basename($_FILES['favicon']['name']);
            $info = pathinfo($image, PATHINFO_EXTENSION);
            $image_name = "favicon.ico";
            $image_source = $_FILES["favicon"]["tmp_name"];
            $image_target_path = $config['IMAGE_PATH'] . '/' . $image_name;
            if (move_uploaded_file($image_source, $image_target_path)) {
                $logoupdate = mysqli_query($con, "UPDATE `config_settings` SET `CS_value` = CASE `CS_option`
										WHEN 'SITE_LOGO' THEN '$image_name'
										ELSE `CS_value`
										END");
            }
        }


         $setupdate = mysqli_query($con, "UPDATE `config_settings` SET `CS_value` = CASE `CS_option`
										WHEN 'SITE_NAME' THEN '$name'
										WHEN 'SITE_URL' THEN '$url'
                                                                                ELSE `CS_value`
										END");

            if ($setupdate) {
                $msg = "General Setting updated successfully";
                //echo "<Image http-equiv='refresh' content='5; url=index.php'>";
            } else {
                if (DEBUG) {
                    echo 'setupdate Error' . mysqli_error($con);
                }
            }
       
    }
}


//
//$getset = mysqli_query($con, "SELECT * FROM config_settings");
//if (mysqli_num_rows($getset) > 0) {
//    $responses = array();
//    while ($row = mysqli_fetch_assoc($getset)) {
//        $responses[] = array(
//            'Option Name' => $row['CS_option'],
//            'Option Value' => $row['CS_value']
//        );
//    }
//}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Setting General</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
       <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               
       
    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include basePath('admin/settings/settings_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Settings Module</h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>

                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Website Settings</h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="index.php" method="post" class="mainForm" enctype="multipart/form-data">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">General Settings</h5></div>

                                        <div class="rowElem noborder"><label>Website Name:</label><div class="formRight"><input name="name" type="text" value="<?php echo $name; ?>"/></div><div class="fix"></div></div>

                                        <div class="rowElem noborder"><label>Website URL:</label><div class="formRight"><input name="url" type="text" value="<?php echo $url; ?>"/></div><div class="fix"></div></div>

                                        <div class="rowElem noborder"><label>Website Logo:</label><div class="formRight"><input type="file" name="logo" /></div><div class="fix"></div></div>

                                        <div class="rowElem noborder"><label>Website Favicon:</label><div class="formRight"><input type="file" name="favicon" /></div><div class="fix"></div></div>


                                        <input type="submit" name="update" value="Update General Settings" class="greyishBtn submitForm" />
                                        <div class="fix"></div>

                                    </div>
                                </fieldset>

                            </form>		


                        </div>










                    </div>
                </div>

            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>
