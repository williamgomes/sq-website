<?php
include ('../../../config/config.php');
include basePath('lib/Zebra_Image.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
/*
  This page is dependent on album module/ if any change in album->image_upload.php then implement it here
 */


if (isset($_REQUEST['keyword'])) {
    $kewWord = $_REQUEST['keyword'];
    $albumIdSql = "SELECT album_id FROM album WHERE album_keyword='" . mysqli_real_escape_string($con, $kewWord) . "'";
    $albumIdSqlResult = mysqli_query($con, $albumIdSql);
    if ($albumIdSqlResult) {
        $albumIdSqlResultRowObj = mysqli_fetch_object($albumIdSqlResult);

        if (isset($albumIdSqlResultRowObj->album_id)) {
            $album_id = $albumIdSqlResultRowObj->album_id;
        } else {
            $link = '../index.php?err=' . base64_encode("Unknown keyword: " . $_REQUEST['keyword']);
            redirect($link);
        }
    } else {
        if (DEBUG) {
            echo '$albumIdSqlResult Error :' . mysqli_error($con);
        } else {
            $err = 'album id Sql Sql  failed';
        }
    }
} else if (isset($_REQUEST['id'])) {
    $album_id = base64_decode($_REQUEST['id']);
} else {
    $link = '../index.php?err=' . base64_encode("Unknown album");
    redirect($link);
}
$author = $_SESSION['admin_id'];
$ALBUM_IMAGE_WIDTH = get_option('ALBUM_IMAGE_WIDTH');
$album_image = '';
$image_title = '';
$image_priority = '';
$image_description = '';
$image_edit = false;
$image_id = 0;


if (isset($_REQUEST['id']) AND isset($_REQUEST['action']) AND $_REQUEST['action'] == 'edit' AND isset($_REQUEST['image_id'])) {
    /* Start image edit */


    $imageSql = "SELECT * FROM album_images WHERE AI_album_id=" . intval($album_id) . " AND  AI_id=" . intval(base64_decode($_REQUEST['image_id']));
    $imageSqlResult = mysqli_query($con, $imageSql);
    if ($imageSqlResult) {
        $imageSqlResultRowObj = mysqli_fetch_object($imageSqlResult);
        if (isset($imageSqlResultRowObj->AI_id)) {
            $album_image = $imageSqlResultRowObj->AI_image_name;
            $image_title = $imageSqlResultRowObj->AI_image_title;
            $image_priority = $imageSqlResultRowObj->AI_image_priority;
            $image_description = $imageSqlResultRowObj->AI_image_description;
            $image_edit = true;
            $image_id = $imageSqlResultRowObj->AI_id;
        }
    } else {

        if (DEBUG) {
            echo 'imageSqlResult Error :' . mysqli_error($con);
        } else {
            $err = 'image Sql  failed';
        }
    }


    /* end image edit */
} elseif (isset($_REQUEST['id']) AND isset($_REQUEST['action']) AND $_REQUEST['action'] == 'del' AND isset($_REQUEST['image_id'])) {
    $imageSQL = "SELECT `AI_image_name` FROM `album_images` WHERE AI_id=" . base64_decode($_REQUEST['image_id']) . " ORDER BY AI_image_priority DESC ";
    $imageSQLResult = mysqli_query($con, $imageSQL);
    if ($imageSQLResult) {
        $imageSQLResultRowObj = mysqli_fetch_object($imageSQLResult);
        if (isset($imageSQLResultRowObj)) {
            if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/' . $imageSQLResultRowObj->AI_image_name)) {
                unlink($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/' . $imageSQLResultRowObj->AI_image_name);
            }
            if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/small/' . $imageSQLResultRowObj->AI_image_name)) {
                unlink($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/small/' . $imageSQLResultRowObj->AI_image_name);
            }
            if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/mid/' . $imageSQLResultRowObj->AI_image_name)) {
                unlink($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/mid/' . $imageSQLResultRowObj->AI_image_name);
            }
            if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/large/' . $imageSQLResultRowObj->AI_image_name)) {
                unlink($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/large/' . $imageSQLResultRowObj->AI_image_name);
            }

            $imageDeleteSql = "DELETE FROM album_images WHERE AI_id=" . base64_decode($_REQUEST['image_id']);
            $imageDeleteSqlResult = mysqli_query($con, $imageDeleteSql);
            if ($imageDeleteSqlResult) {
                $msg = "Delete successfully";
            } else {

                if (DEBUG) {
                    $err = "imageDeleteSqlResult ERROR : " . mysqli_error($con);
                } else {
                    $err = "Image not delete";
                }
            }
        }
    } else {

        if (DEBUG) {
            $err = "imageDeleteSqlResult ERROR : " . mysqli_error($con);
        } else {
            $err = "Image not found";
        }
    }
}





if (isset($_POST['image_upload']) AND $_POST['image_upload'] == 'Submit') {
    extract($_POST);
    $width = 0;
    if ($_FILES["album_image"]["error"] == 0) {
        list($width) = getimagesize($_FILES["album_image"]["tmp_name"]);
    }

//echo $width;
    if ($_FILES["album_image"]["error"] > 0) {
        $err = "Valid Image is required";
    } else if ($width < $ALBUM_IMAGE_WIDTH) {
        $err = "Image width must be atleast <b>{$ALBUM_IMAGE_WIDTH}</b>";
    } else if ($width > $config['IMAGE_UPLOAD_MAX_WIDTH']) {
        $err = "Image width should not exceed <b>{$config['IMAGE_UPLOAD_MAX_WIDTH']}</b>";
    } else if ($image_title == '') {
        $err = 'Image title field is required!!';
    } else if ($image_priority == '') {
        $err = 'Image priority field is required!!';
    } elseif (!is_numeric($image_priority)) {
        $err = 'Image Priority should be numeric!!';
    } else if ($image_description == '') {
        $err = 'Image description field is required!!';
    }
    if ($err == '') {
        $max_image_id = getMaxValue('album_images', 'AI_id');
        $new_image_id = $max_image_id + 1;
        /* Srat: image upload */
        $album_image = basename($_FILES['album_image']['name']);
        $info = pathinfo($album_image, PATHINFO_EXTENSION); /* it will return me like jpeg, gif, pdf, png */
        $album_image_name = str_replace(' ', '_', $image_title) . '-' . $new_image_id . '.' . $info; /* create custom image name color id will add  */
        $album_image_source = $_FILES["album_image"]["tmp_name"];


        $album_images_target_path = $config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/' . $album_image_name;
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/large/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/large/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/mid/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/mid/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/small/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/small/', 0777, TRUE);
        }
        if (!move_uploaded_file($album_image_source, $album_images_target_path)) {
            $album_image_name = '';
        }
        /* End: image upload */ else {

            /* Start: uploaded image resize */


            $album_image_source = $album_images_target_path;
            $image = new Zebra_Image();
            /* Start code for large size image */
            $album_images_target_path_large = $config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/large/' . $album_image_name;

            $image->target_path = $album_images_target_path_large;
            $image->source_path = $album_image_source;

            $image->preserve_aspect_ratio = true;
            $largeSize = ceil($ALBUM_IMAGE_WIDTH);
            if (!$image->resize($largeSize)) {

// if there was an error, let's see what the error is about
                $err = zebraImageErrorHandaling($image->error);

// if no errors
            }
            /* End code for large size image */
            /* Start code for midium size image */
            $album_images_target_path_mid = $config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/mid/' . $album_image_name;

            $image->target_path = $album_images_target_path_mid;
            $image->source_path = $album_image_source;

            $image->preserve_aspect_ratio = true;
            $midSize = ceil($ALBUM_IMAGE_WIDTH / 2);
            if (!$image->resize($midSize)) {

// if there was an error, let's see what the error is about
                $err = zebraImageErrorHandaling($image->error);

// if no errors
            }
            /* End code for midium size image */

            /* Start code for small size image */
            $album_images_target_path_small = $config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/small/' . $album_image_name;
            $image->target_path = $album_images_target_path_small;
            $image->source_path = $album_image_source;

            $image->preserve_aspect_ratio = true;
            $smallSize = ceil($midSize / 2);
            if (!$image->resize($smallSize)) {

// if there was an error, let's see what the error is about
                $err = zebraImageErrorHandaling($image->error);

// if no errors
            }
            /* End code for small size image */

            /* End: uploaded image resize */


            if ($err == '') {

                $imageField = '';
                $imageField .= ' AI_id =' . intval($new_image_id);
                $imageField .= ', AI_album_id =' . intval($album_id);
                $imageField .= ', AI_image_name ="' . mysqli_real_escape_string($con, $album_image_name) . '"';
                $imageField .= ', AI_image_title ="' . htmlentities(mysqli_real_escape_string($con, $image_title)) . '"';
                $imageField .= ', AI_image_priority ="' . htmlentities(mysqli_real_escape_string($con, $image_priority)) . '"';
                $imageField .= ', AI_image_description ="' . htmlentities(mysqli_real_escape_string($con, $image_description)) . '"';
                $imageField .= ', AI_updated ="' . date("Y-m-d") . '"';
                $imageField .= ', AI_updated_by ="' . $author . '"';

                $imageInsSql = "INSERT INTO `album_images` SET $imageField";
                $imageInsSqlResult = mysqli_query($con, $imageInsSql);
                if ($imageInsSqlResult) {
                    $msg = "Image information successfully added";
                    $image_title = '';
                    $image_description = '';
                    $image_priority = '';
                } else {
                    if (DEBUG) {
                        echo 'imageInsSqlResult Error: ' . mysqli_error($con);
                    }
                    $err = "Insert Query failed.";
                }
            }
        }
    }
}


if (isset($_POST['image_upload']) AND $_POST['image_upload'] == 'Edit image') {
    extract($_POST);
    $width = 0;
    if ($_FILES["album_image"]["error"] == 0) {
        list($width) = getimagesize($_FILES["album_image"]["tmp_name"]);
    }

    if ($_FILES["album_image"]["error"] == 0 AND $width < $ALBUM_IMAGE_WIDTH) {
        $err = "Image width must not less than <b>{$ALBUM_IMAGE_WIDTH}</b>";
    } else if ($width > $config['IMAGE_UPLOAD_MAX_WIDTH']) {
        $err = "Image width must not exceed <b>{$config['IMAGE_UPLOAD_MAX_WIDTH']}</b>";
    } else if ($image_title == '') {
        $err = 'Image title field is required!!';
    } else if ($image_priority == '') {
        $err = 'Image priority field is required!!';
    } else if ($image_description == '') {
        $err = 'Image description field is required!!';
    }
    if ($err == '') {


        /* Srat: image upload */
        if ($_FILES["album_image"]["error"] == 0) {
            $album_image = basename($_FILES['album_image']['name']);
            $info = pathinfo($album_image, PATHINFO_EXTENSION); /* it will return me like jpeg, gif, pdf, png */
            $album_image_name = str_replace(' ', '_', $image_title) . '-' . $image_id . '.' . $info; /* create custom image name color id will add  */
            $album_image_source = $_FILES["album_image"]["tmp_name"];


            $album_images_target_path = $config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/' . $album_image_name;

            if (!move_uploaded_file($album_image_source, $album_images_target_path)) {
                $album_image_name = '';
            }
            /* End: image upload */ else {

                /* Start: uploaded image resize */


                $image = new Zebra_Image();
                $album_image_source = $album_images_target_path;
                /* Start code for midium size image */
                $album_images_target_path_large = $config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/large/' . $album_image_name;

                $image->target_path = $album_images_target_path_large;
                $image->source_path = $album_image_source;

                $image->preserve_aspect_ratio = true;
                $largeSize = ceil($ALBUM_IMAGE_WIDTH);
                if (!$image->resize($largeSize)) {

// if there was an error, let's see what the error is about
                    $err = zebraImageErrorHandaling($image->error);

// if no errors
                }
                /* End code for midium size image */
                /* Start code for midium size image */
                $album_images_target_path_mid = $config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/mid/' . $album_image_name;

                $image->target_path = $album_images_target_path_mid;
                $image->source_path = $album_image_source;

                $image->preserve_aspect_ratio = true;
                $midSize = ceil($largeSize / 2);
                if (!$image->resize($midSize)) {

// if there was an error, let's see what the error is about
                    $err = zebraImageErrorHandaling($image->error);

// if no errors
                }
                /* End code for midium size image */

                /* Start code for small size image */
                $album_images_target_path_small = $config['IMAGE_UPLOAD_PATH'] . '/album/' . $album_id . '/small/' . $album_image_name;
                $image->target_path = $album_images_target_path_small;
                $image->source_path = $album_image_source;

                $image->preserve_aspect_ratio = true;
                $smallSize = ceil($midSize / 2);
                if (!$image->resize($smallSize)) {

// if there was an error, let's see what the error is about
                    $err = zebraImageErrorHandaling($image->error);

// if no errors
                }
                /* End code for small size image */

                /* End: uploaded image resize */
            }
        }

        if ($err == '') {
            // printDie($_POST, TRUE);
            $imageField = '';
            $imageField .= ' AI_album_id =' . intval($album_id);
            if (isset($album_image_name) && $album_image_name != '') {
                $imageField .= ', AI_image_name ="' . mysqli_real_escape_string($con, $album_image_name) . '"';
            }
            $imageField .= ', AI_image_title ="' . htmlentities(mysqli_real_escape_string($con, $image_title)) . '"';
            $imageField .= ', AI_image_priority ="' . htmlentities(mysqli_real_escape_string($con, $image_priority)) . '"';
            $imageField .= ', AI_image_description ="' . htmlentities(mysqli_real_escape_string($con, $image_description)) . '"';
            $imageField .= ', AI_updated ="' . date("Y-m-d") . '"';
            $imageField .= ', AI_updated_by ="' . $author . '"';

            $imageInsSql = "UPDATE `album_images` SET $imageField WHERE AI_id= " . $image_id;
            $imageInsSqlResult = mysqli_query($con, $imageInsSql);
            if ($imageInsSqlResult) {
                $msg = "Image information update successfully";
                $image_edit = false;
                $image_title = '';
                $image_description = '';
                $image_priority = '';
            } else {
                if (DEBUG) {
                    echo 'imageInsSqlResult Error: ' . mysqli_error($con);
                }
                $err = "Update Query failed.";
            }
        } /* $_FILES["album_image"]["error"] == 0 */
    } /* $err==0 */
}




$thumbSqlArray = array();
$thumbSql = "SELECT * FROM album_images WHERE AI_album_id=" . intval($album_id) . " ORDER BY AI_image_priority DESC";

$thumbSqlResult = mysqli_query($con, $thumbSql);
if ($thumbSqlResult) {
    while ($thumbSqlResultRowObj = mysqli_fetch_object($thumbSqlResult)) {
        $thumbSqlArray[] = $thumbSqlResultRowObj;
    }
    mysqli_free_result($thumbSqlResult);
} else {
    if (DEBUG) {
        echo "thumbSqlResultRowObj error" . mysqli_error($con);
    }
}
/* Start: Data from album table */

$albumSql = "SELECT * FROM album WHERE album_id=" . intval($album_id);

$albumSqlResult = mysqli_query($con, $albumSql);
if ($albumSqlResult) {
    while ($albumSqlResultRowObj = mysqli_fetch_object($albumSqlResult)) {
        $album_title = $albumSqlResultRowObj->album_title;
    }
    mysqli_free_result($albumSqlResult);
} else {
    if (DEBUG) {
        echo "thumbSqlResultRowObj error" . mysqli_error($con);
    }
}
/* End: Data from album table */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | About slider </title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  

        <!--        Start lightBox -->
        <script src="<?php echo baseUrl('js/lightbox/jquery-1.7.2.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery-ui-1.8.18.custom.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery.smooth-scroll.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/lightbox.js'); ?>"></script>
        <link rel="stylesheet" href="<?php echo baseUrl('css/lightbox/lightbox.css'); ?>" type="text/css" media="screen" />
        <!--        End lightBox -->  

        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               

    </head>

    <body>


        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <?php include ('../about_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Slide Module</h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Images upload To <b><?php echo $album_title; ?></b></h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <?php if ($image_edit): ?>
                                <form action="<?php echo baseUrl('admin/business/slider/index.php') . '?id=' . base64_encode($album_id) . "&action=edit&image_id=" . base64_encode($image_id); ?>" method="post" enctype="multipart/form-data" class="mainForm">
                                <?php else: /* if($image_edit) */ ?>
                                    <form action="<?php echo baseUrl('admin/business/slider/index.php') . '?id=' . base64_encode($album_id); ?>" method="post" enctype="multipart/form-data" class="mainForm">
                                    <?php endif; /* if($image_edit) */ ?>



                                    <!-- Input text fields -->
                                    <fieldset>
                                        <div class="widget first">
                                            <?php if ($image_edit): ?>
                                                <div class="head"><h5 class="iList">Image Edit</h5></div>    
                                            <?php else: /* if($image_edit) */ ?>
                                                <div class="head"><h5 class="iList">Image Add</h5></div>    
                                            <?php endif; /* if($image_edit) */ ?>


                                                <div class="rowElem noborder"><label>Image<?php if(!$image_edit) echo ' (<span class="requiredSpan">*</span>)';?>:</label><div class="formRight"><input type="file" name="album_image" value=""  />Minimum WIDTH : <?php echo $ALBUM_IMAGE_WIDTH; ?>&nbsp;Maximum WIDTH : <?php echo $config['IMAGE_UPLOAD_MAX_WIDTH']; ?></div><div class="fix"></div></div>
                                            <div class="rowElem noborder"><label>Image Priority (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="image_priority" value="<?php echo $image_priority; ?>"  /></div><div class="fix"></div></div>
                                            <div class="rowElem noborder"><label>Image Title (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="image_title" value="<?php echo $image_title; ?>"  /></div><div class="fix"></div></div>

                                            <div class="rowElem noborder"><label> Image Description (<span class="requiredSpan">*</span>):</label><div class="formRight"><textarea rows="8" cols="" class="auto" name="image_description"><?php echo $image_description; ?></textarea></div><div class="fix"></div></div>
                                            <?php if ($image_edit): ?>
                                                <input type="submit" name="image_upload" value="Edit image" class="greyishBtn submitForm" />
                                            <?php else: /* if($image_edit) */ ?>
                                                <input type="submit" name="image_upload" value="Submit" class="greyishBtn submitForm" />
                                            <?php endif; /* if($image_edit) */ ?>

                                            <div class="fix"></div>
                                        </div>
                                    </fieldset>
                                </form>


                                <div class="widget first">
                                    <div class="head"><h5 class="iPreview">Images of <b><?php echo $album_title; ?></b></h5></div>
                                    <div class="pics">
                                        <?php
                                        $thumbSqlArrayCount = count($thumbSqlArray);
                                        if ($thumbSqlArrayCount > 0):
                                            ?>


                                            <div class="imageRow">
                                                <div class="set">


                                                    <?php for ($i = 0; $i < $thumbSqlArrayCount; $i++): ?>
                                                        <ul>

                                                            <?php
                                                            if ($i == 0)
                                                                echo"<div class='single first'>";elseif ($i == $thumbSqlArrayCount - 1)
                                                                echo"<div class='single last'>";
                                                            else
                                                                echo"<div class='single'>";
                                                            ?>
                                                            <li style="height: 65px; "><a href="<?php echo $config['IMAGE_UPLOAD_URL'] . '/album/' . $album_id . '/large/' . $thumbSqlArray[$i]->AI_image_name; ?>" rel="lightbox[plants]" title="<?php echo $thumbSqlArray[$i]->AI_image_title; ?>"><img src="<?php echo $config['IMAGE_UPLOAD_URL'] . '/album/' . $album_id . '/small/' . $thumbSqlArray[$i]->AI_image_name; ?>" width="100" alt="main image " /></a>
                                                                <div class="actions">
                                                                    <a title="edit" href="index.php?id=<?php echo base64_encode($album_id); ?>&action=edit&image_id=<?php echo base64_encode($thumbSqlArray[$i]->AI_id); ?>"><img src="<?php echo baseUrl('admin/images/edit.png'); ?>" alt="" /></a>&nbsp;
                                                                    <a title="delete" onclick="return confirm('Are you sure want to delete?');"href="index.php?id=<?php echo base64_encode($album_id); ?>&action=del&image_id=<?php echo base64_encode($thumbSqlArray[$i]->AI_id); ?>"><img src="<?php echo baseUrl('admin/images/delete.png'); ?>" alt="" /></a>&nbsp;
                                                                    <a title="full screen" href="<?php echo $config['IMAGE_UPLOAD_URL'] . '/album/' . $album_id . '/large/' . $thumbSqlArray[$i]->AI_image_name; ?>" rel="lightbox[images]"title="<?php echo $thumbSqlArray[$i]->AI_image_title; ?>"><img src="<?php echo baseUrl('admin/images/full2.png'); ?>" alt="" /></a>&nbsp;


                                                                </div>
                                                            </li>


                                                        </ul> 
                                                    <?php endfor; ?>
                                                <?php endif; ?>

                                            </div>
                                        </div>
                                        <div class="fix"></div>
                                    </div>

                                </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>