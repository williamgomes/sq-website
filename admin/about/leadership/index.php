<?php
include ('../../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
/*Start Priority Reset code*/
if(isset($_POST["resetPriority"])) {
    if(resetPriority($tableNmae = 'employees', $fieldName = 'employee_priority')){
    $msg = "Priority Field Reset Successfully!!";        
    }
}
/*End Priority Reset code*/
$leaders = array();
$employeeArray = array();
$employeeSql = "SELECT * FROM employees";
$employeeSqlResult = mysqli_query($con, $employeeSql);
if ($employeeSqlResult) {
    while ($employeeSqlResultRowObj = mysqli_fetch_object($employeeSqlResult)) {
        $employeeArray[] = $employeeSqlResultRowObj;
        if (isset($employeeSqlResultRowObj->employee_leadership_status) AND $employeeSqlResultRowObj->employee_leadership_status == 'yes') {
            $leaders[] = $employeeSqlResultRowObj->employee_id;
        }
    }
    mysqli_free_result($employeeSqlResult);
} else {
    if (DEBUG) {
        echo 'employeeSqlResult Error : ' . mysqli_error($con);
    }
}

if (isset($_POST['update'])) {
    $newLeaders = array();
    $removeLeaders = array();
    if (isset($_POST['leader'])) {

        foreach ($_POST['leader'] AS $l) {
            if (!in_array($l, $leaders)) {
                $newLeaders[] = $l;
            }
        }

        foreach ($leaders AS $l) {
            if (!in_array($l, $_POST['leader'])) {
                $removeLeaders[] = $l;
            }
        }
    } else {
        /* if want to delete all then $_POST['leader'] does not exist */
        foreach ($leaders AS $l) {
            if (!in_array($l, array())) {
                $removeLeaders[] = $l;
            }
        }
    }
    $updateFiled = "";
    $updateFiled .="`employee_leadership_status` = CASE ";
    if (count($newLeaders) > 0) {
        $yes = implode(',', $newLeaders);
        $yes = trim($yes, ',');
        $updateFiled .=" WHEN employee_id IN($yes) THEN 'yes' ";
    }
    if (count($removeLeaders) > 0) {
        $no = implode(',', $removeLeaders);
        $no = trim($no, ',');
        $updateFiled .=" WHEN employee_id IN ($no) THEN 'no' ";
    }

    $updateFiled .=" ELSE employee_leadership_status END";

    $updateSql = "UPDATE `employees` SET $updateFiled ";

    if (count($newLeaders) > 0 OR count($removeLeaders) > 0) {

        $updateSqlResult = mysqli_query($con, $updateSql);

        if ($updateSqlResult) {
            $msg = "Leader updated successfully";
            /* refresh list */
            $leaders = array();
            $employeeArray = array();
            $employeeSql = "SELECT * FROM employees";
            $employeeSqlResult = mysqli_query($con, $employeeSql);
            if ($employeeSqlResult) {
                while ($employeeSqlResultRowObj = mysqli_fetch_object($employeeSqlResult)) {
                    $employeeArray[] = $employeeSqlResultRowObj;
                    if (isset($employeeSqlResultRowObj->employee_leadership_status) AND $employeeSqlResultRowObj->employee_leadership_status == 'yes') {
                        $leaders[] = $employeeSqlResultRowObj->employee_id;
                    }
                }
                mysqli_free_result($employeeSqlResult);
            } else {
                if (DEBUG) {
                    echo 'employeeSqlResult Error : ' . mysqli_error($con);
                }
            }
            /* refresh list */
        } else {
            if (DEBUG) {
                echo '$updateSqlResult Error :' . mysqli_error($con);
            } else {
                echo '$updateSqlResult Fail';
            }
        }
    } else {
        $err = "Nothing change";
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="<?php echo baseUrl('admin/images/favicon.ico') ?>" />
        <title>Admin Panel | About : leadership team</title>

        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css' />
        <script src="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type="text/javascript"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, File upload, editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/spinner/ui.spinner.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery-ui.min.js'); ?>"></script>  
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/fileManager/elfinder.min.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/wysiwyg/jquery.wysiwyg.js'); ?>"></script>
        <!--Effect on wysiwyg editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/wysiwyg/wysiwyg.image.js'); ?>"></script>
        <!--Effect on wysiwyg editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/wysiwyg/wysiwyg.link.js'); ?>"></script>
        <!--Effect on wysiwyg editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/wysiwyg/wysiwyg.table.js'); ?>"></script>
        <!--Effect on wysiwyg editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/dataTables/jquery.dataTables.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/dataTables/colResizable.min.js'); ?>"></script>
        <!--Effect on left error menu, top message menu -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/forms/forms.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/forms/autogrowtextarea.js'); ?>"></script>
        <!--Effect on left error menu, top message menu, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/forms/autotab.js'); ?>"></script>
        <!--Effect on left error menu, top message menu -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/forms/jquery.validationEngine.js'); ?>"></script>
        <!--Effect on left error menu, top message menu-->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/colorPicker/colorpicker.js'); ?>"></script>
        <!--Effect on left error menu, top message menu -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/uploader/plupload.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/uploader/plupload.html5.js'); ?>"></script>
        <!--Effect on file upload-->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/uploader/plupload.html4.js'); ?>"></script>
        <!--No effect-->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/uploader/jquery.plupload.queue.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/ui/jquery.tipsy.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,  -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jBreadCrumb.1.1.js'); ?>"></script>
        <!--Effect on left error menu, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/cal.min.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery.collapsible.min.js'); ?>"></script>
        <!--Effect on left error menu, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery.ToTop.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery.listnav.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery.sourcerer.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/custom.js'); ?>"></script>
        <script>
            function related(str)
            {
                var id = document.frm1.pid.value;

                if (str == "")
                {
                    document.getElementById("txtHint").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("shwClr").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajaxcat.php?c=" + str + "&id=" + id, true);
                xmlhttp.send();
            }
        </script>

        <!--Effect on left error menu, top message menu, body-->
        <!--delete tags-->

        <!--select box script-->
        <script type="text/javascript">
            checked = false;
            function checkedAll(frm1) {
                var aa = document.getElementById('frm1');
                if (checked == false)
                {
                    checked = true
                }
                else
                {
                    checked = false
                }
                for (var i = 0; i < aa.elements.length; i++)
                {
                    aa.elements[i].checked = checked;
                }
            }
        </script>
        <!--end select box script-->

    </head>

    <body>


        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->

            <?php include ('../about_left_navigation.php'); ?>


            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>About Leadership team </h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>

                <!-- Charts -->
                <form action="index.php" method="post">
                    <input type="submit" onclick="return confirm('Are You Sure Want TO Reset The Priority?');" name="resetPriority" value="Reset Priority"/>
                </form>


                <form id ="frm1" action="<?php echo basename(__FILE__); ?>" method="post">            


                    <!-- Static table with resizable columns -->
                    <div class="widget">
                        <div class="head"><h5 class="iFrames">Select Employee For Leadership team</h5></div>
                        <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                            <thead>
                                <tr>
                                    <td>Select</td>
                                    <td>Employee Name</td>
                                    <td>Employee Designation</td>
                                    <td>Employee Priority </td>

                                </tr>
                            </thead>
                            <tbody>
                                <?php $employeeArrayCounter = count($employeeArray); ?>
                                <?php
                                for ($i = 0; $i < $employeeArrayCounter; $i++) {
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" name="leader[]" value=<?php echo $employeeArray[$i]->employee_id; ?>
                                            <?php
                                            if ($employeeArray[$i]->employee_leadership_status == 'yes') {
                                                echo 'checked=checked';
                                            }
                                            ?>   /></td>
                                        <td><?php echo $employeeArray[$i]->employee_name; ?></td>
                                        <td><?php echo $employeeArray[$i]->employee_designation; ?></td>
                                        <td><?php echo $employeeArray[$i]->employee_priority; ?></td>
                                    </tr>
                                    <?php
                                } /* for ($i=0; $i< $employeeArrayCounter; $i++)  */
                                ?>


                            </tbody>
                        </table>
                    </div>
                    <!-- Static table with resizable columns -->
                    <input type="submit" name="update" value="Update" class="greyishBtn submitForm" />
                </form>
                <!--form end--> 
            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>
        <!-- Content wrapper -->
        <?php include basePath('admin/footer.php'); ?>
