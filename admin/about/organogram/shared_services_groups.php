<?php
include ('../../../config/config.php');
$moduleName = "organogram";
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
//saving tags in database

$aid = $_SESSION['admin_id']; //getting loggedin admin id
$text = get_option('ABOUT_SHARED_SERVICES_GROUPS_DESCRIPTION');


if (isset($_POST['update'])) {
    extract($_POST);

    if ($err == '') {
        if ($text == '') {
            $err = 'Description field is required';
        }
        $updateFiled = "";
        $updateFiled .="`CS_value` = CASE CS_option";
        $updateFiled .=" WHEN 'ABOUT_SHARED_SERVICES_GROUPS_DESCRIPTION' THEN '" . htmlentities(mysqli_real_escape_string($con, $text)) . "' ";
        $updateFiled .=" ELSE CS_value END,CS_updated_by= CASE CS_option
                            WHEN 'ABOUT_SHARED_SERVICES_GROUPS_DESCRIPTION' THEN '$aid'
                            ELSE CS_updated_by
                            END";

        $updateSql = "UPDATE `config_settings` SET $updateFiled ";

        $updateSqlResult = mysqli_query($con, $updateSql);
        if ($updateSqlResult) {
            $msg = "Shared services groups data updated successfully";
        } else /* ($updateSqlResult) */ {
            if (DEBUG) {
                echo 'updateSqlResult Error' . mysqli_error($con);
            }
        } /* ($updateSqlResult) */
    } /* ($err == '') */
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | About : Shared services groups </title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  

        <!--        Start lightBox -->
        <script src="<?php echo baseUrl('js/lightbox/jquery-1.7.2.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery-ui-1.8.18.custom.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery.smooth-scroll.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/lightbox.js'); ?>"></script>
        <link rel="stylesheet" href="<?php echo baseUrl('css/lightbox/lightbox.css'); ?>" type="text/css" media="screen" />
        <!--        End lightBox -->  

        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               

    </head>

    <body>


        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <?php include ('../about_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>About Module</h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>

                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Shared services groups </h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="<?php echo basename(__FILE__); ?>" method="post" class="mainForm" enctype="multipart/form-data">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">Update Data </h5></div>

                                        <div class="head"><h5 class="iPencil">Description (<span class="requiredSpan">*</span>):</h5></div>  
                                        <div><textarea class="tm" rows="5" cols="" name="text"><?php echo html_entity_decode($text); ?></textarea></div>

                                        <div class="rowElem noborder"><input type="submit" name="update" value="Update" class="greyishBtn submitForm" /></div>
                                        <div class="fix"></div>

                                    </div>
                                </fieldset>

                            </form>		


                        </div>

                    </div>
                </div>

            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>