<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

$company_name = '';
$company_priority = '';
$company_address = '';
$company_description = '';
$company_status = '';
if (isset($_POST['company_create']) AND $_POST['company_create'] == 'Submit') {

    extract($_POST);
    $width = 0;
    if ($_FILES["company_logo"]["error"] == 0) {
        list($width) = getimagesize($_FILES["company_logo"]["tmp_name"]);
    }

    //echo $width;
    if ($_FILES["company_logo"]["error"] > 0) {
        $err = "Valid Company Logo is required";
    } elseif ($width < $config['CONFIG_SETTINGS']['COMPANY_LOGO_WIDTH']) {
        $err = "Company Logo width should be minimum <b>{$config['CONFIG_SETTINGS']['COMPANY_LOGO_WIDTH']}</b>";
    } else if ($width > $config['IMAGE_UPLOAD_MAX_WIDTH']) {
        $err = "Company Logo width should not exceed <b>{$config['IMAGE_UPLOAD_MAX_WIDTH']}</b>";
    } else if ($company_name == '') {
        $err = 'Company name field is required!!';
    } elseif ($company_name != '') {
        $companyCheckSql = "select company_name from `companies` where company_name='" . mysqli_real_escape_string($con, $company_name) . "'";
        $companyCheckSqlResult = mysqli_query($con, $companyCheckSql);
        if ($companyCheckSqlResult) {
            $companyCheckSqlResultRowObj = mysqli_fetch_object($companyCheckSqlResult);
            if (isset($companyCheckSqlResultRowObj->company_name) && $companyCheckSqlResultRowObj->company_name == $company_name) {
                $err = 'Company name (<b>' . $company_name . '</b>) already exist in our databse ';
            }
            mysqli_free_result($companyCheckSqlResult);
        } else {
            if (DEBUG) {
                echo 'pageCheckSqlResult Error: ' . mysqli_error($con);
            }
            $err = "Query failed.";
        }
    } else if ($company_priority == '') {
        $err = 'Company priority field is required!!';
    } else if (!is_numeric($company_priority)) {
        $err = 'Company priority should be numeric!!';
    } elseif ($company_address == '') {
        $err = 'Company Address field is required!!';
    } elseif ($company_description == '') {
        $err = 'Company description field is required!!';
    }


    if ($err == '') {
        $max_company_id = getMaxValue('companies', 'company_id');
        $new_company_id = $max_company_id + 1;
        /* Srat: image upload */
        $company_logo = basename($_FILES['company_logo']['name']);
        $info = pathinfo($company_logo, PATHINFO_EXTENSION); /* it will return me like jpeg, gif, pdf, png */
        $company_logo_name = str_replace(' ', '_', $company_name) . '-' . $new_company_id . '.' . $info; /* create custom image name color id will add  */
        $company_logo_source = $_FILES["company_logo"]["tmp_name"];
        $company_logo_target_path = $config['IMAGE_UPLOAD_PATH'] . '/company_logo/' . $company_logo_name;
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/company_logo/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/company_logo/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/company_logo/large/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/company_logo/large/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/company_logo/mid/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/company_logo/mid/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/company_logo/small/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/company_logo/small/', 0777, TRUE);
        }


        if (!move_uploaded_file($company_logo_source, $company_logo_target_path)) {
            $company_logo_name = '';
        }
        /* End: image upload */ else {
            require basePath('lib/Zebra_Image.php');
            $company_logo_source = $company_logo_target_path;
            $image = new Zebra_Image();
            /* Start code for large size image */
            $company_logo_target_path_large = $config['IMAGE_UPLOAD_PATH'] . '/company_logo/large/' . $company_logo_name;

            $image->target_path = $company_logo_target_path_large;
            $image->source_path = $company_logo_source;

            $image->preserve_aspect_ratio = true;
            $largeSize = ceil($config['CONFIG_SETTINGS']['COMPANY_LOGO_WIDTH']);
            if (!$image->resize($largeSize)) {

// if there was an error, let's see what the error is about
                $err = zebraImageErrorHandaling($image->error);

// if no errors
            }
            /* End code for large size image */
            /* Start code for large size image */
            $company_logo_target_path_large = $config['IMAGE_UPLOAD_PATH'] . '/company_logo/mid/' . $company_logo_name;

            $image->target_path = $company_logo_target_path_large;
            $image->source_path = $company_logo_source;

            $image->preserve_aspect_ratio = true;
            $midSize = ceil($largeSize / 2);
            if (!$image->resize($midSize)) {

// if there was an error, let's see what the error is about
                $err = zebraImageErrorHandaling($image->error);

// if no errors
            }
            /* End code for large size image */
            /* Start code for large size image */
            $company_logo_target_path_large = $config['IMAGE_UPLOAD_PATH'] . '/company_logo/small/' . $company_logo_name;

            $image->target_path = $company_logo_target_path_large;
            $image->source_path = $company_logo_source;

            $image->preserve_aspect_ratio = true;
            $smallSize = ceil($midSize / 2);
            if (!$image->resize($smallSize)) {

// if there was an error, let's see what the error is about
                $err = zebraImageErrorHandaling($image->error);

// if no errors
            }
            /* End code for large size image */
            $companyField = '';
            $companyField .= ' company_id =' . intval($new_company_id);
            $companyField .= ', company_logo ="' . mysqli_real_escape_string($con, $company_logo_name) . '"';
            $companyField .= ', company_name ="' . mysqli_real_escape_string($con, $company_name) . '"';
            $companyField .= ', company_priority ="' . intval($company_priority) . '"';
            $companyField .= ', company_address ="' . htmlentities(mysqli_real_escape_string($con, $company_address)) . '"';
            $companyField .= ', company_description ="' . htmlentities(mysqli_real_escape_string($con, $company_description)) . '"';
            $companyField .= ', company_status ="' . mysqli_real_escape_string($con, $company_status) . '"';

            $companyInsSql = "INSERT INTO companies SET $companyField";
            $companyInsSqlResult = mysqli_query($con, $companyInsSql);
            if ($companyInsSqlResult) {
                //$msg = "Company information successfully added";
                $link = 'index.php?msg=' . base64_encode('Company Information Successfully added');
                redirect($link);
            } else {
                if (DEBUG) {
                    echo 'companyInsSqlResult Error: ' . mysqli_error($con);
                }
                $err = "Insert Query failed.";
            }
        }
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Company Create</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               

    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include ('company_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Company Module</h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Create Company </h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="<?php echo baseUrl('admin/company/company_create.php'); ?>" method="post" enctype="multipart/form-data" class="mainForm">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">Company</h5></div>
                                        <div class="rowElem noborder"><label> Company Name (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="company_name" value="<?php echo $company_name; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>Company Logo (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="file" name="company_logo" value=""  />Minimum WIDTH : <?php echo $config['CONFIG_SETTINGS']['COMPANY_LOGO_WIDTH']; ?>&nbsp;Maximum WIDTH : <?php echo $config['IMAGE_UPLOAD_MAX_WIDTH']; ?></div><div class="fix"></div></div>                                        
                                        <div class="rowElem noborder"><label> Company Priority (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="company_priority" value="<?php echo $company_priority; ?>"  /></div><div class="fix"></div></div>
                                        <div class="head"><h5 class="iPencil">Company Address (<span class="requiredSpan">*</span>):</h5></div>      
                                        <div><textarea class="tm" rows="5" cols="" name="company_address"><?php echo html_entity_decode($company_address); ?></textarea></div>                                      
                                        <div class="head"><h5 class="iPencil">Company Description (<span class="requiredSpan">*</span>):</h5></div>      
                                        <div><textarea class="tm" rows="5" cols="" name="company_description"><?php echo html_entity_decode($company_description); ?></textarea></div>
                                        <div class="rowElem noborder"><label> Company Status:</label><div class="formRight"><select name="company_status"><option value="active" <?php if($company_status == 'active') echo 'selected';?>>Active</option><option value="inactive" <?php if($company_status == 'inactive') echo 'selected';?>>Inactive</option></select></div><div class="fix"></div></div>
                                        <input type="submit" name="company_create" value="Submit" class="greyishBtn submitForm" />
                                        <div class="fix"></div>
                                    </div>
                                </fieldset>
                            </form>


                        </div>
                    </div>
                </div>

            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>
