<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
$company_id = 0;

if (isset($_REQUEST['del']) AND isset($_REQUEST['id'])) {
    $del_company_id = base64_decode($_REQUEST['id']);

    $delCompanySql = "UPDATE companies SET company_status='deleted' WHERE company_id=".intval($del_company_id);
    $delCompanyResult = mysqli_query($con, $delCompanySql);
    if($delCompanyResult) {
        $msg = "The Company has been deleted successfullty";
    } else {
        if (DEBUG) {
        echo "companySQLResultRowObj error" . mysqli_error($con);
    } else {
        $msg = "Deletion Query Failed.";
    }
    }
}
/*Start Priority Reset code*/
if(isset($_POST["resetPriority"])) {
    if(resetPriority($tableNmae = 'companies', $fieldName = 'company_priority')){
    $msg = "Priority Field Reset Successfully!!";        
    }
}
/*End Priority Reset code*/
$companyArray = array();
$companySQL = "SELECT * FROM `companies` WHERE company_status != 'deleted'";
$companySQLResult = mysqli_query($con, $companySQL);
if ($companySQLResult) {
    while ($companySQLResultRowObj = mysqli_fetch_object($companySQLResult)) {
        $companyArray[] = $companySQLResultRowObj;
    }
    mysqli_free_result($companySQLResult);
} else {
    if (DEBUG) {
        echo "companySQLResultRowObj error" . mysqli_error($con);
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Company List</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--        Start lightBox -->
        <script src="<?php echo baseUrl('js/lightbox/jquery-1.7.2.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery-ui-1.8.18.custom.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery.smooth-scroll.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/lightbox.js'); ?>"></script>
        <link rel="stylesheet" href="<?php echo baseUrl('css/lightbox/lightbox.css'); ?>" type="text/css" media="screen" />
        <!--        End lightBox --> 
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               
		<style>
		</style>
    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include (basePath('admin/company/company_left_navigation.php')); ?>
            <?php //include ('../company_banner/company_banner_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">




                <div class="title"><h5>Company Module </h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <form action="index.php" method="post">
                    <input type="submit" onclick="return confirm('Are You Sure Want TO Reset The Priority?');" name="resetPriority" value="Reset Priority"/>
                    </form>
                <div class="table">
                    <div class="head"><h5 class="iFrames">Company List</h5></div>
                    <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                        <thead>
                            <tr>
                                <th>Company Name</th>
                                <th>Company Image</th>
                                <th>Company Priority</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $companyArrayCounter = count($companyArray);
                            if ($companyArrayCounter > 0):
                                ?>
                                <?php for ($i = 0; $i < $companyArrayCounter; $i++): ?>
                                    <tr class="gradeA">
                                        <td><?php echo $companyArray[$i]->company_name; ?></td>
                                        <td><a href="<?php echo $config['IMAGE_UPLOAD_URL'] . '/company_logo/large/' . $companyArray[$i]->company_logo; ?>" rel="lightbox[images]" title="<b><?php echo $companyArray[$i]->company_name;?>'s</b> Company Logo" ><img src="<?php echo baseUrl('admin/images/icons/custom/viewer_image.png'); ?>" height="14" width="14" alt="image" /></a></td>
                                        <td><?php echo $companyArray[$i]->company_priority; ?></td>
                                        <td class="center">

                                            <a href="company_edit.php?id=<?php echo base64_encode($companyArray[$i]->company_id); ?>"><img src="<?php echo baseUrl('admin/images/pencil-grey-icon.png'); ?>" height="14" width="14" alt="Edit" /></a>&nbsp;
                                            <a href="index.php?del=yes&id=<?php echo base64_encode($companyArray[$i]->company_id); ?>"  onclick="return confirm('Are you sure want to delete?');"><img src="<?php echo baseUrl('admin/images/deleteFile.png'); ?>" height="14" width="14" alt="delete" /></a>
                                        </td>
                                    </tr>
                                <?php endfor; /* $i=0; i<$adminArrayCounter; $++  */ ?>
                            <?php endif; /* count($adminArray) > 0 */ ?>


                        </tbody>
                    </table>
                </div>

            </div>


            <!-- Content End -->

            <div class="fix"></div>
        </div>
        <?php include basePath('admin/footer.php'); ?>
