<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
$post_id = 0;

if (isset($_REQUEST['del']) AND isset($_REQUEST['id'])) {
    $del_post_id = base64_decode($_REQUEST['id']);
    if (isset($_REQUEST["status"]) && base64_decode($_REQUEST["status"]) == 'archive') {
        $delPostSql = "DELETE FROM post WHERE post_id=" . intval($del_post_id);
    } else {
        $delPostSql = "UPDATE post SET post_status='archive' WHERE post_id=" . intval($del_post_id);
    }
    $delPostResult = mysqli_query($con, $delPostSql);
    if ($delPostResult) {
        if (isset($status) && $status == 'archive') {
            $msg = "The Post has been moved to trash successfullty";
        } else {
            $msg = "The Post has been deleted successfullty";
        }
    } else {
        if (DEBUG) {
            echo "postSQLResultRowObj error" . mysqli_error($con);
        } else {
            $msg = "Deletion Query Failed.";
        }
    }
}
/* Start Remove Image Code */
if (isset($_REQUEST["delImageId"])) {
    $delImageId = base64_decode($_REQUEST["delImageId"]);
    $imageName = getFieldValue($tableNmae = 'post', $fieldName = 'post_image', $where = 'post_id=' . intval($delImageId));
    $delImageSql = "UPDATE post SET post_image='' WHERE post_id=" . intval($delImageId);
    $delImageResult = mysqli_query($con, $delImageSql);
    if ($delImageResult) {
        if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/post_image/' . $imageName)) {
            unlink($config['IMAGE_UPLOAD_PATH'] . '/post_image/' . $imageName);
        }
        if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/post_image/small/' . $imageName)) {
            unlink($config['IMAGE_UPLOAD_PATH'] . '/post_image/small/' . $imageName);
        }
        $msg = 'Post Image successfully removed';
    } else {
        if (DEBUG) {
            $err = 'delImageResult Error' . mysqli_error($link);
        } else {
            $err = 'Delete query failed';
        }
    }
}

/* End Remove Image Code */
/* Start Priority Reset code */
if (isset($_POST["resetPriority"])) {
    if (resetPriority($tableNmae = 'post', $fieldName = 'post_priority')) {
        $msg = "Priority Field Reset Successfully!!";
    }
}
/* End Priority Reset code */
$postArray = array();
if (isset($_REQUEST["status"])) {
    $status = base64_decode($_REQUEST["status"]);
    if ($status == 'current') {
        $postSQL = "SELECT * FROM `post` WHERE post_status = 'active' AND DATE(NOW()) BETWEEN post_show_from_date AND  post_show_to_date ORDER BY post_priority DESC";
    } else {
        $postSQL = "SELECT * FROM `post` WHERE post_status = '$status' ORDER BY post_priority DESC";
    }
} else {
    $postSQL = "SELECT * FROM `post` ORDER BY post_priority DESC LIMIT 200";
}
$postSQLResult = mysqli_query($con, $postSQL);
if ($postSQLResult) {
    while ($postSQLResultRowObj = mysqli_fetch_object($postSQLResult)) {
        $postArray[] = $postSQLResultRowObj;
    }
    mysqli_free_result($postSQLResult);
} else {
    if (DEBUG) {
        echo "postSQLResultRowObj error" . mysqli_error($con);
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Post List</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--        Start lightBox -->
        <script src="<?php echo baseUrl('js/lightbox/jquery-1.7.2.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery-ui-1.8.18.custom.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery.smooth-scroll.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/lightbox.js'); ?>"></script>
        <link rel="stylesheet" href="<?php echo baseUrl('css/lightbox/lightbox.css'); ?>" type="text/css" media="screen" />
        <!--        End lightBox --> 
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               
        <style>
            input[type=checkbox]:checked + label:before {  
                content: "\2713";  
                text-shadow: 1px 1px 1px rgba(0, 0, 0, .2);  
                font-size: 15px;  
                color: #f3f3f3;  
                text-align: center;  
                line-height: 15px;  
            }
        </style>
    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include (basePath('admin/bulletin/post_left_navigation.php')); ?>
            <?php //include ('../post_banner/post_banner_left_navigation.php');  ?>

            <!-- Content Start -->
            <div class="content">




                <div class="title"><h5>Post Module </h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <form action="index.php" method="post">
                    <input type="submit" onclick="return confirm('Are You Sure Want TO Reset The Priority?');" name="resetPriority" value="Reset Priority"/>
                </form>
                <div class="table">
                    <?php   $postArrayCount = count($postArray);?>
                    <div class="head"><h5 class="iFrames">List Of <?php
                            if (isset($status))
                                echo ucfirst($status);
                            else
                                echo 'All';
                            ?> Post</h5><h5 style="float:right;">Total result : <?php echo $postArrayCount; ?></h5></div>
                    <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                        <thead>
                            <tr>
                                <td width="40%">Post Title</td>
                                <td width="10%">Image</td>
                                <td width="5%">Priority</td>
                                <td width="5%">All</td>
                                <td width="5%">Sq</td>
                                <td width="5%">Feature</td>
                                <td width="8%">Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                          
                            if ($postArrayCount > 0) {
                                for ($i = 0; $i < $postArrayCount; $i++) {
                                    echo"<tr>";
                                    echo"<td><a target='_blank' href='".  baseUrl('bulletin/post_details.php')."?id=".$postArray[$i]->post_id."&title=".  clean($postArray[$i]->post_title)."' title='Click to live view'>" . $postArray[$i]->post_title . "</td>";
                                    echo '<td>';
                                    if ($postArray[$i]->post_image == '') {
                                        ?>
                                                <!--                                        <a href="<?php echo $config['IMAGE_UPLOAD_URL'] . '/post_image/no_image.jpg'; ?>" rel="lightbox[images]" title="<?php echo $postArray[$i]->post_title; ?>'s Image" ><img src="<?php echo baseUrl('admin/images/image.png'); ?>" height="14" width="14" alt="image" /></a>-->
                                        <?php
                                    } else {
                                        ?>
                                        <a href="<?php echo $config['IMAGE_UPLOAD_URL'] . '/post_image/' . $postArray[$i]->post_image; ?>" rel="lightbox[images]" title="<?php echo $postArray[$i]->post_title; ?>'s Image" ><img src="<?php echo baseUrl('admin/images/icons/custom/viewer_image.png'); ?>" height="14" width="14" alt="image" /></a>
                                        <?php
                                    }
                                    if ($postArray[$i]->post_image != '') {
                                        echo"<a onclick='return(confirm(\"Are you sure want to delete?\"));' href='index.php?delImageId=" . base64_encode($postArray[$i]->post_id) . "'><img src=" . baseUrl("admin/images/icons/middlenav/trash.png") . " width='20%' align='right' title='Remove This Image' /></a>";
                                    }
                                    echo"</td><td id='priority_data_".$postArray[$i]->post_id ."'><a href='javascript:resetPriority(" . $postArray[$i]->post_id . "," . $postArray[$i]->post_priority . ");' >" . $postArray[$i]->post_priority . "</a></td>";
                                    echo"<td><input type='checkbox' class='updateStatus' row_id='" . $postArray[$i]->post_id . "' coloum_name='post_all_news' ";
                                    if ($postArray[$i]->post_all_news == 'yes') {
                                        echo 'checked="checked"';
                                    } echo" value='all'/></td>";
                                    echo"<td><input type='checkbox' class='updateStatus' row_id='" . $postArray[$i]->post_id . "' coloum_name='post_about_sq'";
                                    if ($postArray[$i]->post_about_sq == 'yes') {
                                        echo 'checked="checked"';
                                    } echo" value='sq'/></td>";
                                    echo"<td><input type='checkbox' class='updateStatus' row_id='" . $postArray[$i]->post_id . "' coloum_name='post_special_feature' ";
                                    if ($postArray[$i]->post_special_feature == 'yes') {
                                        echo 'checked="checked"';
                                    } echo"  value='feature'/></td>";

                                    echo"<td class='center'>";
                                    ?>
                                    <a href="post_edit.php?id=<?php echo base64_encode($postArray[$i]->post_id); ?>"><img src="<?php echo baseUrl('admin/images/pencil-grey-icon.png'); ?>" height="14" width="14" alt="Edit" /></a>&nbsp;
                                    <?php if (isset($status) && $status == 'archive') { ?>
            <!--                                        <a href="index.php?del=yes && id=<?php echo base64_encode($postArray[$i]->post_id); ?><?php if (isset($status)) echo'&status=' . base64_encode($status); ?>"><img src="<?php echo baseUrl('admin/images/deleteFile.png'); ?>" height="14" width="14" alt="delete" title="Delete" onclick="return confirm('Are you sure want to delete?');" /></a>-->
                                    <?php }else { ?>
                                        <a href="index.php?del=yes && id=<?php echo base64_encode($postArray[$i]->post_id); ?><?php if (isset($status)) echo'&status=' . base64_encode($status); ?>"><img src="<?php echo baseUrl('admin/images/icons/middlenav/trash.png'); ?>" height="14" width="14" title="Trash" alt="trash" onclick="return confirm('Are you sure want to move this post to trash?');" /></a>
                                    <?php } ?>
                                    <?php
                                    echo"</td></tr>";
                                }
                            }
                            ?>

                        </tbody>
                    </table>
                </div>

            </div>


            <!-- Content End -->

            <div class="fix"></div>
        </div>
        <script type="text/javascript">

                        function resetPriority(post_id, post_priority) {
                           

                            var new_priority = prompt("Priority reset ", post_priority);

                            if (new_priority == post_priority)
                            {
                                //do nothing 
                               //  $.jGrowl('Status could not update  ');
                            }else if(new_priority > 0){
                                 //call ajax 
                                
                                 $.post("../ajax/post_priority_update.php", {post_id: post_id, new_priority:new_priority}, function(result) {
                                if (result == 0) {
                                    //No error
                                    $("td#priority_data_"+post_id).html('<a href="javascript:resetPriority('+post_id+','+new_priority+');">'+new_priority+'</a>');
                                    $.jGrowl('Priority updated successfully ');
                                } else if (result == 1) {
                                    //Query failed
                                    $.jGrowl('Priority could not update  ');
                                } else {
                                    $.jGrowl(result);
                                }
                            });
                            }else{
                                // do nothink 
                                 $.jGrowl('Priority could not update  ');
                            }
                        }

                        $(".updateStatus").live("change", function() {
                            var id = $(this).attr('row_id');
                            var coloum_name = $(this).attr('coloum_name');
                            var checked = $(this).attr("checked");
                            $.post("../ajax/post_index.php", {post_id: id, coloum_name: coloum_name, checked: checked}, function(result) {
                                if (result == 0) {
                                    //No error
                                    $.jGrowl('Status updated successfully ');
                                } else if (result == 1) {
                                    //Query failed
                                    $.jGrowl('Status could not update  ');
                                } else {
                                    $.jGrowl(result);
                                }
                            });

                        });
        </script>
        <?php include basePath('admin/footer.php'); ?>
