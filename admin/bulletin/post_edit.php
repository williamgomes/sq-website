<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

if (isset($_REQUEST['id'])) {
    $edit_post_id = base64_decode($_REQUEST['id']);
} else {
    $link = baseUrl('admin/post/index.php?err=' . base64_encode('Id missing.'));
    redirect($link);
}
$post_title = '';
$post_image_name = '';
$post_source_url = '';
$post_source_title = '';
$post_show_from_date = '';
$post_show_to_date = '';
$post_short_description = '';
$post_content = '';
$post_priority = '';
$post_status = '';
$post_source_external = '';
$post_special_feature = '';
$post_todays_pick = '';
$post_about_sq = '';
$post_all_news = '';
$POST_IMAGE_WIDTH = get_option("POST_IMAGE_WIDTH");

if (isset($_POST['post_update']) AND $_POST['post_update'] == 'Update') {

    extract($_POST);
    if (isset($post_source_external) && $post_source_external == 'post_source_external') {
        $post_source_external = 'yes';
    }if (isset($post_all_news) && $post_all_news == 'post_all_news') {
        $post_all_news = 'yes';
    }if (isset($post_about_sq) && $post_about_sq == 'post_about_sq') {
        $post_about_sq = 'yes';
    }if (isset($post_todays_pick) && $post_todays_pick == 'post_todays_pick') {
        $post_todays_pick = 'yes';
    }if (isset($post_special_feature) && $post_special_feature == 'post_special_feature') {
        $post_special_feature = 'yes';
    }
    $width = 0;
    if ($_FILES["post_image"]["error"] == 0) {
        list($width) = getimagesize($_FILES["post_image"]["tmp_name"]);
    }

    if ($post_title == '') {
        $err = 'Post Title field is required!!';
    } elseif($_FILES['post_image']['error'] == 0 && !checkFileType($_FILES['post_image']['name'],'ALLOWED_IMAGE')) {
        $err = "Post Image should be an image";
    } elseif ($_FILES["post_image"]["error"] == 0 && $width < $POST_IMAGE_WIDTH) {
        $err = "Post Image width should be minimum <b>{$POST_IMAGE_WIDTH}</b>";
    } else if ($_FILES["post_image"]["error"] == 0 && $width > $config['IMAGE_UPLOAD_MAX_WIDTH']) {
        $err = "Post Image width must not exceed <b>{$config['IMAGE_UPLOAD_MAX_WIDTH']}</b>";
    } else if ($post_short_description == '') {
        $err = 'Post Short Description field is required!!';
    } else if ($post_content == '') {
        $err = 'Post Content Url field is required!!';
    } else if ($post_show_from_date == '') {
        $err = 'Post Show From Date field is required!!';
    } else if ($post_show_to_date == '') {
        $err = 'Post Show To Date field is required!!';
    } elseif(strtotime($post_show_to_date)-strtotime($post_show_from_date)<0) {
        $err = 'Post Show To Date should be greater than Post Show From Date!!';
    } else if ($post_priority == '') {
        $err = 'Post priority field is required!!';
    } else if (!is_numeric($post_priority)) {
        $err = 'Post priority should be numeric!!';
    }

    if ($err == '') {
        if ($width > 0) {

            /* Srat: image upload */
            $post_image = basename($_FILES['post_image']['name']);
            $info = pathinfo($post_image, PATHINFO_EXTENSION); /* it will return me like jpeg, gif, pdf, png */
            $post_image_name = str_replace(' ', '_', clean($post_title)) . '-' . $edit_post_id . '.' . $info; /* create custom image name color id will add  */
            $post_image_source = $_FILES["post_image"]["tmp_name"];
            $post_image_target_path = $config['IMAGE_UPLOAD_PATH'] . '/post_image/' . $post_image_name;

            if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/post_logo/')) {
                mkdir($config['IMAGE_UPLOAD_PATH'] . '/post_logo/', 0777, TRUE);
            }

            if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/post_image/')) {
                mkdir($config['IMAGE_UPLOAD_PATH'] . '/post_image/', 0777, TRUE);
            }
            if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/post_image/small/')) {
                mkdir($config['IMAGE_UPLOAD_PATH'] . '/post_image/small/', 0777, TRUE);
            }
            if (!move_uploaded_file($post_image_source, $post_image_target_path)) {
                $post_image_name = '';
            } else {
                require basePath('lib/Zebra_Image.php');
                $post_image_source = $post_image_target_path;
                $image = new Zebra_Image();
                /* Start code for large size image */
                $post_image_target_path_large = $config['IMAGE_UPLOAD_PATH'] . '/post_image/small/' . $post_image_name;

                $image->target_path = $post_image_target_path_large;
                $image->source_path = $post_image_source;

                $image->preserve_aspect_ratio = true;
                $largeSize = ceil($POST_IMAGE_WIDTH / 2);
                if (!$image->resize($largeSize)) {

// if there was an error, let's see what the error is about
                    $err = zebraImageErrorHandaling($image->error);

// if no errors
                }
            }
        }
        /* End: image upload */
        $postShowDateFrom = substr($post_show_from_date, 6, 4) . substr($post_show_from_date, 3, 2) . substr($post_show_from_date, 0, 2);
        $postShowDateTo = substr($post_show_to_date, 6, 4) . substr($post_show_to_date, 3, 2) . substr($post_show_to_date, 0, 2);
        $postField = '';
        $postField .= ' post_short_title ="' . mysqli_real_escape_string($con, $post_short_title) . '"';
        $postField .= ', post_title ="' . mysqli_real_escape_string($con, $post_title) . '"';
        if ($post_image_name != '') {
            $postField .= ', post_image ="' . mysqli_real_escape_string($con, $post_image_name) . '"';
        }        
        $postField .= ', post_short_description ="' . mysqli_real_escape_string($con, $post_short_description) . '"';
        $postField .= ', post_content ="' . htmlentities(mysqli_real_escape_string($con, $post_content)) . '"';
        $postField .= ', post_source_url ="' . mysqli_real_escape_string($con, $post_source_url) . '"';
        $postField .= ', post_source_title ="' . mysqli_real_escape_string($con, $post_source_title) . '"';
        if (isset($post_source_external) && $post_source_external == 'yes') {
            $postField .= ', post_source_external ="' . mysqli_real_escape_string($con, 'yes') . '"';
        } else {
            $postField .= ', post_source_external ="' . mysqli_real_escape_string($con, 'no') . '"';
        }
        if (isset($post_all_news) && $post_all_news == 'yes') {
            $postField .= ', post_all_news ="' . mysqli_real_escape_string($con, 'yes') . '"';
        } else {
            $postField .= ', post_all_news ="' . mysqli_real_escape_string($con, 'no') . '"';
        }
        if (isset($post_about_sq) && $post_about_sq == 'yes') {
            $postField .= ', post_about_sq ="' . mysqli_real_escape_string($con, 'yes') . '"';
        } else {
            $postField .= ', post_about_sq ="' . mysqli_real_escape_string($con, 'no') . '"';
        }
        if (isset($post_todays_pick) && $post_todays_pick == 'yes') {
            $postField .= ', post_todays_pick ="' . mysqli_real_escape_string($con, 'yes') . '"';
        } else {
            $postField .= ', post_todays_pick ="' . mysqli_real_escape_string($con, 'no') . '"';
        }
        if (isset($post_special_feature) && $post_special_feature == 'yes') {
            $postField .= ', post_special_feature ="' . mysqli_real_escape_string($con, 'yes') . '"';
        } else {
            $postField .= ', post_special_feature ="' . mysqli_real_escape_string($con, 'no') . '"';
        }
        $postField .= ', post_show_from_date ="' . (mysqli_real_escape_string($con, $postShowDateFrom)) . '"';
        $postField .= ', post_show_to_date ="' . (mysqli_real_escape_string($con, $postShowDateTo)) . '"';
        $postField .= ', post_status ="' . (mysqli_real_escape_string($con, $post_status)) . '"';
        $postField .= ', post_priority ="' . mysqli_real_escape_string($con, $post_priority) . '"';

        $postUpdateSql = "UPDATE  `post` SET $postField WHERE post_id=" . intval($edit_post_id);
        $postUpdateSqlResult = mysqli_query($con, $postUpdateSql);
        if ($postUpdateSqlResult) {
            $msg = "Post information update successfully";
        } else {
            if (DEBUG) {
                echo 'postUpdateSqlResult Error: ' . mysqli_error($con);
            }
            $err = "Update Query failed.";
        }
    }
}
$postSql = "SELECT * FROM `post` WHERE post_id=" . intval($edit_post_id);
$postSqlResult = mysqli_query($con, $postSql);
if ($postSqlResult) {
    $postSqlResultRowObj = mysqli_fetch_object($postSqlResult);
    if (isset($postSqlResultRowObj->post_id)) {

        $post_short_title = $postSqlResultRowObj->post_short_title;
        $post_title = $postSqlResultRowObj->post_title;
        $post_image = $postSqlResultRowObj->post_image;
        $post_short_description = $postSqlResultRowObj->post_short_description;
        $post_content = $postSqlResultRowObj->post_content;
        $post_source_url = $postSqlResultRowObj->post_source_url;
        $post_source_title = $postSqlResultRowObj->post_source_title;
        $post_show_from_date = $postSqlResultRowObj->post_show_from_date;
        $post_show_to_date = $postSqlResultRowObj->post_show_to_date;
        $post_priority = $postSqlResultRowObj->post_priority;
        $post_status = $postSqlResultRowObj->post_status;
        $post_source_external = $postSqlResultRowObj->post_source_external;
        $post_special_feature = $postSqlResultRowObj->post_special_feature;
        $post_todays_pick = $postSqlResultRowObj->post_todays_pick;
        $post_about_sq = $postSqlResultRowObj->post_about_sq;
        $post_all_news = $postSqlResultRowObj->post_all_news;
    }
} else {
    if (DEBUG) {
        echo "postSqlResult error : " . mysqli_error($con);
    } else {
        $link = baseUrl('admin/post/index.php?err=' . base64_encode('Edit sql fail.'));
        redirect($link);
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Post Update</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               

    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include ('post_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Post Module</h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Update Post </h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="<?php echo baseUrl('admin/bulletin/post_edit.php') . '?id=' . base64_encode($edit_post_id); ?>" method="post" enctype="multipart/form-data" class="mainForm">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">Post</h5></div>
                                        <div class="rowElem noborder"><label> Post Short Title:</label><div class="formRight"><input type="text" name="post_short_title" value="<?php echo $post_short_title; ?>"  /></div><div class="fix"></div></div>                                        
                                        <div class="rowElem noborder"><label> Post Title (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="post_title" value="<?php echo $post_title; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>Post Image:</label><div class="formRight"><input type="file" name="post_image" value=""  />Minimum WIDTH : <?php echo $POST_IMAGE_WIDTH; ?>&nbsp;Maximum WIDTH : <?php echo $config['IMAGE_UPLOAD_MAX_WIDTH']; ?></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>Post Short Description (<span class="requiredSpan">*</span>):</label><div class="formRight"><textarea rows="8" cols="" class="auto"  name="post_short_description"><?php echo $post_short_description; ?></textarea></div><div class="fix"></div></div>
                                        <div class="head"><h5 class="iPencil">Post Content (<span class="requiredSpan">*</span>):</h5></div>  
                                        <div><textarea class="tm" rows="5" cols="" name="post_content"><?php echo html_entity_decode($post_content); ?></textarea></div>
                                        <div class="rowElem noborder"><label> Post Source Url:</label><div class="formRight"><input type="text" name="post_source_url" value="<?php echo $post_source_url; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> Post Source Title:</label><div class="formRight"><input type="text" name="post_source_title" value="<?php echo $post_source_title; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> Source External:</label><div class="formRight"><input type="checkbox" name="post_source_external" value='post_source_external' <?php if ($post_source_external == 'yes') { ?>checked="checked" <?php }?> /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> Post Show Date From (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" class="datepicker" name="post_show_from_date" value="<?php echo date("d-m-Y", strtotime($post_show_from_date) ); ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> Post Show Date To (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" class="datepicker" name="post_show_to_date" value="<?php echo date("d-m-Y", strtotime($post_show_to_date) ); ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>Pages:</label><div class="formRight"><input type="checkbox" name="post_all_news" value='post_all_news' <?php if ($post_all_news == 'yes') { ?>checked="checked" <?php } ?> /><label>All News</label>
                                        <input type="checkbox" name="post_about_sq" value='post_about_sq' <?php if ($post_about_sq == 'yes') { ?>checked="checked" <?php } ?> /><label>About Sq</label>
                                        <input type="checkbox" name="post_todays_pick" value='post_todays_pick' <?php if ($post_todays_pick == 'yes') { ?>checked="checked" <?php } ?> /><label>Today's Pick</label>
                                        <input type="checkbox" name="post_special_feature" value='post_special_feature' <?php if ($post_special_feature == 'yes') { ?>checked="checked" <?php } ?> /><label>Special Feature</label></div></div>                                       
                                        <div class="rowElem noborder"><label> Post Priority (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="post_priority" value="<?php echo $post_priority; ?>"  /></div><div class="fix"></div></div>                                                                                
                                        <div class="rowElem noborder"><label> Post Status:</label><div class="formRight"><select name="post_status"><option value="active" <?php if ($post_status == 'active') echo 'selected'; ?>>Active</option><option value="inactive" <?php if ($post_status == 'inactive') echo 'selected'; ?>>Inactive</option><option value="archive" <?php if ($post_status == 'archive') echo 'selected'; ?>>Archive</option></select></div><div class="fix"></div></div>                                       
                                        <input type="submit" name="post_update" value="Update" class="greyishBtn submitForm" />
                                        <div class="fix"></div>
                                    </div>
                                </fieldset>
                            </form>


                        </div>
                    </div>
                </div>

            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>
