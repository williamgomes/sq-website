<?php
include ('../../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

if (isset($_REQUEST['id'])) {
    $edit_sustainability_block_id = base64_decode($_REQUEST['id']);
} else {
    $link = baseUrl('admin/sustainability/block/index.php?err=' . base64_encode('Id missing.'));
    redirect($link);
}
$sustainability_block_title = '';
$sustainability_block_image_name = '';
$sustainability_block_description = '';
$sustainability_block_priority = '';
$SUSTAINABILITY_BLOCK_IMAGE_WIDTH = get_option('SUSTAINABILITY_IMAGE_WIDTH');

if (isset($_POST['sustainability_block_edit']) AND $_POST['sustainability_block_edit'] == 'Update') {

    extract($_POST);
    $width = 0;
    if ($_FILES["sustainability_block_image"]["error"] == 0) {
        list($width) = getimagesize($_FILES["sustainability_block_image"]["tmp_name"]);
    }

    //echo $width;
    if ($sustainability_block_page_name == '') {
        $err = 'You have to select Sustainability Page Name';
    } elseif ($sustainability_block_title == '') {
        $err = 'Sustainability Block Title field is required';
    } elseif ($_FILES["sustainability_block_image"]["error"] == 0 && $width < $SUSTAINABILITY_BLOCK_IMAGE_WIDTH) {
        $err = "Sustainability Block Image width should be minimum <b>{$SUSTAINABILITY_BLOCK_IMAGE_WIDTH}</b>";
    } else if ($_FILES["sustainability_block_image"]["error"] == 0 && $width > $config['IMAGE_UPLOAD_MAX_WIDTH']) {
        $err = "Sustainability Block Image width should not exceed <b>{$config['IMAGE_UPLOAD_MAX_WIDTH']}</b>";
    } else if ($sustainability_block_description == '') {
        $err = 'Sustainability Block Description field is required';
    } else if ($sustainability_block_priority == '') {
        $err = 'sustainability Block priority field is required!!';
    } else if (!is_numeric($sustainability_block_priority)) {
        $err = 'sustainability Block priority should be numeric!!';
    }


    if ($err == '') {
        if ($width > 0) {

            /* Srat: image upload */
            $sustainability_block_image = basename($_FILES['sustainability_block_image']['name']);
            $info = pathinfo($sustainability_block_image, PATHINFO_EXTENSION); /* it will return me like jpeg, gif, pdf, png */
            $sustainability_block_image_name = str_replace(' ', '_', $sustainability_block_title) . '-' . $edit_sustainability_block_id . '.' . $info; /* create custom image name color id will add  */
            $sustainability_block_image_source = $_FILES["sustainability_block_image"]["tmp_name"];
            $sustainability_block_image_target_path = $config['IMAGE_UPLOAD_PATH'] . '/sustainability_block_image/' . $sustainability_block_image_name;
            if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/sustainability_block_image/')) {
                mkdir($config['IMAGE_UPLOAD_PATH'] . '/sustainability_block_image/', 0777, TRUE);
            }
            if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/sustainability_block_image/large/')) {
                mkdir($config['IMAGE_UPLOAD_PATH'] . '/sustainability_block_image/large/', 0777, TRUE);
            }


            if (!move_uploaded_file($sustainability_block_image_source, $sustainability_block_image_target_path)) {
                $sustainability_block_image_name = '';
            }
            /* End: image upload */ else {
                require basePath('lib/Zebra_Image.php');
                $sustainability_block_image_source = $sustainability_block_image_target_path;
                $image = new Zebra_Image();
                /* Start code for large size image */
                $sustainability_block_image_target_path_large = $config['IMAGE_UPLOAD_PATH'] . '/sustainability_block_image/large/' . $sustainability_block_image_name;

                $image->target_path = $sustainability_block_image_target_path_large;
                $image->source_path = $sustainability_block_image_source;

                $image->preserve_aspect_ratio = true;
                $largeSize = ceil($SUSTAINABILITY_BLOCK_IMAGE_WIDTH);
                if (!$image->resize($largeSize)) {

// if there was an error, let's see what the error is about
                    $err = zebraImageErrorHandaling($image->error);

// if no errors
                }
                /* End code for large size image */
            }
        }
        /* End: image upload */
        $sustainabilityField = '';
        $sustainabilityField .= ' sustainability_block_page_name ="' . mysqli_real_escape_string($con, $sustainability_block_page_name) . '"';
        $sustainabilityField .= ', sustainability_block_title ="' . mysqli_real_escape_string($con, $sustainability_block_title) . '"';
        if ($sustainability_block_image_name != '') {
            $sustainabilityField .= ', sustainability_block_image ="' . mysqli_real_escape_string($con, $sustainability_block_image_name) . '"';
        }
        $sustainabilityField .= ', sustainability_block_description ="' . htmlentities(mysqli_real_escape_string($con, $sustainability_block_description)) . '"';
        $sustainabilityField .= ', sustainability_block_priority ="' . mysqli_real_escape_string($con, $sustainability_block_priority) . '"';

        $sustainabilityUpdateSql = "UPDATE  `sustainability_block` SET $sustainabilityField WHERE sustainability_block_id=" . intval($edit_sustainability_block_id);
        $sustainabilityUpdateSqlResult = mysqli_query($con, $sustainabilityUpdateSql);
        if ($sustainabilityUpdateSqlResult) {
            $msg = "Sustainability Block information update successfully";
        } else {
            if (DEBUG) {
                echo 'sustainabilityUpdateSqlResult Error: ' . mysqli_error($con);
            }
            $err = "Update Query failed.";
        }
    }
}
$sustainabilitySql = "SELECT * FROM `sustainability_block` WHERE sustainability_block_id=" . intval($edit_sustainability_block_id);
$sustainabilitySqlResult = mysqli_query($con, $sustainabilitySql);
if ($sustainabilitySqlResult) {
    $sustainabilitySqlResultRowObj = mysqli_fetch_object($sustainabilitySqlResult);
    if (isset($sustainabilitySqlResultRowObj->sustainability_block_id)) {
        $sustainability_block_page_name = $sustainabilitySqlResultRowObj->sustainability_block_page_name;
        $sustainability_block_title = $sustainabilitySqlResultRowObj->sustainability_block_title;
        $sustainability_block_description = $sustainabilitySqlResultRowObj->sustainability_block_description;
        $sustainability_block_priority = $sustainabilitySqlResultRowObj->sustainability_block_priority;
        $edit_sustainability_block_id = $sustainabilitySqlResultRowObj->sustainability_block_id;
    }
} else {
    if (DEBUG) {
        echo "sustainabilitySqlResult error : " . mysqli_error($con);
    } else {
        $link = baseUrl('admin/sustainability/block/index.php?err=' . base64_encode('Edit sql fail.'));
        redirect($link);
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Sustainability Block Update</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               
<script type="text/javascript">
            tinymce.init({
                forced_root_block : 'div',
            });
        </script>
    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include ('../sustainability_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Sustainability Module</h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Update Sustainability </h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="<?php echo baseUrl('admin/sustainability/block/sustainability_block_edit.php') . '?id=' . base64_encode($edit_sustainability_block_id); ?>" method="post" enctype="multipart/form-data" class="mainForm">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">Sustainaility Block</h5></div>
                                        <div class="rowElem noborder"><label> Sustainaility Block Page Name (<span class="requiredSpan">*</span>):</label><div class="formRight">
                                                <select name="sustainability_block_page_name">
                                                <option value="">Select</option>    
                                                <option value="INNOVATION_CENTER" <?php if($sustainability_block_page_name == 'INNOVATION_CENTER') echo'selected';?>>Innovation Centre</option>    
                                                <option value="GREEN_INITIATIVE"  <?php if($sustainability_block_page_name == 'GREEN_INITIATIVE') echo'selected';?>>Green Initiative </option>    
                                                <option value="SQ_STATION" <?php if($sustainability_block_page_name == 'SQ_STATION') echo'selected';?>>SQ station</option>    
                                                <option value="WORK_PLACE_SAFTY" <?php if($sustainability_block_page_name == 'WORK_PLACE_SAFTY') echo'selected';?>>Work Place Safety</option>    
                                                <option value="WORK_PLACE_SAFTY-FIRE_SAFTY" <?php if($sustainability_block_page_name == 'WORK_PLACE_SAFTY-FIRE_SAFTY') echo'selected';?>>Work Place Safety-Fire Safety </option>    
                                                <option value="WORK_PLACE_SAFTY-BUILDING_SAFTY" <?php if($sustainability_block_page_name == 'WORK_PLACE_SAFTY-BUILDING_SAFTY') echo'selected';?>>Work Place Safety-Building Safety</option>    
                                                <option value="WORK_PLACE_SAFTY-JOB_SAFTY" <?php if($sustainability_block_page_name == 'WORK_PLACE_SAFTY-JOB_SAFTY') echo'selected';?>>Work Place Safety-Job Safety </option>    
                                                </select>
                                            </div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>Sustainaility Block Title (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="sustainability_block_title" value="<?php echo$sustainability_block_title; ?>" /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> Sustainaility Block Image:</label><div class="formRight"><input type="file" name="sustainability_block_image"/>Minimum WIDTH : <?php echo $SUSTAINABILITY_BLOCK_IMAGE_WIDTH; ?>&nbsp;Maximum WIDTH : <?php echo $config['IMAGE_UPLOAD_MAX_WIDTH']; ?></div><div class="fix"></div></div>
                                        <div class="head"><h5 class="iPencil">Sustainaility Block Description (<span class="requiredSpan">*</span>):</h5></div>      
                                        <div><textarea class="tm" rows="5" cols="" name="sustainability_block_description"><?php echo html_entity_decode($sustainability_block_description); ?></textarea></div>
                                        <div class="rowElem noborder"><label> Sustainaility Block Priority (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="sustainability_block_priority" value="<?php echo$sustainability_block_priority ?>"/></div><div class="fix"></div></div>
                                        <input type="submit" name="sustainability_block_edit" value="Update" class="greyishBtn submitForm" />
                                        <div class="fix"></div>
                                    </div>
                                </fieldset>
                            </form>


                        </div>
                    </div>
                </div>

            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>