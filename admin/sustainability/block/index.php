<?php
include ('../../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}


if (isset($_REQUEST['del']) AND isset($_REQUEST['id'])) {
    $del_sustainability_id = base64_decode($_REQUEST['id']);
    $sustainabilitySql = "SELECT * FROM sustainability_block WHERE sustainability_block_id=".  intval($del_sustainability_id);
    $sustainabilityResult = mysqli_query($con,$sustainabilitySql);
    $sustainabilityResultRowObj = mysqli_fetch_object($sustainabilityResult);
    $sustainabilityImageName = $sustainabilityResultRowObj->sustainability_block_image;
    $delSustainabilitySql = "DELETE FROM sustainability_block WHERE sustainability_block_id=".intval($del_sustainability_id);
    $delSustainabilityResult = mysqli_query($con, $delSustainabilitySql);
    if($delSustainabilityResult) {
        $msg = "The Sustainability Block has been deleted successfullty";
        if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/sustainability_block_image/'. $sustainabilityImageName)) {
                unlink($config['IMAGE_UPLOAD_PATH'] . '/sustainability_block_image/'. $sustainabilityImageName);
            }
            if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/sustainability_block_image/large/'. $sustainabilityImageName)) {
                unlink($config['IMAGE_UPLOAD_PATH'] . '/sustainability_block_image/'. $sustainabilityImageName);
            }
    } else {
        if (DEBUG) {
        echo "companySQLResultRowObj error" . mysqli_error($con);
    } else {
        $msg = "Deletion Query Failed.";
    }
    }
}

/*Start Priority Reset code*/
if(isset($_POST["resetPriority"])) {
    if(resetPriority($tableNmae = 'sustainability_block', $fieldName = 'sustainability_block_priority')){
    $msg = "Priority Field Reset Successfully!!";        
    }
}
/*End Priority Reset code*/
$sustainabilityArray = array();
$sustainabilitySQL = "SELECT * FROM `sustainability_block`";
$sustainabilitySQLResult = mysqli_query($con, $sustainabilitySQL);
if ($sustainabilitySQLResult) {
    while ($sustainabilitySQLResultRowObj = mysqli_fetch_object($sustainabilitySQLResult)) {
        $sustainabilityArray[] = $sustainabilitySQLResultRowObj;
    }
    mysqli_free_result($sustainabilitySQLResult);
} else {
    if (DEBUG) {
        echo "sustainabilitySQLResultRowObj error" . mysqli_error($con);
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Sustainability Block List</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--        Start lightBox -->
        <script src="<?php echo baseUrl('js/lightbox/jquery-1.7.2.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery-ui-1.8.18.custom.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery.smooth-scroll.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/lightbox.js'); ?>"></script>
        <link rel="stylesheet" href="<?php echo baseUrl('css/lightbox/lightbox.css'); ?>" type="text/css" media="screen" />
        <!--        End lightBox --> 
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               
		<style>
		</style>
    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include ('../sustainability_left_navigation.php'); ?>
            <?php //include ('../company_banner/company_banner_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">




                <div class="title"><h5>Sustainability Block Module </h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <form action="index.php" method="post">
                    <input type="submit" onclick="return confirm('Are You Sure Want TO Reset The Priority?');" name="resetPriority" value="Reset Priority"/>
                    </form>
                <!-- Charts -->
                <div class="table">
                    <div class="head"><h5 class="iFrames">Sustainability Block List</h5></div>
                    <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                        <thead>
                            <tr>
                                <th>Sustainability Block Page Name</th>
                                <th>Sustainability Block Title</th>
                                <th>Sustainability Block Image</th>
                                <th>Sustainability Block Priority</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sustainabilityArrayCounter = count($sustainabilityArray);
                            if ($sustainabilityArrayCounter > 0):
                                ?>
                                <?php for ($i = 0; $i < $sustainabilityArrayCounter; $i++): ?>
                                    <tr class="gradeA">
                                        <td><?php echo $sustainabilityArray[$i]->sustainability_block_page_name; ?></td>
                                        <td><?php echo $sustainabilityArray[$i]->sustainability_block_title; ?></td>
                                        <td><a href="<?php echo $config['IMAGE_UPLOAD_URL'].'/sustainability_block_image/large/'.$sustainabilityArray[$i]->sustainability_block_image; ?>" rel="lightbox[images]" title="<?php echo$sustainabilityArray[$i]->sustainability_block_title;?>" ><img src="<?php echo baseUrl('admin/images/image.png'); ?>" height="14" width="14" alt="image" /></a></td>
                                        <td><?php echo $sustainabilityArray[$i]->sustainability_block_priority; ?></td>
                                        <td class="center">

                                            <a href="sustainability_block_edit.php?id=<?php echo base64_encode($sustainabilityArray[$i]->sustainability_block_id); ?>"><img src="<?php echo baseUrl('admin/images/pencil-grey-icon.png'); ?>" height="14" width="14" alt="Edit" /></a>&nbsp;
                                            <a href="index.php?del=yes&id=<?php echo base64_encode($sustainabilityArray[$i]->sustainability_block_id); ?>"  onclick="return confirm('Are you sure want to delete?');"><img src="<?php echo baseUrl('admin/images/deleteFile.png'); ?>" height="14" width="14" alt="delete" /></a>
                                        </td>
                                    </tr>
                                <?php endfor; /* $i=0; i<$adminArrayCounter; $++  */ ?>
                            <?php endif; /* count($adminArray) > 0 */ ?>


                        </tbody>
                    </table>
                </div>

            </div>


            <!-- Content End -->

            <div class="fix"></div>
        </div>
        <?php include basePath('admin/footer.php'); ?>
