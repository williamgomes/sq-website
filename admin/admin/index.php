<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
$adminArray = array();
$adminSql = "SELECT * FROM admins";
$adminSqlResult = mysqli_query($con, $adminSql);
if ($adminSqlResult) {
    while ($adminSqlResultRowObj = mysqli_fetch_object($adminSqlResult)) {
        $adminArray[] = $adminSqlResultRowObj;
    }
    mysqli_free_result($adminSqlResult);
} else {
    if (DEBUG) {
        echo 'adminSqlResult Error : ' . mysqli_errno($con);
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Admin List</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               

    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include 'admin_left_navigation.php'; ?>

            <!-- Content Start -->
            <div class="content">




                <div class="title"><h5> Admin Module </h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <div class="table">
                    <div class="head"><h5 class="iFrames">Admin List</h5></div>
                    <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $adminArrayCounter = count($adminArray);
                            if ($adminArrayCounter > 0):
                                ?>
                                <?php for ($i = 0; $i < $adminArrayCounter; $i++): ?>
                                    <tr class="gradeA">
                                        <td><?php echo $adminArray[$i]->admin_email; ?></td>
                                        <td><?php echo $adminArray[$i]->admin_full_name; ?></td>
                                        <td><?php echo $adminArray[$i]->admin_type; ?></td>
                                        <td class="center"><?php echo $adminArray[$i]->admin_status; ?></td>
                                        <td class="center">
                                            <?php if (isset($_SESSION['admin_type']) AND ($_SESSION['admin_type'] == 'super' OR $_SESSION['admin_type'] == 'master')): ?>
                                                <a href="admin_edit.php?id=<?php echo base64_encode($adminArray[$i]->admin_id); ?>"><img src="<?php echo baseUrl('admin/images/pencil-grey-icon.png'); ?>" height="14" width="14" alt="Edit" /></a>

                                            <?php else: /* isset($_SESSION['admin_type']) AND ($_SESSION['admin_type'] =='super' OR $_SESSION['admin_type']=='master') */ ?>
                                                &nbsp;
                                            <?php endif; /* isset($_SESSION['admin_type']) AND ($_SESSION['admin_type'] =='super' OR $_SESSION['admin_type']=='master') */ ?>



                                        </td>
                                    </tr>
                                <?php endfor; /* $i=0; i<$adminArrayCounter; $++  */ ?>
                            <?php endif; /* count($adminArray) > 0 */ ?>


                        </tbody>
                    </table>
                </div>

            </div>


            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>
