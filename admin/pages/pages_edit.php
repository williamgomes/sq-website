<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
$edit_page_id = 0;

if (isset($_REQUEST['id'])) {
    $edit_page_id = base64_decode($_REQUEST['id']);
} else {
    $link = 'index.php?msg=' . base64_encode('ID missing.');
    redirect($link);
}

$page_title = '';
$page_body = '';
$pageSql = "SELECT * FROM pages WHERE page_id=" . intval($edit_page_id);
$pageSqlResult = mysqli_query($con, $pageSql);
if ($pageSqlResult) {
    $pageSqlResultRowObj = mysqli_fetch_object($pageSqlResult);
    if (isset($pageSqlResultRowObj->page_id)) {
        $page_id = $pageSqlResultRowObj->page_id;
        $page_title = $pageSqlResultRowObj->page_title;
        $page_body = $pageSqlResultRowObj->page_body;
    }
} else {
    if (DEBUG) {
        echo "pageSqlResult error : " . mysqli_error($con);
    } else {
        $link = baseUrl('admin/pages/index.php?err=' . base64_encode('Edit sql fail.'));
        redirect($link);
    }
}
if (isset($_POST['page_edit']) AND $_POST['page_edit'] == 'Submit') {

    extract($_POST);

    if ($page_title == '') {
        $err = 'Page Title field is required!!';
    } else if ($page_body == '') {
        $err = 'Page Body field is required!!';
    } else {
        $pageCheckSql = "select page_title from pages where page_title='" . mysqli_real_escape_string($con, $page_title) . "' && page_id !='" . $page_id . "'";
        $pageCheckSqlResult = mysqli_query($con, $pageCheckSql);
        if ($pageCheckSqlResult) {
            $pageCheckSqlResultRowObj = mysqli_fetch_object($pageCheckSqlResult);
            if (isset($pageCheckSqlResultRowObj->page_title) && $pageCheckSqlResultRowObj->page_title == $page_title) {
                $err = 'Page Title (<b>' . $page_title . '</b>) already exist in our databse ';
            }
            mysqli_free_result($pageCheckSqlResult);
        } else {
            if (DEBUG) {
                echo 'pageCheckSqlResult Error: ' . mysqli_error($con);
            }
            $err = "Query failed.";
        }
    }

    if ($err == '') {
        $pageInfoFiled = '';
        $pageInfoFiled .=' page_title = "' . mysqli_real_escape_string($con, $page_title) . '"';
        $pageInfoFiled .=', page_body = "' . mysqli_real_escape_string($con, $page_body) . '"';
        $pageInfoInsSql = "UPDATE pages SET $pageInfoFiled where page_id=" . intval($edit_page_id);
        $pageInfoFiledResult = mysqli_query($con, $pageInfoInsSql);
        if ($pageInfoFiledResult) {
            $msg = "Page Information Update successfully";
        } else {
            if (DEBUG) {
                echo 'pageInfoUpdateSqlResult Error: ' . mysqli_error($con);
            }
            $err = "Insert Query failed.";
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Page Update</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 

        <!-- start of lightbox for page url-->
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo baseUrl('admin/css/style.css') ?>">
            <link rel="stylesheet" type="text/css" media="all" href="<?php echo baseUrl('admin/fancybox/jquery.fancybox.css') ?>">
                <script type="text/javascript" src="<?php echo baseUrl('admin/fancybox/jquery.fancybox.js?v=2.0.6') ?>"></script>

                <!-- end of lightbox for page url-->
                <!--Start admin panel js/css --> 
                <?php include basePath('admin/header.php'); ?>   
                <!--End admin panel js/css -->  

                </head>

                <body>

                    <?php include basePath('admin/top_navigation.php'); ?>

                    <?php include basePath('admin/module_link.php'); ?>

                    <div id="inline" style="display: none;">
                        <iframe src=<?php echo baseUrl("admin/file_manager.php"); ?> scrolling=no height="1000" width="730"></iframe> 
                    </div>
                    <!-- Content wrapper -->
                    <div class="wrapper">

                        <!-- Left navigation -->
                        <?php include ('pages_left_navigation.php'); ?>

                        <!-- Content Start -->
                        <div class="content">
                            <div class="title"><h5>Pages Module</h5></div>

                            <!-- Notification messages -->
                            <?php include basePath('admin/message.php'); ?>
                            <!-- Charts -->
                            <div class="widget first">
                                <div class="head">
                                    <h5 class="iGraph">Create Pages </h5></div>
                                <div class="body">
                                    <div class="charts" style="width: 700px; height: auto;">       

                                        <form action="<?php echo baseUrl('admin/pages/pages_edit.php?id=') . base64_encode($edit_page_id); ?>" method="post" enctype="multipart/form-data" class="mainForm">

                                            <!-- Input text fields -->
                                            <fieldset>
                                                <div class="widget first">
                                                    <div class="head"><h5 class="iList">Pages</h5></div>
                                                    <div class="rowElem noborder"><label> Pages Title:</label><div class="formRight"><input type="text" name="page_title" value="<?php echo $page_title; ?>"/></div><div class="fix"></div></div>
                                                    <div class="rowElem noborder"><label><a class="modalbox" href="#inline"><button>Browse Files</button></a></label><div class="fix"></div></div>
                                                    <div class="head"><h5 class="iPencil">Page Body:</h5></div>
                                                    <div><textarea class="tm" rows="5" cols="" name="page_body"><?php echo $page_body; ?></textarea></div>
                                                    <input type="submit" name="page_edit" value="Submit" class="greyishBtn submitForm" />
                                                    <div class="fix"></div>
                                                </div>
                                            </fieldset>
                                        </form>


                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- Content End -->

                        <div class="fix"></div>
                    </div>

<?php include basePath('admin/footer.php'); ?>
