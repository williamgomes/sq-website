<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

//delete query here
if (isset($_REQUEST['del']) && isset($_REQUEST['id'])) {
    $del_circular_id = base64_decode($_REQUEST['id']);
    $circularDeleleteSql = "delete from circular where circular_id=" . intval($del_circular_id);
    $circularDeleleteSqlResult = mysqli_query($con, $circularDeleleteSql);
    if ($circularDeleleteSqlResult) {
        $msg = "Circular Acount Successfully Deleted";
    } else {
        if (DEBUG) {
            $err = "circularDeleleteSqlResult ERROR : " . mysqli_error($con);
        } else {
            $err = "Circular Information Not Deleted";
        }
    }
}
$circularArray = array();
$circularSql = "SELECT * FROM circular";
$circularSqlResult = mysqli_query($con, $circularSql);
if ($circularSqlResult) {
    while ($circularSqlResultRowObj = mysqli_fetch_object($circularSqlResult)) {
        $circularArray[] = $circularSqlResultRowObj;
    }
    mysqli_free_result($circularSqlResult);
} else {
    if (DEBUG) {
        echo 'circularSqlResult Error : ' . mysqli_error($con);
    }
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Circular List</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
       <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               
       
    </head>

    <body>

<?php include basePath('admin/top_navigation.php'); ?>

<?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
<?php include 'circular_left_navigation.php'; ?>

            <!-- Content Start -->
            <div class="content">




                <div class="title"><h5>Circular Module </h5></div>

                <!-- Notification messages -->
<?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <div class="table">
                    <div class="head"><h5 class="iFrames">Circular List</h5></div>
                    <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                        <thead>
                            <tr>
                                <th>Post Name</th>
                                <th>Application Dead Line</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
$circularArrayCounter = count($circularArray);
if ($circularArrayCounter > 0):
    for ($i = 0; $i < $circularArrayCounter; $i++):
        ?><tr class="gradeA"> 
                                        <td><?php echo $circularArray[$i]->circular_position;?></td>
                                        <td><?php echo $circularArray[$i]->circular_application_deadline;?></td>
                                        <td class="center">
                                            <a href="circular_edit.php?id=<?php echo base64_encode($circularArray[$i]->circular_id); ?>"><img src="<?php echo baseUrl('admin/images/pencil-grey-icon.png'); ?>" height="14" width="14" alt="Edit" /></a>&nbsp;
                                            <a href="index.php?del=yes && id=<?php echo base64_encode($circularArray[$i]->circular_id); ?>"><img src="<?php echo baseUrl('admin/images/deleteFile.png'); ?>" height="14" width="14" alt="delete" onclick="return confirm('Are you sure want to delete?');" /></a>
                                        </td></tr>
    <?php
    endfor;
endif;
?>

                        </tbody>
                    </table>
                </div>

            </div>


            <!-- Content End -->

            <div class="fix"></div>
        </div>

<?php 

include basePath('admin/footer.php'); ?>
