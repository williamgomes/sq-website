<?php

include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
$edit_circular_id = 0;

if(isset($_REQUEST['id'])) {
 $edit_circular_id = base64_decode($_REQUEST['id']);
}else{
    $link= 'index.php?msg='.base64_encode('ID missing.');
    redirect($link);
}
$circular_position = '';
$job_responsibilities = '';
$eligibility = '';
$application_deadline = '';
$application_to_email = '';
$location = '';
$business_unit = '';
$position_note = '';
$circular_intro = '';
$circularSql = "SELECT * FROM circular WHERE circular_id=" . intval($edit_circular_id);
$circularSqlResult = mysqli_query($con, $circularSql);
if($circularSqlResult) {
    $circularSqlResultRowObj = mysqli_fetch_object($circularSqlResult);
    if(isset($circularSqlResultRowObj->circular_id)){
        $circular_position = $circularSqlResultRowObj->circular_position;
        $job_responsibilities = $circularSqlResultRowObj->circular_job_responsibilities;
        $eligibility = $circularSqlResultRowObj->circular_eligibility;
        $application_deadline = $circularSqlResultRowObj->circular_application_deadline;
        $application_to_email = $circularSqlResultRowObj->circular_application_to_email;
        $location = $circularSqlResultRowObj->circular_location;
        $business_unit = $circularSqlResultRowObj->circular_business_unit;
        $position_note = $circularSqlResultRowObj->circular_note;
        $circular_intro = $circularSqlResultRowObj->circular_intro;
                 
    }
} else {
    if(DEBUG) {
      echo "circularSqlResult error : " . mysqli_error($con);  
    } else {
        $link = baseUrl('admin/circular/index.php?err=' . base64_encode('Edit sql fail.'));
        redirect($link);
    }
}
if (isset($_POST['circular_edit']) AND $_POST['circular_edit'] == 'Submit') {
    extract($_POST);

    
    if ($circular_position == '') {
        $err = 'Circular Position field is required!!';
    } elseif ($job_responsibilities == '') {
        $err = 'Job Responsibities field is required!!';
    } elseif ($eligibility == '') {
        $err = 'Eligibility field is required!!';
    } elseif ($application_deadline == '') {
        $err = 'Application Deadline field is required!!';
    } elseif ($application_to_email == '') {
        $err = 'Application To Email field is required!!';
    } else if (!filter_var($application_to_email, FILTER_VALIDATE_EMAIL)) {
        $err = 'Valid Email Address is required!!';
    } elseif ($location == '') {
        $err = 'Location field is required!!';
    } elseif ($business_unit == '') {
        $err = 'Business Unit field is required!!';
    }

    if ($err == '') {
                $applicationDeadline = substr($application_deadline, 6, 4).substr($application_deadline, 3, 2).substr($application_deadline, 0, 2);
                
        $circularInfoFiled = '';
        $circularInfoFiled .=' circular_position = "' . mysqli_real_escape_string($con, $circular_position) . '"';
        $circularInfoFiled .=', circular_intro = "' . htmlentities(mysqli_real_escape_string($con, $circular_intro)) . '"';
        $circularInfoFiled .=', circular_job_responsibilities = "' . htmlentities(mysqli_real_escape_string($con, $job_responsibilities)) . '"';
        $circularInfoFiled .=', circular_eligibility = "' . htmlentities(mysqli_real_escape_string($con, $eligibility)) . '"';
        $circularInfoFiled .=', circular_application_deadline = "' . mysqli_real_escape_string($con, $applicationDeadline) . '"';
        $circularInfoFiled .=', circular_application_to_email = "' . mysqli_real_escape_string($con, $application_to_email) . '"';
        $circularInfoFiled .=', circular_location = "' . mysqli_real_escape_string($con, $location) . '"';
        $circularInfoFiled .=', circular_business_unit = "' . mysqli_real_escape_string($con, $business_unit) . '"';
        $circularInfoFiled .=', circular_note = "' . mysqli_real_escape_string($con, $position_note) . '"';
            $circularInfoInsSql = "UPDATE circular SET $circularInfoFiled where circular_id=".intval($edit_circular_id);
            $circularInfoFiledResult = mysqli_query($con, $circularInfoInsSql);
            if ($circularInfoFiledResult) {
                $msg = "Circular Information Update successfully";
            } else {
                if (DEBUG) {
                    echo 'circularInfoUpdateSqlResult Error: ' . mysqli_error($con);
                }
                $err = "Insert Query failed.";
            }
        }
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Circular Update</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
       <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               
       
    </head>

    <body>

<?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
<?php include ('circular_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Circular Module</h5></div>

                <!-- Notification messages -->
<?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Update Circular </h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="<?php echo baseUrl('admin/circular/circular_edit.php?id=').  base64_encode($edit_circular_id); ?>" method="post" enctype="multipart/form-data" class="mainForm">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">Circular Update</h5></div>
                                        <div class="rowElem noborder"><label> Circular Position (<span class="requiredSpan">*</span>) :</label><div class="formRight"><input type="text" name="circular_position" placeholder='maximum 100 character' maxlength="100" value="<?php echo $circular_position; ?>"  /></div><div class="fix"></div></div>
                                        <div class="head"><h5 class="iPencil">Position Introduction:</h5></div>  
                                        <div><textarea class="tm" rows="5" cols="" name="circular_intro"><?php echo html_entity_decode($circular_intro); ?></textarea></div>
                                        <div class="head"><h5 class="iPencil">Job Responsibilities (<span class="requiredSpan">*</span>):</h5></div>  
                                        <div><textarea class="tm" rows="5" cols="" name="job_responsibilities"><?php echo html_entity_decode($job_responsibilities); ?></textarea></div>
                                        <div class="head"><h5 class="iPencil">Eligibility (<span class="requiredSpan">*</span>):</h5></div>  
                                        <div><textarea class="tm" rows="5" cols="" name="eligibility"><?php echo html_entity_decode($eligibility); ?></textarea></div>
                                        <div class="rowElem noborder"><label> Application Deadline (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="application_deadline" class="datepicker" value="<?php echo date("d-m-Y", strtotime($application_deadline) ); ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> Application To Email (<span class="requiredSpan">*</span>): </label><div class="formRight"><input type="text" name="application_to_email" placeholder='maximum 100 character' maxlength="100" value="<?php echo $application_to_email; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> Location  (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="location" placeholder='maximum 400 character' maxlength="400" value="<?php echo $location; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> Business Unit (<span class="requiredSpan">*</span>) :</label><div class="formRight"><input type="text" name="business_unit" placeholder='maximum 400 character' maxlength="400" value="<?php echo $business_unit; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> Position Note :</label><div class="formRight"><input type="text" name="position_note" maxlength="400" value="<?php echo $position_note; ?>"  /></div><div class="fix"></div></div>
                                        <input type="submit" name="circular_edit" value="Submit" class="greyishBtn submitForm" />
                                        <div class="fix"></div>
                                    </div>
                                </fieldset>
                            </form>


                        </div>
                    </div>
                </div>

            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>
<?php include basePath('admin/footer.php'); ?>
