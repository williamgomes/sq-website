<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

//delete query here
if (isset($_REQUEST['del']) && isset($_REQUEST['id'])) {
    $candidate_trash_id = base64_decode($_REQUEST['id']);
    $candidate_trashSql = "UPDATE candidate_db SET candidate_status='trash' WHERE candidate_id=" . intval($candidate_trash_id);
    $candidate_trashResult = mysqli_query($con, $candidate_trashSql);
    if ($candidate_trashResult) {
        $msg = "This application send to trash successfully";
    } else {
        if (DEBUG) {
            $err = "candidate_trashResult ERROR : " . mysqli_error($con);
        } else {
            $err = "Update Failed";
        }
    }
}

//Archive query here
if (isset($_REQUEST['archive']) && isset($_REQUEST['id'])) {
    $candidate_archive_id = base64_decode($_REQUEST['id']);
    $candidate_archiveSql = "UPDATE candidate_db SET candidate_status='archive' WHERE candidate_id=" . intval($candidate_archive_id);
    $candidate_archiveResult = mysqli_query($con, $candidate_archiveSql);
    if ($candidate_archiveResult) {
        $msg = "This application send to archive successfully";
    } else {
        if (DEBUG) {
            $err = "candidate_archiveResult ERROR : " . mysqli_error($con);
        } else {
            $err = "Update Failed";
        }
    }
}
$candidate_dbArray = array();
if (isset($_REQUEST["cv"])) {
    $candidateStatus = base64_decode($_REQUEST["cv"]);
    $candidate_dbSql = "SELECT * FROM candidate_db WHERE candidate_status='$candidateStatus'";
} else {
    $candidate_dbSql = "SELECT * FROM candidate_db WHERE candidate_status !='trash'";
}
$candidate_dbSqlResult = mysqli_query($con, $candidate_dbSql);
if ($candidate_dbSqlResult) {
    while ($candidate_dbSqlResultRowObj = mysqli_fetch_object($candidate_dbSqlResult)) {
        $candidate_dbArray[] = $candidate_dbSqlResultRowObj;
    }
    mysqli_free_result($candidate_dbSqlResult);
} else {
    if (DEBUG) {
        echo 'candidate_dbSqlResult Error : ' . mysqli_error($con);
    }
}

//printDie($candidate_dbArray);
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Resume List</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               

    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include 'circular_left_navigation.php'; ?>

            <!-- Content Start -->
            <div class="content">




                <div class="title"><h5>Circular Module </h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <div class="table">
                    <div class="head"><h5 class="iFrames">Application List <?php if(isset($_REQUEST['cv'])){  echo ucfirst(base64_decode($_REQUEST['cv'])); }else{ echo 'All';}?></h5></div>
                    <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                        <thead>
                            <tr>
                                <th>Name </th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Submission at</th>
                                <th>Applied for </th>
                                <th>CV</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $candidate_dbArrayCounter = count($candidate_dbArray);
                            if ($candidate_dbArrayCounter > 0):
                                for ($i = 0; $i < $candidate_dbArrayCounter; $i++):
                                    ?><tr class="gradeA"> 
                                        <td><?php echo $candidate_dbArray[$i]->candidate_fname . ' ' . $candidate_dbArray[$i]->candidate_lname; ?></td>

                                        <td><?php echo $candidate_dbArray[$i]->candidate_phone; ?></td>
                                        <td><?php echo $candidate_dbArray[$i]->candidate_email; ?></td>
                                        <td><?php echo date("Y/m/d G:ia", strtotime($candidate_dbArray[$i]->submission_date)); ?></td>
                                        <td><?php echo getFieldValue('circular', 'circular_position', 'circular_id=' . $candidate_dbArray[$i]->candidate_circular_id); ?></td>
                                        <td class="center">
                                            <?php if($candidate_dbArray[$i]->candidate_status == 'unread'):?>
                                             <a onclick="update_status(<?php echo$candidate_dbArray[$i]->candidate_id ?>);" href="../../cv/<?php echo $candidate_dbArray[$i]->candidate_cv; ?>" target="_blank">View</a>  
                                            <?php else:?>
                                             <a href="../../cv/<?php echo $candidate_dbArray[$i]->candidate_cv; ?>" target="_blank">View</a>  
                                            <?php endif; /* ($candidate_dbArray[$i]->candidate_status == 'unread')*/ ?>
                                           
                                        </td>
                                        <td>
                                            <?php
                                            if ($candidate_dbArray[$i]->candidate_status == 'archive') {
                                                
                                            } else {
                                                ?>
                                                <a title="Archive" href="dropped_cvs.php?id=<?php echo base64_encode($candidate_dbArray[$i]->candidate_id); ?>&archive=<?php
                                                echo base64_encode('yes');
                                                if (isset($candidateStatus))
                                                    echo '&cv=' . base64_encode($candidateStatus);
                                                ?>" title="Archive" ><img src="<?php echo baseUrl('admin/images/icons/custom/module/archive.png'); ?>" height="16" alt="archive" /></a>
                                                   <?php
                                               }
                                               ?>
                                            <a title="Delete" onclick="return confirm('Are you sure want to delete?');" href="dropped_cvs.php?id=<?php echo base64_encode($candidate_dbArray[$i]->candidate_id); ?>&del=<?php
                                            echo base64_encode('yes');
                                            if (isset($candidateStatus))
                                                echo '?&cv=' . base64_encode($candidateStatus);
                                            ?>"><img src="<?php echo baseUrl("admin/images/icons/middlenav/trash.png"); ?> " height="16" align="right" title="Delete" /></a>
                                        </td>
                                    </tr>
                                    <?php
                                endfor;
                            endif;
                            ?>

                        </tbody>
                    </table>
                </div>

            </div>


            <!-- Content End -->

            <div class="fix"></div>
        </div>
        <script>
                                                function update_status(user_id) {
                                                    $.post("../ajax/update_application_status.php", {userid: user_id}, function() {
                                                    })
                                                }
        </script>

        <?php include basePath('admin/footer.php'); ?>
