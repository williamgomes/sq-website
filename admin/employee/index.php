<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

if (isset($_REQUEST['del']) AND isset($_REQUEST['id'])) {
    $del_employee_id = base64_decode($_REQUEST['id']);
    $deleteImgNameSql = "select * from employees where employee_id=" . intval($del_employee_id);
    $deleteImgNameResult = mysqli_query($con, $deleteImgNameSql);
    $deleteImgNameResultArray = mysqli_fetch_array($deleteImgNameResult);
    $delImgName = ($deleteImgNameResultArray["employee_image"]);
    $delImgPath = $config['IMAGE_UPLOAD_PATH'] . '/employee_image/' . $delImgName;
    $delLargeImgPath = $config['IMAGE_UPLOAD_PATH'] . '/employee_image/large/' . $delImgName;
    $delMidImgPath = $config['IMAGE_UPLOAD_PATH'] . '/employee_image/mid/' . $delImgName;
    $delSmallImgPath = $config['IMAGE_UPLOAD_PATH'] . '/employee_image/small/' . $delImgName;
    unlink($delImgPath);
    unlink($delLargeImgPath);
    unlink($delMidImgPath);
    unlink($delSmallImgPath);
    $employeeDeleteSql = "DELETE FROM employees WHERE employee_id=" . intval($del_employee_id);
    $employeeDeleteSqlResult = mysqli_query($con, $employeeDeleteSql);
    if ($employeeDeleteSqlResult) {
        $msg = "Delete successfully";
    } else {

        if (DEBUG) {
            $err = "employeeDeleteSqlResult ERROR : " . mysqli_error($con);
        } else {
            $err = "Employee information not delete";
        }
    }
}
/* Start Priority Reset code */
if (isset($_POST["resetPriority"])) {
    if (resetPriority($tableNmae = 'employees', $fieldName = 'employee_priority')) {
        $msg = "Priority Field Reset Successfully!!";
    }
}
/* End Priority Reset code */
$employeeArray = array();
$employeeSql = "SELECT * FROM employees";
$employeeSqlResult = mysqli_query($con, $employeeSql);
if ($employeeSqlResult) {
    while ($employeeSqlResultRowObj = mysqli_fetch_object($employeeSqlResult)) {
        $employeeArray[] = $employeeSqlResultRowObj;
    }
    mysqli_free_result($employeeSqlResult);
} else {
    if (DEBUG) {
        echo 'employeeSqlResult Error : ' . mysqli_error($con);
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Employee List</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--        Start lightBox -->
        <script src="<?php echo baseUrl('js/lightbox/jquery-1.7.2.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery-ui-1.8.18.custom.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery.smooth-scroll.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/lightbox.js'); ?>"></script>
        <link rel="stylesheet" href="<?php echo baseUrl('css/lightbox/lightbox.css'); ?>" type="text/css" media="screen" />
        <!--        End lightBox --> 
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->
    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include 'employee_left_navigation.php'; ?>

            <!-- Content Start -->
            <div class="content">




                <div class="title"><h5>Employee Module </h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <form action="index.php" method="post">
                    <input type="submit" onclick="return confirm('Are You Sure Want TO Reset The Priority?');" name="resetPriority" value="Reset Priority"/>
                </form>
                <div class="table">
                    <div class="head"><h5 class="iFrames">Employee List</h5></div>
                    <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                        <thead>
                            <tr>
                                <th>Employee Name</th>
                                <th>Images</th>
                                <th>Designation</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $employeeArrayCounter = count($employeeArray);
                            if ($employeeArrayCounter > 0):
                                ?>
                                <?php for ($i = 0; $i < $employeeArrayCounter; $i++): ?>
                                    <tr class="gradeA">
                                        <td><?php echo $employeeArray[$i]->employee_name; ?></td>
                                        <td><a href="<?php echo $config['IMAGE_UPLOAD_URL'] . '/employee_image/large/' . $employeeArray[$i]->employee_image; ?>" rel="lightbox[images]" title="<b><?php echo $employeeArray[$i]->employee_name; ?></b>"><img src="<?php echo baseUrl('admin/images/avatar.png'); ?>" height="14" width="14" alt="image" /></a></td>
                                        <td><?php echo $employeeArray[$i]->employee_priority; ?></td>
                                        <td class="center">

                                            <a href="employee_edit.php?id=<?php echo base64_encode($employeeArray[$i]->employee_id); ?>"><img src="<?php echo baseUrl('admin/images/pencil-grey-icon.png'); ?>" height="14" width="14" alt="Edit" /></a>&nbsp;
                                            <a onclick="javascriptt:if (!confirm('Are you sure ?'))
                                                                return false" href="index.php?del=yes&id=<?php echo base64_encode($employeeArray[$i]->employee_id); ?>"><img src="<?php echo baseUrl('admin/images/deleteFile.png'); ?>" height="14" width="14" alt="delete" /></a>
                                        </td>
                                    </tr>
                                <?php endfor; /* $i=0; i<$adminArrayCounter; $++  */ ?>
                            <?php endif; /* count($adminArray) > 0 */ ?>


                        </tbody>
                    </table>
                </div>

            </div>


            <!-- Content End -->

            <div class="fix"></div>
        </div>        
        <?php include basePath('admin/footer.php'); ?>
