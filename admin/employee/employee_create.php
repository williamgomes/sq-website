<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
$EMPLOYEE_IMAGE_WIDTH = get_option('EMPLOYEE_IMAGE_WIDTH');
$employee_name = '';
$employee_company_id = '';
$employee_designation = '';
$employee_image = '';
$employee_short_description = '';
$employee_comment = '';
$employee_priority = '';
$employee_leadership_status = '';

// Retrieving Company Name From Company Table
$companyArray = array();
$companySql = 'SELECT * FROM companies';
$companyResult = mysqli_query($con, $companySql);
if ($companyResult) {
    while ($companyResultRowObj = mysqli_fetch_object($companyResult)) {
        $companyArray[] = $companyResultRowObj;
    }
} else {
    if (DEBUG) {
        echo 'CompanySqlResult Error: ' . mysqli_error($con);
    }
    $err = "Insert Query failed.";
}

if (isset($_POST['employee_create']) AND $_POST['employee_create'] == 'Submit') {

    extract($_POST);
    $width = 0;
    if ($_FILES["employee_image"]["error"] == 0) {
        list($width) = getimagesize($_FILES["employee_image"]["tmp_name"]);
    }

//echo $width;
    if ($employee_name == '') {
        $err = 'Employee Name field is required!!';
    } elseif ($employee_company_id == '') {
        $err = 'You have to select Employee Company!!';
    } elseif ($employee_designation == '') {
        $err = 'Employee Designation field is required!!';
    } elseif ($employee_short_description == '') {
        $err = 'Employee Short Description field is required!!';
    } elseif ($employee_comment == '') {
        $err = 'Employee Comment field is required!!';
    } elseif ($employee_priority == '') {
        $err = 'Employee Priority field is required!!';
    } elseif (!is_numeric($employee_priority)) {
        $err = 'Employee Priority should be numeric!!';
    } else if ($_FILES["employee_image"]["error"] > 0) {
        $err = "Valid Employee Image is required";
    } elseif ($width < $EMPLOYEE_IMAGE_WIDTH) {
        $err = "Employee Image width should be atleast < <b>{$EMPLOYEE_IMAGE_WIDTH}</b>";
    } elseif ($width > $config['IMAGE_UPLOAD_MAX_WIDTH']) {
        $err = "Employee Image width should not exceed <b>{$config['IMAGE_UPLOAD_MAX_WIDTH']}</b>";
    }

    if ($err == '') {
        $max_employee_id = getMaxValue('employees', 'employee_id');
        $new_employee_id = $max_employee_id + 1;
        /* Srat: image upload */
        $employee_image = basename($_FILES['employee_image']['name']);
        $info = pathinfo($employee_image, PATHINFO_EXTENSION); /* it will return me like jpeg, gif, pdf, png */
        $employee_image_name = str_replace(' ', '_', $employee_name) . '-' . $new_employee_id . '.' . $info; /* create custom image name color id will add  */
        $employee_image_source = $_FILES["employee_image"]["tmp_name"];
        $employee_image_target_path = $config['IMAGE_UPLOAD_PATH'] . '/employee_image/' . $employee_image_name;
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/employee_image/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/employee_image/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/employee_image/large/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/employee_image/large/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/employee_image/mid/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/employee_image/mid/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/employee_image/small/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/employee_image/small/', 0777, TRUE);
        }

        if (!move_uploaded_file($employee_image_source, $employee_image_target_path)) {
            $employee_image_name = '';
        }
        /* End: image upload */ else {

            require basePath('lib/Zebra_Image.php');
            $employee_image_source = $employee_image_target_path;
            $image = new Zebra_Image();
            /* Start code for large size image */
            $employee_image_target_path_large = $config['IMAGE_UPLOAD_PATH'] . '/employee_image/large/' . $employee_image_name;

            $image->target_path = $employee_image_target_path_large;
            $image->source_path = $employee_image_source;

            $image->preserve_aspect_ratio = true;
            $largeSize = ceil($EMPLOYEE_IMAGE_WIDTH);
            if (!$image->resize($largeSize)) {

// if there was an error, let's see what the error is about
                $err = zebraImageErrorHandaling($image->error);

// if no errors
            }
            /* End code for large size image */
            /* Start code for midium size image */
            $employee_image_target_path_mid = $config['IMAGE_UPLOAD_PATH'] . '/employee_image/mid/' . $employee_image_name;

            $image->target_path = $employee_image_target_path_mid;
            $image->source_path = $employee_image_source;

            $image->preserve_aspect_ratio = true;
            $midSize = ceil($largeSize / 2);
            if (!$image->resize($midSize)) {

// if there was an error, let's see what the error is about
                $err = zebraImageErrorHandaling($image->error);

// if no errors
            }
            /* End code for midium size image */
            /* Start code for small size image */
            $employee_image_target_path_small = $config['IMAGE_UPLOAD_PATH'] . '/employee_image/small/' . $employee_image_name;

            $image->target_path = $employee_image_target_path_small;
            $image->source_path = $employee_image_source;

            $image->preserve_aspect_ratio = true;
            $smallSize = ceil($midSize / 2);
            if (!$image->resize($smallSize)) {

// if there was an error, let's see what the error is about
                $err = zebraImageErrorHandaling($image->error);

// if no errors
            }
            /* End code for small size image */
            $employeeFiled = '';
            $employeeFiled .='  employee_id =' . $new_employee_id;
            $employeeFiled .=', employee_name = "' . mysqli_real_escape_string($con, $employee_name) . '"';
            $employeeFiled .=', employee_company_id = "' . mysqli_real_escape_string($con, $employee_company_id) . '"';
            $employeeFiled .=', employee_designation = "' . mysqli_real_escape_string($con, $employee_designation) . '"';
            $employeeFiled .=', employee_priority ="' . intval($employee_priority) . '"';
            if (isset($_REQUEST["employee_leadership_status"])) {
                $employeeFiled .=', employee_leadership_status ="' . mysqli_real_escape_string($con, 'yes') . '"';
                $employee_leadership_status = 'yes';
            } else {
                $employeeFiled .=', employee_leadership_status ="' . mysqli_real_escape_string($con, 'no') . '"';
                $employee_leadership_status = 'no';
            }
            $employeeFiled .=', employee_image = "' . mysqli_real_escape_string($con, $employee_image_name) . '"';
            $employeeFiled .=', employee_short_description = "' . mysqli_real_escape_string($employee_short_description) . '"';
            $employeeFiled .=', employee_comment ="' . htmlentities(mysqli_real_escape_string($con, $employee_comment)) . '"';

            $employeeInsSql = "INSERT INTO employees SET $employeeFiled";
            $employeeFiledResult = mysqli_query($con, $employeeInsSql);
            if ($employeeFiledResult) {
                $msg = "Employee information insert successfully";
            } else {
                if (DEBUG) {
                    echo 'EmployeeInsSqlResult Error: ' . mysqli_error($con);
                }
                $err = "Insert Query failed.";
            }
        }
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Employee Create</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               

    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include ('employee_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Employee Module</h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Create Employee </h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="<?php echo baseUrl('admin/employee/employee_create.php'); ?>" method="post" enctype="multipart/form-data" class="mainForm">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">Employee</h5></div>
                                        <div class="rowElem noborder"><label> Employee Name (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="employee_name" value="<?php echo $employee_name; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> Employee Company (<span class="requiredSpan">*</span>):</label><div class="formRight"><select name="employee_company_id">
                                                    <option value="">Select</option>
                                                    <?php
                                                    $companyArrayCount = count($companyArray);
                                                    for ($i = 0; $i < $companyArrayCount; $i++) {
                                                        echo"<option value='" . $companyArray[$i]->company_id . "'" . (($companyArray[$i]->company_id == $employee_company_id) ? "selected" : "") . ">" . $companyArray[$i]->company_name . "</option>";
                                                    }
                                                    ?>
                                                </select></div></div>
                                        <div class="rowElem noborder"><label>Employee Image (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="file" name="employee_image" value=""  />Minimum WIDTH : <?php echo $EMPLOYEE_IMAGE_WIDTH; ?>&nbsp;Maximum WIDTH : <?php echo $config['IMAGE_UPLOAD_MAX_WIDTH']; ?></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> Employee Designation (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="employee_designation" value="<?php echo $employee_designation; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>Employee Priority (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="employee_priority" value="<?php echo $employee_priority; ?>" /></div><div class="fix"></div></div>                                        
                                        <div class="rowElem noborder"><label>Employee Leadership Status:</label><div class="formRight"><input type="checkbox" name="employee_leadership_status" <?php
                                                if ($employee_leadership_status == 'yes')
                                                    echo'checked';
                                                else
                                                    echo 'not checked';
                                                ?> /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>Employee Short Description (<span class="requiredSpan">*</span>):</label><div class="formRight"><textarea rows="8" cols="" class="auto"  name="employee_short_description"><?php echo $employee_short_description; ?></textarea></div><div class="fix"></div></div>
                                        <div class="head"><h5 class="iPencil">Employee Description (<span class="requiredSpan">*</span>):</h5></div>      
                                        <div><textarea class="tm" rows="5" cols="" name="employee_comment"><?php echo html_entity_decode($employee_comment); ?></textarea></div>                                                                                
                                        <input type="submit" name="employee_create" value="Submit" class="greyishBtn submitForm" />
                                        <div class="fix"></div>
                                    </div>
                                </fieldset>
                            </form>


                        </div>
                    </div>
                </div>

            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>
