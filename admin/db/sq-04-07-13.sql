-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2013 at 07:11 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sq`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_full_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `admin_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `admin_password` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `admin_hash` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `admin_type` enum('master','super','normal') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'master',
  `admin_status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `admin_last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `admin_update` datetime NOT NULL,
  `admin_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_full_name`, `admin_email`, `admin_password`, `admin_hash`, `admin_type`, `admin_status`, `admin_last_login`, `admin_update`, `admin_updated_by`) VALUES
(1, 'faruk omar', 'faruk@bscheme.com', 'e10adc3949ba59abb#s1q1g1r1o1u1p1*e56e057f20f883e', 'r6gtinu728essjok8lgmg4rhl1', 'master', 'active', '2013-07-04 04:37:16', '2013-06-18 00:00:00', 1),
(2, 'Tanim Ahmed', 'tanim@bscheme.com', 'e10adc3949ba59abb#s1q1g1r1o1u1p1*e56e057f20f883e', '545575#s1q1g1r1o1u1p1*761267', 'super', 'active', '2013-06-12 05:05:29', '2013-06-12 12:49:27', 0),
(3, 'mizan', 'mizan@bscheme.com', 'e10adc3949ba59abb#s1q1g1r1o1u1p1*e56e057f20f883e', '399437#s1q1g1r1o1u1p1*188192', 'super', 'active', '2013-06-22 19:38:06', '2013-06-23 01:43:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `album_id` int(5) NOT NULL AUTO_INCREMENT,
  `album_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `album_description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `album_priority` int(3) NOT NULL,
  `album_updated` datetime NOT NULL,
  `album_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`album_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`album_id`, `album_title`, `album_description`, `album_priority`, `album_updated`, `album_updated_by`) VALUES
(1, 'this is the test album', 'fggg', 1, '2013-06-20 00:00:00', 1),
(2, 'this is the test album', 'fggg', 1, '2013-06-20 00:00:00', 1),
(3, 'this is the test album', 'fggg', 1, '2013-06-20 00:00:00', 1),
(5, 'album2', 'fgdfd', 1, '2013-06-27 00:00:00', 1),
(6, 'album1', 'dfdffdfdf', 1, '2013-06-27 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `album_images`
--

CREATE TABLE IF NOT EXISTS `album_images` (
  `AI_id` int(5) NOT NULL AUTO_INCREMENT,
  `AI_album_id` int(5) NOT NULL,
  `AI_image_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `AI_image_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `AI_image_description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `AI_image_priority` int(3) NOT NULL,
  `AI_updated` datetime NOT NULL,
  `AI_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`AI_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `album_images`
--

INSERT INTO `album_images` (`AI_id`, `AI_album_id`, `AI_image_name`, `AI_image_title`, `AI_image_description`, `AI_image_priority`, `AI_updated`, `AI_updated_by`) VALUES
(1, 1, 'test-1.jpg', 'test', 'fgfg', 466, '2013-06-20 00:00:00', 1),
(2, 1, 'test-2.jpg', 'test', 'fgfg', 4, '2013-06-20 00:00:00', 1),
(3, 1, 'test-3.jpg', 'test', 'fgfg', 6, '2013-06-20 00:00:00', 1),
(4, 1, 'new_1-4.jpg', 'new 1', 'new description 1', 51, '2013-06-20 00:00:00', 1),
(5, 1, 'test-5.jpg', 'test', 'ffgfdgdgfd', 4, '2013-06-27 00:00:00', 1),
(6, 1, 'test-6.jpg', 'test', 'ffgfdgdgfd', 4, '2013-06-27 00:00:00', 1),
(7, 3, 'test-7.jpg', 'test', 'dfdd', 4, '2013-06-29 00:00:00', 1),
(8, 3, 'asdfsdsdsdsd-8.jpg', 'asdfsdsdsdsd', 'dfdd', 4, '2013-06-29 00:00:00', 1),
(9, 6, 'fdfdf-9.jpg', 'fdfdf', 'dfdfdf', 2, '2013-07-03 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `blog_id` int(3) NOT NULL AUTO_INCREMENT,
  `blog_start_date` date NOT NULL,
  `blog_end_date` date NOT NULL,
  `blog_priority` int(3) NOT NULL,
  `blog_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `blog_description` text COLLATE utf8_unicode_ci NOT NULL,
  `blog_fb_post` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blog_id`, `blog_start_date`, `blog_end_date`, `blog_priority`, `blog_title`, `blog_description`, `blog_fb_post`) VALUES
(1, '2013-06-03', '2013-06-17', 10, 'test', 'as', 'yes'),
(4, '2013-06-01', '2013-06-08', 110, 'test', 'test', 'yes'),
(5, '2013-06-01', '2013-06-08', 110, 'test', 'test', 'yes'),
(6, '2013-06-01', '2013-06-08', 110, 'test', '&lt;div class=&quot;oneone&quot;&gt;&lt;p&gt;test&lt;/p&gt;&lt;/div&gt;', 'no'),
(7, '2013-06-01', '2013-06-08', 110, 'test', '&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;p&gt;test&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;', 'no'),
(8, '2013-06-04', '2013-06-10', 10, 'test', 'test', 'yes'),
(9, '2013-06-04', '2013-06-10', 10, 'test11', '&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;p&gt;test&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(5) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category_description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `category_parent_id` int(5) NOT NULL,
  `category_priority` int(11) NOT NULL,
  `category_logo` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `category_created` enum('master','admin') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'admin',
  `category_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_description`, `category_parent_id`, `category_priority`, `category_logo`, `category_created`, `category_updated`, `category_updated_by`) VALUES
(1, 'Categories', 'Categories Categories 1', 0, 2, 'Categories-1372760740.jpg', 'master', '2013-07-02 10:25:40', 1),
(2, 'T-shirt', 'category : T- shirt', 1, 4, 'T-shirt-1372760792.', 'admin', '2013-07-02 10:26:32', 1),
(3, 'Jeans', 'Cat: Jeans ', 1, 5, 'Jeans-1372760832.', 'admin', '2013-07-02 10:27:12', 1),
(4, 'Brand ', 'Brand / partner ', 0, 3, 'Brand -1372760863.', 'master', '2013-07-02 10:27:43', 1),
(5, 'Nike', 'Brand : Nike ', 4, 6, 'Nike-1372760898.', 'admin', '2013-07-02 10:28:18', 1),
(6, 'Bata', 'Brand : Bata', 4, 6, 'Bata-1372760919.', 'admin', '2013-07-02 10:28:39', 1),
(7, 'Slipper ', 'BRAND : Bata : Slipper ', 6, 6, 'Slipper -1372761008.', 'admin', '2013-07-02 10:30:08', 1),
(8, 'Banner ', 'Banner ', 0, 9, 'Banner -1372761060.', 'master', '2013-07-02 10:31:00', 1),
(9, 'sdsdsd', 'sdsdsd', 2, 5, 'sdsdsd-1372913313.jpg', 'admin', '2013-07-04 04:48:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `circular`
--

CREATE TABLE IF NOT EXISTS `circular` (
  `circular_id` int(3) NOT NULL AUTO_INCREMENT,
  `circular_position` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `circular_number_of_vacancy` int(3) NOT NULL,
  `circular_job_responsibilities` text COLLATE utf8_unicode_ci NOT NULL,
  `circular_educational_requirements` text COLLATE utf8_unicode_ci NOT NULL,
  `circular_additional_requirements` text COLLATE utf8_unicode_ci NOT NULL,
  `circular_salary` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `circular_application_deadline` date NOT NULL,
  `circular_start_date` date NOT NULL,
  `circular_end_date` date NOT NULL,
  `circular_fb_post` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes',
  `circular_external_post_url` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`circular_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `client_id` int(3) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `client_logo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `client_country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `client_description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`client_id`, `client_name`, `client_logo`, `client_country`, `client_description`) VALUES
(1, 'mizan', 'mizan-1.jpg', 'bangladesh', '&lt;div class=&quot;oneone&quot;&gt;&lt;p&gt;jkjkjkj&lt;/p&gt;&lt;/div&gt;');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `company_id` int(3) NOT NULL AUTO_INCREMENT,
  `company_logo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `company_mission` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `company_vision` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `company_description` text COLLATE utf8_unicode_ci NOT NULL,
  `company_fb_link` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `company_tw_link` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `company_yt_link` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `company_ln_link` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `company_go_link` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `company_map_code` varchar(600) COLLATE utf8_unicode_ci NOT NULL,
  `company_priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`company_id`, `company_logo`, `company_name`, `company_mission`, `company_vision`, `company_description`, `company_fb_link`, `company_tw_link`, `company_yt_link`, `company_ln_link`, `company_go_link`, `company_map_code`, `company_priority`) VALUES
(4, 'bscheme-4.jpg', 'bscheme', 'asdad', 'asdasdad', '&lt;div class=&quot;oneone&quot;&gt;&lt;p&gt;sdsd&lt;/p&gt;&lt;/div&gt;', '', '', '', '', '', 'sdsd', 4),
(5, 'bscheme1-5.jpg', 'bscheme1', 'xcc', 'xcxc', 'xcxc', 'http://w.favcebook.com', 'http://www.favcebook.com', 'http://www.favcebook.com', 'http://www.favcebook.com', 'http://www.favcebook.com', 'xcxc', 4);

-- --------------------------------------------------------

--
-- Table structure for table `company_banners`
--

CREATE TABLE IF NOT EXISTS `company_banners` (
  `CB_id` int(5) NOT NULL AUTO_INCREMENT,
  `CB_company_id` int(5) NOT NULL,
  `CB_image_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CB_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CB_description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `CB_priority` int(3) NOT NULL,
  `CB_url` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CB_url_type` enum('internal','external') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'internal',
  `CB_updated` datetime NOT NULL,
  `CB_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`CB_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `company_banners`
--

INSERT INTO `company_banners` (`CB_id`, `CB_company_id`, `CB_image_name`, `CB_title`, `CB_description`, `CB_priority`, `CB_url`, `CB_url_type`, `CB_updated`, `CB_updated_by`) VALUES
(1, 4, 'test-1.jpg', 'test', 'CB_banner', 4, 'http://www.gmail.com', 'internal', '2013-07-03 00:00:00', 1),
(2, 4, 'test454-2.jpg', 'test454', 'CB_banner', 4, 'http://www.gmail.com', 'internal', '2013-07-03 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `config_settings`
--

CREATE TABLE IF NOT EXISTS `config_settings` (
  `CS_option` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CS_value` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `config_settings`
--

INSERT INTO `config_settings` (`CS_option`, `CS_value`) VALUES
('SITE_NAME', 'FGDF'),
('SITE_URL', 'ABC'),
('SITE_LOGO', 'FDGDFG'),
('SITE_FAVICON', 'DFGDFG'),
('CATEGORY_BANNER_MAX_SIZE', 'REF'),
('CATEGORY_BANNER_MAX_WIDTH', 'RET'),
('CATEGORY_BANNER_MAX_HEIGHT', 'RTERT'),
('SITE_DEFAULT_META_TITLE', 'RTERT'),
('SITE_DEFAULT_META_DESCRIPTION', 'RETER'),
('SITE_DEFAULT_META_KEYWORDS', 'RETERT'),
('SITE_NAME', 'SQ Group'),
('SITE_URL', ' http://localhost/sq'),
('SITE_LOGO', 'favicon.ico'),
('SITE_FAVICON', 'DFGDFG'),
('ALBUM_IMAGE_WIDTH', '1024'),
('CATEGORY_LOGO_WIDTH', '700'),
('CLIENT_LOGO_WIDTH', '1024'),
('SITE_DEFAULT_META_TITLE', 'title new'),
('SITE_DEFAULT_META_DESCRIPTION', 'description new'),
('SITE_DEFAULT_META_KEYWORDS', 'key, words, new'),
('COMPANY_LOGO_WIDTH', '1024'),
('COMPANY_BANNER_WIDTH', '1024'),
('PRODUCT_IMAGE_WIDTH', '1024'),
('TESTIMONIAL_IMAGE_WIDTH', '1024 '),
('ALBUM_IMAGE_THUMB_WIDTH', '100'),
('MENU_TITLE_CHARACTER_LIMIT', '10');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `faq_id` int(3) NOT NULL AUTO_INCREMENT,
  `faq_question` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `faq_answer` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `faq_priority` int(3) NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`faq_id`, `faq_question`, `faq_answer`, `faq_priority`) VALUES
(12, 'what is todys news?', '&lt;div class=&quot;oneone&quot;&gt;&lt;p&gt;dfdfdfd&lt;/p&gt;&lt;/div&gt;', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `menu_id` int(5) NOT NULL AUTO_INCREMENT,
  `menu_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_title`) VALUES
(1, 'menu1'),
(2, 'ssdsdsdsd'),
(3, 'sdfsdfsdfsdfsd');

-- --------------------------------------------------------

--
-- Table structure for table `menu_url`
--

CREATE TABLE IF NOT EXISTS `menu_url` (
  `MU_id` int(5) NOT NULL AUTO_INCREMENT,
  `MU_menu_id` int(5) NOT NULL,
  `MU_url` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `MU_priority` int(3) NOT NULL,
  `MU_url_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MU_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `menu_url`
--

INSERT INTO `menu_url` (`MU_id`, `MU_menu_id`, `MU_url`, `MU_priority`, `MU_url_title`) VALUES
(1, 1, 'http://www', 5, 'jjkjok');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `page_id` int(3) NOT NULL AUTO_INCREMENT,
  `page_url` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `page_short_description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `page_body` text COLLATE utf8_unicode_ci NOT NULL,
  `page_meta_title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `page_meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `page_meta_keywords` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `page_priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `page_url`, `page_title`, `page_short_description`, `page_body`, `page_meta_title`, `page_meta_description`, `page_meta_keywords`, `page_priority`) VALUES
(1, 'http://www.uson-bd.org', 'test title', 'test title', 'test title', 'test title', 'test title', 'test title', NULL),
(2, 'http://www.usonp-bd.org', 'test title1', 'sdfghj', '<div class="oneone"><p>asdfghjk</p></div>', 'asdfgh', 'asdfhj', 'asdfgh', 44);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(6) NOT NULL AUTO_INCREMENT,
  `product_category_id` int(5) NOT NULL,
  `product_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_description` text COLLATE utf8_unicode_ci NOT NULL,
  `product_updated` datetime NOT NULL,
  `product_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_category_id`, `product_title`, `product_description`, `product_updated`, `product_updated_by`) VALUES
(11, 1, 'test', 'ghgfhgfh', '2013-06-23 00:00:00', 1),
(12, 1, 'test44', '&lt;div class=&quot;oneone&quot;&gt;&lt;p&gt;ghgfhgfh&lt;/p&gt;&lt;/div&gt;', '2013-06-23 00:00:00', 1),
(13, 1, 'test4444', '&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;p&gt;ghgfhgfh&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;', '2013-06-23 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
  `PC_id` int(11) NOT NULL AUTO_INCREMENT,
  `PC_product_id` int(11) NOT NULL,
  `PC_category_id` int(11) NOT NULL,
  `PC_date` date NOT NULL,
  `PC_created_by` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`PC_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='PC = product category' AUTO_INCREMENT=8 ;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`PC_id`, `PC_product_id`, `PC_category_id`, `PC_date`, `PC_created_by`) VALUES
(2, 12, 7, '2002-07-13', '1'),
(3, 12, 5, '2002-07-13', '1'),
(4, 11, 1, '2002-07-13', '1'),
(6, 11, 4, '2004-07-13', '1'),
(7, 11, 8, '2004-07-13', '1');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `PI_id` int(5) NOT NULL AUTO_INCREMENT,
  `PI_product_id` int(5) NOT NULL,
  `PI_image_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PI_priority` int(3) NOT NULL,
  PRIMARY KEY (`PI_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`PI_id`, `PI_product_id`, `PI_image_name`, `PI_priority`) VALUES
(6, 12, '4-6.jpg', 4),
(7, 11, '1-11.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_packs`
--

CREATE TABLE IF NOT EXISTS `product_packs` (
  `PP_id` int(5) NOT NULL AUTO_INCREMENT,
  `PP_product_id` int(5) NOT NULL,
  `PP_size_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`PP_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `product_packs`
--

INSERT INTO `product_packs` (`PP_id`, `PP_product_id`, `PP_size_name`) VALUES
(6, 0, 'large'),
(7, 0, 'large'),
(8, 11, 'large'),
(9, 12, 'largenew'),
(13, 13, 'large'),
(14, 13, 'small'),
(15, 13, 'midium'),
(16, 13, 'midium'),
(17, 0, 'small22'),
(18, 0, 'small'),
(19, 11, 'midium'),
(20, 11, 'midium');

-- --------------------------------------------------------

--
-- Table structure for table `product_sizes`
--

CREATE TABLE IF NOT EXISTS `product_sizes` (
  `PS_id` int(5) NOT NULL AUTO_INCREMENT,
  `PS_product_id` int(5) NOT NULL,
  `PS_size` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`PS_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `product_sizes`
--

INSERT INTO `product_sizes` (`PS_id`, `PS_product_id`, `PS_size`) VALUES
(6, 13, 'sdsdsd'),
(7, 13, 'qwqwqwqw'),
(8, 11, 'large');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE IF NOT EXISTS `testimonials` (
  `testimonial_id` int(3) NOT NULL AUTO_INCREMENT,
  `testimonial_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `testimonial_image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `testimonial_comment` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `testimonial_priority` int(3) NOT NULL,
  PRIMARY KEY (`testimonial_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonial_id`, `testimonial_name`, `testimonial_image`, `testimonial_comment`, `testimonial_priority`) VALUES
(1, 'test  Name', 'test__Name-1.jpg', '&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;p&gt;sdasdsd&lt;/p&gt;&lt;/d', 2),
(2, 'testName1', 'testName1-2.jpg', '&lt;div class=&quot;oneone&quot;&gt;&lt;div class=&quot;oneone&quot;&gt;&lt;p&gt;sdasdsd&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;', 2),
(3, 'testimonial123', 'testimonial123-3.jpg', 'sasdadasd&lt;br&gt;asdadsa&lt;br&gt;asdasdsd', 10),
(4, 'testName12', 'testName12-4.jpg', 'sdfsdf', 2),
(5, 'faruk omar45', 'faruk_omar45-5.jpg', 'kjkjk', 2),
(6, 'faruk omar', 'faruk_omar-6.jpg', 'kkl', 7),
(7, 'testName454', 'testName454-7.jpg', 'hjhjh', 2),
(8, 'asd', 'asd-8.jpg', 'sdsad', 2),
(9, 'testName1247', 'testName1247-9.jpg', 'dffdfd', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
