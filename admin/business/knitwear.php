<?php
include ('../../config/config.php');
include basePath('lib/Zebra_Image.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
//saving tags in database

$aid = $_SESSION['admin_id']; //getting loggedin admin id
$bannerTitle = get_option('KNITWEAR_BANNER_TITLE');
$bannerImage = get_option('KNITWEAR_BANNER');
$title = get_option('KNITWEAR_TITLE');
$description = get_option('KNITWEAR_DESCRIPTION');
$quickRead = get_option('KNITWEAR_QUICKREAD');
$bottomImageUrl = get_option('KNITWEAR_BOTTOM_IMAGE_URL');

$BUSINESS_BANNER_WIDTH = get_option('BUSINESS_BANNER_WIDTH');



if (isset($_POST['update'])) {
    extract($_POST);
    if($_FILES['bannerImage']['error'] == 0) {
    list($width) = getimagesize($_FILES['bannerImage']['tmp_name']);
    }
    if($_FILES['bannerImage']['error'] == 0 && !checkFileType($_FILES['bannerImage']['name'], 'ALLOWED_IMAGE')) {
        $err = "Banner should be an image";
    } elseif($_FILES['bannerImage']['error'] == 0 && $width < 1200) {
        $err = "Banner Width should not less than 1200px";
    } elseif($_FILES['bannerImage']['error'] == 0 && $width > 1600) {
        $err = "Banner Width should not exceed 1600px";
    } elseif($bannerTitle == '') {
        $err = "Banner Title field is required";
    } elseif($title == '') {
        $err = "Title field is required";
    } elseif($description == '') {
        $err = "Banner Description field is required";
    }
    
    if ($err == '') {
        if ($_FILES['bannerImage']['size'] > 0 || !empty($_FILES['bannerImage']['tmp_name'])) {
            //uploading banner if given
            /* if image select for banner */
            $image = basename($_FILES['bannerImage']['name']);
            $info = pathinfo($image, PATHINFO_EXTENSION);
            $image_name = "KNITWEAR_BANNER." . $info;
            $image_source = $_FILES["bannerImage"]["tmp_name"];
            $image_target_path = $config['IMAGE_UPLOAD_PATH'] . '/business/knitwear/' . $image_name;
            if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/business/knitwear/')) {
                mkdir($config['IMAGE_UPLOAD_PATH'] . '/business/knitwear/', 0777, TRUE);
            }
            if (move_uploaded_file($image_source, $image_target_path)) {
                $bannerImage = $image_name;
            }
        } /* ($_FILES['bannerImage']['size'] > 0 || !empty($_FILES['bannerImage']['tmp_name'])) */

        $updateFiled = "";
        $updateFiled .="`CS_value` = CASE CS_option";
        $updateFiled .=" WHEN 'KNITWEAR_BANNER_TITLE' THEN '" . mysqli_real_escape_string($con, $bannerTitle) . "' ";
        $updateFiled .=" WHEN 'KNITWEAR_TITLE' THEN '" . mysqli_real_escape_string($con, $title) . "' ";
        $updateFiled .=" WHEN 'KNITWEAR_DESCRIPTION' THEN '" . htmlentities(mysqli_real_escape_string($con, $description)) . "' ";
        $updateFiled .=" WHEN 'KNITWEAR_BANNER' THEN '" . mysqli_real_escape_string($con, $bannerImage) . "' ";
        $updateFiled .=" WHEN 'KNITWEAR_QUICKREAD' THEN '" . mysqli_real_escape_string($con, $quickRead) . "' ";
        $updateFiled .=" WHEN 'KNITWEAR_BOTTOM_IMAGE_URL' THEN '" . mysqli_real_escape_string($con, $bottomImageUrl) . "' ";
        $updateFiled .=" ELSE CS_value END,CS_updated_by= CASE CS_option
                            WHEN 'KNITWEAR_BANNER_TITLE' THEN '$aid'
                            WHEN 'KNITWEAR_TITLE' THEN '$aid'
                            WHEN 'KNITWEAR_DESCRIPTION' THEN '$aid'
                            WHEN 'KNITWEAR_BANNER' THEN '$aid'
                            WHEN 'KNITWEAR_QUICKREAD' THEN '$aid'
                            WHEN 'KNITWEAR_BOTTOM_IMAGE_URL' THEN '$aid'
                            ELSE CS_updated_by
                            END";

        $updateSql = "UPDATE `config_settings` SET $updateFiled ";

        $updateSqlResult = mysqli_query($con, $updateSql);
        if ($updateSqlResult) {
                $msg = "Knitwear data updated successfully";
                } else /* ($updateSqlResult) */ {
            if (DEBUG) {
                echo 'updateSqlResult Error' . mysqli_error($con);
            }
        } /* ($updateSqlResult) */
    } /* ($err == '') */
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Business : Knitwear </title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  

        <!--        Start lightBox -->
        <script src="<?php echo baseUrl('js/lightbox/jquery-1.7.2.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery-ui-1.8.18.custom.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery.smooth-scroll.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/lightbox.js'); ?>"></script>
        <link rel="stylesheet" href="<?php echo baseUrl('css/lightbox/lightbox.css'); ?>" type="text/css" media="screen" />
        <!--        End lightBox -->  

        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               

    </head>

    <body>


        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <?php include ('business_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Business Module</h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>

                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Knitwear Update Page</h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="<?php echo basename(__FILE__); ?>" method="post" class="mainForm" enctype="multipart/form-data">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">Update Data </h5></div>
                                        <div class="rowElem noborder"><label>Banner:</label><div class="formRight"><input type="file" name="bannerImage" />Min Width:1200px Max Width:1600px<a href="<?php echo baseUrl('upload/business/knitwear/' . $bannerImage) ?>"  rel="lightbox[plants]" title="Banner Image"> Current banner</a></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>Banner Title (<span class="requiredSpan">*</span>):</label><div class="formRight"><input name="bannerTitle" type="text" value="<?php echo $bannerTitle; ?>"/></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>Title (<span class="requiredSpan">*</span>):</label><div class="formRight"><input name="title" type="text" value="<?php echo $title; ?>"/></div><div class="fix"></div></div>
                                        <div class="head"><h5 class="iPencil">Description (<span class="requiredSpan">*</span>):</h5></div>  
                                        <div><textarea class="tm" rows="5" cols="" name="description"><?php echo html_entity_decode($description); ?></textarea></div>
                                        <div class="head"><h5 class="iPencil">Quick Read (<span class="requiredSpan">*</span>):</h5></div>  
                                        <div><textarea class="tm" rows="5" cols="" name="quickRead"><?php echo html_entity_decode($quickRead); ?></textarea></div>
                                        <div class="rowElem noborder"><label>Bottom Image Url (<span class="requiredSpan">*</span>):</label><div class="formRight"><input name="bottomImageUrl" type="text" value="<?php echo $bottomImageUrl; ?>"/></div><div class="fix"></div></div>
                                        
                                        <div class="rowElem noborder"><input type="submit" name="update" value="Update" class="greyishBtn submitForm" /></div>
                                        <div class="fix"></div>

                                    </div>
                                </fieldset>

                            </form>		


                        </div>

                    </div>
                </div>

            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>
