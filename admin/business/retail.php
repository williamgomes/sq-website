<?php
include ('../../config/config.php');
include basePath('lib/Zebra_Image.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
//saving tags in database

$aid = $_SESSION['admin_id']; //getting loggedin admin id
$bannerLogo = get_option('RETAIL_BANNER_LOGO');
$bannerImage = get_option('RETAIL_BANNER');
$title = get_option('RETAIL_TITLE');
$description = get_option('RETAIL_DESCRIPTION');
$retailLink = get_option('RETAIL_LINK');
$retailLinkTitle = get_option('RETAIL_LINK_TITLE');
$BUSINESS_BANNER_WIDTH = get_option('BUSINESS_BANNER_WIDTH');



if (isset($_POST['update'])) {
    extract($_POST);
    if($_FILES['bannerImage']['error'] == 0) {
    list($width) = getimagesize($_FILES['bannerImage']['tmp_name']);
    }
    if($_FILES['bannerLogo']['error'] == 0) {
    list($widthLogo) = getimagesize($_FILES['bannerLogo']['tmp_name']);
    }
    if($_FILES['bannerImage']['error'] == 0 && !checkFileType($_FILES['bannerImage']['name'], 'ALLOWED_IMAGE')) {
        $err = "Banner should be an image";
    } 
    elseif($_FILES['bannerImage']['error'] == 0 && $width < 1200) {
        $err = "Banner Image Width should not less than 1200px";
    } elseif($_FILES['bannerImage']['error'] == 0 && $width > 1600) {
        $err = "Banner Image Width should not exceed 1600px";
    } 
    elseif($_FILES['bannerLogo']['error'] == 0 && !checkFileType($_FILES['bannerLogo']['tmp_name'],'ALLOWED_IMAGE')) {
        $err = "Logo should be an Image";
    }
    elseif($_FILES['bannerLogo']['error'] == 0 && $widthLogo < $BUSINESS_BANNER_WIDTH) {
        $err = "Banner Logo Width should not less than $BUSINESS_BANNER_WIDTH";
    } elseif($_FILES['bannerLogo']['error'] == 0 && $widthLogo > $config['IMAGE_UPLOAD_MAX_WIDTH']) {
        $err = "Banner Logo Width should not exceed $BUSINESS_BANNER_WIDTH";
    } 
    elseif($title == '') {
        $err = "Banner Title field is required";
    } elseif($description == '') {
        $err = "Banner Description field is required";
    } elseif($retailLink == '') {
        $err = "Url field is required";
    }  elseif(filter_var($retailLink, FILTER_VALIDATE_URL) === FALSE) {
        $err = "Url should be valid";
    } elseif($retailLinkTitle == '') {
        $err = "Url Title field is required";
    }
    if ($err == '') {
        if ($_FILES['bannerImage']['size'] > 0 || !empty($_FILES['bannerImage']['tmp_name'])) {
            //uploading banner if given
            /* if image select for banner */
            $image = basename($_FILES['bannerImage']['name']);
            $info = pathinfo($image, PATHINFO_EXTENSION);
            $image_name = "RETAIL_BANNER." . $info;
            $image_source = $_FILES["bannerImage"]["tmp_name"];
            $image_target_path = $config['IMAGE_UPLOAD_PATH'] . '/business/retail/banner/' . $image_name;
            if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/business/retail/banner/')) {
                mkdir($config['IMAGE_UPLOAD_PATH'] . '/business/retail/banner/', 0777, TRUE);
            }
            if (move_uploaded_file($image_source, $image_target_path)) {
                $bannerImage = $image_name;
            }
        } /* ($_FILES['bannerImage']['size'] > 0 || !empty($_FILES['bannerImage']['tmp_name'])) */
        
        if ($_FILES['bannerLogo']['size'] > 0 || !empty($_FILES['bannerLogo']['tmp_name'])) {
            //uploading banner if given
            /* if image select for banner */
            $image = basename($_FILES['bannerLogo']['name']);
            $info = pathinfo($image, PATHINFO_EXTENSION);
            $image_name = "RETAIL_BANNER_LOGO." . $info;
            $image_source = $_FILES["bannerLogo"]["tmp_name"];
            $image_target_path = $config['IMAGE_UPLOAD_PATH'] . '/business/retail/logo/' . $image_name;
            if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/business/retail/logo/')) {
                mkdir($config['IMAGE_UPLOAD_PATH'] . '/business/retail/logo/', 0777, TRUE);
            }
            if (move_uploaded_file($image_source, $image_target_path)) {
                $bannerLogo = $image_name;
            }
        }
        $updateFiled = "";
        $updateFiled .="`CS_value` = CASE CS_option";
        $updateFiled .=" WHEN 'RETAIL_BANNER' THEN '" . mysqli_real_escape_string($con, $bannerImage) . "' ";
        $updateFiled .=" WHEN 'RETAIL_BANNER_LOGO' THEN '" . mysqli_real_escape_string($con, $bannerLogo) . "' ";
        $updateFiled .=" WHEN 'RETAIL_TITLE' THEN '" . mysqli_real_escape_string($con, $title) . "' ";
        $updateFiled .=" WHEN 'RETAIL_DESCRIPTION' THEN '" . htmlentities(mysqli_real_escape_string($con, $description)) . "' ";
        $updateFiled .=" WHEN 'RETAIL_LINK' THEN '" . mysqli_real_escape_string($con, $retailLink) . "' ";
        $updateFiled .=" WHEN 'RETAIL_LINK_TITLE' THEN '" . mysqli_real_escape_string($con, $retailLinkTitle) . "' ";
        $updateFiled .=" ELSE CS_value END,CS_updated_by= CASE CS_option
                            WHEN 'RETAIL_BANNER' THEN '$aid'
                            WHEN 'RETAIL_BANNER_LOGO' THEN '$aid'
                            WHEN 'RETAIL_TITLE' THEN '$aid'
                            WHEN 'RETAIL_DESCRIPTION' THEN '$aid'
                            WHEN 'RETAIL_LINK' THEN '$aid'
                            WHEN 'RETAIL_LINK_TITLE' THEN '$aid'
                            ELSE CS_updated_by
                            END";

        $updateSql = "UPDATE `config_settings` SET $updateFiled ";

        $updateSqlResult = mysqli_query($con, $updateSql);
        if ($updateSqlResult) {
                $msg = "Retail data updated successfully";
        } else /* ($updateSqlResult) */ {
            if (DEBUG) {
                echo 'updateSqlResult Error' . mysqli_error($con);
            }
        } /* ($updateSqlResult) */
    } /* ($err == '') */
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Business : Retail </title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  

        <!--        Start lightBox -->
        <script src="<?php echo baseUrl('js/lightbox/jquery-1.7.2.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery-ui-1.8.18.custom.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery.smooth-scroll.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/lightbox.js'); ?>"></script>
        <link rel="stylesheet" href="<?php echo baseUrl('css/lightbox/lightbox.css'); ?>" type="text/css" media="screen" />
        <!--        End lightBox -->  

        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               

    </head>

    <body>


        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <?php include ('business_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Business Module</h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>

                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Retail Update Page</h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="<?php echo basename(__FILE__); ?>" method="post" class="mainForm" enctype="multipart/form-data">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">Update Data </h5></div>
                                        <div class="rowElem noborder"><label>Banner:</label><div class="formRight"><input type="file" name="bannerImage" />Min Width:1200px Max Width:1600px<a href="<?php echo baseUrl('upload/business/retail/banner/' . $bannerImage) ?>"  rel="lightbox[plants]" title="Banner Image"> Current banner</a></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>Banner Logo:</label><div class="formRight"><input type="file" name="bannerLogo" /><a href="<?php echo baseUrl('upload/business/retail/logo/' . $bannerLogo) ?>"  rel="lightbox[plants]" title="Banner Logo">Current Logo</a></div><div class="fix"></div></div>                                        
                                        <div class="rowElem noborder"><label>Title (<span class="requiredSpan">*</span>):</label><div class="formRight"><input name="title" type="text" value="<?php echo $title; ?>"/></div><div class="fix"></div></div>
                                        <div class="head"><h5 class="iPencil">Description (<span class="requiredSpan">*</span>):</h5></div>  
                                        <div><textarea class="tm" rows="5" cols="" name="description"><?php echo html_entity_decode($description); ?></textarea></div>
                                        <div class="rowElem noborder"><label>Url (<span class="requiredSpan">*</span>):</label><div class="formRight"><input name="retailLink" type="text" value="<?php echo $retailLink; ?>"/></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>Url Title (<span class="requiredSpan">*</span>):</label><div class="formRight"><input name="retailLinkTitle" type="text" value="<?php echo $retailLinkTitle; ?>"/></div><div class="fix"></div></div>

                                        <input type="submit" name="update" value="Update" class="greyishBtn submitForm" />
                                        <div class="fix"></div>

                                    </div>
                                </fieldset>

                            </form>		


                        </div>

                    </div>
                </div>

            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>
