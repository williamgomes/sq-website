<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
/** Start:saving tags in database * */
$banner_image = "";
$phone_call = "";
$email = "";
$visit = "";
$skype = "";
$aid = $_SESSION["admin_id"];
$CONTACT_BANNER_WIDTH = get_option('CONTACT_BANNER_WIDTH');
if (isset($_POST['update'])) {
    extract($_POST);
    if ($_FILES["banner_image"]["error"] == 0) {
        list($width) = getimagesize($_FILES["banner_image"]["tmp_name"]);
    }
    if ($_FILES["banner_image"]["error"] == 0 && $width<$CONTACT_BANNER_WIDTH) {
        $err = "Banner Image should be atleast $CONTACT_BANNER_WIDTH px.";
    }elseif ($_FILES["banner_image"]["error"] == 0 && $width>$config['IMAGE_UPLOAD_MAX_WIDTH']) {
        $err = "Banner Image should be exceed". $config['IMAGE_UPLOAD_MAX_WIDTH']." px.";
    } elseif ($phone_call == "") {
        $err = "Phone call field is required.";
    } elseif ($email == "") {
        $err = "Email filed is required.";
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $err = "Invalid Email address";
    } elseif ($visit == "") {
        $err = "visit field is required.";
    } elseif ($skype == '') {
        $err = "skype username  is required ";
    }

    if ($err == "") {
        $banner_image = basename($_FILES['banner_image']['name']);
        $info = pathinfo($banner_image, PATHINFO_EXTENSION); /* it will return me like jpeg, gif, pdf, png */
        $banner_image_name = 'contact_banner.' . $info; /* create custom image name color id will add  */
        $banner_image_source = $_FILES["banner_image"]["tmp_name"];
        $banner_image_target_path = $config['IMAGE_UPLOAD_PATH'] . '/contact_banner_image/' . $banner_image_name;
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/contact_banner_image/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/contact_banner_image/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/contact_banner_image/small/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/contact_banner_image/small/', 0777, TRUE);
        }


        if (!move_uploaded_file($banner_image_source, $banner_image_target_path)) {
            $banner_image = '';
        } else {
                        require basePath('lib/Zebra_Image.php');
            $banner_image_source = $banner_image_target_path;
            $image = new Zebra_Image();
            /* Start code for large size image */
            $banner_image_target_path_small = $config['IMAGE_UPLOAD_PATH'] . '/contact_banner_image/small/' . $banner_image_name;

            $image->target_path = $banner_image_target_path_small;
            $image->source_path = $banner_image_source;

            $image->preserve_aspect_ratio = true;
            if (!$image->resize(962)) {

// if there was an error, let's see what the error is about
                $err = zebraImageErrorHandaling($image->error);

// if no errors
            }
            /* End code for large size image */
        }
        $update_contact_query = '';
        $update_contact_query .= "UPDATE contact SET contact_value = CASE contact_option";
                                                                                if($_FILES["banner_image"]["error"] == 0) {
										$update_contact_query .= " WHEN 'BANNER_IMAGE' THEN '" . mysqli_real_escape_string($con, $banner_image_name) . "'";
                                                                                }
										$update_contact_query .= " WHEN 'PHONE_CALL' THEN '" . mysqli_real_escape_string($con, $phone_call) . "'
										WHEN 'EMAIL' THEN '" . mysqli_real_escape_string($con, $email) . "'
										WHEN 'VISIT' THEN '" . htmlentities(mysqli_real_escape_string($con, $visit)) . "'
                                                                                WHEN 'SKYPE' THEN '" . mysqli_real_escape_string($con, $skype) . "'
										ELSE contact_value END , contact_updated_by = CASE contact_option
                                                                                WHEN 'BANNER_IMAGE' THEN '$aid'
                                                                                WHEN 'PHONE_CALL' THEN '$aid'
										WHEN 'EMAIL' THEN '$aid'
										WHEN 'VISIT' THEN '$aid'
                                                                                WHEN 'SKYPE' THEN '$aid'
										ELSE contact_updated_by END";
        $setupdate = mysqli_query($con, $update_contact_query);

        if ($setupdate) {
            $msg = "Contact updated successfully";
            //echo "<meta http-equiv='refresh' content='5; url=index.php'>";
        } else {
            if (DEBUG) {
                echo 'setupdate Error' . mysqli_error($con);
            }
        }
    }
}
/** End: saving tags in database * */
/** Start: Query FOR cantact Value * */
$responses = array();
$getset = mysqli_query($con, "SELECT * FROM contact");
if (mysqli_num_rows($getset) > 0) {

    while ($row = mysqli_fetch_object($getset)) {
        $responses[$row->contact_option] = $row->contact_value;
    }
}
/** END: Query FOR cantact Value * */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Contact Us</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               

    </head>

    <body>
        <?php include basePath('admin/top_navigation.php'); ?>
        <?php include basePath('admin/module_link.php'); ?>
        <!-- Content wrapper -->
        <div class="wrapper">
            <!-- Left navigation -->
            <?php include basePath('admin/contact/contact_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Contact Module</h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>

                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Contact Settings</h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="<?php echo baseUrl('admin/contact/index.php'); ?>" method="post" class="mainForm" enctype="multipart/form-data">
                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="rowElem noborder"><label>Banner Image:</label><div class="formRight"><input type="file" name="banner_image" value=""  />Minimum WIDTH : <?php echo $CONTACT_BANNER_WIDTH; ?>&nbsp;Maximum WIDTH : <?php echo $config['IMAGE_UPLOAD_MAX_WIDTH']; ?></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>phone call (<span class="requiredSpan">*</span>):</label><div class="formRight"><input name="phone_call" type="text" value="<?php echo $responses['PHONE_CALL']; ?>"/></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>email (<span class="requiredSpan">*</span>):</label><div class="formRight"><input name="email" type="text" value="<?php echo $responses['EMAIL']; ?>"/></div><div class="fix"></div></div>
                                        <div class="head"><h5 class="iPencil">visit (<span class="requiredSpan">*</span>):</h5></div>  
                                        <div><textarea class="tm" rows="5" cols="" name="visit"><?php echo html_entity_decode($responses['VISIT']); ?></textarea></div>
                                        <div class="rowElem noborder"><label>Skype (<span class="requiredSpan">*</span>):</label><div class="formRight"><input name="skype" type="text" value="<?php echo $responses['SKYPE']; ?>"/></div><div class="fix"></div></div>
                                        <input type="submit" name="update" value="Update Contact Information" class="greyishBtn submitForm" />
                                        <div class="fix"></div>
                                    </div>
                                </fieldset>
                            </form>		
                        </div>

                    </div>
                </div>

            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>
