<?php
include '../../config/config.php';
//$xml = "http://www.thedailystar.net/beta2/rss-category-news/";

$rssData = array();
$xml = "http://feeds.feedburner.com/thefinancialexpress-bd/IouH?format=xml";

$xmlDoc = new DOMDocument();
$xmlDoc->load($xml);

//get elements from "<channel>"
$channel = $xmlDoc->getElementsByTagName('channel')->item(0);
$channel_title = $channel->getElementsByTagName('title')
                ->item(0)->childNodes->item(0)->nodeValue;
$channel_link = $channel->getElementsByTagName('link')
                ->item(0)->childNodes->item(0)->nodeValue;
$channel_desc = $channel->getElementsByTagName('description')
                ->item(0)->childNodes->item(0)->nodeValue;

//output elements from "<channel>"

$rssData['channel_link'] = $channel_link;
$rssData['channel_title'] = $channel_title;
$rssData['rss_item_link_type'] = 'external';
$rssData['rss_items'] = array();


//get and output "<item>" elements
$x = $xmlDoc->getElementsByTagName('item');
$numberOfItem = $x->length;
for ($i = 0; $i < $numberOfItem; $i++) {
    $item_title = $x->item($i)->getElementsByTagName('title')
                    ->item(0)->childNodes->item(0)->nodeValue;
    $item_link = $x->item($i)->getElementsByTagName('link')
                    ->item(0)->childNodes->item(0)->nodeValue;
    $item_desc = $x->item($i)->getElementsByTagName('description')
                    ->item(0)->childNodes->item(0)->nodeValue;


    $rssData['rss_items'][$i]['title'] = $item_title;
    $rssData['rss_items'][$i]['link'] = $item_link;
    $rssData['rss_items'][$i]['description'] = $item_desc;
}


if (isset($_POST['submit']) AND $_POST['submit'] == 'Save') {
    //printDie($_POST);

    $status = array();
    if (count($_POST['news']) > 0) {

        for ($j = 0; $j < count($_POST['news']); $j++) {
            $insFiled = '';
            $insFiled .=" rss_status = 'active'";
            $insFiled .=",  rss_channel_title= '" . mysqli_real_escape_string($con, $rssData['channel_title']) . "'";
            $insFiled .=",  rss_channel_link= '" . mysqli_real_escape_string($con, $rssData['channel_link']) . "'";
            $insFiled .=", rss_item_link = '" . mysqli_real_escape_string($con, $rssData['rss_items'][$_POST['news'][$j]]['link']) . "'";
            $insFiled .=",  rss_item_link_type= 'external'";
            $insFiled .=",  rss_item_title= '" . mysqli_real_escape_string($con, $rssData['rss_items'][$_POST['news'][$j]]['title']) . "'";
            $insFiled .=",  rss_item_description= '" . mysqli_real_escape_string($con, $rssData['rss_items'][$_POST['news'][$j]]['description']) . "'";
            $insFiled .=",  rss_item_added= '".date("Y-m-d H:i:s")."'";

            $insSql = "INSERT INTO rss SET $insFiled";
            $insSqlResult=  mysqli_query($con, $insSql);
            if($insSqlResult){
                $status[]=1;
            }else{
                if(DEBUG){
                    echo 'insSqlResult Error '.  mysqli_error($con);
                }else{
                    echo 'insSqlResult Error';
                }
            }
            
            
        }
        
        $msg= count($status)." item addedd";
    }
}

//printDie($rssData);
?> 




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="<?php echo baseUrl('admin/images/favicon.ico') ?>" />
        <title>Package Category Product Panel | Package Category Product</title>

        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css' />
        <script src="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type="text/javascript"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, File upload, editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/spinner/ui.spinner.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery-ui.min.js'); ?>"></script>  
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/fileManager/elfinder.min.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/wysiwyg/jquery.wysiwyg.js'); ?>"></script>
        <!--Effect on wysiwyg editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/wysiwyg/wysiwyg.image.js'); ?>"></script>
        <!--Effect on wysiwyg editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/wysiwyg/wysiwyg.link.js'); ?>"></script>
        <!--Effect on wysiwyg editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/wysiwyg/wysiwyg.table.js'); ?>"></script>
        <!--Effect on wysiwyg editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/dataTables/jquery.dataTables.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/dataTables/colResizable.min.js'); ?>"></script>
        <!--Effect on left error menu, top message menu -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/forms/forms.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/forms/autogrowtextarea.js'); ?>"></script>
        <!--Effect on left error menu, top message menu, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/forms/autotab.js'); ?>"></script>
        <!--Effect on left error menu, top message menu -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/forms/jquery.validationEngine.js'); ?>"></script>
        <!--Effect on left error menu, top message menu-->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/colorPicker/colorpicker.js'); ?>"></script>
        <!--Effect on left error menu, top message menu -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/uploader/plupload.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/uploader/plupload.html5.js'); ?>"></script>
        <!--Effect on file upload-->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/uploader/plupload.html4.js'); ?>"></script>
        <!--No effect-->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/uploader/jquery.plupload.queue.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/ui/jquery.tipsy.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,  -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jBreadCrumb.1.1.js'); ?>"></script>
        <!--Effect on left error menu, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/cal.min.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery.collapsible.min.js'); ?>"></script>
        <!--Effect on left error menu, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery.ToTop.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery.listnav.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery.sourcerer.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/custom.js'); ?>"></script>
        <script>
            function related(str)
            {
                var id = document.frm1.pid.value;

                if (str == "")
                {
                    document.getElementById("txtHint").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("shwClr").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajaxcat.php?c=" + str + "&id=" + id, true);
                xmlhttp.send();
            }
        </script>

        <!--Effect on left error menu, top message menu, body-->
        <!--delete tags-->

        <!--select box script-->
        <script type="text/javascript">
            checked = false;
            function checkedAll(frm1) {
                var aa = document.getElementById('frm1');
                if (checked == false)
                {
                    checked = true
                }
                else
                {
                    checked = false
                }
                for (var i = 0; i < aa.elements.length; i++)
                {
                    aa.elements[i].checked = checked;
                }
            }
            

        </script>
        <!--end select box script-->

    </head>

    <body>


<?php include basePath('admin/top_navigation.php'); ?>

<?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <div class="leftNav">
<?php include basePath('admin/rss/rss_left_navigation.php'); ?>
            </div>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Rss: Thefinancialexpress-bd </h5></div>

                <!-- Notification messages -->
<?php include basePath('admin/message.php'); ?>

                <!-- Charts -->


                <form id ="frm1" action="thefinancialexpress-bd.php" method="post">            
                    <div class="table">
                        <div class="head">
                            <h5 class="iFrames">Select News</h5></div>
                        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                            <thead>
                                <tr>
                                    <th style="mystyle">Select</th>
                                    <th>Title</th>
                                    <th>Link</th>
                                </tr>
                            </thead>
                            <tbody>

<?php for ($i = 0; $i < $numberOfItem; $i++): ?>
                                    <tr class="gradeA">
                                        <td><input type="checkbox" class="news" name="news[]" value="<?php echo $i; ?>" /></td>
                                        <td><?php echo $rssData['rss_items'][$i]['title']; ?></td>
                                        <td><a href="<?php echo  $rssData['rss_items'][$i]['link']; ?>" target="_blank">Visit</a></td>
                                    </tr>
    <?php
endfor; /* ($i=0; $i < $numberOfItem; $i++) */
?>
                            </tbody>
                        </table>
                        <input type="submit" name="submit" value="Save" class="greyishBtn submitForm" />
                    </div>
                </form>
            </div>
              <!-- Content End -->
        </div>
      
 <!-- Content wrapper -->
        <div class="fix"></div>
        </div>

<?php include basePath('admin/footer.php'); ?>
        <script type="text/javascript">
//                        //$("input[name*='task']")
//            $(".news").click(function(){
//                var n = $("input[name*='news']").length;
//                alert($("#frm1").html());
//            });
            
            
            var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
            var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
            var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1");
        </script>
