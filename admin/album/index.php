<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
$album_id = 0;

if (isset($_REQUEST['del']) AND isset($_REQUEST['id'])) {
    $del_album_id = base64_decode($_REQUEST['id']);

    $imageSQL = "SELECT `AI_image_name` FROM album_images WHERE AI_album_id = " . $del_album_id;
    $imageSQLResult = mysqli_query($con, $imageSQL);
    if ($imageSQLResult) {
        while ($imageSQLResultRowObj = mysqli_fetch_object($imageSQLResult)) {
            
            if (isset($imageSQLResultRowObj->AI_image_name)) {
                
                if (unlink($config['IMAGE_UPLOAD_PATH'] . '/album/' . $del_album_id . '/' . $imageSQLResultRowObj->AI_image_name)) {
                    
                    unlink($config['IMAGE_UPLOAD_PATH'] . '/album/' . $del_album_id . '/large/' . $imageSQLResultRowObj->AI_image_name);
                    unlink($config['IMAGE_UPLOAD_PATH'] . '/album/' . $del_album_id . '/mid/' . $imageSQLResultRowObj->AI_image_name);
                    unlink($config['IMAGE_UPLOAD_PATH'] . '/album/' . $del_album_id . '/small/' . $imageSQLResultRowObj->AI_image_name);
                    $imageDELSQL = "DELETE FROM `album_images` WHERE AI_album_id=" . intval($del_album_id);
                    $imageDELSQLResult = mysqli_query($con, $imageDELSQL);
                }
            }/* isset($imageSQLResultRowObj->AI_album_name */
        }/* $imageSQLResultRowObj = mysql_fetch_object($imageSQLResult) */
        if(file_exists($config['IMAGE_UPLOAD_PATH'] . '/album/' . $del_album_id.'/large')) {
        rmdir($config['IMAGE_UPLOAD_PATH'] . '/album/' . $del_album_id.'/large');
        }
        if(file_exists($config['IMAGE_UPLOAD_PATH'] . '/album/' . $del_album_id.'/mid')) {
        rmdir($config['IMAGE_UPLOAD_PATH'] . '/album/' . $del_album_id.'/mid');
        }
        if(file_exists($config['IMAGE_UPLOAD_PATH'] . '/album/' . $del_album_id.'/small')) {
        rmdir($config['IMAGE_UPLOAD_PATH'] . '/album/' . $del_album_id.'/small');
        }
        
    }/* $imageSQLResult */ else {
        echo 'image not select';
    }
    if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/album/' . $del_album_id) && rmdir($config['IMAGE_UPLOAD_PATH'] . '/album/' . $del_album_id)) {
        $albumDeleSQL = "DELETE FROM `album` WHERE album_id=" . intval($del_album_id);
        $albumDeleSQLResult = mysqli_query($con, $albumDeleSQL);
        $msg = "Album has been deleted successfully";
    }
}

/*Start Priority Reset code*/
if(isset($_POST["resetPriority"])) {
    if(resetPriority($tableNmae = 'album', $fieldName = 'album_priority')){
    $msg = "Priority Field Reset Successfully!!";        
    }
}
/*End Priority Reset code*/

$albumArray = array();
$albumSQL = "SELECT * FROM `album`";
$albumSQLResult = mysqli_query($con, $albumSQL);
if ($albumSQLResult) {
    while ($albumSQLResultRowObj = mysqli_fetch_object($albumSQLResult)) {
        $albumArray[] = $albumSQLResultRowObj;
    }
    mysqli_free_result($albumSQLResult);
} else {
    if (DEBUG) {
        echo "albumSQLResultRowObj error" . mysqli_error($con);
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Album list</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
       <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               
       
    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include 'album_left_navigation.php'; ?>

            <!-- Content Start -->
            <div class="content">




                <div class="title"><h5>Album Module </h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <form action="index.php" method="post">
                    <input type="submit" onclick="return confirm('Are You Sure Want TO Reset The Priority?');" name="resetPriority" value="Reset Priority"/>
                    </form>
                <!-- Charts -->
                <div class="table">
                    <div class="head"><h5 class="iFrames">Album List</h5></div>
                    <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                        <thead>
                            <tr>
                                <th>Album Title</th>
                                <th>Upload Image</th>
                                <th>Album Priority</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            update_option($option_name = 'ghhj', $option_value = 'fgfgdfgfgdf');
                            $albumArrayCounter = count($albumArray);
                            if ($albumArrayCounter > 0):
                                ?>
                                <?php for ($i = 0; $i < $albumArrayCounter; $i++): ?>
                                    <tr class="gradeA">
                                        <td><?php echo $albumArray[$i]->album_title; ?></td>
                                        <td><a href="image_upload.php?id=<?php echo base64_encode($albumArray[$i]->album_id); ?>">Images Upload</a></td>
                                        <td><?php echo $albumArray[$i]->album_priority; ?></td>
                                        <td class="center">
                                            
                                            <a title="edit" href="album_edit.php?id=<?php echo base64_encode($albumArray[$i]->album_id); ?>"><img src="<?php echo baseUrl('admin/images/pencil-grey-icon.png'); ?>" height="14" width="14" alt="Edit" /></a>&nbsp;
                                            <a title="delete" onclick="<?php if($albumArray[$i]->album_type == 'built-in') {?> alert('You Can Not Delete This Album !!'); return false; <?php } else {?> return confirm('Are You Sure Want To Delete?');<?php } ?>" href="index.php?del=yes&id=<?php echo base64_encode($albumArray[$i]->album_id); ?>"><img src="<?php echo baseUrl('admin/images/deleteFile.png'); ?>" height="14" width="14" alt="delete" /></a>
                                        </td>
                                    </tr>
                                <?php endfor; /* $i=0; i<$adminArrayCounter; $++  */ ?>
                            <?php endif; /* count($adminArray) > 0 */ ?>


                        </tbody>
                    </table>
                </div>

            </div>


            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>