<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

$album_id = 0;
if (isset($_REQUEST['id'])) {
    $edit_album_id = base64_decode($_REQUEST['id']);
} else {
    $link = baseUrl('admin/album/index.php?err=' . base64_encode('Id missing.'));
    redirect($link);
}

$author = $_SESSION['admin_id'];
$album_title = '';
$album_priority = '';
$album_description = '';

$albumSql = "SELECT * FROM `album` WHERE album_id=" . intval($edit_album_id);
$albumSqlResult = mysqli_query($con, $albumSql);
if ($albumSqlResult) {
   $albumSqlResultRowObj = mysqli_fetch_object($albumSqlResult);
    if (isset($albumSqlResultRowObj->album_id)) {

        $album_title = $albumSqlResultRowObj->album_title;
        $album_priority = $albumSqlResultRowObj->album_priority;
        $album_description = $albumSqlResultRowObj->album_description;
        

    }
} else {
    if (DEBUG) {
        echo "albumSqlResult error : " . mysqli_error($con);
    } else {
        $link = baseUrl('admin/album/index.php?err=' . base64_encode('Edit sql fail.'));
        redirect($link);
    }
}


if (isset($_POST['album_update']) AND $_POST['album_update'] == 'Update') {

    extract($_POST);
    //echo $width;
    if ($album_title == '') {
        $err = 'Album title field is required!!';
         } elseif ($album_title != '') {
        $albumCheckSql = "select album_title from `album` where album_title='" . mysqli_real_escape_string($con, $album_title) . "'AND album_id != ".intval($edit_album_id);
        $albumCheckSqlResult = mysqli_query($con, $albumCheckSql);
        if ($albumCheckSqlResult) {
            $albumCheckSqlResultRowObj = mysqli_fetch_object($albumCheckSqlResult);
            if (isset($albumCheckSqlResultRowObj->album_title) && $albumCheckSqlResultRowObj->album_title == $album_title) {
                $err = 'Album Title (<b>' . $album_title . '</b>) already exist in our databse ';
            }
            mysqli_free_result($albumCheckSqlResult);
        } else {
            if (DEBUG) {
                echo 'albumCheckSqlResult Error: ' . mysqli_error($con);
            }
            $err = "Query failed.";
        }
    } elseif ($album_priority == '') {
        $err = 'Album priority field is required!!';
    } elseif (!is_numeric($album_priority)) {
        $err = 'Album priority should be numeric!!';
    }


    if ($err == '') {


//        $max_album_id = getMaxValue('album', 'album_id');
//        $new_album_id = $max_album_id + 1;
//        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/' . $new_album_id)) {
//            mkdir($config['IMAGE_UPLOAD_PATH'] . '/' . $new_album_id, 0777, TRUE);
//        }

        $albumField = '';
        //$albumField .= ' album_id =' . intval($new_album_id);
        $albumField .= ' album_title ="' . mysqli_real_escape_string($con, $album_title) . '"';
        $albumField .= ', album_priority ="' . htmlentities(mysqli_real_escape_string($con, $album_priority)) . '"';
        $albumField .= ', album_description ="' . htmlentities(mysqli_real_escape_string($con, $album_description)) . '"';
        $albumField .= ', album_updated ="' . date("Y-m-d") . '"';
        $albumField .= ', album_updated_by ="' . $author . '"';

        $albumUpSql = "UPDATE `album` SET $albumField WHERE album_id=".intval($edit_album_id);
        $albumUpSqlResult = mysqli_query($con, $albumUpSql);
        if ($albumUpSqlResult) {
            $msg = "Album information update successfully";
        } else {
            if (DEBUG) {
                echo 'albumUpSqlResult Error: ' . mysqli_error($con);
            }
            $err = "Update Query failed.";
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Album Update</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
       <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               
       
    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include ('album_left_navigation.php'); ?>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Album Module</h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Update Album </h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="<?php echo baseUrl('admin/album/album_edit.php').'?id='. base64_encode($edit_album_id); ?>" method="post" enctype="multipart/form-data" class="mainForm">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">Album</h5></div>
                                        <div class="rowElem noborder"><label>Album Title (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="album_title" value="<?php echo $album_title; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>Album Priority (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="album_priority" value="<?php echo $album_priority; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label>Album Description:</label><div class="formRight"><textarea rows="8" cols="" class="auto" name="album_description"><?php echo $album_description; ?></textarea></div><div class="fix"></div></div>
                                        <input type="submit" name="album_update" value="Update" class="greyishBtn submitForm" />
                                        <div class="fix"></div>
                                    </div>
                                </fieldset>
                            </form>


                        </div>
                    </div>
                </div>

            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>
