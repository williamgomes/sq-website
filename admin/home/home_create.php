<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

$HOME_A_BOTTOM_IMAGE_WIDTH = get_option('HOME_A_BOTTOM_IMAGE_WIDTH');
$HOME_A_TOP_IMAGE_WIDTH = get_option('HOME_A_TOP_IMAGE_WIDTH');

$home_title = '';
$a_bottom_url = '';
$b_url = '';
$priority = '';
$status = '';

if (isset($_POST['home_create']) AND $_POST['home_create'] == 'Submit') {

    extract($_POST);
    $width_a_top = 0;
    $width_a_bottom = 0;
    $width_b = 0;
    if ($_FILES["a_top_image"]["error"] == 0) {
        list($width_a_top) = getimagesize($_FILES["a_top_image"]["tmp_name"]);
    }
    if ($_FILES["a_bottom_image"]["error"] == 0) {
        list($width_a_bottom) = getimagesize($_FILES["a_bottom_image"]["tmp_name"]);
    }
    if ($_FILES["b_image"]["error"] == 0) {
        list($width_b) = getimagesize($_FILES["b_image"]["tmp_name"]);
    }

    if ($home_title == '') {
        $err = "Home Title is required";
    } else if ($_FILES["a_top_image"]["error"] > 0) {
        $err = "Valid A Image is required";
    } else if ($width_a_top < $HOME_A_TOP_IMAGE_WIDTH) {
        $err = "A Top Image Width should be atleast <b>{$HOME_A_TOP_IMAGE_WIDTH}</b>";
    } else if ($width_a_top > $config['IMAGE_UPLOAD_MAX_WIDTH']) {
        $err = "A Top Image Width should not exceed <b>{$config['IMAGE_UPLOAD_MAX_WIDTH']}</b>";
    } else if ($_FILES["a_bottom_image"]["error"] > 0) {
        $err = "Valid A Image is required";
    } else if ($width_a_bottom < $HOME_A_BOTTOM_IMAGE_WIDTH) {
        $err = "A Bottom Image Width should be atleast <b>{$HOME_A_BOTTOM_IMAGE_WIDTH}</b>";
    } else if ($width_a_bottom > $config['IMAGE_UPLOAD_MAX_WIDTH']) {
        $err = "A Bottom Image Width should not exceed <b>{$config['IMAGE_UPLOAD_MAX_WIDTH']}</b>";
    } else if ($a_bottom_url == '') {
        $err = "A Bottom Url field is required";
    } /* 
     else if (!filter_var($a_bottom_url, FILTER_VALIDATE_URL)) {
        $err = 'Valid A Bottom Url field is required!!';
    }
     */ elseif ($_FILES["b_image"]["error"] > 0) {
        $err = "Valid B Image field is required";
    } else if ($width_b < $config['CONFIG_SETTINGS']['HOME_B_IMAGE_WIDTH']) {
        $err = "B Image Width should be atleast <b>{$config['CONFIG_SETTINGS']['HOME_B_IMAGE_WIDTH']}</b>";
    } else if ($width_b > $config['IMAGE_UPLOAD_MAX_WIDTH']) {
        $err = "B Image Width should not exceed <b>{$config['IMAGE_UPLOAD_MAX_WIDTH']}</b>";
    } /*
     else if (!filter_var($b_url, FILTER_VALIDATE_URL)) {
        $err = 'Valid B Url field is required!!';
    } 
     */ else if ($priority == '') {
        $err = "Priority Field field is required";
    } elseif (!is_numeric($priority)) {
        $err = 'Priority should be numeric!!';
    }

    if ($err == '') {
        $max_home_id = getMaxValue('home', 'home_id');
        $new_home_id = $max_home_id + 1;
        /* Srat: image upload */
        $a_top_image = basename($_FILES['a_top_image']['name']);
        $info = pathinfo($a_top_image, PATHINFO_EXTENSION); /* it will return me like jpeg, gif, pdf, png */
        $a_top_image_name = str_replace(' ', '_', $home_title) . '_a_top-' . $new_home_id . '.' . $info; /* create custom image name color id will add  */
        $a_top_image_source = $_FILES["a_top_image"]["tmp_name"];

        $a_bottom_image = basename($_FILES['a_bottom_image']['name']);
        $info = pathinfo($a_bottom_image, PATHINFO_EXTENSION); /* it will return me like jpeg, gif, pdf, png */
        $a_bottom_image_name = str_replace(' ', '_', $home_title) . '_a_bottom-' . $new_home_id . '.' . $info; /* create custom image name color id will add  */
        $a_bottom_image_source = $_FILES["a_bottom_image"]["tmp_name"];

        $b_image = basename($_FILES['b_image']['name']);
        $info = pathinfo($a_bottom_image, PATHINFO_EXTENSION); /* it will return me like jpeg, gif, pdf, png */
        $b_image_name = str_replace(' ', '_', $home_title) . '_b-' . $new_home_id . '.' . $info; /* create custom image name color id will add  */
        $b_image_source = $_FILES["b_image"]["tmp_name"];

        $a_top_image_target_path = $config['IMAGE_UPLOAD_PATH'] . '/home/a_top_image/' . $a_top_image_name;
        $a_bottom_image_target_path = $config['IMAGE_UPLOAD_PATH'] . '/home/a_bottom_image/' . $a_bottom_image_name;
        $b_image_target_path = $config['IMAGE_UPLOAD_PATH'] . '/home/b_image/' . $b_image_name;
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/home/a_top_image/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/home/a_top_image/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/home/a_top_image/large/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/home/a_top_image/large/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/home/a_bottom_image/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/home/a_bottom_image/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/home/a_bottom_image/large/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/home/a_bottom_image/large/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/home/b_image/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/home/b_image/', 0777, TRUE);
        }
        if (!is_dir($config['IMAGE_UPLOAD_PATH'] . '/home/b_image/large/')) {
            mkdir($config['IMAGE_UPLOAD_PATH'] . '/home/b_image/large/', 0777, TRUE);
        }


        if (!move_uploaded_file($a_top_image_source, $a_top_image_target_path) || !move_uploaded_file($a_bottom_image_source, $a_bottom_image_target_path) || !move_uploaded_file($b_image_source, $b_image_target_path)) {
            $a_top_image_name = '';
            $a_bottom_image_name = '';
            $b_image_name = '';
        }
        /* End: image upload */ else {
            require basePath('lib/Zebra_Image.php');
            $image = new Zebra_Image();
            /* Start code for size image For A Top Image */
            $a_top_image_source = $a_top_image_target_path;
            $a_top_image_target_path = $config['IMAGE_UPLOAD_PATH'] . '/home/a_top_image/large/' . $a_top_image_name;

            $image->target_path = $a_top_image_target_path;
            $image->source_path = $a_top_image_source;

            $image->preserve_aspect_ratio = true;
            $largeSize = ceil($HOME_A_TOP_IMAGE_WIDTH);
            if (!$image->resize($largeSize)) {

// if there was an error, let's see what the error is about
                $err = zebraImageErrorHandaling($image->error);

// if no errors
            }
            /* End code for size image For A Top Image */

            /* Start code for size image For A Bottom Image */
            $a_bottom_image_source = $a_bottom_image_target_path;
            $a_bottom_image_target_path = $config['IMAGE_UPLOAD_PATH'] . '/home/a_bottom_image/large/' . $a_bottom_image_name;

            $image->target_path = $a_bottom_image_target_path;
            $image->source_path = $a_bottom_image_source;

            $image->preserve_aspect_ratio = true;
            $largeSize = ceil($HOME_A_BOTTOM_IMAGE_WIDTH);
            if (!$image->resize($largeSize)) {

// if there was an error, let's see what the error is about
                $err = zebraImageErrorHandaling($image->error);

// if no errors
            }
            /* End code for size image For A Bottom Image */

            /* Start code for size image For B Image */
            /* Start Large size For B */
            $b_image_source = $b_image_target_path;
            $b_image_target_path = $config['IMAGE_UPLOAD_PATH'] . '/home/b_image/large/' . $b_image_name;

            $image->target_path = $b_image_target_path;
            $image->source_path = $b_image_source;

            $image->preserve_aspect_ratio = true;
            $largeSize = ceil($config['CONFIG_SETTINGS']['HOME_B_IMAGE_WIDTH']);
            if (!$image->resize($largeSize)) {

// if there was an error, let's see what the error is about
                $err = zebraImageErrorHandaling($image->error);

// if no errors
            }
            /* End Large size For B */

            /* End code for size image For B Image */

            $homeInfoFiled = '';
            $homeInfoFiled .=' home_title = "' . mysqli_real_escape_string($con, $home_title) . '"';
            $homeInfoFiled .=', home_a_top_image = "' . mysqli_real_escape_string($con, $a_top_image_name) . '"';
            $homeInfoFiled .=', home_a_bottom_image = "' . mysqli_real_escape_string($con, $a_bottom_image_name) . '"';
            $homeInfoFiled .=', home_a_bottom_url = "' . mysqli_real_escape_string($con, $a_bottom_url) . '"';
            $homeInfoFiled .=', home_b_image = "' . mysqli_real_escape_string($con, $b_image_name) . '"';
            $homeInfoFiled .=', home_b_url = "' . mysqli_real_escape_string($con, $b_url) . '"';
            $homeInfoFiled .=', home_priority = "' . mysqli_real_escape_string($con, $priority) . '"';
            $homeInfoFiled .=', home_status = "' . mysqli_real_escape_string($con, $status) . '"';
            $homeInfoInsSql = "INSERT INTO home SET $homeInfoFiled";
            $homeInfoInsResult = mysqli_query($con, $homeInfoInsSql);
            if ($homeInfoInsResult) {
                // $msg = "Circular Information insert successfully";
                $link = 'index.php?msg=' . base64_encode('Home Information Successfully added');
                redirect($link);
            } else {
                if (DEBUG) {
                    echo 'homeInfoInsSqlResult Error: ' . mysqli_error($con);
                }
                $err = "Insert Query failed.";
            }
        }
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Home Create</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--        Start lightBox -->
        <script src="<?php echo baseUrl('js/lightbox/jquery-1.7.2.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery-ui-1.8.18.custom.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery.smooth-scroll.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/lightbox.js'); ?>"></script>
        <link rel="stylesheet" href="<?php echo baseUrl('css/lightbox/lightbox.css'); ?>" type="text/css" media="screen" />
        <!--        End lightBox -->  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               

    </head>

    <body>
        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include ('home_left_navigation.php'); ?>
           
            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Home Module</h5></div>
                 
                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <b><a href="<?php echo baseUrl('admin/screen_shot_image/home_icon.jpg') ?>"  rel="lightbox[plants]"title="User Guide For Home"><button>User Guide For Home Module</button></a> </b>
                <div class="widget first">                    
                    <div class="head">
                        <h5 class="iGraph">Create Home</h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="<?php echo baseUrl('admin/home/home_create.php'); ?>" method="post" enctype="multipart/form-data" class="mainForm">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">Home</h5></div>
                                        <div class="rowElem noborder"><label> Home Title (<span class="requiredSpan">*</span>) :</label><div class="formRight"><input type="text" name="home_title" value="<?php echo $home_title; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> A Top Image  (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="file" name="a_top_image"  /> Minimum WIDTH : <?php echo $HOME_A_TOP_IMAGE_WIDTH; ?>&nbsp; maximum WIDTH:<?php echo $config['IMAGE_UPLOAD_MAX_WIDTH']; ?></div><div class="fix"></div></div>                                        
                                        <div class="rowElem noborder"><label> A Bottom Image (<span class="requiredSpan">*</span>) :</label><div class="formRight"><input type="file" name="a_bottom_image"  /> Minimum WIDTH : <?php echo $HOME_A_BOTTOM_IMAGE_WIDTH; ?>&nbsp; maximum WIDTH:<?php echo $config['IMAGE_UPLOAD_MAX_WIDTH']; ?></div><div class="fix"></div></div>                                        
                                        <div class="rowElem noborder"><label> A Bottom Url  (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="a_bottom_url" value="<?php echo $a_bottom_url; ?>"  /></div><div class="fix"></div></div>                                        
                                        <div class="rowElem noborder"><label> B Image (<span class="requiredSpan">*</span>) :</label><div class="formRight"><input type="file" name="b_image"  />&nbsp; MINIMUM WIDTH : <?php echo $config['CONFIG_SETTINGS']['HOME_B_IMAGE_WIDTH']; ?></div><div class="fix"></div></div>                                        
                                        <div class="rowElem noborder"><label> B Url  (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="b_url" value="<?php echo $b_url; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> Priority  (<span class="requiredSpan">*</span>):</label><div class="formRight"><input type="text" name="priority" value="<?php echo $priority; ?>"  /></div><div class="fix"></div></div>
                                        <div class="rowElem noborder"><label> Status :</label><div class="formRight"><select name="status" ><option value="active" <?php if ($status == 'active') echo'selected'; ?>>Active</option><option value="inactive" <?php if ($status == 'inactive') echo'selected'; ?>>Inactive</option></select></div><div class="fix"></div></div>
                                        <input type="submit" name="home_create" value="Submit" class="greyishBtn submitForm" />
                                        <div class="fix"></div>
                                    </div>
                                </fieldset>
                            </form>


                        </div>
                    </div>
                </div>

            </div>
            <!-- Content End -->

            <div class="fix"></div>
        </div>
        <?php include basePath('admin/footer.php'); ?>
