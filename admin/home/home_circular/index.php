<?php
include ('../../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

$showHomeCount = 0;
$circular_title = '';
$circularSql = "SELECT circular_id FROM circular WHERE circular_application_deadline>=DATE(NOW())";
$circularSqlResult = mysqli_query($con, $circularSql);
if ($circularSqlResult) {
    while ($circularSqlResultRowObj = mysqli_fetch_object($circularSqlResult)) {
        $circularArray[] = $circularSqlResultRowObj;
    }
    mysqli_free_result($circularSqlResult);
} else {
    if (DEBUG) {
        echo 'circularSqlResult Error : ' . mysqli_error($con);
    }
}
if (isset($_POST["submit"])) {

    foreach ($circularArray as $circular) {
        $circularoutput[] = $circular->circular_id;
    }
    $allCircular = implode(',', $circularoutput);
    $showHome = '';
    if (isset($_POST["showhome"])) {
        $showHome = implode(',', $_POST["showhome"]);
        $showHome = trim($showHome, ',');
        $showHomeCount = count(explode(',', $showHome));
    }

    //$showHomeSql = "UPDATE circular SET circular_home_show_status='yes' WHERE circular_id IN ($showHome)";
    $field = "";
    if ($showHomeCount != 0) {
        $field .="circular_home_show_status = CASE WHEN circular_id IN ($showHome) THEN 'yes' ";
        $field .="WHEN circular_id NOT IN ($showHome) THEN 'no' ";
    } else {
        $field .="circular_home_show_status = CASE WHEN circular_id IN ($allCircular) THEN 'no' ";
    }
    $field .= "END";


    $showHomeSql = "UPDATE `circular` SET $field
WHERE `circular_id` IN ($allCircular);";
    $showHomeResult = mysqli_query($con, $showHomeSql);
    if ($showHomeResult) {
        $msg = "Update Successfully!!!";
    } else {
        if (DEBUG) {
            $err = 'showHomeResultError' . mysqli_error($con);
        } else {
            $err = "Update Query Failed";
        }
    }
}
$circularArray = array();
$circularSql = "SELECT * FROM circular WHERE circular_application_deadline>=DATE(NOW())";
$circularSqlResult = mysqli_query($con, $circularSql);
if ($circularSqlResult) {
    while ($circularSqlResultRowObj = mysqli_fetch_object($circularSqlResult)) {
        $circularArray[] = $circularSqlResultRowObj;
    }
    mysqli_free_result($circularSqlResult);
} else {
    if (DEBUG) {
        echo 'circularSqlResult Error : ' . mysqli_error($con);
    }
}

if (isset($_POST["circular_title_create"])) {
    $homeCircularTitle = $_POST["homeCircularTitle"];
    if ($homeCircularTitle == '') {
        $err = " Circular Title Field Is Required";
    }
    if ($err == '') {
        $homeCircularTitle = htmlentities(mysqli_real_escape_string($con, $homeCircularTitle));
        $homeCircularTitleSql = "UPDATE config_settings SET CS_value='$homeCircularTitle' ,CS_updated_by='$_SESSION[admin_id]' WHERE CS_option='HOME_CAREER_TITLE'";
        $homeCircularTitleResult = mysqli_query($con, $homeCircularTitleSql);
        if ($homeCircularTitleResult) {
            $msg = "Update Successfully!!";
        } else {
            if (DEBUG) {
                $err = "homeCircularTitleResult Error" . mysqli_error($con);
            } else {
                $err = "update query failed";
            }
        }
    }
}
$homeCircularTitleShowSql = "SELECT CS_value FROM config_settings WHERE CS_option='HOME_CAREER_TITLE'";
$homeCircularTitleShowResult = mysqli_query($con, $homeCircularTitleShowSql);
if ($homeCircularTitleShowResult) {
    $homeCircularTitleShowResultRowObj = mysqli_fetch_object($homeCircularTitleShowResult);
    $homeCircularTitle = $homeCircularTitleShowResultRowObj->CS_value;
} else {
    if (DEBUG) {
        $err = "homeCircularTitleShowResult error" . mysqli_error($con);
    } else {
        $err = "Query Failed";
    }
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Home : Circular List</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->               

    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include '../home_left_navigation.php'; ?>

            <!-- Content Start -->
            <div class="content">




                <div class="title"><h5>Home Circular Module </h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <div class="body">
                    <div class="charts" style="width: 700px; height: auto;">
                        <form action="index.php" method="post" class="mainForm">

                            <!-- Input text fields -->
                            <fieldset>
                                <div class="widget first">
                                    <div class="head"><h5 class="iList">Option Insertion</h5></div>
                                    <div class="head"><h5 class="iPencil">Circular Title :</h5></div>  
                                    <div><textarea class="tm" rows="5" cols="" name="homeCircularTitle"><?php echo html_entity_decode($homeCircularTitle); ?></textarea></div>
                                    <input type="submit" name="circular_title_create" value="Update" class="greyishBtn submitForm" />
                                    <div class="fix"></div>
                                </div>
                            </fieldset>
                        </form>		
                    </div>
                </div>
                <form id ="frm1" action="index.php" method="post"> 
                    <div class="table">
                        <div class="head"><h5 class="iFrames">Circular List</h5></div>
                        <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                            <thead>
                                <tr>
                                    <th>Active</th>
                                    <th>Post Name</th>
                                    <th>Application Dead Line</th>

                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $circularArrayCounter = count($circularArray);
                                if ($circularArrayCounter > 0):
                                    for ($i = 0; $i < $circularArrayCounter; $i++):
                                        ?><tr class="gradeA"> 

                                            <td><input type ='checkbox' name='showhome[]' <?php if ($circularArray[$i]->circular_home_show_status == 'yes') echo 'checked'; ?> value="<?php echo $circularArray[$i]->circular_id; ?>"/> </td>
                                            <td><?php echo $circularArray[$i]->circular_position; ?></td>
                                            <td><?php echo $circularArray[$i]->circular_application_deadline; ?></td>

                                        </tr>
                                        <?php
                                    endfor;
                                endif;
                                ?>

                            </tbody>
                        </table>
                        <input type='submit' name='submit' value='submit' class="greyishBtn submitForm">
                            </form>
                    </div>

            </div>


            <!-- Content End -->

            <div class="fix"></div>
        </div>

        <?php include basePath('admin/footer.php'); ?>
