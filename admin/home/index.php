<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

//delete query here
if (isset($_REQUEST['del']) && isset($_REQUEST['id'])) {
    $del_home_id = base64_decode($_REQUEST['id']);
    $imageSql = "SELECT * FROM home WHERE home_id = " . intval($del_home_id);
    $imageResult = mysqli_query($con, $imageSql);
    $imageResultRowObj = mysqli_fetch_object($imageResult);
    $homeDeleleteSql = "delete from home where home_id=" . intval($del_home_id);
    $homeDeleleteSqlResult = mysqli_query($con, $homeDeleleteSql);
    if ($homeDeleleteSqlResult) {
        $msg = "Home Acount Successfully Deleted";
        if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/home/a_top_image/' . $imageResultRowObj->home_a_top_image)) {
            unlink($config['IMAGE_UPLOAD_PATH'] . '/home/a_top_image/' . $imageResultRowObj->home_a_top_image);
        }
        if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/home/a_top_image/large/' . $imageResultRowObj->home_a_top_image)) {
            unlink($config['IMAGE_UPLOAD_PATH'] . '/home/a_top_image/large/' . $imageResultRowObj->home_a_top_image);
        }
        if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/home/a_bottom_image/' . $imageResultRowObj->home_a_bottom_image)) {
            unlink($config['IMAGE_UPLOAD_PATH'] . '/home/a_bottom_image/' . $imageResultRowObj->home_a_bottom_image);
        }
        if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/home/a_bottom_image/large/' . $imageResultRowObj->home_a_bottom_image)) {
            unlink($config['IMAGE_UPLOAD_PATH'] . '/home/a_bottom_image/large/' . $imageResultRowObj->home_a_bottom_image);
        }
        if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/home/b_image/' . $imageResultRowObj->home_b_image)) {
            unlink($config['IMAGE_UPLOAD_PATH'] . '/home/b_image/' . $imageResultRowObj->home_b_image);
        }
        if (file_exists($config['IMAGE_UPLOAD_PATH'] . '/home/b_image/large/' . $imageResultRowObj->home_b_image)) {
            unlink($config['IMAGE_UPLOAD_PATH'] . '/home/b_image/large/' . $imageResultRowObj->home_b_image);
        }
    } else {
        if (DEBUG) {
            $err = "homeDeleleteSqlResult ERROR : " . mysqli_error($con);
        } else {
            $err = "Home Information Not Deleted";
        }
    }
}
/*Start Priority Reset code*/
if(isset($_POST["resetPriority"])) {
    if(resetPriority($tableNmae = 'home', $fieldName = 'home_priority')){
    $msg = "Priority Field Reset Successfully!!";        
    }
}
/*End Priority Reset code*/
$homeArray = array();
$homeSql = "SELECT * FROM home";
$homeSqlResult = mysqli_query($con, $homeSql);
if ($homeSqlResult) {
    while ($homeSqlResultRowObj = mysqli_fetch_object($homeSqlResult)) {
        $homeArray[] = $homeSqlResultRowObj;
    }
    mysqli_free_result($homeSqlResult);
} else {
    if (DEBUG) {
        echo 'homeSqlResult Error : ' . mysqli_error($con);
    }
}

?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
        <title>Admin Panel | Home</title>   
        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" /> 
        <script src="<?php echo baseUrl('admin/js/jquery.min.js'); ?>" type="text/javascript"></script>  
        <!--        Start lightBox -->
        <script src="<?php echo baseUrl('js/lightbox/jquery-1.7.2.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery-ui-1.8.18.custom.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/jquery.smooth-scroll.min.js'); ?>"></script>
        <script src="<?php echo baseUrl('js/lightbox/lightbox.js'); ?>"></script>
        <link rel="stylesheet" href="<?php echo baseUrl('css/lightbox/lightbox.css'); ?>" type="text/css" media="screen" />
        <!--        End lightBox -->  
        <!--tree view -->  
        <script src="<?php echo baseUrl('admin/js/treeViewJquery.min.js'); ?>"></script> 
        <script src ="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type = "text / javascript" ></script>   
        <!--tree view --> 
        <!--Start admin panel js/css --> 
        <?php include basePath('admin/header.php'); ?>   
        <!--End admin panel js/css -->             
    </head>

    <body>

        <?php include basePath('admin/top_navigation.php'); ?>

        <?php include basePath('admin/module_link.php'); ?>



        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <?php include 'home_left_navigation.php'; ?>

            <!-- Content Start -->
            <div class="content">




                <div class="title"><h5>Home Module </h5></div>

                <!-- Notification messages -->
                <?php include basePath('admin/message.php'); ?>
                <!-- Charts -->
                <form action="index.php" method="post">
                    <input type="submit" onclick="return confirm('Are You Sure Want TO Reset The Priority?');" name="resetPriority" value="Reset Priority"/>
                    </form>
                <div class="table">
                    <div class="head"><h5 class="iFrames">Home List</h5></div>
                    <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                        <thead>
                            <tr>
                                <th>A Title</th>
                                <th>A Top Image</th>
                                <th>A Bottom Image</th>
                                <th>B Image</th>
                                <th>Priority</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $homeArrayCounter = count($homeArray);
                            if ($homeArrayCounter > 0):
                                ?>
                                <?php for ($i = 0; $i < $homeArrayCounter; $i++): ?>
                                    <tr class="gradeA">
                                        <td><?php echo $homeArray[$i]->home_title; ?></td>
                                        <td align="center"><a href="<?php echo $config['IMAGE_UPLOAD_URL'] . '/home/a_top_image/large/' . $homeArray[$i]->home_a_top_image; ?>" rel="lightbox[plants]" title="A Top Image For <?php echo $homeArray[$i]->home_title;?>"><img src="<?php echo baseUrl('admin/images/icons/custom/viewer_image.png'); ?>" height="14" width="14" alt="image" /></a></td>
                                        <td align="center"><a href="<?php echo $config['IMAGE_UPLOAD_URL'] . '/home/a_bottom_image/large/' . $homeArray[$i]->home_a_bottom_image; ?>" rel="lightbox[plants]" title="A Bottom Image For <?php echo $homeArray[$i]->home_title;?>"><img src="<?php echo baseUrl('admin/images/icons/custom/viewer_image.png'); ?>" height="14" width="14" alt="image" /></a></td>
                                        <td align="center"><a href="<?php echo $config['IMAGE_UPLOAD_URL'] . '/home/b_image/large/' . $homeArray[$i]->home_b_image; ?>" rel="lightbox[plants]" title="B Image For <?php echo $homeArray[$i]->home_title;?>"><img src="<?php echo baseUrl('admin/images/icons/custom/viewer_image.png'); ?>" height="14" width="14" alt="image" /></a></td>
                                        <td><?php echo $homeArray[$i]->home_priority; ?></td>
                                        <td><?php echo $homeArray[$i]->home_status; ?></td>
                                        <td class="center">

                                            <a href="home_edit.php?id=<?php echo base64_encode($homeArray[$i]->home_id); ?>"><img src="<?php echo baseUrl('admin/images/pencil-grey-icon.png'); ?>" height="14" width="14" alt="Edit" /></a>&nbsp;
                                            <a onclick="javascript:if (!confirm('Are you sure ?'))
                                                                            return false" href="index.php?del=yes&id=<?php echo base64_encode($homeArray[$i]->home_id); ?>"><img src="<?php echo baseUrl('admin/images/deleteFile.png'); ?>" height="14" width="14" alt="delete" /></a>
                                        </td>
                                    </tr>
                                <?php endfor; /* $i=0; i<$adminArrayCounter; $++  */ ?>
                            <?php endif; /* count($adminArray) > 0 */ ?>


                        </tbody>
                    </table>
                </div>

            </div>


            <!-- Content End -->

            <div class="fix"></div>
        </div>
        <?php include basePath('admin/footer.php'); ?>
