
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<link rel="stylesheet"  href="<?php echo baseUrl(); ?>css/style.css" type="text/css">
<link rel="shortcut icon" href="<?php echo baseUrl(); ?>favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-152x152.png" />
<!-- Custom scrollbars CSS -->
<link href="<?php echo baseUrl(); ?>css/jquery.mCustomScrollbar.css" rel="stylesheet" />


<!--[if IE 6]>
	<link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	
<!--[if IE 7]>
	<link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	
<!--[if IE 8]>
	<link href="<?php echo baseUrl(); ?>css/ie" rel="stylesheet" type="text/css" />
	<![endif]-->
	
<!--[if IE 9]>
	<link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->

<!--[if IE 10]>
	<link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->

<!--[if IE 8]>
	<link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script src="<?php echo baseUrl(); ?>js/jquery.min.js"></script>


<link rel="stylesheet" href="<?php echo baseUrl(); ?>css/responsivemobilemenu.css" type="text/css"/>
<script type="text/javascript" src="js/responsivemobilemenu.js"></script>


<script type="text/javascript">

window.onorientationchange = function()
{
   window.location.reload();
}


</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43689390-1', 'sqgc.com');
  ga('send', 'pageview');

</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-43689390-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


