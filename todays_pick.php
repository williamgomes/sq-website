<?php
include './config/config.php';
$rss = array();
$sql = "SELECT * FROM rss WHERE rss_status='active'";
$sqlResult = mysqli_query($con, $sql);
if ($sqlResult) {
    while ($sqlResultRowObj = mysqli_fetch_object($sqlResult)) {
        $rss[] = $sqlResultRowObj;
    }
} else {
    if (DEBUG) {
        echo '$sqlResult Error' . mysqli_error($con);
    } else {
        echo 'sqlResult Fail';
    }
}

//printDie($rss);
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet"  href="css/style.css" type="text/css">
                <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
                <link rel="apple-touch-icon" href="apple-touch-icon.png" />
                <link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png" />
                <link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png" />
                <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png" />
                <link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png" />
                <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png" />
                <link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png" />
                <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png" />
                <link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png" />
                <!-- Custom scrollbars CSS -->
                <link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" />

                <title>SQ GROUP | Todays Pick</title>

                <!--[if IE 6]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 7]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 8]>
                        <link href="css/ie" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 9]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 10]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 8]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                <![endif]-->

                <script src="js/jquery.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(sizeContent);
                    //Every resize of window
                    $(window).resize(sizeContent);
                    //Dynamically assign height
                    function sizeContent() {
                        var newWidth = $("html").width() - $("#leftPanel").width() + "px";
                        $("#rightPanel").css("width", newWidth);
                    }
                </script>

                <script type="text/javascript">
                    if ($(window).width() < 699) {
                        //small screen, load other JS files
                        $.getScript('js/device.js', function() {
                            //the script has been added to the DOM, you can now use it's code
                        });
                    }

                    window.onorientationchange = function()
                    {
                        window.location.reload();
                    }


                    window.onorientationchange = function()
                    {
                        window.location.reload();
                    }


                </script>

                <link rel="stylesheet"  href="css/bulletin.css" type="text/css">
                    <link rel="stylesheet" href="css/responsivemobilemenu.css" type="text/css"/>
                    <script type="text/javascript" src="js/responsivemobilemenu.js"></script>

                    </head>
                    <body class="bulletinBody">
                        <div id="topBarContainer"> </div>
                        <div class="mobilemenu" style="display:none">
                            <div class="rmm">
                                <ul>
                                    <li><a href="bulletin.html">All News</a></li>
                                    <li><a href="all_about_sq.html" >All About Sq</a></li>
                                    <li><a class="active" href="todays_pick.php" >Today's Picks</a></li>

                                    <li><a  href="special_feature.html">Special Features</a></li>
                                </ul>
                            </div>
                        </div>
                        <div id="wrapper">
                            <div id="leftPanel">
                                <div class="shadowIe"></div>
                                <div class="logo">
                                    <p align="center"><a href="index.html"> <img width="100" src="images/logo.png" /></a><br />
                                        <span class="sus"><a class="sasLogoLink" href="bulletin.html"> Bulletin </a></span> </p>
                                </div>

                                <div class="leftMenu">
                                    <ul>
                                        <li><a href="bulletin.html">All News</a></li>
                                        <li><a href="all_about_sq.html" >All About Sq</a></li>
                                        <li><a   class="active"href="todays_pick.php" >Today's Picks</a></li>

                                        <li><a  href="special_feature.php">Special Features</a></li>
                                    </ul>
                                </div>

                                <div class="hrcenter"></div>
                                <div class="slogan"><p align="center">
                                        <img width="140" src="images/slogan.png" />
                                    </p>


                                </div>

                                <div class="hrcenter"></div>

                            </div>
                            <div id="rightPanel" >

                                <div id="container">


                                    <div id="bulletinwrapper">
                                        <div class="bulletinInner">

                                            <div class="bulletinColumn bulletinColumnPost">
                                                <?php $rssCounter = count($rss); ?> 
                                                <?php if ($rssCounter > 0): ?>

                                                    <?php for ($i = 0; $i < $rssCounter; $i++): ?>  

                                                        <div class="newsBox">



                                                            <a style=" text-decoration:none"  class="bulletinTitle" href="<?php echo $rss[$i]->rss_item_link; ?>" <?php
                                                            if ($rss[$i]->rss_item_link_type == 'external') {
                                                                echo 'target="_balnk"';
                                                            }
                                                            ?>  ><?php echo $rss[$i]->rss_item_title; ?></a>
                                                            <p>
                                                                <img src="images/fe-logo.png" /><br /> <br />

                                                                <?php echo $rss[$i]->rss_item_description; ?></p>


                                                        </div><!--newsBox-->



                                                    <?php endfor; /* ($i=0; $i < $rssCounter; $i++): */ ?>  


                                                <?php endif; /* ($rssCounter) */ ?>

                                                <div class="newsBox">



                                                    <a style=" text-decoration:none" class="bulletinTitle">Stoll products used by the Royal Family</a>
                                                    <p><img src="images/today-pick/1374609639_kate-middleton-prince-william-baby-zoom.jpg" width="300" /> <img width="300" src="images/today-pick/photo_1374729793485-1-HD.jpg"><br> <br> </p>
                                                                    <p>Flat knitting machine manufacturer Stoll has revealed that the white knitted blanket baby Prince George Alexander Louis of Cambridge was wrapped in when he was presented to the press and public for the first time outside St Mary's Hospital, London, was manufactured on an original Stoll flat knitting machine by Nottinghamshire knitwear manufacturer.</p>
                                                                    <p>SQ Group has also invested in this fully automated German Stoll machines.</p>

                                                                    <p><a target="_blank" href="http://www.knittingtradejournal.com">Extract taken from</a></p>


                                                                    </div>

                                                                    <div class="newsBox">



                                                                        <a style=" text-decoration:none" class="bulletinTitle">Wal-Mart announces a $50m loan to Bangladeshi factory owners</a>

                                                                        <p>
                                                                            Wal-Mart Stores Inc. Thursday announced that it would provide up to $50 million loan to Bangladeshi factory owners, who would be joining global retailers in the drive for improving safety standards in the country's disaster-prone garment industry.  The Wal-Mart loan will carry low interest rate and will be disbursed following the Bangladesh Bank's approval. According to the foreign exchange policy, the central bank needs to approve disbursement of any foreign currency loan. Details about lending rates also need to be finalised.  The loan will be dispatched from a fund of more than $100 million, committed in July by a group of North American companies.  The group of retailers, called the Alliance for Bangladesh Worker Safety (ABWS), pledged to set safety standards by October and refused to buy from factories deemed unsafe.   Gap Inc, JC Penney Co and Sears Holdings Corp Li & Fung Ltd, the global outsourcer that supplies to Wal-Mart, are also among the 17 signatories of ABWS.  ABWS is separated from a European group, formed in May combining nearly 80 retailers and trade union groups, to be known as the Accord on Fire and Building Safety in Bangladesh (AFBSB).  AFBSB was signed, among others, by leading European retailers like Hennes & Mauritz AB (HMB) and Inditex SA, two largest clothing retailers of the continent, who pledged at least $60 million over five years to monitor safety in Bangladesh plants.  
                                                                        </p>
                                                                        <p>
                                                                            The company has become more vocal about its allies' activities in Bangladesh over the past several months, including posting a list of banned factories online in May. It banned more than 250 local factories for not having adequate safety compliance.  Wal-Mart is considering several options, like paying factories for orders more promptly; paying for orders even before products are delivered, either through loan or payment; or having a bank issue loan with the retailer standing behind it as a credit guarantor in order to keep the interest rate lower, Jorgensen said.  Wal-Mart also started thorough inspections of the roughly 280 Bangladeshi factories it gets goods from.   Gap said in October it would provide loan up to $20 million to vendors for safety improvements

                                                                        </p>

                                                                        <p><a target="_blank" href="http://www.thefinancialexpress-bd.com">Extract taken from</a></p>


                                                                    </div>


                                                                    <div class="newsBox">



                                                                        <a style=" text-decoration:none" class="bulletinTitle">GSP comeback seems unlikely</a>

                                                                        <p>
                                                                            Bangladesh may miss the opportunity to regain a US trade benefit through a review in December as the country is going slow on an action plan prescribed by the Obama administration. “I can’t exactly say how much of the work has been done so far to get the GSP back in December. I may say this in November,” Labour and Employment Secretary Mikail Shipar said yesterday. The US suspended the GSP (generalised system of preferences) for Bangladesh on June 27 citing poor labour rights and working conditions in its factories. He said his ministry is going to appoint 23 inspectors soon and the appointment of 37 more are under process. Ensuring worker safety, their full freedom to join unions, and labour and EPZ laws reforms, a database of factories, and withdrawal of a few cases against workers are among the major things Bangladesh has to get done to get the GSP status back. The government has already reformed the labour law, withdrawn the cases against labour leaders Kalpona Akter and Babul Akter and published advertisements in newspapers seeking information on the killers of labour leader Aminul Islam, Shipar said. The government also restored the licences of two NGOs — Bangladesh Centre for Worker Solidarity, and Social Activities for the Environment, in line with a condition in the plan. The secretary also said he will sit with the two inspection committees of the US and EU retailers on September 7 to prepare a common checklist for factory inspection. The committees will inspect 1,750 factories across the country, but are yet to disclose the names of the plants.
                                                                        </p>


                                                                        <p><a target="_blank" href="http://www.thedailystar.net">Extract taken from</a></p>


                                                                    </div>

                                                                    <div class="newsBox">



                                                                        <a style=" text-decoration:none" class="bulletinTitle">Government and BGMEA are disappointed to be left out of factory inspection team</a>

                                                                        <p>
                                                                            The government and garment exporters have expressed disappointment as IndustriALL — a global trade union — has excluded them from a core committee that will inspect Bangladeshi factories under an accord signed by 85 global retailers and brands. The Bangladesh sides were barred from the panel as IndustriALL fears they could influence the inspection, an official of the trade union said. Also, the 85 retailers and brands will pay $12.5 million each in the next five years for the inspection and as compensation to workers, but the Bangladesh government and the garment manufacturers will have no such contribution, the official said.
                                                                            Now the government and Bangladesh Garment Manufacturers and Exporters Association seek membership of the steering committee of the building and fire safety accord. However, IndustriALL, the initiator of the agreement, wants to keep them in the advisory council. In the six-member steering committee, Monica Campbell from IndustriALL, John Hoffman from UNI Global Union, and Roy Ramesh Chandra, general secretary of IndustriALL Bangladesh Council, have been named under the unions category. The rest three representatives were selected from the international brands and retailers — Inditex, PVH and C&A, Roy Ramesh Chandra said.

                                                                        </p>


                                                                        <p><a target="_blank" href="http://www.thedailystar.net">Extract taken from</a></p>


                                                                    </div>

                                                                    <div class="newsBox">



                                                                        <a style=" text-decoration:none" class="bulletinTitle">Firms barred from giving out dividends through asset revaluation</a>

                                                                        <p>
                                                                            Bangladesh Securities and Exchange Commission (BSEC) has barred listed companies from issuing dividends through asset revaluation, in a bid to instil greater financial discipline into the markets. The move comes as a host of companies in recent times have handed out dividends by showing a surplus in their balance sheets through asset revaluation. The stockmarket regulator put in place a tighter asset revaluation guideline on August 18. Now, a listed company will have to present a good reason for embarking on the revaluation process and the asset class, too, has to be stated. The new guideline bars upward revaluation of leased property, second-hand and lower economic life machinery, tin-roofed buildings, vehicles, furniture and loose tools. It also stipulates that the time difference for valuation of the same class of asset cannot be less than three years and no upward revaluation of any asset can be done within two years of acquisition. The valuation report should include the financial statement of the company and the auditor must verify that all relevant laws and accounting standards were followed during preparation of the statement. The auditor also has to certify that the provisioning of tax and other liabilities have been considered during the valuation, it said.

                                                                        </p>


                                                                        <p><a target="_blank" href="http://www.thedailystar.net">Extract taken from</a></p>


                                                                    </div>
                                                                    <div class="newsBox">



                                                                        <a style=" text-decoration:none" class="bulletinTitle">Transfer pricing law to be enforced from next June</a>

                                                                        <p>
                                                                            The income tax wing is set to enforce the transfer pricing law from June next in a bid to check tax evasion through capital flight by both local and multinational companies.  To materialise the plan, the wing recently formed a 'Tax Policy Pool' comprising 50 tax officials who will help the government to implement the law successfully from the next fiscal year.  The National Board of Revenue has arranged training programmes for the selected taxmen with the technical support of the International Monetary Fund (IMF).  The training programme will be run phase by phase to develop skill of the taxmen to find out transfer pricing by the MNCs and local companies.   Recently, an IMF consultant trained the taxmen on the TP law and international best practices. There will be trainings in November and January next.  The TP law has been passed by Parliament in the last fiscal year. A high-powered team of the NBR, led by its member Md Alauddin, prepared the law to check capital flight from the country.  The law is likely to be enforced from next year on a pilot basis, said Md Alauddin.   Taxmen have felt the need of training for building capacities and skills of the field-level tax officials to implement the law, he added.  "The tax policy pool will prepare policy guidelines to enforce the TP law," he said.  NBR member Syed Aminul Karim, who heads the tax policy pool, said the trained officials will work on Large Taxpayers Unit (LTU) to deal with the tax files of large companies.   "Our aim is to develop skill of the taxmen in line with the international best practices," said. Domestic and international transfer pricing, amortisation, fraud analysis etc will be covered in the training for TP, he added.  Both senior and mid-level officials who are members of the team will receive training, he said.
                                                                        </p>


                                                                        <p><a target="_blank" href="http://www.thefinancialexpress-bd.com">Extract taken from</a></p>


                                                                    </div>

                                                                    <div class="newsBox">



                                                                        <a style=" text-decoration:none" class="bulletinTitle">Yunus leads global effort for garment labour wages</a>

                                                                        <p>
                                                                            Nobel laureate Muhammad Yunus has proposed an international minimum wage for garment workers to be worked out for each country separately, as part of a global effort to solve labour issues in all garment producing nations. The wage, he said, should be accepted as a compliance issue, and should not be a subject of any price negotiation. International buyers will accept it as part of production cost and make sure price negotiation should not impact on this wage in any country, Yunus said. Prof Yunus spoke at a preparatory conference at the Humboldt-Viadrina School of Governance in Berlin on Thursday to address the issues raised in Bangladesh after the Rana Plaza tragedy. The Humboldt-Viadrina School of Governance also organised a global conference to launch Garment Industry Transparency Initiative (GITI) at Yunus’s suggestion. The approach is set out as a joint approach of governments, factory owners, labour, the retailing companies, and the civil society in both producing and consuming countries. GITI does not intend to compete with other efforts, but rather support these and complement their actions. Yunus Centre in Dhaka and the Humboldt-Viadrina School of Governance are jointly working on the initiative. Yunus also explained his idea of creating social businesses to offer “Happy Workers Tags” at an additional cost of minimum 10 percent on top of the production cost. The money will be used by social businesses to deliver important social and health services for a specific period, to the specific group of workers involved in producing the tagged garments. A six-member steering committee co-chaired by Yunus and Eigen was also created at the conference. A formal launch has been set for October 14 in Berlin.
                                                                        </p>


                                                                        <p><a target="_blank" href="http://www.thedailystar.net">Extract taken from</a></p>


                                                                    </div>




                                                                    </div><!--bulletinColumn-->
                                                                    <div class="bulletinColumn noMarginRt">


                                                                        <!--newsBox-->


                                                                    </div><!--bulletinColumn-->
                                                                    <!--bulletinColumnRt-->
                                                                    </div><!--bulletinInner-->
                                                                    </div>



                                                                    </div>
                                                                    <div style="clear:both"></div>



                                                                    <div style="clear:both"></div>


                                                                    <div style="clear:both"></div>

                                                                    </div>

                                                                    <div style="clear:both"></div>

                                                                    <!-- Container --> 
                                                                    </div>
                                                                    </div>

                                                                    </body>
                                                                    </html>



