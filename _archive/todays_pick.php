<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"  href="css/style.css" type="text/css">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png" />
<!-- Custom scrollbars CSS -->
<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" />
<title>SQ GROUP</title>
  <meta name="viewport" content="width=device-width">

<!--[if IE 6]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	
<!--[if IE 7]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	
<!--[if IE 8]>
	<link href="css/ie" rel="stylesheet" type="text/css" />
	<![endif]-->
	
<!--[if IE 9]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->

<!--[if IE 10]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->

<!--[if IE 8]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script src="js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(sizeContent);
//Every resize of window
$(window).resize(sizeContent);
//Dynamically assign height
	function sizeContent() {
		var newWidth = $("html").width() - $("#leftPanel").width() + "px";
		$("#rightPanel").css("width", newWidth);
}
</script>


<script type="text/javascript">
if ($(window).width() < 699 ) {
    //small screen, load other JS files
    $.getScript('js/device.js', function () {
        //the script has been added to the DOM, you can now use it's code
    });
}

window.onorientationchange = function()
{
   window.location.reload();
}

</script>

  
  <link rel="stylesheet" href="css/bulletin-rs.css">
  <link rel="stylesheet" href="css/style.css">

  <script src="js/modernizr-2.5.3.min.js"></script>
  <link rel="stylesheet" href="css/responsivemobilemenu.css" type="text/css"/>
	<script type="text/javascript" src="js/responsivemobilemenu.js"></script>
  

</head>
<body class="bulletinBody">
<div id="topBarContainer"> </div>
<div class="mobilemenu" style="display:none">
              <div class="rmm">
                <ul>
                    <li><a href="bulletin.html">All News</a></li>
                     <li><a href="all_about_sq.html" >All About Sq</a></li>
                    <li><a href="todays_pick.php" >Today's Picks</a></li>
                     <li><a href="special_feature.php">Special Features</a></li>
                    </ul>
                </div>
    </div>
<div id="wrapper">
  <div id="leftPanel">
  <div class="shadowIe"></div>
    <div class="logo">
          <p align="center"><a href="index.html"> <img width="100" src="images/logo.png" /></a><br />
          <span class="sus"><a class="sasLogoLink" href="bulletin.html"> Bulletin </a></span> </p>
     </div>
    
    <div class="leftMenu">
      <ul>
            <li><a href="bulletin.html">All News</a></li>
             <li><a href="all_about_sq.html" >All About Sq</a></li>
               <li><a class="active" href="todays_pick.php" >Today's Picks</a></li>
            
             <li><a href="special_feature.php">Special Features</a></li>
      </ul>
    </div>
    
      <div class="hrcenter"></div>
    <div class="slogan"><p align="center">
    <img width="140" src="images/slogan.png" />
    </p>
    
    
     </div>
    
    <div class="hrcenter"></div>
    
  </div>
  <div id="rightPanel" >
    
    <div id="container">
    
    
      <div id="bulletinwrapper">
        <!--bulletinHeader-->
        <div id="content" class="container clearfix">
        
        <div class="item">
           

<a href="bulletin-post12.html"><img src="images/today-pick/photo_1374729793485-1-HD.jpg" width=""></a> 
<a class="bTitle" href="bulletin-post12.html">Stoll products used by the Royal Family</a>
                                                    
    <p> <a class="" href="bulletin-post12.html">Flat knitting machine manufacturer Stoll has revealed that the white knitted blanket baby Prince George Alexander.</a></p>
                                                                  


                                                                    
    
        </div>
        
    
     
     
        
        <div class="item">
          

 <a class="bTitle" href="bulletin-post13.html">Wal-Mart announces a $50m loan to Bangladeshi factory owners</a>

                                                                        <p>
                                                                            <a class="" href="bulletin-post13.html"> Wal-Mart Stores Inc. Thursday announced that it would provide up to $50 million loan to Bangladeshi factory owners</a></p>
                                                                    

                                                                    
              
        </div>
      
        <div class="item">
       


<a class="bTitle" href="bulletin-post14.html"> GSP comeback seems unlikely</a>

                                                                        <p><a class="" href="bulletin-post14.html"> 
                                                                            Bangladesh may miss the opportunity to regain a US trade benefit through a review in December as the country is going slow on  </a> </p>




                                                                    
                
              
        </div>
    
        
        <div class="item">
         



    <a class="bTitle" href="bulletin-post15.html">Government and BGMEA are disappointed to be left out of factory inspection team</a>

                                                                        <p>
                                                                        <a class="" href="bulletin-post15.html"> The government and garment exporters have expressed disappointment as IndustriALL — a global trade union — has excluded them from a core committee that will inspect Bangladeshi factories under an accord signed by 85 global retailers and brands.</a></p>


                                                                    
        </div>
        
        <div class="item">
         



 <a class="bTitle" href="bulletin-post16.html">Firms barred from giving out dividends through asset revaluation</a>

                                                                        <p>
 <a class="" href="bulletin-post16.html"> Bangladesh Securities and Exchange Commission (BSEC) has barred listed companies from issuing dividends through asset revaluation </a>  </p>

                                                                    
        </div>
        
        <div class="item">
           



    <a class="bTitle" href="bulletin-post17.html">Transfer pricing law to be enforced from next June</a>

     <p> <a class="" href="bulletin-post17.html">
                                                                            The income tax wing is set to enforce the transfer pricing law from June next in a bid to check tax evasion through capital flight by both local and multinational companies.  To materialise the plan  </a>       </p>




                                                                    
        </div>
        
        <div class="item ">
          
          


<a class="bTitle" href="bulletin-post18.html">Yunus leads global effort for garment labour wages</a>

                                                                        <p>
<a class="" href="bulletin-post18.html">
Nobel laureate Muhammad Yunus has proposed an international minimum wage for garment workers to be worked out for each country separately, as part of a global effort to solve labour issues in all garment producing nations.</a>
                                                                        </p>




                                                                    
          
        </div>
  
        
        
        
            
        
        
      </div>
    </div>
     
    </div>

    </div>
    
    <div style="clear:both"></div>
    
    <!-- Container --> 
  </div>
</div>

  <script src="js/jquery.masonry.min.js"></script>
  <script src="js/script.js"></script>
        

	
</body>
</html>