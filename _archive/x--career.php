<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"  href="css/style.css" type="text/css">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png" />
<!-- Custom scrollbars CSS -->
<title>SQ GROUP</title>
<!-- Google CDN jQuery with fallback to local -->
<script src="js/jquery.min.js"></script>
<link rel="stylesheet" href="css/responsivemobilemenu.css" type="text/css"/>
<script type="text/javascript" src="js/responsivemobilemenu.js"></script>

	
  <!--[if IE 6]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	
<!--[if IE 7]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	
<!--[if IE 8]>
	<link href="css/ie" rel="stylesheet" type="text/css" />
	<![endif]-->
	
<!--[if IE 9]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->

<!--[if IE 10]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->

<!--[if IE 8]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
<![endif]--></head>
<body>
<div id="topBarContainer"> </div>
<div class="mobilemenu" style="display:none">
              <div class="rmm">
                <ul>
                    <li><a href="about.html">About US</a></li>
                    <li><a href="business.html" >Business</a></li>
                    <li><a href="supportindustry.html">Supporting Industries</a></li>
                    <li><a href="sustainability.html" >Sustainability </a></li>
                    <li><a class="active" href="career.html">Careers</a></li>
                    <li><a href="bulletin.html" >Bulletin</a></li>
                    <li><a href="contact.html">Contact us</a></li>
                    </ul>
                </div>
    </div>
<div id="wrapper">

<div id="leftPanel">
<div class="shadowIe"></div>
    <div class="logo">
      <p align="center">
      <a href="index.html"><img width="100" src="images/logo.png" /></a>
        <br>
<span class="sus">
<a class="sasLogoLink sasLogoLinkExtra" href="career.html">  
CAREERS   </a>
</span>
      </p>
     </div>
    
    <div class="leftMenu">
      <ul>
        <li><a href="about.html">About US</a></li>
        <li><a href="business.html" >Business</a></li>
         <li><a href="supportindustry.html">Supporting Industries</a></li>
        <li><a href="sustainability.html" >Sustainability </a></li>
        <li><a class="active" href="career.html">Careers</a></li>
        <li><a href="bulletin.html" >Bulletin</a></li>
        <li><a href="contact.html">Contact us</a></li>
      </ul>
    </div>
    <div class="hrcenter"></div>
    <div class="slogan"><p align="center">
    <img width="140" src="images/slogan.png" />
    </p>
    
    
     </div>
    
    <div class="hrcenter"></div>
    
  </div>
  
  <div id="rightPanel" class="rightInner ">
  
  	<div class="careerContainer">
    <div class="careerTop">
    <!--<a href="index.html"><img src="images/logo_career.png" height="100" alt="logo" /></a>-->
    </div><!--careerTop-->
    <div class="careerInner">
     <h2>Careers</h2>
     <p>Bangladesh clothing industry is considered to be one of the likely successors to China's crown as the World's ultimate manufacturing destination. This is tough but definitely a break. We want to strategically align and shape up SQ for manipulating such prospect. <br />
<br />

SQ brings together a diverse and talented workforce. We provide our team the resources and opportunities they need to make the impact. If you have a natural motivation to challenge the status-quo and Stand Tall, we invite you to join us for a rewarding career and share our passion for growth and success.
</p>

<div style="width:100%; height:20px; clear:both"></div>

			<div class="careerLeft">
                <div class="careerLeftInner">
                        <h6>career@sqgc.com</h6>
                    <ul>
                        <li><a href="#">View All Job Post</a></li>
                       <li><a href="#">Recent Job Post</a></li>
                    </ul>
                    </div>
            </div><!--careerLeft-->
            <div class="careerRight">
            <table width="100%" border="1" class="sqCareer">
  <tr class="careerHeading">
    <th width="40%"><strong>Title</strong></th>
    <th width="15%"><strong>End Date</strong></th>
    <th width="15%"><strong>Location</strong></th>
    <th width="30%"><strong>Business units</strong></th>
  </tr>
  <tr class="jobDetails ">
    <td><a href="career_details.html">Factory Manager (Shirt)</a></td>
    <td>20th August, 2013</td>
    <td>SQ Factory</td>
    <td>Factory</td>
  </tr>
    <tr class="jobDetails odd">
    <td><a href="knitwear_design.html">Head of knitwear design</a></td>
    <td>20th August, 2013</td>
    <td>SQ Factory</td>
    <td>Factory</td>
  </tr>
    <tr class="jobDetails ">
     <td><a href="garment _technician.html">Garment Technician</a></td>
    <td>20th August, 2013</td>
    <td>SQ Factory</td>
    <td>Factory</td>
  </tr>
    <tr class="jobDetails odd">
     <td><a href="Executive_Industrial_Engineering.html">Executive, Industrial Engineering</a></td>
    <td>20th August, 2013</td>
    <td>SQ Factory</td>
    <td>Factory</td>
  </tr>
    <tr class="jobDetails ">
     <td><a href="career_details.html">Gerente de Boutique</a></td>
    <td>20th August, 2013</td>
    <td>SQ Factory</td>
    <td>Factory</td>
  </tr>
    
    
</table>

            </div><!--careerRight-->
    </div><!--careerInner-->
        
    </div>
    

    
    
    
    
    <!-- Container --> 
  </div>
 </div>


 

</body>
</html>