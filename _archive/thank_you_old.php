<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SQ Group | Thank You</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/form.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Aldrich' rel='stylesheet' type='text/css'>

<style type="text/css">
.header{
background-color: #ffffff;
}
@font-face
{
font-family: proxima;
src: url('fonts/ProximaNova-Reg-webfont.ttf'),
	url('fonts/ProximaNova-Reg-webfont.svg'),
	url('fonts/ProximaNova-Reg-webfont.woff'),
     url('font/ProximaNova-Reg-webfont.eot'); /* IE9 */
}

.thanks {
    border-top: 2px solid;
    color:#9bb18d;
    font-family:proxima;
    font-size: 60px;
    margin-top: 10%;
	line-height:100px;
}

.msg {
    border-bottom: 2px solid;
    color:#9bb18d;
    font-family:proxima;
    font-size: 25px;
    line-height:100px;
}

</style>

</head>

<body bgcolor="#ffffff">

		<div style="width:500px; height:auto; display:block; margin:0 auto; margin-top:200px;">
   		  <!--banner-->
                <div class="container">
                
                    <div class="emailwrapper">
                    <h3 class="thanks" align="center"> Thank You </h3>
                    
                    <h6 class="msg" align="center"> We will keep in touch with you. </h6>
                        
            		</div>
             
                
                </div><!--container-->
        </div><!--wrapper-->



</body>
</html>
