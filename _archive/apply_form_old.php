<?php
include('lib2/config.php');
$err = "";
$fname = "";
$lname = "";
$email = "";
$dob = "";
$position = "";
$nationality = "";
$phone = "";
$gender = "";
$education = "";
$extension = array();
$extension = array('doc','docx','pdf'); //valid extensions for cv




if(isset($_POST['submit'])){
	
	extract($_POST);
	$CheckEmail = mysql_query("SELECT * FROM candidate_db WHERE candidate_email='$email'");
	$CountRow = mysql_num_rows($CheckEmail);
	
	if($CountRow > 0){
		$err = "You already submitted your CV.";
	} else {
	
		if($fname == "") {
			$err = "First Name field is required.";
		} elseif($lname == "") {
			$err = "Last Name field is required.";
		} elseif($dob == "") {
			$err = "Date of Birth field is required.";
		} elseif($email == "") {
			$err = "E-mail Address field is required.";
		} elseif($phone == "") {
			$err = "Phone No. field is required.";
		} elseif($gender == "") {
			$err = "Gender field is required.";
		} elseif($nationality == "") {
			$err = "Nationality field is required.";
		} elseif($education == "") {
			$err = "Last Education field is required.";
		} elseif(!ctype_alpha($fname)) {
			$err = "First Name can only be alphabet.";
		} elseif(!ctype_alpha($lname)) {
			$err = "Last Name can only be alphabet.";
		} elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$err = "Please provide a valid E-mail Address.";
		} elseif($_FILES["cv"]["tmp_name"] == "") {
			$err = "Please upload your CV.";
		} else {
			
			$CircularID = $_GET['circular_id'];
			
			//preparing cv for upload
			$cv_upload = basename($_FILES['cv']['name']);
			$ext = pathinfo($cv_upload, PATHINFO_EXTENSION); /* it will return me like jpeg, gif, pdf, png */
			$cv_name = $fname . '' . $lname . '.' . $ext; /* create custom image name color id will add  */
			$cv_source = $_FILES["cv"]["tmp_name"];
			
			if(!in_array($ext,$extension)){
				$err = "Your CV must be in .doc, .docx or .pdf format.";
			} else {
				
				//creating directory
				if(!is_dir('cv/')){
					mkdir('cv/',0777, TRUE);
				}
				
				$cv_target = 'cv/' . $cv_name;
				move_uploaded_file($cv_source, $cv_target);
				
				$CandidateAdd = '';
				$CandidateAdd .=' candidate_fname = "' . mysql_real_escape_string($fname) . '"';
				$CandidateAdd .=', candidate_lname = "' . mysql_real_escape_string($lname) . '"';
				$CandidateAdd .=', candidate_dob = "' . mysql_real_escape_string($dob) . '"';
				$CandidateAdd .=', candidate_phone = "' . mysql_real_escape_string($phone) . '"';
				$CandidateAdd .=', candidate_email = "' . mysql_real_escape_string($email) . '"';
				$CandidateAdd .=', candidate_gender = "' . mysql_real_escape_string($gender) . '"';
				$CandidateAdd .=', candidate_last_edu = "' . mysql_real_escape_string($education) . '"';
				$CandidateAdd .=', candidate_circular_id = "' . mysql_real_escape_string($CircularID) . '"';
				$CandidateAdd .=', candidate_nationality = "' . mysql_real_escape_string($nationality) . '"';
				$CandidateAdd .=', candidate_cv = "' . mysql_real_escape_string($cv_name) . '"';
				
				$CandidateSql = "INSERT INTO candidate_db SET $CandidateAdd";
				$AddCandidate = mysql_query($CandidateSql);
				if ($AddCandidate) {
					
							require("class.phpmailer.php");
							//getting email information
							$mail = new PHPMailer();
					
							$mail->IsSMTP(); // send via SMTP
						
							$mail->SMTPDebug = 1;
						
							//IsSMTP(); // send via SMTP
						
							$mail->SMTPAuth = true; // turn on SMTP authentication
						
							$mail->Username = "bluetest"; // Enter your SMTP username
						
							$mail->Password = "bluepass2012"; // SMTP password
						
							$webmaster_email = 'no-reply@sqgc.com'; //Add reply-to email address
						
							$mail->From = $webmaster_email;
						
							$mail->FromName = "SQ Group";
							$name = $fname.' '.$lname;
							$mail->AddAddress($email,$name);
							
					
							$mail->AddReplyTo($webmaster_email,"SQ Group");
						
							$mail->extension=php_openssl.dll;
						
							$mail->WordWrap = 50; // set word wrap
						
							/*$mail->AddAttachment("/var/tmp/file.tar.gz"); // attachment
						
							$mail->AddAttachment("/tmp/image.jpg", "new.jpg"); // attachment*/
						
							$mail->IsHTML(true); // send as HTML
						
							
						
							$mail->Subject = "Thank you";
						
							
							$host = $_SERVER['HTTP_HOST'];
							$parent = basename(dirname($_SERVER['PHP_SELF']));
							$location = "http://".$host."/".$parent;
							
						
							$mail->Body = '<html>
							
							'.$fname.' '.$lname.',<br><br>
							
							Thank you for submitting your resume. Our management team is reviewing your qualifications and will contact you if there is a match with our current requirements for the position you applied.<br><br>
							
							We appreciate your interest in SQ Group and wish you the best of luck in your job search.<br><br>
							
							Thank you.<br><br>
							
							SQ Group
							
						
							</html>';      //HTML Body
						
							
						
							$mail->AltBody = '<html>
							
							'.$fname.' '.$lname.',<br><br>
							
							Thank you for submitting your resume. Our management team is reviewing your qualifications and will contact you if there is a match with our current requirements for the position you applied.<br><br>
							
							We appreciate your interest in SQ Group and wish you the best of luck in your job search.<br><br>
							
							Thank you.<br><br>
							
							SQ Group
							
						
							</html>';      //HTML Body
							
							
							$error = 0;
							$success = 0;
						
							if(!$mail->Send())
						
							{
								$error ++;
							} 
						
							else 
						
							{
								$success ++;
							}
						
						
						
						
						
						
						//getting corrosponding email addresses for the post
						$SelectCircular = mysql_query("SELECT * FROM circular WHERE circular_id=$CircularID");
						$GetCircular = mysql_fetch_object($SelectCircular);
						
						$emails = explode(",", $GetCircular->circular_application_to_email);
						
						
						foreach($emails as $sendemail)
						{
							include_once("class.phpmailer.php");
							//getting email information
							$mail = new PHPMailer();
					
							$mail->IsSMTP(); // send via SMTP
						
							$mail->SMTPDebug = 1;
						
							//IsSMTP(); // send via SMTP
						
							$mail->SMTPAuth = true; // turn on SMTP authentication
						
							$mail->Username = "bluetest"; // Enter your SMTP username
						
							$mail->Password = "bluepass2012"; // SMTP password
						
							$webmaster_email = 'no-reply@sqgc.com'; //Add reply-to email address
						
							$mail->From = $webmaster_email;
						
							$mail->FromName = "SQ Group";
							
							$email = $sendemail;
							$name = $sendemail;
							
							$mail->AddAddress($email,$name);
							
					
							$mail->AddReplyTo($webmaster_email,"SQ Group");
						
							$mail->extension=php_openssl.dll;
						
							$mail->WordWrap = 50; // set word wrap
						
							/*$mail->AddAttachment("/var/tmp/file.tar.gz"); // attachment
						
							$mail->AddAttachment("/tmp/image.jpg", "new.jpg"); // attachment*/
						
							$mail->IsHTML(true); // send as HTML
						
							
						
							$mail->Subject = "New CV Submission Alert";
						
							
							$host = $_SERVER['HTTP_HOST'];
							$parent = basename(dirname($_SERVER['PHP_SELF']));
							$location = "http://".$host."/".$parent;
							
						
							$mail->Body = '<html>
							
							We have received new CV from <strong>'.$fname.' '.$lname.'</strong> for the post '.$GetCircular->circular_position.'.<br>
							
							Please use below credential to view details.<br><br>
							
							URL: <a href="'.$location.'/admin/">'.$location.'/admin/</a><br>
							Email: admin@bscheme.com<br>
							Password: 123456<br><br>
							
							Thank you.<br><br>
							
							SQ Group
							
						
							</html>';      //HTML Body
						
							
						
							$mail->AltBody = '<html>
							
							We have received new CV from <strong>'.$fname.' '.$lname.'</strong>.<br>
							
							Please use below credential to view details.<br><br>
							
							URL: <a href="'.$location.'/admin-panel/">'.$location.'/admin-panel/</a><br>
							Email: admin@bscheme.com<br>
							Password: 123456<br><br>
							
							Thank you.<br><br>
							
							SQ Group
							
						
							</html>';      //Plain Text Body
							
							$error = 0;
							$success = 0;
						
							if(!$mail->Send())
						
							{
								$error ++;
							} 
						
							else 
						
							{
								$success ++;
							}
						
							
						}
					
		
					
					
					$link = 'thank_you.php';
					redirect($link);
				} else {
					echo 'AddCandidate Error: ' . mysql_error();
					$err = "Insert Query failed.";
				}
			}
		}
	}
}



$CircularID = $_GET['circular_id'];
$SelectCircular = mysql_query("SELECT * FROM circular WHERE circular_id=$CircularID");
$GetCircular = mysql_fetch_object($SelectCircular);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SQ Group | Online Job Application Submission</title>

<!-- date picker -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css" />
  <script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  </script>
  <!-- end date picker -->

<style>
.header{
background-color: #ffffff;
}
@font-face
{
font-family: proxima;
src: url('fonts/ProximaNova-Reg-webfont.ttf'),
	url('fonts/ProximaNova-Reg-webfont.svg'),
	url('fonts/ProximaNova-Reg-webfont.woff'),
     url('font/ProximaNova-Reg-webfont.eot'); /* IE9 */
}

</style>
</head>

<body bgcolor="#ffffff">
<div style="display:block; width:900px; height:100px; margin:0 auto; margin-bottom: 150px;">
	<div class="header" style="display:block; width:900px; height:100px; margin:0 auto">
    <a href="index.html"><img src="image/logo.png" height="80" width="80" style="float:left; display:block; padding-top:10px; margin-left:120px;" /></a>
    <p align="center" style="vertical-align:middle; top:20px; position:relative; margin-right:100px"><font style="font-family:proxima; font-size:40px; color:#9bb18d">Online Job Application Form</font></p>
    </div>
    
    <div style="width:900px; clear:both; height:40px"></div>
<?php

if($err != ""){
	?>    
    <div style="width:400px; height:30px; display:block; border:#C03 1px solid; margin:0 auto; background-color:#C99; font:Tahoma; font-size:18px; color:#C03"><p style="bottom:15px; position:relative; display:block" align="center"><?php echo $err; ?></p></div>

<?php
}
?>

<?php
if($_GET['circular_id'] != 0){
	?>    
    <div style="display:block; width:900px; height:50px; margin:0 auto; ">
    <font style="font-family:proxima; font-size:18px; color: #9bb18d;">You are applying for <strong><?php echo $GetCircular->circular_position; ?></strong></font>
    </div>
<?php
}
	?>
    
    <div style="display:block; width:900px; height:100px; margin:0 auto; margin-bottom: 150px;">
    <fieldset>
    <legend title="Application Instruction"><font style="font-family:proxima; font-size:15px; color: #9bb18d;"><strong>Application Instruction</strong></font></legend>
    <font style="font-family:proxima; font-size:13px; line-height:20px; color: #000;">Its simple.<br /><br />
    <strong>1.</strong> Fill up the Personal Details form below.<br />
    <strong>2.</strong> Upload your resume.<br /><br />
    <font style="size:22px; font-weight:bolder; line-height:30px">Important Notes</font><br />
    If all the boxes are not filled up, the application will not be submitted.
     </font>
    </fieldset>
    </div>
    
    
    
    <form name="appsubmit" action="apply_form.php?circular_id=<?php echo $_GET['circular_id']; ?>" method="post" enctype="multipart/form-data">
    
    <div style="display:block; width:900px; height:100px; margin:0 auto; margin-bottom: 150px;">
    <fieldset>
    <legend title="Application Instruction"><font style="font-family:proxima; font-size:15px; color: #9bb18d;"><strong>Personal Details</strong></font></legend>
    	<div style="display:block; float:left; height:auto; width:425px; line-height:50px">
        <font style="font-family:proxima; font-size:15px;color: #000;">First Name:</font><input type="text" name="fname" value="<?php echo $fname; ?>" style="height:20px; width:200px; margin-left:50px; position:relative" /><br />
        <font style="font-family:proxima; font-size:15px;color: #000;">Date of Birth:</font><input type="text" value="<?php echo $dob; ?>" name="dob" id="datepicker" style="height:20px; width:200px; margin-left:35px; position:relative" /><br />
        <font style="font-family:proxima; font-size:15px;color: #000;">Phone No.:</font><input type="text" value="<?php echo $phone; ?>" name="phone" style="height:20px; width:200px; margin-left:52px; position:relative" /><br />
        <font style="font-family:proxima; font-size:15px;color: #000;">Last Education:</font>
        <select style="height:25px; width:200px; margin-left:20px; position:relative" name="education">
        <option value=""> -- Select Last Education -- </option>
        <option value="College">College</option>
        <option value="Associate Degree">Associate Degree</option>
        <option value="Bachelor's Degree">Bachelor's Degree</option>
        <option value="Postgraduate">Postgraduate</option>
        <option value="Master's Degree">Master's Degree</option>
        <option value="Ph.D">Ph.D</option>
        </select>
        </div>
        <div style="display:block; float:right; height:auto; width:425px; line-height:50px">
        <font style="font-family:proxima; font-size:15px;color: #000;">Last Name:</font><input type="text" value="<?php echo $lname; ?>" name="lname" style="height:20px; width:200px; margin-left:50px; position:relative" /><br />
        <font style="font-family:proxima; font-size:15px;color: #000;">E-mail Address:</font><input type="text" value="<?php echo $email; ?>" name="email" style="height:20px; width:200px; margin-left:20px; position:relative" /><br />
        <font style="font-family:proxima; font-size:15px;color: #000;">Nationality:</font><input type="text" value="<?php echo $nationality; ?>" name="nationality" style="height:20px; width:200px; margin-left:48px; position:relative" /><br />
        <font style="font-family:proxima; font-size:15px;color: #000;">Gender:</font>
        <select style="height:25px; width:200px; margin-left:70px; position:relative" name="gender">
        <option value=""> -- Select Gender -- </option>
        <option value="male">Male</option>
        <option value="female">Female</option>
        </select>
        </div>
    </fieldset>
    </div>
    
    
    
    <div style="display:block; width:900px; height:100px; margin:0 auto; margin-bottom: 150px;">
    <fieldset>
    <legend title="Application Instruction"><font style="font-family:proxima; font-size:15px; color: #9bb18d;"><strong>Upload Resume</strong></font></legend>
    	<font style="size:22px; font-weight:bolder; line-height:30px;font-family:proxima;">Important Notes</font><br />
    <font style="font-family:proxima; font-size:13px; color: #000;">Please upload a latest copy of your CV in here. Your CV can be in Word Document <strong>(.doc, .docx)</strong> or PDF <strong>(.pdf)</strong> format. Any other format is not allowed.
     </font>
     
     <div style="display:block; float:left; height:auto; width:425px; line-height:50px; margin-top:30px">
        <font style="font-family:proxima; font-size:15px; color: #000;">Upload your CV:</font><input type="file" name="cv" style=" margin-left:40px;" /><br />
        
        </div>
        
    </fieldset>
    </div>
    
    
    <div style="display:block; width:900px; height:100px; margin:0 auto; margin-bottom: 150px;">
    	<button type="submit" style="background-color:#9bb18d; color:#FFF; height:30px; width:200px; font-family:proxima; font-size:16px; float:right" name="submit">Submit Application</button>
    </div>
    
    </form>
</div>

</body>
</html>