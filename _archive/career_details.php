<?php
include './config/config.php';
$careerTitle = '';
$careerResponsibilities = '';
$careerEligibility = '';
$careerNote = '';
$careerIntro = '';
$thejob = '';
$eligibility = '';
$circular_id = $_REQUEST["circular_id"];
$careerSql = "SELECT * FROM circular WHERE circular_id=" . intval($circular_id);
$careerSqlResult = mysqli_query($con, $careerSql);
if ($careerSqlResult) {
    if (mysqli_num_rows($careerSqlResult) > 0) {
        $careerSqlResultRowObj = mysqli_fetch_object($careerSqlResult);
        $careerTitle = $careerSqlResultRowObj->circular_position;
        $careerResponsibilities = $careerSqlResultRowObj->circular_job_responsibilities;
        $careerEligibility = $careerSqlResultRowObj->circular_eligibility;
        $careerLastDate = $careerSqlResultRowObj->circular_application_deadline;
        $careerNote = $careerSqlResultRowObj->circular_note;
        $careerIntro = $careerSqlResultRowObj->circular_intro;
    }
} else {
    if (DEBUG) {

        die("careerSqlResult Error: " . mysqli_error($con));
    } else {
        die("careerSqlResult Fail");
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet"  href="css/style.css" type="text/css">
                <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
                <link rel="apple-touch-icon" href="apple-touch-icon.png" />
                <link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png" />
                <link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png" />
                <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png" />
                <link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png" />
                <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png" />
                <link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png" />
                <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png" />
                <link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png" />
                <!-- Custom scrollbars CSS -->
                <title>SQ GROUP</title>
                <!-- Google CDN jQuery with fallback to local -->
                <script src="js/jquery.min.js"></script>
                <link rel="stylesheet" href="css/responsivemobilemenu.css" type="text/css"/>
                <script type="text/javascript" src="js/responsivemobilemenu.js"></script>
                <!-- Load ScrollTo -->
                <script src="js/jquery.scrollTo-1.4.2-min.js"></script>
                <!-- Load LocalScroll -->
                <script src="js/jquery.localscroll-1.2.7-min.js"></script>
                <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
                <script type="text/javascript" src="js/script.js"></script>
                <script>

                    // When the document is loaded...
                    $(document).ready(function()
                    {
                        // Scroll the whole document 
                        $('#box-links').localScroll({
                            target: 'body'
                        });
                        // Scroll the content inside the #scroll-container div
                        $('#small-box-links').localScroll({
                            target: '#small-box-container'
                        });
                    });
                </script>

                <script src="js/jquery.easing.1.3.js"></script>
                <script type="text/javascript" src="js/jquery.cycle.all.js"></script>
                <!-- Script From Jquery.Cycle.all for Category slideshow -->
                <script type="text/javascript">
                    $(document).ready(function() {

                        $('.sasSliderContent')<!--.before('<div class="pgr2">') -->
                                .cycle({
                            fx: 'scrollHorz',
                            easeIn: 'easeInOutQuint', //easeOutQuart
                            easeOut: 'easeInOutQuint',
                            speed: 1500,
                            timeout: 600, // auto slide
                            next: '.next2',
                            prev: '.prev2',
                            pause: 1,
                            pager: '#pgr2'
                        });


                    });
                </script>


                <!--[if IE 6]>
                      <link href="css/ie.css" rel="stylesheet" type="text/css" />
                      <![endif]-->

                <!--[if IE 7]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 8]>
                        <link href="css/ie" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 9]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 10]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 8]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                <![endif]--></head>
                <body>
                    <div id="topBarContainer"> </div>
                    <div class="mobilemenu" style="display:none">
                        <div class="rmm">
                            <ul>
                                <li><a href="about.html">About US</a></li>
                                <li><a href="business.html" >Business</a></li>
                                <li><a href="supportindustry.html">Supporting Industries</a></li>
                                <li><a href="sustainability.html" >Sustainability </a></li>
                                <li><a class="active" href="career.html">Careers</a></li>
                                <li><a href="bulletin.html" >Bulletin</a></li>
                                <li><a href="contact.html">Contact us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="wrapper">
                        <div id="leftPanel">
                            <div class="shadowIe"></div>
                            <div class="logo">
                                <p align="center">
                                    <a href="index.html"><img width="100" src="images/logo.png" /></a>
                                    <br>
                                        <span class="sus">
                                            <a class="sasLogoLink sasLogoLinkExtra" href="career.php">  
                                                CAREERS   </a>
                                        </span>
                                </p>
                            </div>

                            <div class="leftMenu">
                                <ul>
                                    <li><a href="about.html">About US</a></li>
                                    <li><a href="business.html" >Business</a></li>
                                    <li><a href="supportindustry.html">Supporting Industries</a></li>
                                    <li><a href="sustainability.html" >Sustainability </a></li>
                                    <li><a class="active" href="career.php">Careers</a></li>
                                    <li><a href="bulletin.html" >Bulletin</a></li>
                                    <li><a href="contact.html">Contact us</a></li>
                                </ul>
                            </div>
                            <div class="hrcenter"></div>
                            <div class="slogan"><p align="center">
                                    <img width="140" src="images/slogan.png" />
                                </p>


                            </div>

                            <div class="hrcenter"></div>

                        </div>


                        <div id="rightPanel" class="rightInner ">

                            <div class="careerContainer">
                                <div class="careerTop">

                                </div><!--careerTop-->
                                <div class="careerInner">
                                    
									<div style="clear:both"></div>
									
                                    <div style="clear:both"></div>
                                    
                                    <div class="careerLeft">
                                    <p style="font-size:15px; color:#fff; padding-top:15px;">If you do not meet the requirements of the job vacancies available now, but are interested to work at SQ, please drop your resume <a style="color:#ddd;" href="apply_form.php?circular_id=0">here</a>. It will remain in our database and we will contact you if anything suitable comes along.</p>
                                    <p style="font-size:15px;"></p>
                                        <!--<h6>career@sqgc.com</h6>
                                        <ul>
                                            <li><a href="career.php?job_type=all">View All Job Post</a></li>
                                            <li><a href="career.php?job_type=recent">Recent Job Post</a></li>
                                        </ul>-->
                                    </div><!--careerLeft-->
                                    
                                    <div class="careerRight">


                                        <h2><?php echo $careerTitle; ?></h2>
                                        <div class="careerDescripTion">
                                        <?php
										if ($careerIntro != '') {
											echo'<p class="carrerIntro" style="font-size:14px;">' . $careerIntro . '</p>';
										}
										?>
                                            <h3>The Job</h3>
                                            <?php
                                            $theJob = str_replace("<p>", '', html_entity_decode($careerResponsibilities));
                                            $theJob = str_replace("</p>", '', $theJob);
                                            echo $theJob;
                                            ?>

                                            <h3>Competency</h3>
                                            <?php
                                            $eligibility = str_replace("<p>", '', html_entity_decode($careerEligibility));
                                            $eligibility = str_replace("</p>", '', $eligibility);
                                            echo $eligibility;
                                            ?>
                                            <?php
                                            $date = date_create($careerLastDate . " 17:45:12");
                                            $careerLastDate = date_format($date, 'jS F Y');
                                            ?>
                                            <p>Closing date: <?php echo $careerLastDate; ?></p>
                                            <?php
                                            echo '<a class="applyOnline" href="apply_form.php?circular_id=' . $circular_id . '">apply online</a>';
                                            ?>
                                        </div><!--careerDescripTion-->


                                    </div><!--careerRight-->
                                </div><!--careerInner-->

                            </div>






                            <!-- Container --> 
                        </div>
                    </div>


                    <script type="text/javascript">
                        $(function() {
                            $('.imgOpa').each(function() {
                                $(this).hover(
                                        function() {
                                            $(this).stop().animate({opacity: 0.7}, 800);
                                        },
                                        function() {
                                            $(this).stop().animate({opacity: 0.9}, 800);
                                        })
                            });
                        });
                    </script>

                    <script src="js/jquery.backstretch.js"></script>
                    <script>$(".fimg1").backstretch("images/img31.jpg");</script>
                    <script>$(".fimg2a").backstretch("images/img22.jpg");</script>
                    <script>$(".fimg3").backstretch("images/img31.jpg");</script>
                    <script>$(".fimg4").backstretch("images/img31.jpg");</script>
                    <script>$(".fimg5").backstretch("images/img31.jpg");</script>



                </body>
                </html>