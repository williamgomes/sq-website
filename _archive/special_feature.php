<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet"  href="css/style.css" type="text/css">
                <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
                <link rel="apple-touch-icon" href="apple-touch-icon.png" />
                <link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png" />
                <link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png" />
                <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png" />
                <link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png" />
                <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png" />
                <link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png" />
                <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png" />
                <link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png" />
                <!-- Custom scrollbars CSS -->
                <link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" />

                <title>SQ GROUP</title>

                <!--[if IE 6]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 7]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 8]>
                        <link href="css/ie" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 9]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 10]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 8]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                <![endif]-->

                <script src="js/jquery.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(sizeContent);
                    //Every resize of window
                    $(window).resize(sizeContent);
                    //Dynamically assign height
                    function sizeContent() {
                        var newWidth = $("html").width() - $("#leftPanel").width() + "px";
                        $("#rightPanel").css("width", newWidth);
                    }
                </script>

                <script type="text/javascript">
                    if ($(window).width() < 699) {
                        //small screen, load other JS files
                        $.getScript('js/device.js', function() {
                            //the script has been added to the DOM, you can now use it's code
                        });
                    }

                    window.onorientationchange = function()
                    {
                        window.location.reload();
                    }


                    window.onorientationchange = function()
                    {
                        window.location.reload();
                    }


                </script>

                <link rel="stylesheet"  href="css/bulletin.css" type="text/css">
                    <link rel="stylesheet" href="css/responsivemobilemenu.css" type="text/css"/>
                    <script type="text/javascript" src="js/responsivemobilemenu.js"></script>

                    </head>
                    <body class="bulletinBody">
                        <div id="topBarContainer"> </div>
                        <div class="mobilemenu" style="display:none">
                            <div class="rmm">
                                <ul>
                                    <li><a href="bulletin.html">All News</a></li>
                                    <li><a href="all_about_sq.html" >All About Sq</a></li>
                                    <li><a  href="todays_pick.php" >Today's Picks</a></li>

                                    <li><a class="active"  href="special_feature.php">Special Features</a></li>
                                </ul>
                            </div>
                        </div>
                        <div id="wrapper">
                            <div id="leftPanel">
                                <div class="shadowIe"></div>
                                <div class="logo">
                                    <p align="center"><a href="index.html"> <img width="100" src="images/logo.png" /></a><br />
                                        <span class="sus"><a class="sasLogoLink" href="bulletin.html"> Bulletin </a></span> </p>
                                </div>

                                <div class="leftMenu">
                                    <ul>
                                        <li><a href="bulletin.html">All News</a></li>
                                        <li><a href="all_about_sq.html" >All About Sq</a></li>
                                        <li><a href="todays_pick.php" >Today's Picks</a></li>

                                        <li><a class="active" href="special_feature.php">Special Features</a></li>
                                    </ul>
                                </div>

                                <div class="hrcenter"></div>
                                <div class="slogan"><p align="center">
                                        <img width="140" src="images/slogan.png" />
                                    </p>


                                </div>

                                <div class="hrcenter"></div>

                            </div>
                            <div id="rightPanel" >

                                <div id="container">


                                    <div id="bulletinwrapper">
                                        <div class="bulletinInner">

                                            <div class="bulletinColumn bulletinColumnPost">


                                                <div class="newsBox">



                                                    <a style=" text-decoration:none" class="bulletinTitle">What does the new Bangladesh safety accord entail?</a>

                                                    <p>
                                                       Yesterday (13 May) was a big day for the Bangladesh garment industry, with retailers finally signing an agreement to improve fire safety in the country's ready-made garment industry after two years of being caught in a stalemate. But what exactly does the new accord entail? Petah Marian takes a first look.
                                                    </p>
                                                    <p>
                                                        H&M was first to sign the new Accord on Fire and Building Safety in Bangladesh, kicking off a domino effect that was quickly followed by Zara owner Inditex, Primark and Tesco. Today (14 May) Marks & Spencer signed, and IndustriAll staff are hopeful that more will come on board with their support ahead of tomorrow's (15 May) deadline.
                                                    </p>
                                                    <p>
                                                        The tragedy of the Rana Plaza factory building collapse last month, which killed over 1,000 people, may have been what finally tipped brands into supporting this legally-binding pact. But the plan has been languishing for a number of years, and few industry watchers expected it to get off the ground.
                                                    </p>
                                                    
                                                    <p>
                                                        The latest accord evolved from the Bangladesh Fire and Building Safety Agreement, which was initially supported by PVH (owner of the Calvin Klein and Tommy Hilfiger brands) and German retailer Tchibo. However, PVH said in March last year that in order for the agreement to go into effect, three other international brands would have to join.
                                                    </p>
                                                    <p>
                                                        The key to the success of the latest scheme seems to be that having a unified set of standards across the Bangladesh apparel industry - so that a non-compliant factory will have difficulties producing for anyone until it achieves acceptable levels.
                                                    </p>
                                                    <p>
                                                        Concerns that the new agreement would be a watered-down version of the original one appear to be unfounded, with the two being fairly similar - although the new agreement is for five years, compared to the initial two-year plan.
                                                    </p>
                                                    <p>
                                                        The aims of the scheme are laudable, providing a framework for brands and retailers to raise the standard of factories in the country. But this is just a first step, and there are many more issues that will need to be overcome as Bangladesh looks to improve safety standards.
                                                    </p>
                                                    <p>
                                                        While the deal says brands and retailers must offer commercial terms with their suppliers to ensure it is financially feasible for factories to maintain safe workplaces and comply with upgrade and remediation requirements, they are not required to help factories find funding.
                                                    </p>
                                                    <p>
                                                        In an earlier article for just-style, Inneke Zeldenrust from the Clean Clothes Campaign estimated that it will cost around US$3bn to upgrade the more than 5,000 apparel facilities in Bangladesh - averaging out to $600,000 per factory.
                                                    </p>
                                                    <p>
                                                        One industry insider who declined to be named, said the success of the scheme would be down to the "effectiveness of the organisation", highlighting concerns around ongoing corruption in the country.
                                                    </p>
                                                    <p>
                                                        Another said that improving wages will be key to reducing corruption. However, in Bangladesh, where clothing manufacturers hold broad sway with the government, one has to wonder what appetite there will be to increase worker wages.
                                                    </p>
                                                    
                                                    <p>
                                                        <b>The agreement</b><br/>
                                                    The agreement, which was released this afternoon will be driven by a steering committee formed by the signatories. They will also appoint a safety inspector and training coordinator.
                                                    </p>
                                                    <p>
                                                        Factories will be inspected by the team created by the safety inspector. Tier one suppliers will receive safety inspections, remediation and fire safety training. Tier two factories will receive inspection and remediation, while there will be limited initial inspections at tier three factories.
                                                    </p>
                                                    <p>
                                                        When factories are found to require improvements, the signatory companies will require that supplier to carry out corrective actions that are both mandatory and time-bound.
                                                    </p>
                                                    <p>
                                                        Factories will also be required to maintain workers' employment and regular income during any period they are closed for renovations. Signatories will make reasonable efforts to ensure workers who lose their jobs as a result of lost orders at a factory are offered employment with safe suppliers.
                                                    </p>
                                                    <p>
                                                        Signatories will also require their suppliers to respect the right of a worker to refuse work that he or she has reasonable justification to believe is unsafe.
                                                    </p>
                                                    <p>
                                                        Extensive fire training will be undertaken at tier one facilities, while suppliers will need to set up health and safety committees in all factories.
                                                    </p>
                                                    <p>
                                                        <b>Transparency</b><br/>
                                                        A list of all Bangladeshi suppliers used by the signatory companies will be made public, while written inspection reports will be made available to interested parties, as will public statements by the safety inspector identifying any factory that is not acting fast enough to implement remedial recommendations.
                                                    </p>
                                                    
                                                    <p>
                                                        <b>Funding and support</b><br/>
                                                       Signatory brands will contribute a maximum $500,000 per year to pay for the steering committee, safety inspector and training coordinator. Payments will depend on each company's annual garment production in Bangladesh.
                                                    </p>
                                                    <p>
                                                        The agreement does not require signatories to contribute to bringing factories up to standard. It says that if suppliers fail to participate fully in the inspection, remediation, health and safety and, where applicable, training activities, they will be issued with a notice and warning process, leading to termination.
                                                    </p>
                                                    <p>
                                                        It also requires participating brands and retailers to negotiate commercial terms with their suppliers that ensure it is financially feasible for the factories to maintain safe workplaces and comply with upgrade and remediation requirements.
                                                    </p>
                                                    <p>
                                                        "Each signatory company may use alternative means to ensure factories have the financial capacity to comply with remediation requirements, including but not limited to joint investments, providing loans, accessing donor or government support, through offering business incentives or through paying for renovations directly," the agreement states.
                                                    </p>
                                                    <p>
                                                        Companies that sign up to the accord will commit to maintaining long-term sourcing relationships with Bangladesh over the next five years. 
                                                    </p>
                                                    <p>
                                                        For at least two years, they will continue to place business at order volumes comparable with the year preceding the agreement - assuming that such business is commercially viable, and the factory continues to meet the brand or retailer's terms.
                                                    </p>
                                                    <p>
                                                        The full Accord on Building and Fire Safety in Bangladesh is available <a target="_blank" href="http://www.uniglobalunion.org/Apps/UNINews.nsf/vwLkpById/EC90FA91A0DB11C0C1257B6B0028A4DE/$FILE/2013-05-13%20-%20Accord%20on%20Fire%20and%20Building%20Safety%20in%20Bangladesh.pdf">here</a>. 
                                                    </p>
                                                    <p><a target="_blank" href="http://www.just-style.com/">Extract taken from</a></p>


                                                </div>




                                            </div><!--bulletinColumn-->
                                            <div class="bulletinColumn noMarginRt">


                                                <!--newsBox-->


                                            </div><!--bulletinColumn-->
                                            <!--bulletinColumnRt-->
                                        </div><!--bulletinInner-->
                                    </div>



                                </div>
                                <div style="clear:both"></div>



                                <div style="clear:both"></div>


                                <div style="clear:both"></div>

                            </div>

                            <div style="clear:both"></div>

                            <!-- Container --> 
                        </div>
                        </div>

                    </body>
                    </html>



