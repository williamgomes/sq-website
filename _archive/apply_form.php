<?php
include('lib2/config.php');
$err = "";
$fname = "";
$lname = "";
$email = "";
$dob = "";
$position = "";
$nationality = "";
$phone = "";
$gender = "";
$education = "";
$extension = array();
$extension = array('doc','docx','pdf'); //valid extensions for cv




if(isset($_POST['submit'])){
  
  extract($_POST);
  $CheckEmail = mysql_query("SELECT * FROM candidate_db WHERE candidate_email='$email'");
  $CountRow = mysql_num_rows($CheckEmail);
  
  if($CountRow > 0){
    $err = "You already submitted your CV.";
  } else {
  
    if($fname == "") {
      $err = "First Name field is required.";
    } elseif($lname == "") {
      $err = "Last Name field is required.";
    } elseif($dob == "") {
      $err = "Date of Birth field is required.";
    } elseif($email == "") {
      $err = "E-mail Address field is required.";
    } elseif($phone == "") {
      $err = "Phone No. field is required.";
    } elseif($gender == "") {
      $err = "Gender field is required.";
    } elseif($nationality == "") {
      $err = "Nationality field is required.";
    } elseif(!ctype_alpha($fname)) {
      $err = "First Name can only be alphabet.";
    } elseif(!ctype_alpha($lname)) {
      $err = "Last Name can only be alphabet.";
    } elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $err = "Please provide a valid E-mail Address.";
    } elseif($_FILES["cv"]["tmp_name"] == "") {
      $err = "Please upload your CV.";
    } else {
      
      $CircularID = $_GET['circular_id'];
      
      //preparing cv for upload
      $cv_upload = basename($_FILES['cv']['name']);
      $ext = pathinfo($cv_upload, PATHINFO_EXTENSION); /* it will return me like jpeg, gif, pdf, png */
      $cv_name = $fname . '' . $lname . '.' . $ext; /* create custom image name color id will add  */
      $cv_source = $_FILES["cv"]["tmp_name"];
      
      if(!in_array($ext,$extension)){
        $err = "Your CV must be in .doc, .docx or .pdf format.";
      } else {
        
        //creating directory
        if(!is_dir('cv/')){
          mkdir('cv/',0777, TRUE);
        }
        
        $cv_target = 'cv/' . $cv_name;
        move_uploaded_file($cv_source, $cv_target);
        
        $CandidateAdd = '';
        $CandidateAdd .=' candidate_fname = "' . mysql_real_escape_string($fname) . '"';
        $CandidateAdd .=', candidate_lname = "' . mysql_real_escape_string($lname) . '"';
        $CandidateAdd .=', candidate_dob = "' . mysql_real_escape_string($dob) . '"';
        $CandidateAdd .=', candidate_phone = "' . mysql_real_escape_string($phone) . '"';
        $CandidateAdd .=', candidate_email = "' . mysql_real_escape_string($email) . '"';
        $CandidateAdd .=', candidate_gender = "' . mysql_real_escape_string($gender) . '"';
        $CandidateAdd .=', candidate_circular_id = "' . mysql_real_escape_string($CircularID) . '"';
        $CandidateAdd .=', candidate_nationality = "' . mysql_real_escape_string($nationality) . '"';
        $CandidateAdd .=', candidate_cv = "' . mysql_real_escape_string($cv_name) . '"';
        
        $CandidateSql = "INSERT INTO candidate_db SET $CandidateAdd";
        $AddCandidate = mysql_query($CandidateSql);
        if ($AddCandidate) {
          
              require("class.phpmailer.php");
              //getting email information
              $mail = new PHPMailer();
          
              $mail->IsSMTP(); // send via SMTP
            
              $mail->SMTPDebug = 1;
            
              //IsSMTP(); // send via SMTP
            
              $mail->SMTPAuth = true; // turn on SMTP authentication
            
              $mail->Username = "bluetest"; // Enter your SMTP username
            
              $mail->Password = "bluepass2012"; // SMTP password
            
              $webmaster_email = 'no-reply@sqgc.com'; //Add reply-to email address
            
              $mail->From = $webmaster_email;
            
              $mail->FromName = "SQ Group";
              $name = $fname.' '.$lname;
              $mail->AddAddress($email,$name);
              
          
              $mail->AddReplyTo($webmaster_email,"SQ Group");
            
              $mail->extension=php_openssl.dll;
            
              $mail->WordWrap = 50; // set word wrap
            
              /*$mail->AddAttachment("/var/tmp/file.tar.gz"); // attachment
            
              $mail->AddAttachment("/tmp/image.jpg", "new.jpg"); // attachment*/
            
              $mail->IsHTML(true); // send as HTML
            
              
            
              $mail->Subject = "Thank you";
            
              
              $host = $_SERVER['HTTP_HOST'];
              $parent = basename(dirname($_SERVER['PHP_SELF']));
              $location = "http://".$host."/".$parent;
              
            
              $mail->Body = '<html>
              
              '.$fname.' '.$lname.',<br><br>
              
              Thank you for submitting your resume. Our management team is reviewing your qualifications and will contact you if there is a match with our current requirements for the position you applied.<br><br>
              
              We appreciate your interest in SQ Group and wish you the best of luck in your job search.<br><br>
              
              Thank you.<br><br>
              
              SQ Group
              
            
              </html>';      //HTML Body
            
              
            
              $mail->AltBody = '<html>
              
              '.$fname.' '.$lname.',<br><br>
              
              Thank you for submitting your resume. Our management team is reviewing your qualifications and will contact you if there is a match with our current requirements for the position you applied.<br><br>
              
              We appreciate your interest in SQ Group and wish you the best of luck in your job search.<br><br>
              
              Thank you.<br><br>
              
              SQ Group
              
            
              </html>';      //HTML Body
              
              
              $error = 0;
              $success = 0;
            
              if(!$mail->Send())
            
              {
                $error ++;
              } 
            
              else 
            
              {
                $success ++;
              }
            
            
            
            
            
            
            //getting corrosponding email addresses for the post
            $SelectCircular = mysql_query("SELECT * FROM circular WHERE circular_id=$CircularID");
            $GetCircular = mysql_fetch_object($SelectCircular);
            
            $emails = explode(",", $GetCircular->circular_application_to_email);
            
            
            foreach($emails as $sendemail)
            {
              include_once("class.phpmailer.php");
              //getting email information
              $mail = new PHPMailer();
          
              $mail->IsSMTP(); // send via SMTP
            
              $mail->SMTPDebug = 1;
            
              //IsSMTP(); // send via SMTP
            
              $mail->SMTPAuth = true; // turn on SMTP authentication
            
              $mail->Username = "bluetest"; // Enter your SMTP username
            
              $mail->Password = "bluepass2012"; // SMTP password
            
              $webmaster_email = 'no-reply@sqgc.com'; //Add reply-to email address
            
              $mail->From = $webmaster_email;
            
              $mail->FromName = "SQ Group";
              
              $email = $sendemail;
              $name = $sendemail;
              
              $mail->AddAddress($email,$name);
              
          
              $mail->AddReplyTo($webmaster_email,"SQ Group");
            
              $mail->extension=php_openssl.dll;
            
              $mail->WordWrap = 50; // set word wrap
            
              /*$mail->AddAttachment("/var/tmp/file.tar.gz"); // attachment
            
              $mail->AddAttachment("/tmp/image.jpg", "new.jpg"); // attachment*/
            
              $mail->IsHTML(true); // send as HTML
            
              
            
              $mail->Subject = "New CV Submission Alert";
            
              
              $host = $_SERVER['HTTP_HOST'];
              $parent = basename(dirname($_SERVER['PHP_SELF']));
              $location = "http://".$host."/".$parent;
              
            
              $mail->Body = '<html>
              
              We have received new CV from <strong>'.$fname.' '.$lname.'</strong> for the post '.$GetCircular->circular_position.'.<br>
              
              Please use below credential to view details.<br><br>
              
              URL: <a href="'.$location.'/admin/">'.$location.'/admin/</a><br>
              Email: admin@bscheme.com<br>
              Password: 123456<br><br>
              
              Thank you.<br><br>
              
              SQ Group
              
            
              </html>';      //HTML Body
            
              
            
              $mail->AltBody = '<html>
              
              We have received new CV from <strong>'.$fname.' '.$lname.'</strong>.<br>
              
              Please use below credential to view details.<br><br>
              
              URL: <a href="'.$location.'/admin-panel/">'.$location.'/admin-panel/</a><br>
              Email: admin@bscheme.com<br>
              Password: 123456<br><br>
              
              Thank you.<br><br>
              
              SQ Group
              
            
              </html>';      //Plain Text Body
              
              $error = 0;
              $success = 0;
            
              if(!$mail->Send())
            
              {
                $error ++;
              } 
            
              else 
            
              {
                $success ++;
              }
            
              
            }
          
    
          
          
          $link = 'thank_you.php';
          redirect($link);
        } else {
          echo 'AddCandidate Error: ' . mysql_error();
          $err = "Insert Query failed.";
        }
      }
    }
  }
}



$CircularID = $_GET['circular_id'];
$SelectCircular = mysql_query("SELECT * FROM circular WHERE circular_id=$CircularID");
$GetCircular = mysql_fetch_object($SelectCircular);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"  href="css/style.css" type="text/css">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png" />
<!-- Custom scrollbars CSS -->
<title>SQ GROUP</title>
<!-- Google CDN jQuery with fallback to local -->
<script src="js/jquery.min.js"></script>
<link rel="stylesheet" href="css/responsivemobilemenu.css" type="text/css"/>
<link rel="stylesheet" href="css/forms.css" type="text/css"/>
<script type="text/javascript" src="js/responsivemobilemenu.js"></script>
<!-- date picker -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  </script>
  <!-- end date picker -->
	
  <!--[if IE 6]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	
<!--[if IE 7]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	
<!--[if IE 8]>
	<link href="css/ie" rel="stylesheet" type="text/css" />
	<![endif]-->
	
<!--[if IE 9]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->

<!--[if IE 10]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->

<!--[if IE 8]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script type="text/javascript">
if ($(window).width() < 699 ) {
    //small screen, load other JS files
    $.getScript('js/device.js', function () {
        //the script has been added to the DOM, you can now use it's code
    });
}


window.onorientationchange = function()
{
   window.location.reload();
}


</script>
</head>
<body>
<div id="topBarContainer"> </div>
<div class="mobilemenu" style="display:none">
              <div class="rmm">
                <ul>
                    <li><a href="about.html">About US</a></li>
                    <li><a href="business.html" >Business</a></li>
                    <li><a href="supportindustry.html">Supporting Industries</a></li>
                    <li><a href="sustainability.html" >Sustainability </a></li>
                    <li><a class="active" href="career.html">Careers</a></li>
                    <li><a href="bulletin.html" >Bulletin</a></li>
                    <li><a href="contact.html">Contact us</a></li>
                    </ul>
                </div>
    </div>
<div id="wrapper">

<div id="leftPanel">
                            <div class="shadowIe"></div>
                            <div class="logo">
                                <p align="center">
                                    <a href="index.html"><img width="100" src="images/logo.png"></a>
                                    <br>
                                        <span class="sus">
                                            <a href="career.php" class="sasLogoLink sasLogoLinkExtra">  
                                                Careers   </a>
                                        </span>
                                </p>
                            </div>

                            <div class="leftMenu">
                                <ul>
                                    <li><a href="about.html">About US</a></li>
                                    <li><a href="business.html">Business</a></li>
                                    <li><a href="supportindustry.html">Supporting Industries</a></li>
                                    <li><a href="sustainability.html">Sustainability </a></li>
                                    <li><a href="career.php" class="active">Careers</a></li>
                                    <li><a href="bulletin.html">Bulletin</a></li>
                                    <li><a href="contact.html">Contact us</a></li>
                                </ul>
                            </div>
                            <div class="hrcenter"></div>
                            <div class="slogan"><p align="center">
                                    <img width="140" src="images/slogan.png">
                                </p>


                            </div>

          <div class="hrcenter"></div>

          </div>
  
  <div id="rightPanel" class="rightInner ">
  
  	<div class="careerContainer">
    <div class="careerTop">
    <!--<a href="index.html"><img src="images/logo_career.png" height="100" alt="logo" /></a>-->
    </div><!--careerTop-->
    <div class="careerInner"
>     <h2>Online Job Application Form</h2>
    
    <div style="width:900px; clear:both; height:40px"></div>
<?php

if($err != ""){
  ?>    
    <div style="width: 400px; display: block; margin: 0px auto; font-size: 18px; color: rgb(204, 0, 51);">
<p align="center" style="display: block; color: rgb(255, 255, 255); background: none repeat scroll 0% 0% rgb(204, 153, 153); padding: 5px; position: relative; border: 1px solid rgb(204, 0, 51);"><?php echo $err; ?></p></div>

<?php
}
?>

<?php
if($_GET['circular_id'] != 0){
  ?>    
    <div style="display:block; width:900px; height:50px; margin:0 auto; ">
    <p class="help_text">You are applying for <strong><?php echo $GetCircular->circular_position; ?></p>
    </div>
<?php
}
  ?>
    
    <div class="sections">
    <fieldset>
    <legend title="Application Instruction">Application Instruction</legend>
    <ol class="listed">
      <li>Fill up the Personal Details form below.</li>
      <li>Upload your resume.</li>
    </ol>
    <p class="help_text">If all the boxes are not filled up, the application will not be accepted.</p>
    </fieldset>
    </div>

    <div style="clear: both;"></div>

    <form name="appsubmit" action="apply_form.php?circular_id=<?php echo $_GET['circular_id']; ?>" method="post" enctype="multipart/form-data">
    
    <div class="sections">
    <fieldset>
    <legend title="Application Instruction">Personal Details</legend>
      <div style="display:block; float:left; height:auto; width:100%; line-height:50px">
        <table>
          <tr>
        <td class="col1"><label>First Name:</label></td><td class="col2"><input type="text" name="fname" value="<?php echo $fname; ?>" /></td>
        <td class="col1"><label>Last Name:</label></td><td class="col2"><input type="text" value="<?php echo $lname; ?>" name="lname" /></td>
      </tr>
      <tr>
        <td class="col1"><label>Date of Birth:</label></td><td class="col2"><input type="text" value="<?php echo $dob; ?>" name="dob" id="datepicker"/></td>
        <td class="col1"><label>E-mail Address:</label></td><td class="col2"><input type="text" value="<?php echo $email; ?>" name="email" /></td>
      </tr>
      <tr>
        <td class="col1"><label>Phone No.:</label></td><td class="col2"><input type="text" value="<?php echo $phone; ?>" name="phone" /></td>
        <td class="col1"><label>Nationality:</label></td><td class="col2"><input type="text" value="<?php echo $nationality; ?>" name="nationality" /></td>
      </tr>
      <tr>
      	<td class="col1"><label>Gender:</label></td>
        <td class="col2"><select name="gender">
        <option value=""> -- Select Gender -- </option>
        <option value="male">Male</option>
        <option value="female">Female</option>
        </select>
        </td>
        <td class="col1"><!--<label>Last Education:</label>--></td>
        <td class="col2"><!--<select name="education">
        <option value=""> -- Select Last Education -- </option>
        <option value="College">College</option>
        <option value="Associate Degree">Associate Degree</option>
        <option value="Bachelor's Degree">Bachelor's Degree</option>
        <option value="Postgraduate">Postgraduate</option>
        <option value="Master's Degree">Master's Degree</option>
        <option value="Ph.D">Ph.D</option>
        </select>--></td>
        
      </tr>
    </table>
        
        
      
        </div>
    </fieldset>
    </div>
        
    <div style="clear: both;"></div>
    
    <div class="sections">
    <fieldset style="margin-top: 10px;">
    <legend title="Application Instruction">Upload Resume</legend>
     <table>
      <tr>
        <td class="col3"><label>Upload your CV:</</label></td>
        <td class="col4"><input type="file" name="cv" style=" margin-left:40px; width:auto !important;" /></td>
        </tr>
      </table>
    
    <p class="help_text">File can be in Word Document (.doc, .docx) or PDF (.pdf) format. Any other format is not allowed.</p>    
    </fieldset>
    </div>
    
    
    <div class="sections">
      <button type="submit" class="btn" name="submit">Submit Application</button>
    </div>
    
    </form>
</div>
    
    
    
    

    </div><!--careerInner-->
        
    </div>
    

    
    
    
    
    <!-- Container --> 
  </div>
 </div>

 

</body>
</html>