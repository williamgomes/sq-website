<?php
include 'config/config.php';

$careerSql = "SELECT * FROM circular WHERE circular_application_deadline>=DATE(NOW()) ORDER BY circular_application_deadline ASC";
$careerArray = array();
$careerSqlResult = mysqli_query($con, $careerSql);
if ($careerSqlResult) {
    if (mysqli_num_rows($careerSqlResult) > 0) {
        while ($careerSqlResultRowObj = mysqli_fetch_object($careerSqlResult)) {
            $careerArray[] = $careerSqlResultRowObj;
        }
    }
} else {
    if (DEBUG) {

        die("careerSqlResult Error: " . mysqli_error($con));
    } else {
        die("careerSqlResult Fail");
    }
}
/*

  if (isset($_REQUEST["job_type"]) && $_REQUEST["job_type"] == 'recent') {
  $careerSql = "SELECT * FROM circular ORDER BY circular_id ASC LIMIT 10";
  } else {
  $careerSql = "SELECT * FROM circular ORDER BY circular_id ASC";
  }

  $dbConnection = mysql_connect('db008.zabco.net','dba10021','17yin59');
  if(!$dbConnection){
  die('Mysql Connection fail');
  }
  mysql_select_db('db10021', $dbConnection);
  $careerArray = array();
  $careerSqlResult = mysql_query($careerSql);
  if ($careerSqlResult) {
  if (mysql_num_rows($careerSqlResult) > 0) {
  while ($careerSqlResultRowObj = mysql_fetch_object($careerSqlResult)) {
  $careerArray[] = $careerSqlResultRowObj;
  }
  }
  } else {


  die("careerSqlResult Error: " . mysql_error());

  // die("careerSqlResult Fail");

  }
 */

/* Start code for menu active*/
$pageUrlName = '';
$pageUrlName = basename(__FILE__, '.php');
$pageUrlName = $pageUrlName . '.php';
/* End code for menu active*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="SQ-Group,KNITWEAR,LINGERIE,SHIRT,RETAIL,Biriqina">
            <meta name="author" content="SQ-Group">
                <meta name="description" content="SQ visions to build an organization that would thrive on its tribal culture. After all, we all have a common story – SQ. The story of SQ, its attitude and the way it conducts its business are part of this segment. It's all about what we are, not what we have.">

                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <link rel="stylesheet"  href="css/style.css" type="text/css">
                            <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
                            <link rel="apple-touch-icon" href="apple-touch-icon.png" />
                            <link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png" />
                            <link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png" />
                            <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png" />
                            <link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png" />
                            <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png" />
                            <link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png" />
                            <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png" />
                            <link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png" />
                            <!-- Custom scrollbars CSS -->
                            <title>SQ GROUP | Career</title>
                            <!-- Google CDN jQuery with fallback to local -->
                            <script src="js/jquery.min.js"></script>
                            <link rel="stylesheet" href="css/responsivemobilemenu.css" type="text/css"/>
                            <script type="text/javascript" src="js/responsivemobilemenu.js"></script>


                            <!--[if IE 6]>
                                  <link href="css/ie.css" rel="stylesheet" type="text/css" />
                                  <![endif]-->

                            <!--[if IE 7]>
                                    <link href="css/ie.css" rel="stylesheet" type="text/css" />
                                    <![endif]-->

                            <!--[if IE 8]>
                                    <link href="css/ie" rel="stylesheet" type="text/css" />
                                    <![endif]-->

                            <!--[if IE 9]>
                                    <link href="css/ie.css" rel="stylesheet" type="text/css" />
                                    <![endif]-->

                            <!--[if IE 10]>
                                    <link href="css/ie.css" rel="stylesheet" type="text/css" />
                                    <![endif]-->

                            <!--[if IE 8]>
                                    <link href="css/ie.css" rel="stylesheet" type="text/css" />
                            <![endif]-->
                            <script>
                                (function(i, s, o, g, r, a, m) {
                                    i['GoogleAnalyticsObject'] = r;
                                    i[r] = i[r] || function() {
                                        (i[r].q = i[r].q || []).push(arguments)
                                    }, i[r].l = 1 * new Date();
                                    a = s.createElement(o),
                                            m = s.getElementsByTagName(o)[0];
                                    a.async = 1;
                                    a.src = g;
                                    m.parentNode.insertBefore(a, m)
                                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                                ga('create', 'UA-43689390-1', 'sqgc.com');
                                ga('send', 'pageview');

                            </script>
                            </head>
                            <body>
                                <div id="topBarContainer"> </div>
                                <div class="mobilemenu" style="display:none">
                                    <div class="rmm">
                                        <?php include 'home_left_menu.php'; ?>
                                    </div>
                                </div>
                                <div id="wrapper">

                                    <div id="leftPanel">
                                        <div class="shadowIe"></div>
                                        <div class="logo">
                                            <p align="center">
                                                <a href="index.php"><img width="100" src="images/logo.png" /></a>
                                                <br>
                                                    <span class="sus">
                                                        <a class="sasLogoLink sasLogoLinkExtra" href="career.php">  
                                                            Careers   </a>
                                                    </span>
                                            </p>
                                        </div>

                                        <div class="leftMenu">
                                            <?php include 'home_left_menu.php'; ?>
                                        </div>
                                        <div class="hrcenter"></div>
                                        <div class="slogan"><p align="center">
                                                <img width="140" src="images/slogan.png" />
                                            </p>


                                        </div>

                                        <div class="hrcenter"></div>

                                    </div>

                                    <div id="rightPanel" class="rightInner ">

                                        <div class="careerContainer">
                                            <div class="careerTop">
                                            <!--<a href="index.html"><img src="images/logo_career.png" height="100" alt="logo" /></a>-->
                                            </div><!--careerTop-->
                                            <div class="careerInner">

                                                <p>Bangladesh clothing industry is considered to be one of the likely successors to China's crown as the World's ultimate manufacturing destination. This is tough but definitely a break. We want to strategically align and shape up SQ for manipulating such prospect. <br />
                                                    <br />

                                                    SQ brings together a diverse and talented workforce. We provide our team the resources and opportunities they need to make the impact. If you have a natural motivation to challenge the status-quo and Stand Tall, we invite you to join us for a rewarding career and share our passion for growth and success.
                                                </p>

                                                <div style="width:100%; height:20px; clear:both"></div>

                                                <div class="careerLeft">
                                                    <div class="careerLeftInner">
                                                        <p style="font-size:15px; color:#fff; padding-top:15px;">If you do not meet the requirements of the job vacancies available now, but are interested to work at SQ, please drop your resume <a style="color:#ddd;" href="apply_form.php?circular_id=0">here</a>. It will remain in our database and we will contact you if anything suitable comes along.</p>
                                                    </div>
                                                </div><!--careerLeft-->
                                                <div class="careerRight">
                                                    <table width="100%" border="1" class="sqCareer">
                                                        <tr class="careerHeading">
                                                            <th width="40%"><strong>Title</strong></th>
                                                            <th width="15%"><strong>End Date</strong></th>
                                                            <th width="15%"><strong>Location</strong></th>
                                                            <th width="30%"><strong>Business units</strong></th>
                                                        </tr>
                                                        <?php
                                                        $careerArrayCount = count($careerArray);
                                                        for ($i = 0; $i < $careerArrayCount; $i++) {
                                                            if ($i % 2 != 0) {
                                                                echo '<tr class="jobDetails odd">';
                                                            } else {
                                                                echo '<tr class="jobDetails">';
                                                            }
                                                            echo '<td><a href="career_details.php?circular_id=' . $careerArray[$i]->circular_id . '">' . $careerArray[$i]->circular_position . '</a></td>';
                                                            $date = date_create($careerArray[$i]->circular_application_deadline . " 17:45:12");
                                                            $ApplicationDeadLine = date_format($date, 'jS M Y');
                                                            echo "<td>" . $ApplicationDeadLine . "</td>";
                                                            echo "<td>" . $careerArray[$i]->circular_location . "</td>";
                                                            echo "<td>" . $careerArray[$i]->circular_business_unit . "</td>";
                                                        }
                                                        ?>
                                                    </table>

                                                </div><!--careerRight-->
                                            </div><!--careerInner-->

                                        </div>
                                        <!-- Container --> 
                                    </div>
                                </div>
                            </body>
                            </html>