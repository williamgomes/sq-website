<?php

class Category {

    //The connection to a MySQL server
    //var $dbConnection = null;
    //The categories as an array
    private $con = null;
    public $parent_id="0";
    var $categories = array();
    var $inputType = 'checkbox';
    var $checked = array();
   

    function __construct($con) {
       // $this->Category($cnn);
        $this->con =$con;
    }

   /*function Category($cnn) {
        //Initialize the connection and the categories
        $this->dbConnection = $cnn;
        $this->categories = array();
        $this->getCategories();
    }*/

   /* function getCategories() {
        //Read the records and fill the categories
        $query = "SELECT * FROM categories ORDER BY category_id";
        $result = mysqli_query($this->dbConnection,$query) or die(mysqli_error($this->dbConnection));

        $i = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $this->categories[$i] = $row;
            $i++;
        }
        //Free the resource
        mysqli_free_result($result);
    }*/
/* start new implemented viewTree function */

/*function viewTree($parent=0)
{
     $menu ='';

     echo $menuSql = "SELECT a.category_id, a.category_name, Deriv1.Count FROM `categories` a LEFT OUTER JOIN (SELECT category_parent_id, COUNT(*) AS Count FROM `categories` GROUP BY category_parent_id) Deriv1 ON a.category_id = Deriv1.category_parent_id WHERE a.category_parent_id=" . $parent;
  
    $menuSqlResult = mysqli_query($this->con, $menuSql);
     
            $menu .="<ul>";
            while ($menuSqlResultRowObj = mysqli_fetch_object($menuSqlResult)) {
                if (($menuSqlResultRowObj->Count) > 0) {
                    $menu .='<li>';
                    $menu .='<a href="index.php?id=' . $menuSqlResultRowObj->category_id . '">' . $menuSqlResultRowObj->category_name . '</a>';
                    $menu .= $this->viewTree($menuSqlResultRowObj->category_id);
                    $menu .='</li>';
                } elseif ($menuSqlResultRowObj->Count == 0) {

                    $menu .='<li>';
                    $menu .='<a href="index.php?id=' . $menuSqlResultRowObj->category_id . '">' . $menuSqlResultRowObj->category_name . '</a>';
                    $menu .='</li>';
                }
            }
            $menu .= "</ul>";
            echo $menu;
            exit();
            //return $menu;

}*/

/*============= end viewTree function========== */
    function viewTree() {
        //Generate tree list
        $output = '';
        for ($i = 0; $i < count($this->categories); $i++) {
            if ($this->categories[$i]["category_parent_id"] == "0") {
                $output .= "<li title=\"" . $this->categories[$i]["category_description"] . "\">";
                $output .= "<a></a>";
                
                if (in_array($this->categories[$i]["category_id"], $this->checked)) {
                    $output .='<input checked="checked" type="' . $this->inputType . '" name="categories[]" value="' . $this->categories[$i]["category_id"] . '"/>';
                } else {
                    $output .='<input type="' . $this->inputType . '" name="categories[]" value="' . $this->categories[$i]["category_id"] . '"/>';
                }

                $output.= $this->categories[$i]["category_name"] . "\n<ul>\n";
                $output .= $this->getAllChildren($this->categories[$i]["category_id"]);
                $output .= "\n</ul>\n</li>";
            }
        }
        return $output;
    } 
    /* ============= Start Creating Tree View ================ */
    function buildtree($src_arr, $parent_id = 0, $tree = array())
    {
        foreach($src_arr as $idx => $row)
        {
                foreach($row as $k => $v)
                $tree[$row['category_id']][$k] = $v;
        }
        ksort($tree);
        return $tree;
    
    }
    
    function r( $a, $parent_id) {
               $r = '' ;
               foreach ( $a as $i ) {
                   if ($i['category_parent_id'] == $parent_id ) {
                      $r .= "<li><in" . $i['category_name'] . $this->r( $a, $i['category_id'] ) . "</li>";
                   }
               }
               return ($r==''?'':"<ul>". $r . "</ul>");
    }

    function viewTree()
    {
        $query ="SELECT category_id,category_name,category_parent_id FROM categories";
        $result =mysqli_query($this->con,$query);
        $data = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $data[]= $row;
        }
       return $this->r($this->buildtree($data),$this->parent_id);
    }

   //==================== End creating Tree view =================================//

    function getAllChildren($parent_id, $inputType = 'checkbox') {
        //Get all the nodes for particular ID
        $output = "";
        for ($i = 0; $i < count($this->categories); $i++) {
            if ($this->categories[$i]["category_parent_id"] == $parent_id) {
                $output .= "<li title=\"" . $this->categories[$i]["category_description"] . "\">";
                $output .= "<a></a>";
                if (in_array($this->categories[$i]["category_id"], $this->checked)) {
                    $output .='<input checked="checked"  type="' . $this->inputType . '" name="categories[]" value="' . $this->categories[$i]["category_id"] . '"/>';
                } else {
                    $output .='<input type="' . $this->inputType . '" name="categories[]" value="' . $this->categories[$i]["category_id"] . '"/>';
                }

                $output .=$this->categories[$i]["category_name"] . "\n<ul>\n";
                $output .= $this->getAllChildren($this->categories[$i]["category_id"]);
                $output .= "\n</ul>\n</li>";
            }
        }
        return $output;
    }

}

?>