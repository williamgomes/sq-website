<?php


/* ======================================================= EMAIL FUNCTIONS START =================================================== */

/**
 * This function used to send emails using PHP Mailer Class<br> 
 * @input: $UserEmail, $UserName, $ReplyToEmail, $EmailSubject, $EmailBody; return true/$status(if error)
 * used for signup.php 
 */
function sendEmailFunction($UserEmail = '', $UserName = '', $ReplyToEmail = '', $EmailSubject = '', $EmailBody = '') {

    global $config, $con;
	
	
	$options = array();
	$fields = "'SMTP_SERVER_ADDRESS','SMTP_PORT_NO','HOSTING_ID','HOSTING_PASS','EMAIL_ADDRESS_GENERAL','EMAIL_ADDRESS_GENERAL'";
	$susFieldsSql = "SELECT CS_option,CS_value FROM config_settings WHERE CS_option IN ($fields)";
	$susFieldsResult = mysqli_query($con, $susFieldsSql);
	if ($susFieldsResult) {
		while ($susFieldsResultRowObj = mysqli_fetch_object($susFieldsResult)) {
			$options[$susFieldsResultRowObj->CS_option] = $susFieldsResultRowObj->CS_value;
		}
	} else {
		if (DEBUG) {
			$err = "susFieldsResult error" . mysqli_error($con);
		} else {
			$err = "susFieldsResult query failed";
		}
	}

	
    $status = '';

    if ($ReplyToEmail == '') {
        $ReplyToEmail = 'noreply@webmaster.com';
    }


    if ($UserEmail == '' OR $UserName == '' OR $EmailSubject == '' OR $EmailBody == '') {

        $status = "Parameters missing.";
    } else {

        require_once(basePath("lib/email/class.phpmailer.php"));
        $mail = new PHPMailer();
        $mail->Host =$options['SMTP_SERVER_ADDRESS'];
        $mail->Port = $options['SMTP_PORT_NO'];
        $mail->SMTPSecure = 'ssl';
        $mail->IsSMTP(); // send via SMTP
        $mail->SMTPDebug = 0;
        //IsSMTP(); // send via SMTP
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->Username = $options['HOSTING_ID']; // Enter your SMTP username
        $mail->Password = $options['HOSTING_PASS']; // SMTP password
        $webmaster_email = $options['EMAIL_ADDRESS_GENERAL']; //Add reply-to email address
        $email = $UserEmail; // Add recipients email address
        $name = $UserName; // Add Your Recipient's name
        $mail->From = $options['EMAIL_ADDRESS_GENERAL'];
        $mail->FromName = $options['EMAIL_ADDRESS_GENERAL'];
        $mail->AddAddress($email, $name);
        $mail->AddReplyTo($ReplyToEmail, "Webmaster");
        //$mail->extension=php_openssl.dll;
        $mail->WordWrap = 50; // set word wrap
        /* $mail->AddAttachment("/var/tmp/file.tar.gz"); // attachment
          $mail->AddAttachment("/tmp/image.jpg", "new.jpg"); // attachment */
        $mail->IsHTML(true); // send as HTML
        $mail->Subject = $EmailSubject;
        $mail->Body = $EmailBody;
        $mail->AltBody = $mail->Body;     //Plain Text Body

        if (!$mail->Send()) {
            $status = "Email sending failed.";
        } else {
           
        }
    }

    if ($status == '') {
        return true;
    } else {
        return $status;
    }
}



/* ======================================================= EMAIL FUNCTIONS END =================================================== */
?>