/*
 * jQuery cutoms Effect 
 	Tanim Ahmed 6/3/2013
*/

 $(document).ready(function(){
	
	// start footer animation

	$('.toggleNav').click(function(){
    $(".topMenuExpand").slideToggle('5000', "easeOutSine",function(){
		
 if($(this).css('display')=='none'){
      $(".show_hide").removeClass("icon-minus-sign").addClass("icon-plus-sign");
	  $(".footerBar").removeClass("bgcolor12").addClass("bgcolor11");
	}
 else{
	  $(".show_hide").removeClass("icon-plus-sign").addClass("icon-minus-sign");
	  $(".footerBar").removeClass("bgcolor11").addClass("bgcolor12");
		 }
		});
    });
	
	 $(".closemenu").click(function(){
   $(".topMenuExpand").slideUp("slow");
  });
  

$(".menulist a").click(function(){
   $(".topMenuExpand").slideUp("slow"); 
  });
	// cart  animation start here
	
$(".cart").click(function () {
		$(".cartDropDown").slideToggle(function(){
			
			 if($(this).css('display')=='none'){
		  $(".cartContentInner").removeClass("cartbgreds inHide").addClass("cartbg ");
		  $(".cart").removeClass("cartnoanime").addClass("cartanime ");
		  $(".cartContentMouseover").removeClass("cartContentMouseoverHeight").addClass("cartContentMouseovernormal "			 		);
	 }
 
	 else{
		  $(".cartContentInner").removeClass("cartbg").addClass("cartbgred inHide");
		  $(".cartContentMouseover").removeClass(" cartContentMouseovernormal").addClass("cartContentMouseoverHeight ");
		  $(".cart").removeClass("cartanime").addClass("cartnoanime");
	 }
	});
	
		

 $(".cartContentMouseoverx ").mouseout(function(){
   $(".cartDropDown").slideUp("slow"); 
  });

				 });

});

// windows scroll function

(function($){
			$(window).load(function(){
				$(".innerScroll").mCustomScrollbar(
				{
				//theme:"dark".
				theme:"dark"	,
				mouseWheelPixels: "400"
				}
				);
				$(".innerScrollfaq").mCustomScrollbar(
				{
				//theme:"dark".
				theme:"dark"	,
				mouseWheelPixels: "382"
				}
				);
				
			});
		})(jQuery);