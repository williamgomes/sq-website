<?php
include './config/config.php';
$pageName = basename(__FILE__);
$firstBImage = '';
/* Start: page content icon query */
$shortIcon = array();
$shortIconSql = "SELECT * FROM home WHERE home_status='active' ORDER BY home_priority DESC";
$shortIconSqlResult = mysqli_query($con, $shortIconSql);
if ($shortIconSqlResult) {
    $i = 0;
    while ($shortIconSqlResultRowObj = mysqli_fetch_object($shortIconSqlResult)) {
        $shortIcon[] = $shortIconSqlResultRowObj;
        if ($i == 0) {
            $firstBImage = $shortIconSqlResultRowObj->home_b_image;
        }
        $i++;
    }
    mysqli_free_result($shortIconSqlResult);
} else {
    if (DEBUG) {
        echo 'shortIconSqlResult Error: ' . mysqli_error($con);
    } else {
        echo 'shortIconSqlResult Fail';
    }
}
/* End: page content icon query */
//printDie($shortIcon, TRUE);
$circularArray = array();
$circularSql = "SELECT * FROM circular WHERE circular_application_deadline>=DATE(NOW()) AND circular_home_show_status='yes' ORDER BY circular_application_deadline ASC LIMIT 6";
$circularSqlResult = mysqli_query($con, $circularSql);
if ($circularSqlResult) {
    while ($circularSqlResultRowObj = mysqli_fetch_object($circularSqlResult)) {
        $circularArray[] = $circularSqlResultRowObj;
    }
    mysqli_free_result($circularSqlResult);
} else {
    if (DEBUG) {
        echo 'circularSqlResult Error : ' . mysqli_error($con);
    }
}

$homeCircularTitle = get_option('HOME_CAREER_TITLE');


/* Start code for menu active*/
$pageUrlName = '';
$pageUrlName = basename(__FILE__, '.php');
$pageUrlName = $pageUrlName . '.php';
/* End code for menu active*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo $config['CONFIG_SETTINGS']['SITE_DEFAULT_META_TITLE']; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="<?php echo $config['CONFIG_SETTINGS']['SITE_DEFAULT_META_KEYWORDS']; ?>">
            <meta name="author" content="SQ-Group">
                <meta name="description" content="<?php echo $config['CONFIG_SETTINGS']['SITE_DEFAULT_META_DESCRIPTION']; ?>">


                    <?php include 'header.php'; ?>
                    <style type="text/css">
                        .con2, .con1 {
                            background:url(images/loader.gif) center center no-repeat !important;
                        }
                    </style>


                    <script type="text/javascript">
                        var isMobile = function() {
                            //console.log("Navigator: " + navigator.userAgent);
                            return /(iphone|ipod|ipad|android|blackberry|windows ce|palm|symbian)/i.test(navigator.userAgent);
                        };

                        if (isMobile()) {
                            window.location.href = "mindex.php";
                        }
                    </script>
                    </head>
                    <body>
                        <div id="topBarContainer"> </div>

                        <div class="mobilemenu" style="display:none">
                            <div class="rmm">
                                <?php include 'home_left_menu.php'; ?>
                            </div>
                        </div>

                        <div id="wrapper">
                            <div id="leftPanel" class="leftPanelHome">
                                <div class="logo">
                                    <p align="center">
                                        <a href="<?php echo baseUrl(); ?>"><img width="100" src="<?php echo baseUrl(); ?>images/logo.png" /></a>
                                    </p>
                                </div>

                                <div class="leftMenu">
                                    <?php include 'home_left_menu.php'; ?>
                                </div>

                                <div class="hrcenter"></div>
                                <div class="slogan"><p align="center">
                                        <img width="140" src="<?php echo baseUrl(); ?>images/slogan.png" />
                                    </p>

                                </div>

                                <div class="hrcenter"></div>

                            </div>
                            <div id="rightPanel">
<div class="quickReadPanel"><p>Introducing <img style="margin: -8px 0;" src="<?php echo baseUrl(); ?>images/quick-read-hm.png" alt="read" /> A button that does exactly what it says, lets you have a quick read of the website.</p></div>
                                
                             
                                
                                <?php $shortIconCounter = count($shortIcon); ?>
                                <?php if ($shortIconCounter > 0): ?>
                                    <!--                              Start:  first container -->
                                    <div id="container">

                                        <div class="leftWidth">
                                            <div class="con2 fimg1">
                                                <a href="<?php echo $shortIcon[0]->home_b_url; ?>"><img src="images/trans1.png" alt="<?php echo $shortIcon[0]->home_title; ?> Large image " /></a>
                                            </div>
                                        </div>

                                        <div class="rightWidth">
                                            <div class="con1">
                                                <img class="imgOpa" src="<?php echo baseUrl("upload/home/a_top_image/large/" . $shortIcon[0]->home_a_top_image); ?>" alt="<?php echo $shortIcon[0]->home_title; ?> Small Top image " />
                                                <div class="arrow"> </div>
                                            </div>

                                            <div style="clear:both"></div>
                                            <div class="con1">
                                                <a href="<?php echo $shortIcon[0]->home_a_bottom_url; ?>"><img class="" src="<?php echo baseUrl("upload/home/a_bottom_image/large/" . $shortIcon[0]->home_a_bottom_image); ?>" alt="<?php echo $shortIcon[0]->home_title; ?> Small Bottom image " /></a>
                                            </div>

                                        </div>


                                    </div>

                                    <!--                               End first container -->
                                    <div style="clear:both"></div>


                                    <?php for ($i = 1; $i < $shortIconCounter; $i++): ?>
                                        <?php if (($i + 1) % 2 == 0): ?>
                                            <!--                                    Start: Left Large Image Box-->
                                            <div id="container">
                                                <div class="left">
                                                    <div class="con1">
                                                        <img class="imgOpa" src="<?php echo baseUrl("upload/home/a_top_image/large/" . $shortIcon[$i]->home_a_top_image); ?>" alt="<?php echo $shortIcon[$i]->home_title; ?> Small Top image " />
                                                        <div class="arrow"> </div>
                                                    </div>

                                                    <div style="clear:both"></div>

                                                    <div class="con1">


                                                        <a href="<?php echo $shortIcon[$i]->home_a_bottom_url; ?>"><img class="" src="<?php echo baseUrl("upload/home/a_bottom_image/large/" . $shortIcon[$i]->home_a_bottom_image); ?>" alt="<?php echo $shortIcon[$i]->home_title; ?> Small Bottom image " /></a>


                                                    </div>

                                                </div>

                                                <div class="right">
                                                    <div class="con2 fimg2a">
                                                        <a href="<?php echo $shortIcon[$i]->home_b_url; ?>"><img src="<?php echo baseUrl("upload/home/b_image/large/" . $shortIcon[$i]->home_b_image); ?>" alt="<?php echo $shortIcon[$i]->home_title; ?> large image " /></a>
                                                    </div>

                                                </div>

                                            </div>

                                            <!--                                    End: Left Large Image Box-->
                                        <?php else: /* (($i+1)%2==0) */ ?>    
                                            <!--                                    Start: Right Large Image Box-->


                                            <div id="container">
                                                <div class="leftWidth">
                                                    <div class="con2">
                                                        <a href="<?php echo $shortIcon[$i]->home_b_url; ?>"><img src="<?php echo baseUrl("upload/home/b_image/large/" . $shortIcon[$i]->home_b_image); ?>" alt="<?php echo $shortIcon[$i]->home_title; ?> large image " /></a>
                                                    </div>
                                                </div>

                                                <div class="rightWidth">
                                                    <div class="con1">
                                                        <img class="imgOpa" src="<?php echo baseUrl("upload/home/a_top_image/large/" . $shortIcon[$i]->home_a_top_image); ?>" alt="<?php echo $shortIcon[$i]->home_title; ?> Small Top image " />
                                                        <div class="arrow"> </div>
                                                    </div>

                                                    <div style="clear:both"></div>

                                                    <div class="con1">
                                                        <a href="<?php echo $shortIcon[$i]->home_a_bottom_url; ?>"><img class="" src="<?php echo baseUrl("upload/home/a_bottom_image/large/" . $shortIcon[$i]->home_a_bottom_image); ?>" alt="<?php echo $shortIcon[$i]->home_title; ?> Small Bottom image " /></a>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--                                    End: Right Large Image Box-->

                                        <?php endif; /* (($i+1)%2==0) */ ?>    

                                        <div style="clear:both"></div>

                                    <?php endfor; /* ($i=0; $i < $shortIconCounter; $i++) */ ?>

                                <?php endif; /* ($shortIconCounter > 0) */ ?>


                                <?php
                                $circularArrayCount = count($circularArray);
                                if ($circularArrayCount > 0) {
                                    ?>

                                    <div id="creerHomeContainer">
                                        <div class="containerBottomCarrerInner">
                                            <table class="jobNotice"> 
                                                <tr>
                                                    <td class="borDerRightTd"> 
                                                        <div class="hireText"> <?php echo html_entity_decode($homeCircularTitle);?> <br />
                                                            </div>

                                                    </td>

                                                    <td> 
                                                        <?php
                                                        for ($i = 0; $i < $circularArrayCount; $i++) {
                                                            echo '<a href="' . baseUrl() . 'career_details.php?circular_id=' . $circularArray[$i]->circular_id . '">' . $circularArray[$i]->circular_position . '</a> <br />';
                                                        }
                                                        ?>
                                                    </td>

                                                </tr> 

                                            </table>

                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <div style="clear:both"></div>



                            <!-- Container --> 
                        </div>
                        </div>
                        <script type="text/javascript" src="<?php echo baseUrl(); ?>js/jquery.mCustomScrollbar.concat.min.js"></script> 

                                <script type="text/javascript">                             (function($) {
                        $(window).load(function() {

                    $("#leftPanel").mCustomScrollbar({
                        scrollButtons: {                                             enable: true
                        },
                        theme: "dark"                                     });

                    $("body,.content").mCustomScrollbar({
                    scrollButtons: {
                    enable: true
                    },
                                theme: "dark",
                                mouseWheelPixels: "600",
                });
});
                    })(jQuery);
                        </script> 




                        <script type="text/javascript" src="<?php echo baseUrl(); ?>js/jquery.backstretch.js"></script>

                        <script type="text/javascript">
                            //Initial load of page
                            $(document).ready(sizeContent);

                            //Every resize of window
                            $(window).resize(sizeContent);

                            //Dynamically assign height
                            function sizeContent() {
                                var newHeight = $("html").height() + "px";

                                if ($(window).width() > 699) {
                                    //small screen, load other JS files
                                    var newHeight = $("html").height() + "px";
                                    var newWidth = $("html").width() - $(".mCSB_scrollTools").width() - $("#leftPanel").width() + "px";

                                }

                                if ($(window).width() < 699) {
                                    //small screen, load other JS files
                                    var newHeight = $("html").height() + "px";
                                    var newWidth = $("html").width() + "px";

                                }

                                $("#rightPanel").css("width", newWidth);
                                $(".quickReadPanel").css("width", newWidth);
                                $("#container").css("height", newHeight);
                                $(".fimg1").backstretch("<?php echo baseUrl('upload/home/b_image/large/' . $firstBImage); ?>", {fade: 500});



                            }
                        </script>


<script type="text/javascript">
$('.quickReadPanel').delay(3000).animate(
    
   { opacity: 1 }, // what we are animating
   {
    duration: '1500', // how fast we are animating
   // easing: 'easeOutQuad', // the type of easing
    complete: function() { // the callback
     //alert('done');
    }
   });
</script>


                    </body>
                    </html>