<?php
include '../config/config.php';
$options = array();
$fields = "'BUSINESS_INTRO_BANNER_TITLE','BUSINESS_INTRO_BANNER','BUSINESS_INTRO_DESCRIPTION'";
$businessFieldsSql = "SELECT CS_option,CS_value FROM config_settings WHERE CS_option IN ($fields)";
$businessFieldsResult = mysqli_query($con, $businessFieldsSql);
if ($businessFieldsResult) {
    while ($businessFieldsResultRowObj = mysqli_fetch_object($businessFieldsResult)) {
        $options[$businessFieldsResultRowObj->CS_option] = $businessFieldsResultRowObj->CS_value;
    }
} else {
    if (DEBUG) {
        $err = "businessFieldsResult error" . mysqli_error($con);
    } else {
        $err = "businessFieldsResult query failed";
    }
}
$pageUrlName = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>SQ GROUP | Business</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="SQ-Group,KNITWEAR,LINGERIE,SHIRT,RETAIL,Biriqina">
            <meta name="author" content="SQ-Group">
                <meta name="description" content="SQ visions to build an organization that would thrive on its tribal culture. After all, we all have a common story – SQ. The story of SQ, its attitude and the way it conducts its business are part of this segment. It's all about what we are, not what we have.">

                    <?php include("business_header.php"); ?>
                    </head>
                    <body class="productbody">
                        <div id="topBarContainer"> </div>
                        <div class="mobilemenu" style="display:none">
                            <div class="rmm">
                                <?php include("business_left_navigation.php"); ?>
                            </div>
                        </div>
                        <div id="wrapper">
                            <div id="leftPanel">
                                <div class="shadowIe">
                                </div>
                                <div class="logo">
                                    <p align="center">
                                        <a href="<?php echo baseUrl('index.php') ?>"><img width="100" src="<?php echo baseUrl('images/logo.png') ?>" /></a>
                                        <br />
                                        <span class="sus">
                                            <a class="sasLogoLink" href="<?php echo baseUrl('business.php') ?>"> business </a>
                                        </span>
                                    </p>
                                </div>
                                <div class="leftMenu">
                                    <?php
                                    include("business_left_navigation.php");
                                    ?>
                                </div>
                                <div class="hrcenter"></div>
                                <div class="slogan"><p align="center">
                                        <img width="140" src="<?php echo baseUrl('images/slogan.png') ?>" />
                                    </p>
                                </div>
                                <div class="hrcenter"></div>
                            </div>
                            <div id="rightPanel">
                                <div class="contentbox">
                                    <div class="prxtop1 businessBg" style="background:url(<?php echo baseUrl('upload/business/intro/' . $options['BUSINESS_INTRO_BANNER']); ?>) top  fixed">
                                        <h2 class="titleXlarge titleMLarge shadowTxt">
                                            <?php
                                            if (isset($options["BUSINESS_INTRO_BANNER_TITLE"])) {
                                                echo $options["BUSINESS_INTRO_BANNER_TITLE"];
                                            } else {
                                                echo '';
                                            }
                                            ?></h2>
                                    </div>
                                    <div class="prxtop2">
                                        <div class="proInner">
                                            <div class="introContent">
                                                <?php
                                                if (isset($options["BUSINESS_INTRO_DESCRIPTION"])) {
                                                    echo html_entity_decode($options["BUSINESS_INTRO_DESCRIPTION"]);
                                                } else {
                                                    echo '';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </div>
                        </div> <!-- wrapper --> 
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $(window).bind('scroll', function(e) {
                                    parallaxScroll();
                                });
                                function parallaxScroll() {
                                    var scrolledY = $(window).scrollTop();
                                    $('.prxtop1').css('background-position', 'center -' + ((scrolledY * 0.2)) + 'px');
                                    $('.titleXlarge').css('marginTop', '-' + ((scrolledY * 0.5)) + 'px');
                                    $('.fish').css('top', '-' + ((scrolledY * 0.8)) + 'px');
                                }

                            });
                        </script>
                        <!-- Here we want a DYNAMIC value -->
                    </body>
                    </html>