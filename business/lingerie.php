<?php
include '../config/config.php';
$options = array();
$fields = "'LINGERIE_DESCRIPTION','LINGERIE_TITLE','LINGERIE_BANNER_TITLE','LINGERIE_BANNER','LINGERIE_QUICKREAD','LINGERIE_BOTTOM_IMAGE_URL'";
$businessFieldsSql = "SELECT CS_option,CS_value FROM config_settings WHERE CS_option IN ($fields)";
$businessFieldsResult = mysqli_query($con, $businessFieldsSql);
if ($businessFieldsResult) {
    while ($businessFieldsResultRowObj = mysqli_fetch_object($businessFieldsResult)) {
        $options[$businessFieldsResultRowObj->CS_option] = $businessFieldsResultRowObj->CS_value;
    }
} else {
    if (DEBUG) {
        $err = "businessFieldsResult error" . mysqli_error($con);
    } else {
        $err = "businessFieldsResult query failed";
    }
}
$slideArray = array();
$slideSql = "SELECT AI_album_id,AI_image_name FROM album_images WHERE AI_album_id IN (SELECT album_id FROM album WHERE album_keyword='LINGERIE') ORDER BY AI_image_priority DESC";
$slideResult = mysqli_query($con, $slideSql);
if ($slideResult) {
    while ($slideResultRowObj = mysqli_fetch_object($slideResult)) {
        $slideArray[] = $slideResultRowObj->AI_image_name;
        $slide_album_id = $slideResultRowObj->AI_album_id;
    }
} else {
    if (DEBUG) {
        echo'slideResult Error' . mysqli_error($con);
    } else {
        echo'Slide Result Error';
    }
}
/* Start code for menu active */
$pageUrlName = '';
$pageUrlName = basename(__FILE__, '.php');
$pageUrlName = $pageUrlName . '.php';
/* End code for menu active */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <title>SQ GROUP | Business : Lingerie</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="SQ-Group,KNITWEAR,LINGERIE,SHIRT,RETAIL,Biriqina">
            <meta name="author" content="SQ-Group">
                <meta name="description" content="SQ visions to build an organization that would thrive on its tribal culture. After all, we all have a common story – SQ. The story of SQ, its attitude and the way it conducts its business are part of this segment. It's all about what we are, not what we have.">

                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <link rel="stylesheet"  href="<?php echo baseUrl(); ?>css/style.css" type="text/css">
                            <link rel="shortcut icon" href="<?php echo baseUrl(); ?>favicon.ico" type="image/x-icon" />
                            <link rel="apple-touch-icon" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon.png" />
                            <link rel="apple-touch-icon" sizes="57x57" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-57x57.png" />
                            <link rel="apple-touch-icon" sizes="60x60" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-60x60.png" />
                            <link rel="apple-touch-icon" sizes="72x72" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-72x72.png" />
                            <link rel="apple-touch-icon" sizes="76x76" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-76x76.png" />
                            <link rel="apple-touch-icon" sizes="114x114" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-114x114.png" />
                            <link rel="apple-touch-icon" sizes="120x120" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-120x120.png" />
                            <link rel="apple-touch-icon" sizes="144x144" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-144x144.png" />
                            <link rel="apple-touch-icon" sizes="152x152" href="<?php echo baseUrl(); ?>images/apple-touch-icon/apple-touch-icon-152x152.png" />

                            <!-- Custom scrollbars CSS -->
                            <title>SQ GROUP</title>


                            <!-- Google CDN jQuery with fallback to local -->
                            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
                            <link rel="stylesheet"  href="<?php echo baseUrl(); ?>css/modal-sg2.css" type="text/css">
                                <link href="<?php echo baseUrl(); ?>owl-carousel/owl.carousel.css" rel="stylesheet">
                                    <link href="<?php echo baseUrl(); ?>owl-carousel/owl.theme-sg.css" rel="stylesheet">
                                        <link rel="stylesheet" href="<?php echo baseUrl(); ?>css/responsivemobilemenu.css" type="text/css"/>
                                        <script type="text/javascript" src="<?php echo baseUrl(); ?>js/responsivemobilemenu.js"></script>


                                        <script type="text/javascript">
                                            if ($(window).width() < 699) {
                                                //small screen, load other JS files
                                                $.getScript('../js/device.js', function() {
                                                    //the script has been added to the DOM, you can now use it's code
                                                });
                                            }

                                            window.onorientationchange = function()
                                            {
                                                window.location.reload();
                                            }

                                        </script>



                                        <script src="<?php echo baseUrl(); ?>js/jquery.easing.1.3.js"></script>
                                        <script type="text/javascript" src="<?php echo baseUrl(); ?>js/jquery.cycle.all.js"></script>
                                        <!-- Script From Jquery.Cycle.all for Category slideshow -->
                                        <script type="text/javascript">
                                            $(document).ready(function() {

                                                $('.prokSideContent') //< !--.before('<div class="pgr2">') -- >
                                                        .cycle({
                                                            fx: 'scrollHorz',
                                                            easeIn: 'easeOutExpo', //easeOutQuart
                                                            easeOut: 'easeOutExpo',
                                                            speed: 1500,
                                                            timeout: 0, // auto slide
                                                            next: '.next2',
                                                            prev: '.prev2',
                                                            pause: 1, //pager:  '#pgr2' 
                                                        });
                                            });</script>


                                        <!--[if IE 6]>
                                              <link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
                                              <![endif]-->

                                        <!--[if IE 7]>
                                                <link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
                                                <![endif]-->

                                        <!--[if IE 8]>
                                                <link href="<?php echo baseUrl(); ?>css/ie" rel="stylesheet" type="text/css" />
                                                <![endif]-->

                                        <!--[if IE 9]>
                                                <link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
                                                <![endif]-->

                                        <!--[if IE 10]>
                                                <link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
                                                <![endif]-->

                                        <!--[if IE 8]>
                                                <link href="<?php echo baseUrl(); ?>css/ie.css" rel="stylesheet" type="text/css" />
                                        <![endif]--><script>
                                            (function(i, s, o, g, r, a, m) {
                                                i['GoogleAnalyticsObject'] = r;
                                                i[r] = i[r] || function() {
                                                    (i[r].q = i[r].q || []).push(arguments)
                                                }, i[r].l = 1 * new Date();
                                                a = s.createElement(o),
                                                        m = s.getElementsByTagName(o)[0];
                                                a.async = 1;
                                                a.src = g;
                                                m.parentNode.insertBefore(a, m)
                                            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                            ga('create', 'UA-43689390-1', 'sqgc.com');
                                            ga('send', 'pageview');</script>

                                        <script type="text/javascript">

                                            var _gaq = _gaq || [];
                                            _gaq.push(['_setAccount', 'UA-43689390-2']);
                                            _gaq.push(['_trackPageview']);
                                            (function() {
                                                var ga = document.createElement('script');
                                                ga.type = 'text/javascript';
                                                ga.async = true;
                                                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(ga, s);
                                            })();</script>

                                        </head>
                                        <body class="productbody">



                                            <div id="topBarContainer"> </div>
                                            <div class="mobilemenu" style="display:none">
                                                <div class="rmm">
                                                    <?php
                                                    include('business_left_navigation.php');
                                                    ?>
                                                </div>
                                            </div>
                                            <div id="wrapper">

                                                <div id="leftPanel">
                                                    <div class="shadowIe"></div>
                                                    <div class="logo">
                                                        <p align="center">
                                                            <a href="<?php echo baseUrl(); ?>index.php"><img width="100" src="<?php echo baseUrl(); ?>images/logo.png" /></a>
                                                            <br>
                                                                <span class="sus">
                                                                    <a class="sasLogoLink " href="<?php echo baseUrl(); ?>business/"> business </a>
                                                                </span>
                                                        </p>
                                                    </div>

                                                    <div class="leftMenu">
                                                        <?php
                                                        include('business_left_navigation.php');
                                                        ?>
                                                    </div>
                                                    <div class="hrcenter"></div>
                                                    <div class="slogan"><p align="center">
                                                            <img width="140" src="<?php echo baseUrl(); ?>images/slogan.png" />
                                                        </p>


                                                    </div>

                                                    <div class="hrcenter"></div>

                                                </div>

                                                <div id="rightPanel">
                                                    <div class="contentbox" id="">

                                                        <div class="prxtop1 kintBg" style="background: url(<?php echo baseUrl('upload/business/lingerie/' . $options['LINGERIE_BANNER']); ?>) repeat fixed center top rgba(0, 0, 0, 0);">

                                                            <h2 class="titleXlarge shadowTxt"> <?php
                                                                if (isset($options["LINGERIE_BANNER_TITLE"])) {
                                                                    echo $options["LINGERIE_BANNER_TITLE"];
                                                                } else {
                                                                    echo '';
                                                                }
                                                                ?> </h2>
                                                            <h4 class="titleSub"></h4>

                                                        </div>

                                                        <div class="prxtop2">
                                                            <div class="proInner">
                                                                <h2 class="protitle2 quickRead modallink" data-name="lingerieModal" style="opacity:1 !important;">
                                                                    <?php
                                                                    if (isset($options["LINGERIE_TITLE"])) {
                                                                        echo $options["LINGERIE_TITLE"];
                                                                    } else {
                                                                        echo '';
                                                                    }
                                                                    ?></h2>

                                                                <?php
                                                                if (isset($options["LINGERIE_DESCRIPTION"])) {
                                                                    echo html_entity_decode($options["LINGERIE_DESCRIPTION"]);
                                                                } else {
                                                                    echo '';
                                                                }
                                                                ?>

                                                            </div>

                                                        </div>

                                                        <div style="clear:both"></div>
                                                        <?php
                                                        if ($options["LINGERIE_BOTTOM_IMAGE_URL"] != '') {
                                                            ?>
                                                            <div class="sqTables clearfix">
                                                                <h1 class="text-center"> <img alt="knitwear" src="<?php echo $options["LINGERIE_BOTTOM_IMAGE_URL"]; ?>" class="img-responsive"></h1>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>

                                                        <div style="clear:both"></div>

                                                        <?php
                                                        $slideArrayCount = count($slideArray);
                                                        if ($slideArrayCount > 0) {
                                                            ?>

                                                            <div class="prokSide">
                                                                <div class="slidePng">
                                                                    <a class="prev2" href="#"></a>

                                                                    <a class="next2" href="#"></a>  


                                                                </div>

                                                                <div style=" clear:both"></div>

                                                                <div class="prokSideContent">
                                                                    <?php
                                                                    for ($i = 0; $i < $slideArrayCount; $i++) {
                                                                        ?>
                                                                        <img src="<?php echo baseUrl(); ?>upload/album/<?php echo$slide_album_id . "/" . $slideArray[$i] ?>" alt="lingerie" />
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>

                                                                <div style=" clear:both"></div>

                                                            </div>

                                                            <?php
                                                        }
                                                        ?>





                                                    </div>

                                                </div>




                                            </div> <!-- wrapper --> 

                                            <div class="sqModal fullW" data-id="lingerieModal"> <a class="modalclose"> X </a>
                                                <div class="modalinner2 modalinner-80">
                                                    <div id="modal" class="owl-carousel">
                                                        <?php
                                                        if (isset($options["LINGERIE_QUICKREAD"])) {
                                                            echo html_entity_decode($options["LINGERIE_QUICKREAD"]);
                                                        }
                                                        ?>

                                                    </div>
                                                </div>
                                            </div>

                                            </div>





                                            <script type="text/javascript">
                                                $(document).ready(function() {
                                                    $(window).bind('scroll', function(e) {
                                                        parallaxScroll();
                                                    });

                                                    function parallaxScroll() {
                                                        var scrolledY = $(window).scrollTop();
                                                        $('.kintBg').css('background-position', 'center -' + ((scrolledY * 0.2)) + 'px');
                                                        $('.titleXlarge').css('marginTop', '-' + ((scrolledY * 0.5)) + 'px');
                                                        $('.fish').css('top', '-' + ((scrolledY * 0.8)) + 'px');
                                                    }

                                                });
                                            </script>

                                            <script>
                                                $(document).ready(sizeContent);

                                                //Every resize of window
                                                $(window).resize(sizeContent);

                                                //Dynamically assign height
                                                function sizeContent() {

                                                    if ($(window).width() > 699) {
                                                        var newWidth = $("html").width() - $("#leftPanel").width() + "px";
                                                    }

                                                    if ($(window).width() < 699) {
                                                        //small screen, load other JS files
                                                        var newWidth = $("html").width() + "px";

                                                    }

                                                    $(".sqModal").css("width", newWidth);
                                                }

                                            </script>
                                            <?php
                                            include(basePath('quick_reader.php'));
                                            ?>

                                            <!-- Here we want a DYNAMIC value -->

                                        </body>
                                        </html>