<?php
include '../config/config.php';
include './config/quiz.config.php';



$userId = $_SESSION['quiz_user_id'];

$email = '';
$password = '';
$msg = '';
$err = '';
if (isset($_POST['submit']) AND $_POST['submit'] == 'Submit') {

    $current_password = trim($_POST['current_password']);
    $new_password = trim($_POST['new_password']);
    $confirm_new_password = trim($_POST['confirm_new_password']);
    if (isset($_POST['current_password']) AND $_POST['current_password'] == '') {
        $err = 'Current password field is required!!';
    } elseif (isset($_POST['new_password']) AND $_POST['new_password'] == '') {
        $err = 'New password field is required!!';
    } elseif (isset($_POST['new_password']) AND (strlen($_POST['new_password']) > $config['ADMIN_PASSWORD_LENGTH_MAX'] OR strlen($_POST['new_password']) < $config['ADMIN_PASSWORD_LENGTH_MIN'])) {
        $err = 'New Password length should be with in maximum: ' . $config['ADMIN_PASSWORD_LENGTH_MAX'] . ' AND Minimum: ' . $config['ADMIN_PASSWORD_LENGTH_MIN'] . '!! Given password length is :' . strlen($_POST['new_password']);
    } elseif (isset($_POST['confirm_new_password']) AND $_POST['confirm_new_password'] == '') {
        $err = 'Confirm New password  field is required!!';
    } elseif (isset($_POST['new_password']) AND isset($_POST['confirm_new_password']) AND $_POST['new_password'] != $_POST['confirm_new_password']) {
        $err = 'Confirm New Password does not match with new password  !!';
    } else {
        /* Start :Checking the user already exist or not */
        $userCheckSql = "SELECT * FROM users WHERE id='" . intval($userId) . "' and password = '" . mysqli_real_escape_string($con, securedPass($current_password)) . "'";
        $userCheckSqlResult = mysqli_query($con, $userCheckSql);
        if ($userCheckSqlResult) {
            $num_rows = mysqli_num_rows($userCheckSqlResult);


            if ($num_rows > 0) {
                $passSecure = securedPass($new_password);

                 $updateUserSql = "update users set password = '" . mysqli_real_escape_string($con, $passSecure) . "' where id = '$userId'";
                $updateUserSqlResult = mysqli_query($con, $updateUserSql);
                if ($updateUserSqlResult) {
                    $msg = "Your Password Successfully Changed";
                } else {
                    if (DEBUG) {
                        $err = 'updateUserSqlResult Error: ' . mysqli_error($con);
                    } else {

                        $err = 'updateUserSqlResult Error:';
                    }
                }

                /*
                if (checkQuizLogin()) {
                    $link = 'index.php';
                    redirect($link);
                }*/
            } else {
                $err = 'Your provided Current Password is not correct';
            }
            mysqli_free_result($userCheckSqlResult);
        } else {
            if (DEBUG) {
                echo 'userCheckSqlResult Error: ' . mysqli_error($con);
            }
        }

        /* End :Checking the user already exist or not */
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>FIFA WORLD CUP BRAZIL </title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/datepicker.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,700' rel='stylesheet' type='text/css'>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
              <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>
    <body>
        <div id="wrapper">
            <div class="jobintro">
         <?php include './logo.php';?>
            </div>
            <div class="jobs-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 page-content">
                            <div class="inner-box">
                                <h2 class="title-1">World Cup quiz Change Password</h2>
                                <hr>
<?php if ($err != ''): ?>
                                    <div class="alert alert-danger">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                                        <strong>Warnings! </strong> <?php echo $err; ?>
                                    </div>
<?php endif; ?>
<?php if ($msg != ''): ?>
                                    <div class="alert alert-success">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                                        <strong>Success! </strong> <?php echo $msg; ?>
                                    </div>
<?php endif; ?>

                                <!--                                <hr>-->
                                <form action="" method="post" role="form">

                                    <div class="form-group">
                                        <label for="current_password">Current Password</label>
                                        <input type="password" name="current_password" class="form-control" id="current_password" placeholder="Enter Current Password">
                                    </div>  

                                    <div class="form-group">
                                        <label for="new_password">New Password</label>
                                        <input type="password" name="new_password" class="form-control" id="new_password" placeholder="Enter New Password">
                                    </div> 
                                    <div class="form-group">
                                        <label for="confirm_new_password">Confirm New Password</label>
                                        <input type="password" name="confirm_new_password" class="form-control" id="confirm_new_password" placeholder="Enter Confirm New Password">
                                    </div> 




                                    <input type="submit" class=" btn-submit" name="submit" value="Submit">  
                                </form>

                            </div>
                        </div>
<?php include 'right_sidebar.php'; ?>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="col-lg-12 text-center footer" >
                    <p>Copyright 2014 © SQ Group Ltd.| Powerd by Bluescheme Ltd.</p>
                </div>
            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
        <!-- Include all compiled plugins (below), or include individual files as needed --> 
        <script src="js/bootstrap.min.js"></script> 
        <script src="js/jquery.cycle.lite.js" type="text/javascript"></script> 
        <script src="js/Selectyze.jquery.min.js"></script> 
        <script src="js/bootstrap-datepicker.js"></script> 
        <script type="text/javascript">
            $(document).ready(function() {
                $('.site-bg').cycle({
                    fx: 'fade',
                    speed: 1000,
                    timeout: 5000,
                });
            });
        </script>
    </body>
</html>