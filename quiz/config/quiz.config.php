<?php
if ($domain == 'brazil2014.sqgc.com' OR $domain == 'sqgc.com') {
    $config['DB_NAME'] = 'sqgccom_quiz';
} else {
    $config['DB_NAME'] = 'quiz';
}
$con = mysqli_connect($config['DB_HOST'], $config['DB_USER'], $config['DB_PASSWORD'], $config['DB_NAME']);

if (!$con) {
    die('Databse Connect Error: ' . mysqli_connect_error());
}

function checkQuizLogin() {
    $status = array();

    if (isset($_SESSION['quiz_user_email'])) {
        $status[] = 1;
    }
    if (isset($_SESSION['quiz_user_full_name'])) {
        $status[] = 1;
    }
    if (isset($_SESSION['quiz_user_designation'])) {
        $status[] = 1;
    }
    if (isset($_SESSION['quiz_user_id'])) {
        $status[] = 1;
    }
    if (count($status) >= 4) {
        return true;
    } else {
        return false;
    }
}

function checkQuizLogout() {
    unset($_SESSION['quiz_user_email']);
    unset($_SESSION['quiz_user_full_name']);
    unset($_SESSION['quiz_user_designation']);
    unset($_SESSION['quiz_user_id']);
    $status = array();

    if (isset($_SESSION['quiz_user_email'])) {
        $status[] = 1;
    }
    if (isset($_SESSION['quiz_user_full_name'])) {
        $status[] = 1;
    }
    if (isset($_SESSION['quiz_user_designation'])) {
        $status[] = 1;
    }
    if (isset($_SESSION['quiz_user_id'])) {
        $status[] = 1;
    }
    

    if (count($status) >= 4) {
        return false;
    } else {
        return true;
    }
}


function getDomainFromEmail($email) {
// Get the data after the @ sign
    $emailDomain = substr(strrchr($email, "@"), 1);
    if ($emailDomain == 'sqgc.com' OR $emailDomain =='bscheme.com') {
        return true;
    } else {
        return false;
    }
}
?>
