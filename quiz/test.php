<?php

function getDomainFromEmail($email) {
// Get the data after the @ sign
    $domain = substr(strrchr($email, "@"), 1);
    if ($domain == 'sqgc.com') {
        return true;
    } else {
        return false;
    }
}

// Example

$email = 'the_username_here@sqgc.com';

$domain = getDomainFromEmail($email);

echo $domain; // yahoo.com
?>