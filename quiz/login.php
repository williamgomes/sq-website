
<?php
include '../config/config.php';
include './config/quiz.config.php';
if (checkQuizLogin()) {
    $link = 'index.php';
    redirect($link);
}
$email = '';
$password = '';
$msg = '';
$err = '';
$user_score = 0;
if (isset($_POST['submit']) AND $_POST['submit'] == 'Submit') {


    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
    if (isset($_POST['email']) AND $_POST['email'] == '') {
        $err = 'Email field is required!!';
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL) || !preg_match('!^[\w @.-]*$!', $email)) {
        $err = 'Not valid email address';
    } elseif (!getDomainFromEmail($email)) {
        $err = 'Only  sqgc.com email is allowed ';
    } elseif (isset($_POST['password']) AND $_POST['password'] == '') {
        $err = 'password field is required!!';
    } else {
        /* Start :Checking the user already exist or not */
        $userCheckSql = "SELECT * FROM users WHERE email='" . mysqli_real_escape_string($con, $email) . "' and password = '" . mysqli_real_escape_string($con, securedPass($password)) . "' ";
        $userCheckSqlResult = mysqli_query($con, $userCheckSql);
        if ($userCheckSqlResult) {
            $num_rows = mysqli_num_rows($userCheckSqlResult);
            $userCheckSqlResultRowObj = mysqli_fetch_object($userCheckSqlResult);

            if ($num_rows > 0) {

                $userScoreSql = "SELECT  sum(case when questions.type = 'his' then 1 else 3 end) as total_my_score, users_questions_answers.user_id
FROM users_questions_answers join answers on (answers.id= users_questions_answers.answer_id and answers.correctness='yes')
join questions on questions.id = answers.question_id
where users_questions_answers.user_id = $userCheckSqlResultRowObj->id";
                $userScoreSqlResult = mysqli_query($con, $userScoreSql);
                if ($userScoreSqlResult) {
                    if (mysqli_num_rows($userScoreSqlResult) > 0) {
                        $userScoreSqlResultObj = mysqli_fetch_object($userScoreSqlResult);
                        $user_score = $userScoreSqlResultObj->total_my_score;
                    }
                } else {
                    if (DEBUG) {
                        $err = 'userScoreSqlResult Error: ' . mysqli_error($con);
                    } else {
                        $err = 'userScoreSqlResult Error: ';
                    }
                }


                $msg = 'You Successfully Logged in';

                $_SESSION['quiz_user_email'] = $userCheckSqlResultRowObj->email;
                $_SESSION['quiz_user_full_name'] = $userCheckSqlResultRowObj->full_name;
                $_SESSION['quiz_user_designation'] = $userCheckSqlResultRowObj->designation;
                $_SESSION['quiz_user_id'] = $userCheckSqlResultRowObj->id;
                $_SESSION['quiz_user_score'] = $user_score;


                if (checkQuizLogin()) {
                    $link = 'index.php';
                    redirect($link);
                }
            } else {
                $err = 'Email or password does not match. Please try again';
            }
            mysqli_free_result($userCheckSqlResult);
        } else {
            if (DEBUG) {
                echo 'userCheckSqlResult Error: ' . mysqli_error($con);
            }
        }

        /* End :Checking the user already exist or not */
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>FIFA WORLD CUP BRAZIL </title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/datepicker.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,700' rel='stylesheet' type='text/css'>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
              <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>
    <body>
        <div id="wrapper">
            <div class="jobintro">
            <?php include './logo.php';?>
            </div>
            <div class="jobs-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 page-content">
                            <div class="inner-box">
                                <h2 class="title-1">World Cup quiz Log in</h2>
                                <hr>
<?php if ($err != ''): ?>
                                    <div class="alert alert-danger">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                                        <strong>Warnings! </strong> <?php echo $err; ?>
                                    </div>
<?php endif; ?>
<?php if ($msg != ''): ?>
                                    <div class="alert alert-success">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                                        <strong>Success! </strong> <?php echo $msg; ?>
                                    </div>
<?php endif; ?>

                                <!--                                <hr>-->
                                <form action="" method="post" role="form">

                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" id="email" placeholder="Enter Your SQ Group Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password">
                                    </div> 


                                    
                                    <p> * For security reason, please DO NOT use the same password as your Corporate Email Account</p>
                                    <input type="submit" class=" btn-submit" name="submit" value="Submit">  

                                    <div>
                                        <a  href="forget_pass.php"> Forgot Password ?  </a>
                                        <a  href="registration.php"><strong> Register Here  </strong> </a>


                                    </div>
                                </form>

                            </div>
                        </div>
<?php include 'right_sidebar.php'; ?>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="col-lg-12 text-center footer" >
                    <p>Copyright 2014 © SQ Group Ltd.| Powerd by Bluescheme Ltd.</p>
                </div>
            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
        <!-- Include all compiled plugins (below), or include individual files as needed --> 
        <script src="js/bootstrap.min.js"></script> 
        <script src="js/jquery.cycle.lite.js" type="text/javascript"></script> 
        <script src="js/Selectyze.jquery.min.js"></script> 
        <script src="js/bootstrap-datepicker.js"></script> 
        <script type="text/javascript">
            $(document).ready(function() {
                $('.site-bg').cycle({
                    fx: 'fade',
                    speed: 1000,
                    timeout: 5000,
                });
            });
        </script>
    </body>
</html>


