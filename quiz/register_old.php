<?php
include '../config/config.php';
include './config/quiz.config.php';

$full_name = '';
$designation = '';
$email = '';
$password = '';
$msg = '';
$err = '';
$confirm_password = '';
if (isset($_POST['submit']) AND $_POST['submit'] == 'Submit') {

    $full_name = trim($_POST['full_name']);
    $designation = trim($_POST['designation']);
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
    $confirm_password = trim($_POST['confirm_password']);
    if (isset($_POST['full_name']) AND $_POST['full_name'] == '') {
        $err = 'Full Name field is required!!';
    } elseif (isset($_POST['designation']) AND $_POST['designation'] == '') {
        $err = 'Designation field is required!!';
    } elseif (isset($_POST['email']) AND $_POST['email'] == '') {
        $err = 'Email field is required!!';
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL) || !preg_match('!^[\w @.-]*$!', $email)) {
        $err = 'Not valid email address';
    }
    /*
    elseif (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $allowed = array('sqgc.com');
        $domain = array_pop(explode('@', $email));

        if (!in_array($domain, $allowed)) {
            // Not allowed
        }
    }*/ elseif (isset($_POST['password']) AND $_POST['password'] == '') {
        $err = 'password field is required!!';
    } elseif (isset($_POST['password']) AND (strlen($_POST['password']) > $config['ADMIN_PASSWORD_LENGTH_MAX'] OR strlen($_POST['password']) < $config['ADMIN_PASSWORD_LENGTH_MIN'])) {
        $err = 'Password length should be with in maximum: ' . $config['ADMIN_PASSWORD_LENGTH_MAX'] . ' AND Minimum: ' . $config['ADMIN_PASSWORD_LENGTH_MIN'] . '!! Given password length is :' . strlen($_POST['password']);
    } elseif (isset($_POST['confirm_password']) AND $_POST['confirm_password'] == '') {
        $err = 'Confirm password  field is required!!';
    } elseif (isset($_POST['confirm_password']) AND $_POST['confirm_password'] != $_POST['password']) {
        $err = 'Confirm Password does not match with  password  !!';
    } else {
        /* Start :Checking the user already exist or not */
        $userCheckSql = "SELECT email FROM users WHERE email='" . mysqli_real_escape_string($con, $email) . "'";
        $userCheckSqlResult = mysqli_query($con, $userCheckSql);
        if ($userCheckSqlResult) {
            $num_rows = mysqli_num_rows($userCheckSqlResult);

            if ($num_rows > 0) {
                $err = '(<b>' . $email . '</b>) already exist in the system';
            }
            mysqli_free_result($userCheckSqlResult);
        } else {
            if (DEBUG) {
                echo 'userCheckSqlResult Error: ' . mysqli_error($con);
            }
        }

        /* End :Checking the user already exist or not */
    }
    if ($err == '') {

        $secured_password = securedPass($password);
        $userInsert = '';
        $userInsert .= ' full_name = "' . mysqli_real_escape_string($con, $full_name) . '"';
        $userInsert .= ', designation = "' . mysqli_real_escape_string($con, $designation) . '"';
        $userInsert .= ', email = "' . mysqli_real_escape_string($con, $email) . '"';
        $userInsert .= ', password = "' . mysqli_real_escape_string($con, $secured_password) . '"';
        $userInsert .= ', status = "active"';

        $userInsertSql = "INSERT INTO users SET $userInsert";
        $userInsertSqlResult = mysqli_query($con, $userInsertSql);
        if ($userInsertSqlResult) {
            $msg = "You Successfully Registered in FIFA Quiz contest";
        } else {
            if (DEBUG) {
                $err = 'userInsertSqlResult Error: ' . mysqli_error($con);
            }
        }
    }
}
?>

<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <form name="register" action="" method="post">
            <div>
                <h3> Register as a candidate in FIFA quiz </h3>
<?php
if ($err != '') {
    echo "<h2>Error : $err </h2>";
}
if ($msg != '') {
    echo "<h2>Success ! : $msg </h2>";
}
?>

                <table border="1" width="700" align="center">

                    <tbody>
                        <tr>
                            <td>Full Name : </td>
                            <td><input type="text" name="full_name" value="<?php echo $full_name; ?>" /></td>
                        </tr>
                        <tr>
                            <td>Designation : </td>
                            <td><input type="text" name="designation" value="<?php echo $designation; ?>" /></td>
                        </tr>
                        <tr>
                            <td>Email : </td>
                            <td><input type="email" name="email" value="<?php echo $email; ?>" /></td>
                        </tr>
                        <tr>
                            <td>Password :</td>
                            <td><input type="password" name="password" value="" /></td>
                        </tr>
                        <tr>
                            <td>Confirm Password :</td>
                            <td><input type="password" name="confirm_password" value="" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" name="submit" value="Submit" /></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </form>
    </body>
</html>
