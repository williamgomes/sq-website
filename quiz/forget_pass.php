<?php
include '../config/config.php';
include './config/quiz.config.php';


$email = '';
$msg = '';
$err = '';
if (isset($_POST['submit']) AND $_POST['submit'] == 'Submit') {


    $email = trim($_POST['email']);
    if (isset($_POST['email']) AND $_POST['email'] == '') {
        $err = 'Email field is required!!';
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL) || !preg_match('!^[\w @.-]*$!', $email)) {
        $err = 'Not valid email address';
    } elseif (!getDomainFromEmail($email)) {
        $err = 'Only  sqgc.com email is allowed ';
    }else {
        /* Start :Checking the user already exist or not */
        $userCheckSql = "SELECT * FROM users WHERE email='" . mysqli_real_escape_string($con, $email) . "' ";
        $userCheckSqlResult = mysqli_query($con, $userCheckSql);
        if ($userCheckSqlResult) {
            $num_rows = mysqli_num_rows($userCheckSqlResult);


            if ($num_rows > 0) {
                $userCheckSqlResultRowObj = mysqli_fetch_object($userCheckSqlResult);


                $pass = passwordGenerator();
                $emailTo = $userCheckSqlResultRowObj->email;
                $subject = 'New password for FIFA 2014 quiz';
                $message = 'Name : ' . $userCheckSqlResultRowObj->full_name . '<br/>';
                $message .= 'Designation : ' . $userCheckSqlResultRowObj->designation . '<br/>';
                $message .= 'New Password : ' . $pass . '<br/>';
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: fifaquiz@sqgc.com' . "\r\n" .
                        'Reply-To: fifaquiz@sqgc.com' . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();
                
                if (mail($emailTo, $subject, $message, $headers)) {

                    $passSecure = securedPass($pass);
                    //update databse
                    $updateUserSql = "update users set password = '$passSecure' where email = '$emailTo'";
                    $updateUserSqlResult = mysqli_query($con, $updateUserSql);
                    if ($updateUserSqlResult) {
                        $msg = "Your new password is send to $emailTo . Pleas check your mail.";
                    } else {
                        if (DEBUG) {
                            $err = 'updateUserSqlResult Error: ' . mysqli_error($con);
                        } else {

                            $err = 'updateUserSqlResult Error:';
                        }
                    }
                } else {
                    $err = "Mail Sending Failed";
                }
            } else {
                $err = 'Email does not match. Please try again';
            }
            mysqli_free_result($userCheckSqlResult);
        } else {
            if (DEBUG) {
                echo 'userCheckSqlResult Error: ' . mysqli_error($con);
            }
        }

        /* End :Checking the user already exist or not */
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>FIFA WORLD CUP BRAZIL </title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/datepicker.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,700' rel='stylesheet' type='text/css'>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
              <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>
    <body>
        <div id="wrapper">
            <div class="jobintro">
         <?php include './logo.php';?>
            </div>
            <div class="jobs-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 page-content">
                            <div class="inner-box">
                                <h2 class="title-1">World Cup quiz Forgot Password</h2>
                                <hr>
                                <?php if ($err != ''): ?>
                                    <div class="alert alert-danger">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                                        <strong>Warnings! </strong> <?php echo $err; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($msg != ''): ?>
                                    <div class="alert alert-success">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                                        <strong>Success! </strong> <?php echo $msg; ?>
                                    </div>
                                <?php endif; ?>

                                <!--                                <hr>-->
                                <form action="" method="post" role="form">

                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" id="email" placeholder="Enter Your SQ Group Email">
                                    </div>                                                                     



                                    <input type="submit" class=" btn-submit" name="submit" value="Submit">
                                    <br>
                                    <a href="login.php"> Login </a>
                                </form>

                            </div>
                        </div>
                        <?php include 'right_sidebar.php'; ?>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="col-lg-12 text-center footer" >
                    <p>Copyright 2014 © SQ Group Ltd.| Powerd by Bluescheme Ltd.</p>
                </div>
            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
        <!-- Include all compiled plugins (below), or include individual files as needed --> 
        <script src="js/bootstrap.min.js"></script> 
        <script src="js/jquery.cycle.lite.js" type="text/javascript"></script> 
        <script src="js/Selectyze.jquery.min.js"></script> 
        <script src="js/bootstrap-datepicker.js"></script> 
        <script type="text/javascript">
            $(document).ready(function() {
                $('.site-bg').cycle({
                    fx: 'fade',
                    speed: 1000,
                    timeout: 5000,
                });
            });
        </script>
    </body>
</html>