
<?php
include '../config/config.php';
include './config/quiz.config.php';
if (checkQuizLogin()) {
    $link = 'index.php';
    redirect($link);
}
$full_name = '';
$designation = '';
$email = '';
$password = '';
$favorite_team = '';
$msg = '';
$err = '';
$confirm_password = '';
if (isset($_POST['submit']) AND $_POST['submit'] == 'Submit') {

    $full_name = trim($_POST['full_name']);
    $designation = trim($_POST['designation']);
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
    $confirm_password = trim($_POST['confirm_password']);
    $favorite_team = trim($_POST['favorite_team']);
    if (isset($_POST['full_name']) AND $_POST['full_name'] == '') {
        $err = 'Full Name field is required!!';
    } elseif (isset($_POST['designation']) AND $_POST['designation'] == '') {
        $err = 'Designation field is required!!';
    } elseif (isset($_POST['email']) AND $_POST['email'] == '') {
        $err = 'Email field is required!!';
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL) || !preg_match('!^[\w @.-]*$!', $email)) {
        $err = 'Not valid email address';
    } elseif (!getDomainFromEmail($email)) {
        $err = 'Only  sqgc.com email is allowed ';
    } elseif (isset($_POST['password']) AND $_POST['password'] == '') {
        $err = 'password field is required!!';
    } elseif (isset($_POST['password']) AND ( strlen($_POST['password']) > $config['ADMIN_PASSWORD_LENGTH_MAX'] OR strlen($_POST['password']) < $config['ADMIN_PASSWORD_LENGTH_MIN'])) {
        $err = 'Password length should be with in maximum: ' . $config['ADMIN_PASSWORD_LENGTH_MAX'] . ' AND Minimum: ' . $config['ADMIN_PASSWORD_LENGTH_MIN'] . '!! Given password length is :' . strlen($_POST['password']);
    } elseif (isset($_POST['confirm_password']) AND $_POST['confirm_password'] == '') {
        $err = 'Confirm password  field is required!!';
    } elseif (isset($_POST['confirm_password']) AND $_POST['confirm_password'] != $_POST['password']) {
        $err = 'Confirm Password does not match with  password  !!';
    } elseif (isset($_POST['favorite_team']) AND $_POST['favorite_team'] == '') {
        $err = 'Favorite team password  field is required!!';
    } else {
        /* Start :Checking the user already exist or not */
        $userCheckSql = "SELECT email FROM users WHERE email='" . mysqli_real_escape_string($con, $email) . "'";
        $userCheckSqlResult = mysqli_query($con, $userCheckSql);
        if ($userCheckSqlResult) {
            $num_rows = mysqli_num_rows($userCheckSqlResult);

            if ($num_rows > 0) {
                $err = '(<b>' . $email . '</b>) already exist in the system';
            }
            mysqli_free_result($userCheckSqlResult);
        } else {
            if (DEBUG) {
                echo 'userCheckSqlResult Error: ' . mysqli_error($con);
            }
        }

        /* End :Checking the user already exist or not */
    }
    if ($err == '') {

        $secured_password = securedPass($password);
        $userInsert = '';
        $userInsert .= ' full_name = "' . mysqli_real_escape_string($con, $full_name) . '"';
        $userInsert .= ', designation = "' . mysqli_real_escape_string($con, $designation) . '"';
        $userInsert .= ', favorite_team = "' . mysqli_real_escape_string($con, $favorite_team) . '"';
        $userInsert .= ', email = "' . mysqli_real_escape_string($con, $email) . '"';
        $userInsert .= ', password = "' . mysqli_real_escape_string($con, $secured_password) . '"';
        $userInsert .= ', status = "active"';

        $userInsertSql = "INSERT INTO users SET $userInsert";
        $userInsertSqlResult = mysqli_query($con, $userInsertSql);
        if ($userInsertSqlResult) {
            $msg = "You Successfully Registered in FIFA Quiz contest";
            $insert_id = mysqli_insert_id($con);
            $_SESSION['quiz_user_email'] = $email;
            $_SESSION['quiz_user_full_name'] = $full_name;
            $_SESSION['quiz_user_designation'] = $designation;
            $_SESSION['quiz_user_id'] = $insert_id;
            $_SESSION['quiz_user_score'] = 0;
            if (checkQuizLogin()) {
                $link = 'index.php';
                redirect($link);
            }
        } else {
            if (DEBUG) {
                $err = 'userInsertSqlResult Error: ' . mysqli_error($con);
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>FIFA WORLD CUP BRAZIL </title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/datepicker.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,700' rel='stylesheet' type='text/css'>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
              <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>
    <body>
        <div id="wrapper">
            <div class="jobintro">
                <?php include './logo.php'; ?>
            </div>
            <div class="jobs-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 page-content">
                            <div class="inner-box">
                                <h2 class="title-1">World Cup quiz Registration</h2>
                                <hr>
                                <?php if ($err != ''): ?>
                                    <div class="alert alert-danger">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                                        <strong>Warnings! </strong> <?php echo $err; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($msg != ''): ?>
                                    <div class="alert alert-success">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                                        <strong>Success! </strong> <?php echo $msg; ?>
                                    </div>
                                <?php endif; ?>

                                <!--                                <hr>-->
                                <form action="" method="post" role="form">
                                    <div class="form-group">
                                        <label for="full_name">Full Name</label>
                                        <input type="text" name="full_name" value="<?php echo $full_name; ?>" class="form-control" id="full_name" placeholder="Enter Your Full Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="designation">Designation</label>
                                        <input type="text" name="designation" value="<?php echo $designation; ?>" class="form-control" id="designation" placeholder="Enter Your Designation">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" id="email" placeholder="Enter Your SQ Group Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password">
                                    </div>
                                    <div class="form-group">
                                        <label for="confirm_password">Confirm Password</label>
                                        <input type="password" name="confirm_password" class="form-control" id="confirm_password" placeholder="Retype Password">
                                    </div> 
                                    <div class="form-group">
                                        <label for="favorite_team">Favorite Team</label>
                                        <select class="form-control" name="favorite_team">
                                            <option value="">Select a Team </option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="Argentina">Argentina</option>
                                            <option value="Australia">Australia</option>
                                            <option value="Belgium">Belgium</option>
                                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                            <option value="Brazil">Brazil</option>
                                            <option value="Cameroon">Cameroon</option>
                                            <option value="Chile">Chile</option>


                                            <option value="Colombia">Colombia</option>
                                            <option value="Costa Rica">Costa Rica</option>
                                            <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                                            <option value="Croatia">Croatia</option>
                                            <option value="Ecuador">Ecuador</option>
                                            <option value="England">England</option>
                                            <option value="France">France</option>
                                            <option value="Germany">Germany</option>

                                            <option value="Ghana">Ghana</option>
                                            <option value="Greece">Greece</option>
                                            <option value="Honduras">Honduras</option>
                                            <option value="Iran">Iran</option>
                                            <option value="Italy">Italy</option>
                                            <option value="Japan">Japan</option>
                                            <option value="Korea Republic">Korea Republic</option>
                                            <option value="Mexico">Mexico</option>

                                            <option value="Netherlands">Netherlands</option>
                                            <option value="Nigeria">Nigeria</option>
                                            <option value="Portugal">Portugal</option>
                                            <option value="Russia">Russia</option>
                                            <option value="Spain">Spain</option>
                                            <option value="Switzerland">Switzerland</option>
                                            <option value="Uruguay">Uruguay</option>
                                            <option value="USA">USA</option>
                                        </select>

                                    </div>                                     



                                    <input type="submit" class=" btn-submit" name="submit" value="Submit">  

                                    <div>
                                        If you already have account Click
                                        <a  href="login.php"><strong> Here  </strong> </a>


                                    </div>
                                </form>

                            </div>
                        </div>
                        <?php include 'right_sidebar.php'; ?>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="col-lg-12 text-center footer" >
                    <p>Copyright 2014 © SQ Group Ltd.| Powerd by Bluescheme Ltd.</p>
                </div>
            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
        <!-- Include all compiled plugins (below), or include individual files as needed --> 
        <script src="js/bootstrap.min.js"></script> 
        <script src="js/jquery.cycle.lite.js" type="text/javascript"></script> 
        <script src="js/Selectyze.jquery.min.js"></script> 
        <script src="js/bootstrap-datepicker.js"></script> 
        <script type="text/javascript">
            $(document).ready(function() {
                $('.site-bg').cycle({
                    fx: 'fade',
                    speed: 1000,
                    timeout: 5000,
                });
            });
        </script>
    </body>
</html>

