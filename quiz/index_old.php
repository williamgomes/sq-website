<?php
include '../config/config.php';
include './config/quiz.config.php';
$today = date("Y-m-d H:i:s");
if (isset($_REQUEST['time'])) {
    $today = $_REQUEST['time'];
}

/* SELECT `id`, `text`, `date`, `type`, `status` FROM `questions` WHERE 1 */
/* SELECT `id`, `question_id`, `text`, `correctness` FROM `answers` WHERE 1 */

if (!checkQuizLogin()) {
    $link = 'login.php';
    redirect($link);
}

$userId = $_SESSION['quiz_user_id']; // get it from session 
$ans = array();
$err = '';
$msg = '';

if (isset($_REQUEST['ans_submit'])) {
    extract($_POST);
    /* User before submitted check */

    //printDie($_POST);

    if (count($ans) > 0) {

        $questionArray = array_keys($ans);
        $questionStr = implode(",", $questionArray);
        $checkSql = "SELECT * FROM users_questions_answers where user_id = $userId and question_id in ( $questionStr)";
        $checkSqlResult = mysqli_query($con, $checkSql) or die(mysqli_error($con));
        $checkSqlResultNumRows = mysqli_num_rows($checkSqlResult);
        if ($checkSqlResultNumRows > 0) {
            $err = "You Already Give Answer. Not allowed for second time";
        } else {
            foreach ($ans as $key => $value) {

                $key;
                if (count($value) != 2) {
                    $err = "you have to select two teams from each group.";
                } else {

                    foreach ($value AS $v) {
                        $insertQuerySql = "INSERT INTO  users_questions_answers (question_id , answer_id , user_id)
                        VALUES ( '$key',  '$v',  '$userId');";
                        $insertQueryResult = mysqli_query($con, $insertQuerySql) or die(mysqli_error($con));

                        if ($insertQueryResult) {
                            $msg = "Successfully Submitted";
                        } else {
                            if (DEBUG) {
                                $err = 'insertQuerySql query Error: ' . mysqli_error($con);
                            } else {
                                $err = 'insertQuerySql query fail';
                            }
                        }
                    }
                }
            }
        }
    } else {
        $err = "You don't answer any questions";
    }
    //printDie($ans);
}
$quizArray = array();
$quizArrayCounter = 0;

$quizSql = "SELECT * FROM questions WHERE status ='active' AND  start_time <='" . $today . "' AND end_time >= '" . $today . "' AND id  NOT IN  (SELECT question_id FROM users_questions_answers WHERE users_questions_answers.question_id =questions.id AND  user_id=" . $userId . ")";

$quizSqlResult = mysqli_query($con, $quizSql);

if ($quizSqlResult) {
    $quizArrayCounter = mysqli_num_rows($quizSqlResult);
    while ($quizSqlResultRowObj = mysqli_fetch_object($quizSqlResult)) {
        $ansSql = "SELECT * FROM answers WHERE question_id=" . $quizSqlResultRowObj->id;
        $ansSqlResult = mysqli_query($con, $ansSql);
        if ($ansSqlResult) {

            while ($ansSqlResultRowObj = mysqli_fetch_object($ansSqlResult)) {
                $quizSqlResultRowObj->ans[] = $ansSqlResultRowObj;
            }

            mysqli_free_result($ansSqlResult);
        } else {
            if (DEBUG) {
                $err = 'ansSqlResult query Error: ' . mysqli_error($con);
            } else {
                $err = 'ansSqlResult query fail';
            }
        }
        $quizArray[] = $quizSqlResultRowObj;
    }
    mysqli_free_result($quizSqlResult);
} else {
    if (DEBUG) {
        $err = 'quizSqlResult query Error: ' . mysqli_error($con);
    } else {
        $err = 'quizSqlResult query fail';
    }
}

if ($quizArrayCounter < 1) {

    $nextQuizSql = "SELECT * FROM questions WHERE status ='active' AND  start_time >='" . $today . "' ORDER BY start_time ASC LIMIT 1";

    $nextQuizSqlResult = mysqli_query($con, $nextQuizSql);

    if ($nextQuizSqlResult) {
        $nextQuizSqlResultRowObj = mysqli_fetch_object($nextQuizSqlResult);
       // printDie($nextQuizSqlResultRowObj);
    } else {
        if (DEBUG) {
            $err = 'nextQuizSqlResult query Error: ' . mysqli_error($con);
        } else {
            $err = 'nextQuizSqlResult query fail';
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>FIFA WORLD CUP BRAZIL </title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/datepicker.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,700' rel='stylesheet' type='text/css'>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
              <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>
    <body>
        <div id="wrapper">
            <div class="jobintro">
                <?php include './logo.php'; ?>
            </div>
        </div>
        <div class="jobs-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 page-content">
                        <div class="inner-box">
                            <h2 class="title-1">World Cup quiz</h2>
                            <hr>
                            <?php if ($err != ''): ?>
                                <div class="alert alert-danger">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                    <strong>Warnings! </strong> <?php echo $err; ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($msg != ''): ?>
                                <div class="alert alert-success">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                    <strong>Success! </strong> <?php echo $msg; ?>
                                </div>
                            <?php endif; ?>

                            <!--                                <hr>-->
                            <form action="" method="post">
                                <?php if ($quizArrayCounter > 0): ?>
                                    <h3 class="title-3">Select two teams from each group for next round.</h3>
                                    <?php for ($i = 0; $i < $quizArrayCounter; $i++): ?>
                                        <div class="inner-box-content">
                                            <h3 class="title-3"> <?php echo $quizArray[$i]->text; ?></h3>
                                            <?php $ansCounter = count($quizArray[$i]->ans); ?>
                                            <?php if ($ansCounter > 0): ?>

                                                <?php for ($j = 0; $j < $ansCounter; $j++): ?>
                                                    <?php
                                                    $check = '';
                                                    if (isset($ans[$quizArray[$i]->id]) AND ( $ans[$quizArray[$i]->id] == $quizArray[$i]->ans[$j]->id)) {
                                                        $check = 'checked';
                                                    }
                                                    ?>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="checkbox" <?php echo $check; ?> name="ans[<?php echo $quizArray[$i]->id; ?>][]"  value="<?php echo $quizArray[$i]->ans[$j]->id; ?>" > <?php echo $quizArray[$i]->ans[$j]->text; ?>
                                                        </label>
                                                    </div>

                                                <?php endfor; /* ($j=0;$j < $ansCounter;$j++): */ ?>
                                            <?php else: /* ($ansCounter > 0) */ ?>
                                                <p>No answer found</p>
                                            <?php endif; /* ($ansCounter > 0) */ ?>
                                        </div>
                                        <hr>
                                    <?php endfor; /* ($i=0; $i< $quizArrayCounter; $i++) */ ?>
                                <?php else: /* ($quizArrayCounter > 0) */ ?>
                                    <?php if (count($nextQuizSqlResultRowObj) > 0): ?>     

                                        <p>Next quiz will be appeared <strong><?php echo date(" l jS F", strtotime($nextQuizSqlResultRowObj->start_time)) ?> </strong><?php echo 'From ' . date("ga", strtotime($nextQuizSqlResultRowObj->start_time)) . ' To ' . date("ga", strtotime($nextQuizSqlResultRowObj->end_time)); ?></p>
                                    <?php endif; /* (count($nextQuizSqlResultRowObj) > 0) */ ?>  
                                <?php endif; /* ($quizArrayCounter > 0) */ ?>                                    

                                <?php if ($quizArrayCounter > 0): ?>  
                                    <input type="submit" class=" btn-submit" name="ans_submit" value="Submit"> 
                                <?php endif; /* ($quizArrayCounter > 0): */ ?>
                                </div>
                            </form>

                        </div>
                        <?php include 'right_sidebar.php'; ?>
                    </div>
                </div>
            </div>
            <span style="display: none;"><?php echo $today; ?></span>
            <div class="container">
                <div class="col-lg-12 text-center footer" >
                    <p>Copyright 2014 © SQ Group Ltd.| Powerd by Bluescheme Ltd.</p>
                </div>
            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
        <!-- Include all compiled plugins (below), or include individual files as needed --> 
        <script src="js/bootstrap.min.js"></script> 
        <script src="js/jquery.cycle.lite.js" type="text/javascript"></script> 
        <script src="js/Selectyze.jquery.min.js"></script> 
        <script src="js/bootstrap-datepicker.js"></script> 
        <script type="text/javascript">
            $(document).ready(function() {
                $('.site-bg').cycle({
                    fx: 'fade',
                    speed: 1000,
                    timeout: 5000,
                });
            });
        </script>
    </body>
</html>