<?php
$topScorerResultArray = array();
$topScorerSql = "SELECT sum(CASE WHEN questions.type = 'his' THEN 1 ELSE 3 END) AS totalScore,users_questions_answers.user_id, users.full_name, users.designation
FROM users_questions_answers join answers on (answers.id= users_questions_answers.answer_id and answers.correctness='yes')
join questions on questions.id = answers.question_id
join users on users.id = users_questions_answers.user_id
WHERE users.email IS NOT NULL 
group by users_questions_answers.user_id 

order by totalScore desc, users_questions_answers.datetime ASC limit 5";

$topScorerSqlResult = mysqli_query($con, $topScorerSql);
if ($topScorerSqlResult) {
    while ($topScorerSqlResultRowObj = mysqli_fetch_object($topScorerSqlResult)) {
        $topScorerResultArray[] = $topScorerSqlResultRowObj;
    }


    mysqli_free_result($topScorerSqlResult);
} else {
    if (DEBUG) {
        echo 'topScorerSqlResult Error: ' . mysqli_error($con);
    } else {
        echo 'topScorerSqlResult Error: ';
    }
}

$pupularteamrSqlArray = array();
$pupularteamrSqlArrayCounter = 0;
$pupularteamrSql = "SELECT `favorite_team`, COUNT(*) AS total_supporter FROM `users` WHERE `favorite_team` !='' GROUP BY `favorite_team` ORDER BY total_supporter DESC LIMIT 5";

$pupularteamrSqlResult = mysqli_query($con, $pupularteamrSql);
if ($pupularteamrSqlResult) {
    $pupularteamrSqlArrayCounter = mysqli_num_rows($pupularteamrSqlResult);
    while ($pupularteamrSqlResultRowObj = mysqli_fetch_object($pupularteamrSqlResult)) {
        $pupularteamrSqlArray[] = $pupularteamrSqlResultRowObj;
    }


    mysqli_free_result($pupularteamrSqlResult);
} else {
    if (DEBUG) {
        echo 'pupularteamrSqlResult Error: ' . mysqli_error($con);
    } else {
        echo 'pupularteamrSqlResult Error: ';
    }
}
?>


<div class="col-sm-4 page-sidebar">
    <aside>
        <?php if (checkQuizLogin()): ?>
            <div class="inner-box">
                <h5 class="title-5"><i class="glyphicon glyphicon-user"></i> User Panel</h5>
                <div class="inner-box-content">
                    <ul style="" class="nav nav-pills nav-stacked" data-tax="job_category">
                        <li class="cat-item cat-7 cat-business"> 
                            <a data="business" href="index.php" class="alert-info"> <i class="glyphicon glyphicon-home"></i> Home                             </a>

                        </li>
                        <li class="cat-item cat-7 cat-business"> 
                            <a data="business" href="change_pass.php" class="alert-info"> <i class="glyphicon glyphicon-cog"></i> Change Password                             </a>

                        </li>
                        <li class="cat-item cat-7 cat-business"> 
                            <a data="business" href="logout.php" class="alert-info"><i class="glyphicon glyphicon-off"></i> Logout                             </a> 
                        </li>
                    </ul>
                </div>
            </div>
            <div class="inner-box">
                <h5 class="title-5"><i class="glyphicon glyphicon-user"></i> MY SCORE</h5>
                <div class="inner-box-content">
                    <table class="table">

                        <tbody>
                            <tr>

                                <td><?php echo $_SESSION['quiz_user_full_name']; ?><br>
                                    <span class="post"> <?php echo $_SESSION['quiz_user_designation']; ?> </span><br>
                                    <span class="post"> <?php echo $_SESSION['quiz_user_email']; ?> </span>
                                </td>
                                <td><span class="badge"><?php echo $_SESSION['quiz_user_score']; ?></span></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        <?php else: /* (checkQuizLogin()) */ ?>
            <!--            <div class="inner-box">
                            <h5 class="title-5"><i class="glyphicon glyphicon-user"></i> User Panel</h5>
                            <div class="inner-box-content">
                                <ul style="" class="nav nav-pills nav-stacked" data-tax="job_category">
                                    <li class="cat-item cat-7 cat-business"> 
                                        <a class="alert-info" href="login.php" data="business">
                                            <div class="name"> Login </div>
                                        </a> 
                                    </li>
                                </ul>
                            </div>
                        </div>-->
        <?php endif; /* (checkQuizLogin()) */ ?>
        <div class="inner-box">
            <h5 class="title-5"><i class="glyphicon glyphicon-star"></i> Leader Board</h5>
            <div class="inner-box-content">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th> Name</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if (count($topScorerResultArray) > 0):
                            $i = 0;
                            foreach ($topScorerResultArray as $data):
                                $i++;
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $data->full_name; ?> <br>
                                        <span class="post"> <?php echo $data->designation; ?> </span></td>
                                    <td><span class="badge"><?php echo $data->totalScore; ?></span></td>
                                </tr>

                                <?php
                            endforeach;

                        else:
                            ?>
                            <tr>
                                <td colspan="3">Top Scorer Information Not Found</td>

                            </tr>

                        <?php
                        endif;
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
        <?php if ($pupularteamrSqlArrayCounter > 0): ?>
            <div class="inner-box">
                <h5 class="title-5"><i class="glyphicon glyphicon-thumbs-up"></i> Popular Teams</h5>
                <div class="inner-box-content">
                    
                     <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Team</th>
                            <th>Total Supporter</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php  if ($pupularteamrSqlArrayCounter > 0): ?>
                          
                            
                          <?php for ($i = 0; $i < $pupularteamrSqlArrayCounter; $i++): ?>
                                <tr>
                                    <td><?php echo $i+1; ?></td>
                                    <td><?php echo $pupularteamrSqlArray[$i]->favorite_team; ?></td>
                                    <td class=""><span class="badge"><?php echo $pupularteamrSqlArray[$i]-> total_supporter; ?></span></td>
                                </tr>

                             <?php endfor; /* ($i=0;$i < $pupularteamrSqlArrayCounter;$i++) */ ?>
          
                       <?php endif; /* ($pupularteamrSqlArrayCounter > 0) */ ?>
                    </tbody>
                </table>
                    

                </div>
            </div>
        <?php endif; /* ($pupularteamrSqlArrayCounter > 0) */ ?>

    </aside>
</div>