<?php
include '../config/config.php';
$options = array();
$fields = "'ABOUT_INTRO_BANNER', 'ABOUT_INTRO_BANNER_TITLE', 'ABOUT_INTRO_DESCRIPTION', 'ABOUT_JOURNEY_TITLE', 'ABOUT_JOURNEY_DESCRIPTION', 'ABOUT_WAY_OF_LIFE_TITLE', 'ABOUT_WAY_OF_LIFE_DESCRIPTION','ABOUT_BRAND_TITLE','ABOUT_BRAND_DESCRIPTION','AMIRA_FARUQ_TEXT','BOUTAINA_FARUQ_TEXT','MOUAMMAD_GHULAM_FARUQ_TEXT','SOUKAINA_FARUQ_TEXT','SALIMA_BENSAID_TEXT', 'ABOUT_BOARD_TITLE', 'ABOUT_BOARD_DESCRIPTION','ABOUT_ORGANOGRAM_TITLE','ABOUT_ORGANOGRAM_DESCRIPTION','ABOUT_CHIEF_EXECUTIVE_OFFICER_DESCRIPTION','ABOUT_CORE_OPERATIONS_LEADER_DESCRIPTION','ABOUT_LEVERAGE_TEAMS_DESCRIPTION','ABOUT_SHARED_SERVICES_GROUPS_DESCRIPTION','ABOUT_LEADERSHIP_TEAM_TITLE','ABOUT_LEADERSHIP_TEAM_DESCRIPTION','ABOUT_JOURNEY_QUICKREAD','ABOUT_WAY_OF_LIFE_QUICKREAD','ABOUT_BRAND_QUICKREAD','ABOUT_BOARD_QUICKREAD'";
$aboutFieldsSql = "SELECT CS_option,CS_value FROM config_settings WHERE CS_option IN ($fields)";
$aboutFieldsResult = mysqli_query($con, $aboutFieldsSql);
if ($aboutFieldsResult) {
    while ($aboutFieldsResultRowObj = mysqli_fetch_object($aboutFieldsResult)) {
        $options[$aboutFieldsResultRowObj->CS_option] = $aboutFieldsResultRowObj->CS_value;
    }
} else {
    if (DEBUG) {
        $err = "aboutFieldsResult error" . mysqli_error($con);
    } else {
        $err = "aboutFieldsResult query failed";
    }
}
$aboutTableSql = "SELECT about_title, about_text FROM about";
$aboutTableResult = mysqli_query($con, $aboutTableSql);
if ($aboutTableResult) {
    while ($aboutTableResultRowObj = mysqli_fetch_object($aboutTableResult)) {
        $options[$aboutTableResultRowObj->about_title] = $aboutTableResultRowObj->about_text;
    }
} else {
    if (DEBUG) {
        echo "aboutTableResult error" . mysqli_error($con);
    } else {
        echo "aboutTableResult query failed";
    }
}
/* Start Leadership Team */
$employeeArray = array();
$employeeSql = "SELECT * FROM employees 
LEFT JOIN  companies ON  companies.company_id=   employees.employee_company_id 
WHERE employee_leadership_status='yes'";
$employeeResult = mysqli_query($con, $employeeSql);
if ($employeeResult) {
    while ($employeeResultRowObj = mysqli_fetch_object($employeeResult)) {
        $employeeArray[] = $employeeResultRowObj;
    }
} else {
    if (DEBUG) {
        echo "employeeResult error" . mysqli_error($con);
    } else {
        echo "employeeResult query failed";
    }
}
/* End Leadeership Team */

/* Start slide for Brand */
$brandSlide = array();
$brandSlideSql = "SELECT * FROM album_images WHERE AI_album_id IN (SELECT album_id FROM album WHERE album_keyword= 'BRAND') ORDER BY AI_image_priority DESC";
$brandSlideResult = mysqli_query($con, $brandSlideSql);
if ($brandSlideResult) {
    while ($brandSlideResultRowObj = mysqli_fetch_object($brandSlideResult)) {
        $brandSlide[] = $brandSlideResultRowObj->AI_image_name;
        $album_id = $brandSlideResultRowObj->AI_album_id;
    }
} else {
    if (DEBUG) {
        echo "brandSlideResultRowObj error" . mysqli_error($con);
    }
}
/* End slide for Brand */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>SQ GROUP | About Us</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="SQ-Group,KNITWEAR,LINGERIE,SHIRT,RETAIL,Biriqina">
            <meta name="author" content="SQ-Group">
                <meta name="description" content="SQ visions to build an organization that would thrive on its tribal culture. After all, we all have a common story – SQ. The story of SQ, its attitude and the way it conducts its business are part of this segment. It's all about what we are, not what we have.">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">

                        <?php
                        include("about_header.php");
                        ?>
                        </head>
                        <body class="AboutPage">
                            <div class="mobilemenu fixedMenu" style="display:none">
                                <div class="rmm">
                                    <?php
                                    include("about_left_navigation.php");
                                    ?>
                                </div>
                            </div>
                            <div class="background"></div>
                            <div class="wrapper">
                                <div id="leftPanel">
                                    <div class="shadowIe"></div>
                                    <div class="logo">
                                        <p align="center">
                                            <a style="text-decoration:none;"  href="<?php echo baseUrl('index.php') ?>">
                                                <img class="mainLogo"  style="width:100px !important;" width="100" src="<?php echo baseUrl('images/logo.png') ?>" alt="Sq Group" />
                                            </a>
                                            <br>
                                                <span class="sus"> <a class="sasLogoLink sasLogoLinkExtra nav-button" href="#intro">  
                                                        About Us </a> </span>
                                        </p>
                                    </div>
                                    <div class="leftMenu">
                                        <?php
                                        include("about_left_navigation.php");
                                        ?>
                                    </div>
                                    <div class="hrcenter"></div>
                                    <div class="slogan">
                                        <p align="center">
                                            <img width="140" src="<?php echo baseUrl('images/slogan.png') ?>" alt="stand tall or naught" />
                                        </p>
                                    </div>
                                    <div class="hrcenter"></div>
                                </div>
                                <div id="rightPanel">
                                    <div id="container">
                                        <div class="slide" id="intro">
                                            <div class="prxtop1 sq-about-us" style="background-position: center 0px; background:url(<?php echo baseUrl('upload/about/intro/' . $options['ABOUT_INTRO_BANNER']); ?>) top fixed;">
                                                <h2 class="titleMLarge shadowTxt " style="margin-top: 0px;"><?php
                                                    if (isset($options["ABOUT_INTRO_BANNER_TITLE"])) {
                                                        echo $options["ABOUT_INTRO_BANNER_TITLE"];
                                                    } else {
                                                        echo '';
                                                    }
                                                    ?></h2>
                                            </div>
                                            <div class="wrapperinner">
                                                <div class="aboutContent AboutMargin">
                                                    <div class="right-side-content aboutIntroText">
                                                        <?php
                                                        if (isset($options["ABOUT_INTRO_DESCRIPTION"])) {
                                                            echo html_entity_decode($options["ABOUT_INTRO_DESCRIPTION"]);
                                                        } else {
                                                            echo '';
                                                        }
                                                        ?>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="slide" id="journey">
                                            <div class="wrapperinner">
                                                <div class="aboutContent AboutMargin">
                                                    <table width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td> 

                                                                    <h1 style="opacity:1 !important" data-name="modal1" class=" quickReadAbt modallink">
                                                                        <?php
                                                                        if (isset($options["ABOUT_JOURNEY_TITLE"])) {
                                                                            echo $options["ABOUT_JOURNEY_TITLE"];
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?> </h1></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="100%">
                                                                    <section class="right-side-content">
                                                                        <?php
                                                                        if (isset($options["ABOUT_JOURNEY_DESCRIPTION"])) {
                                                                            echo html_entity_decode($options["ABOUT_JOURNEY_DESCRIPTION"]);
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?>
                                                                    </section>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!--end aboutContent AboutMargin-->
                                            </div>
                                            <!--end wrapperinner-->
                                        </div>
                                        <div class="slide" id="philosophy">
                                            <div class="wrapperinner">
                                                <div class="homeContent">

                                                    <h1 style="opacity:1 !important" data-name="wayLife" class=" quickReadAbt modallink">
                                                        <?php
                                                        if (isset($options["ABOUT_WAY_OF_LIFE_TITLE"])) {
                                                            echo $options["ABOUT_WAY_OF_LIFE_TITLE"];
                                                        } else {
                                                            echo '';
                                                        }
                                                        ?></h1>
                                                    <h2 align="left"><img style="width:180px;" src="<?php echo baseUrl('images/slogan.png') ?>" alt="slogan" alt="stand tall or naught" /> </h2>

                                                    <section class="right-side-content">

                                                        <?php
                                                        if (isset($options["ABOUT_WAY_OF_LIFE_DESCRIPTION"])) {
                                                            echo html_entity_decode($options["ABOUT_WAY_OF_LIFE_DESCRIPTION"]);
                                                        } else {
                                                            echo '';
                                                        }
                                                        ?>
                                                    </section>
                                                </div>
                                            </div>
                                        </div> <!-- why end -->
                                        <div class="slide" id="brand">
                                            <div class="wrapperinner">
                                                <div class="homeContent">
                                                    <h1 style="opacity:1 !important;padding:10px 0 !important;" data-name="brandModal" class=" quickReadAbt modallink">

                                                        <?php
                                                        if (isset($options["ABOUT_BRAND_TITLE"])) {
                                                            echo $options["ABOUT_BRAND_TITLE"];
                                                        } else {
                                                            echo '';
                                                        }
                                                        ?>
                                                    </h1>
                                                    <div class="brandSlide">
                                                        <div class="slidePng">
                                                            <a class="next2" ></a>  
                                                            <a class="prev2"></a>
                                                        </div>
                                                        <div style=" clear:both"></div>
                                                        <div class="brandSideContent">
                                                            <?php
                                                            $brandSlideCount = count($brandSlide);
                                                            if ($brandSlideCount > 0) {
                                                                for ($i = 0; $i < $brandSlideCount; $i++) {
                                                                    ?>
                                                                    <img src="<?php echo baseUrl("upload/album/$album_id/$brandSlide[$i]"); ?>" alt="brand" />
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    if (isset($options["ABOUT_BRAND_DESCRIPTION"])) {
                                                        echo html_entity_decode($options["ABOUT_BRAND_DESCRIPTION"]);
                                                    } else {
                                                        echo '';
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div> <!-- brand end-->
                                        <div class="slide who" id="board">
                                            <div class="wrapperinner">
                                                <div class="aboutContent businessContent">
                                                    <div class="content ">
                                                        <div class="businessText">
                                                            <div class="businessText">
                                                                <h1 style="opacity:1 !important;padding:10px 0 !important;" data-name="boardModal" class=" quickReadAbt modallink">
                                                                    <?php
                                                                    if (isset($options["ABOUT_BOARD_TITLE"])) {
                                                                        echo $options["ABOUT_BOARD_TITLE"];
                                                                    } else {
                                                                        echo '';
                                                                    }
                                                                    ?>
                                                                </h1>
                                                            </div>
                                                            <?php
                                                            if (isset($options["ABOUT_BOARD_DESCRIPTION"])) {
                                                                echo html_entity_decode($options["ABOUT_BOARD_DESCRIPTION"]);
                                                            } else {
                                                                echo '';
                                                            }
                                                            ?>
                                                            <div class="contentTxtNormal ">
                                                            </div>
                                                            <div class="imgTall BoardMemberBox">
                                                                <div class="boardMember1Details">
                                                                    <div class="teamclose closepop">close</div> 
                                                                    <h3>Muhammad Ghulam Faruq</h3>
                                                                    <div class="scroll-pane">
                                                                        <?php
                                                                        if (isset($options['MOUAMMAD_GHULAM_FARUQ_TEXT'])) {
                                                                            echo html_entity_decode($options['MOUAMMAD_GHULAM_FARUQ_TEXT']);
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <div class="boardMember2Details">
                                                                    <div class="teamclose closepop">close</div> 
                                                                    <h3>Salima Bensaid</h3>
                                                                    <div class="scroll-pane">
                                                                        <?php
                                                                        if (isset($options['SALIMA_BENSAID_TEXT'])) {
                                                                            echo html_entity_decode($options['SALIMA_BENSAID_TEXT']);
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <div class="boardMember3Details">
                                                                    <div class="teamclose closepop">close</div> 
                                                                    <h3> Boutaina Faruq</h3>
                                                                    <div class="scroll-pane">
                                                                        <?php
                                                                        if (isset($options['BOUTAINA_FARUQ_TEXT'])) {
                                                                            echo html_entity_decode($options['BOUTAINA_FARUQ_TEXT']);
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <div class="boardMember4Details">
                                                                    <div class="teamclose closepop">close</div> 
                                                                    <h3>   Soukaina Faruq</h3>
                                                                    <div class="scroll-pane">
                                                                        <?php
                                                                        if (isset($options['SOUKAINA_FARUQ_TEXT'])) {
                                                                            echo html_entity_decode($options['SOUKAINA_FARUQ_TEXT']);
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?>
                                                                    </div>    
                                                                </div>
                                                                <div class="boardMember5Details">
                                                                    <div class="teamclose closepop">close</div> 
                                                                    <h3>  Amira Faruq</h3>
                                                                    <div class="scroll-pane">
                                                                        <?php
                                                                        if (isset($options['AMIRA_FARUQ_TEXT'])) {
                                                                            echo html_entity_decode($options['AMIRA_FARUQ_TEXT']);
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?>
                                                                    </div>    
                                                                </div>
                                                                <!------------- Board Member Name -------------->
                                                                <div class="boardMember1">

                                                                    <div class="boardMember1Link"> 

                                                                        Muhammad Ghulam Faruq 

                                                                    </div>



                                                                </div>



                                                                <div class="boardMember2">

                                                                    <div class="boardMember2Link"> 

                                                                        Salima Bensaid 

                                                                    </div>







                                                                </div>



                                                                <div class="boardMember3">

                                                                    <div class="boardMember3Link"> 

                                                                        Boutaina Faruq 

                                                                    </div>





                                                                </div>



                                                                <div class="boardMember4">

                                                                    <div class="boardMember4Link"> 

                                                                        Soukaina Faruq

                                                                    </div>







                                                                </div>



                                                                <div class="boardMember5">

                                                                    <div class="boardMember5Link"> 

                                                                        Amira Faruq

                                                                    </div>





                                                                </div>



                                                                <img src="<?php echo baseUrl('images/img/board.jpg') ?>" alt="board">

                                                                    <p style="margin-top: 10px;">Click on individual board members to read what they have to say about SQ</p>

                                                                    <div style="clear:both;"></div>

                                                                    <div class="boardMemberInindividual" style="display:none;">

                                                                        <p class="boardMember1Link">Muhammad Ghulam Faruq</p>

                                                                        <p class="boardMember2Link">Salima Bensaid </p>

                                                                        <p class="boardMember3Link">Boutaina Faruq </p>

                                                                        <p class="boardMember4Link">Soukaina Faruq</p>

                                                                        <p class="boardMember5Link">Amira Faruq</p>

                                                                    </div><!--boardMemberInindividual-->



                                                            </div>





                                                        </div>



                                                        <div class="imageContent">

                                                            <div class="businessText">

                                                            </div>



                                                        </div>





                                                    </div>



                                                </div>

                                            </div>



                                        </div>


                                        <div class="slide CarrerPage businessContent" id="organogram" style="">



                                            <div class="wrapperinner">



                                                <div class="aboutContent businessContent keymanagement org">





                                                    <div class="content">

                                                        <div class="businessText orgTxt">

                                                            <h1> <?php
                                                                if (isset($options["ABOUT_ORGANOGRAM_TITLE"])) {
                                                                    echo $options["ABOUT_ORGANOGRAM_TITLE"];
                                                                } else {
                                                                    echo '';
                                                                }
                                                                ?>

                                                            </h1>

                                                        </div>



                                                        <?php
                                                        if (isset($options["ABOUT_ORGANOGRAM_DESCRIPTION"])) {
                                                            echo html_entity_decode($options["ABOUT_ORGANOGRAM_DESCRIPTION"]);
                                                        } else {
                                                            echo '';
                                                        }
                                                        ?>



                                                        <div class="organogramImg">

                                                            <div class="overlay" style="display:none"></div><!--overlay-->



                                                            <div class="or-link4pop popcss">

                                                                <div class="teamclose closepop">close</div>

                                                                <p><?php
                                                                    if (isset($options["ABOUT_CHIEF_EXECUTIVE_OFFICER_DESCRIPTION"])) {
                                                                        echo html_entity_decode($options["ABOUT_CHIEF_EXECUTIVE_OFFICER_DESCRIPTION"]);
                                                                    } else {
                                                                        echo '';
                                                                    }
                                                                    ?></p>

                                                            </div>



                                                            <div class="or-link1pop popcss">

                                                                <div class="teamclose closepop">close</div>

                                                                <p><?php
                                                                    if (isset($options["ABOUT_CORE_OPERATIONS_LEADER_DESCRIPTION"])) {
                                                                        echo html_entity_decode($options["ABOUT_CORE_OPERATIONS_LEADER_DESCRIPTION"]);
                                                                    } else {
                                                                        echo '';
                                                                    }
                                                                    ?></p>

                                                            </div>



                                                            <div class="or-link2pop popcss">

                                                                <div class="teamclose closepop">close</div>

                                                                <p><?php
                                                                    if (isset($options["ABOUT_LEVERAGE_TEAMS_DESCRIPTION"])) {
                                                                        echo html_entity_decode($options["ABOUT_LEVERAGE_TEAMS_DESCRIPTION"]);
                                                                    } else {
                                                                        echo '';
                                                                    }
                                                                    ?></p>

                                                            </div>



                                                            <div class="or-link3pop popcss">

                                                                <div class="teamclose closepop">close</div>

                                                                <p><?php
                                                                    if (isset($options["ABOUT_SHARED_SERVICES_GROUPS_DESCRIPTION"])) {
                                                                        echo html_entity_decode($options["ABOUT_SHARED_SERVICES_GROUPS_DESCRIPTION"]);
                                                                    } else {
                                                                        echo '';
                                                                    }
                                                                    ?></p>

                                                            </div>





                                                            <div class="or-link1">

                                                            </div>



                                                            <div class="or-link2">

                                                            </div>



                                                            <div class="or-link3">

                                                            </div>



                                                            <div class="or-link4">

                                                            </div>

                                                            <img class="organogramImage" style="margin:0 !important;" width="700" src="<?php echo baseUrl('images/img/organogram.png') ?>" alt="organogram">

                                                        </div>



                                                    </div>



                                                </div>



                                            </div>

                                        </div>





                                        <div class="slide CarrerPage SustainabilityPage" id="management" style="padding:39px 0 20rem;">

                                            <h3> </h3>

                                            <div class="wrapperinner">



                                                <div class="aboutContent businessContent keymanagement">





                                                    <div class="content">

                                                        <div class="businessText">

                                                            <h1> <?php
                                                                if (isset($options["ABOUT_LEADERSHIP_TEAM_TITLE"])) {
                                                                    echo $options["ABOUT_LEADERSHIP_TEAM_TITLE"];
                                                                } else {
                                                                    echo '';
                                                                }
                                                                ?>

                                                            </h1>  

                                                        </div>



                                                        <?php
                                                        if (isset($options["ABOUT_LEADERSHIP_TEAM_DESCRIPTION"])) {
                                                            echo html_entity_decode($options["ABOUT_LEADERSHIP_TEAM_DESCRIPTION"]);
                                                        } else {
                                                            echo '';
                                                        }
                                                        ?>



                                                        <div class="imageContent">





                                                            <table class="sas" width="100%" border="0">

                                                                <tr>

                                                                    <td class="temTop" colspan="4">
                                                                        <h3>    
                                                                            <div class="slidePng"> 
                                                                                <a class="next2" href="#"></a>  <a class="prev2" href="#"></a>
                                                                            </div>
                                                                        </h3>
                                                                    </td>

                                                                </tr>



                                                                <tr> 
                                                                    <!--start tr teamSlider-->
                                                                    <td colspan="4" class="teamSlider">


                                                                        <!--start td teamSlider-->

                                                                        <!--Start Computer leader--> 
                                                                        <div id="mycarousel" class="carouselpc">


                                                                            <?php $employeeArrayCount = count($employeeArray); ?>
                                                                            <?php if ($employeeArrayCount > 0): ?>
                                                                                <div class="sliderContainer">
                                                                                    <?php for ($i = 0; $i < $employeeArrayCount; $i++): ?>



                                                                                        <div class="team <?php
                                                                                        if (($i + 1) % 3 == 0) {
                                                                                            echo 'nomarginright';
                                                                                        }
                                                                                        ?>">

                                                                                            <img src="<?php echo baseUrl('upload/employee_image/' . $employeeArray[$i]->employee_image); ?>" alt="<?php echo $employeeArray[$i]->employee_name; ?>" />
                                                                                            <div class="teamInfo">
                                                                                                <span class="teamMemberName"><?php echo $employeeArray[$i]->employee_name; ?> </span>
                                                                                                <span class="temPostion"><?php echo $employeeArray[$i]->employee_designation . '<br /> ' . $employeeArray[$i]->company_name; ?></span>
                                                                                                <div class="teamDetailsLink poplink<?php echo $employeeArray[$i]->employee_id; ?>"> Details</div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <?php
                                                                                        if (($i + 1) % 3 == 0) {
                                                                                            echo '</div> <div class="sliderContainer">';
                                                                                        }
                                                                                        ?>




                                                                                    <?php endfor; /* ($i=0; $i < $employeeArrayCount; $i++): */ ?>
                                                                                </div>
                                                                            <?php endif; /* ($employeeArrayCount > 0): */ ?>



                                                                        </div>

                                                                        <!--End Computer leader--> 



                                                                        <!--Start mobile leader--> 

                                                                        <div id="mycarousel2" style="display:none" class="carouselmobile">
                                                                            <?php if ($employeeArrayCount > 0): ?>
                                                                                <div class="sliderContainer">
                                                                                    <?php for ($i = 0; $i < $employeeArrayCount; $i++): ?>



                                                                                        <div class="team <?php
                                                                                        if (($i + 1) % 2 == 0) {
                                                                                            echo 'nomarginright';
                                                                                        }
                                                                                        ?>">

                                                                                            <img src="<?php echo baseUrl('upload/employee_image/' . $employeeArray[$i]->employee_image); ?>" alt="<?php echo $employeeArray[$i]->employee_name; ?>" />
                                                                                            <div class="teamInfo">
                                                                                                <span class="teamMemberName"><?php echo $employeeArray[$i]->employee_name; ?> </span>
                                                                                                <span class="temPostion"><?php echo $employeeArray[$i]->employee_designation . '<br /> ' . $employeeArray[$i]->company_name; ?></span>
                                                                                                <div class="teamDetailsLink poplink<?php echo $employeeArray[$i]->employee_id; ?>"> Details</div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <?php
                                                                                        if (($i + 1) % 2 == 0) {
                                                                                            echo '</div> <div class="sliderContainer">';
                                                                                        }
                                                                                        ?>




                                                                                    <?php endfor; /* ($i=0; $i < $employeeArrayCount; $i++): */ ?>
                                                                                </div>
                                                                            <?php endif; /* ($employeeArrayCount > 0): */ ?>

                                                                        </div>
                                                                        <!--End mobile leader--> 
                                                                        </div>
                                                                        <!--end imageContent-->
                                                                    </td>
                                                                    <!--end  td teamSlider-->
                                                                </tr>
                                                                <!--end  tr teamSlider-->


                                                            </table>



                                                            <div style="clear:both">  </div>



                                                            <div class="overlay" style="display:none"></div> <!--overlay-->

                                                            <?php if ($employeeArrayCount > 0): ?>
                                                                <?php for ($i = 0; $i < $employeeArrayCount; $i++): ?>

                                                                    <div id="team1" class="team1 tembox popin<?php echo $employeeArray[$i]->employee_id; ?>" style="display:none">

                                                                        <div class="teamclose closepop">close</div>



                                                                        <h3><?php echo $employeeArray[$i]->employee_name; ?></h3>

                                                                        <span><?php echo $employeeArray[$i]->employee_designation . ', ' . $employeeArray[$i]->company_name; ?></span>

                                                                        <div class="scroll-pane">

                                                                            <?php echo html_entity_decode($employeeArray[$i]->employee_comment); ?>
                                                                        </div>



                                                                    </div>
                                                                <?php endfor; /* ($i = 0; $i < $employeeArrayCount; $i++): */ ?>

                                                            <?php endif; /* ($employeeArrayCount > 0) */ ?>

                                                        </div>
                                                        <!--                                End Content-->





                                                    </div>
                                                    <!--end aboutContent businessContent keymanagement -->


                                                </div>
                                                <!--                        end wrapperinner-->

                                            </div>

                                            <!--end slide CarrerPage SustainabilityPage-->


                                        </div>

                                        <!--end container-->




                                    </div>

                                </div>

                            </div>

                            </div>





                            <div class="sqModal fullW" data-id="modal1"> <a class="modalclose"> X </a>
                                <div class="modalinner2 modalinner-80">
                                    <div id="owl-demo" class="owl-carousel">
                                        <?php
                                        if (isset($options["ABOUT_JOURNEY_QUICKREAD"])) {
                                            echo html_entity_decode($options["ABOUT_JOURNEY_QUICKREAD"]);
                                        }
                                        ?>

                                    </div>
                                </div>
                            </div>

                            <div class="sqModal fullW" data-id="wayLife"> <a class="modalclose">X</a>
                                <div class="modalinner2 modalinner-80">
                                    <div id="way-life" class="owl-carousel">

                                        <?php
                                        if (isset($options["ABOUT_WAY_OF_LIFE_QUICKREAD"])) {
                                            echo html_entity_decode($options["ABOUT_WAY_OF_LIFE_QUICKREAD"]);
                                        }
                                        ?>


                                    </div>
                                </div>
                            </div>


                            <div class="sqModal fullW" data-id="brandModal"> <a class="modalclose"> X </a>
                                <div class="modalinner2 modalinner-80">
                                    <div id="brand-modal" class="owl-carousel">
                                        <?php
                                        if (isset($options["ABOUT_BRAND_QUICKREAD"])) {
                                            echo html_entity_decode($options["ABOUT_BRAND_QUICKREAD"]);
                                        }
                                        ?>



                                    </div>
                                </div>
                            </div>


                            <div class="sqModal fullW" data-id="boardModal"> <a class="modalclose"> X </a>
                                <div class="modalinner2 modalinner-80">
                                    <div id="board-modal" class="owl-carousel">
                                        <?php
                                        if (isset($options["ABOUT_BOARD_QUICKREAD"])) {
                                            echo html_entity_decode($options["ABOUT_BOARD_QUICKREAD"]);
                                        }
                                        ?>



                                    </div>
                                </div>
                            </div>

                            <script>
                                $(".leftMenu ul li a").click(function(){
                                    $('.sqModal').removeClass('active');
                                });
                                </script>

                            <?php
                            include("about_footer.php");
                            ?>

                        </body></html>