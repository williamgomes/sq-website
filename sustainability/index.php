<?php
include '../config/config.php';
$options = array();
$fields = "'SUSTAINABILITY_INTRO_DESCRIPTION','SUSTAINABILITY_INTRO_BANNER','GREEN_INITIATIVE_BANNER','GREEN_INITIATIVE_BANNER_TITLE','INNOVATION_CENTRE_BANNER_TITLE','INNOVATION_CENTRE_BANNER','SQ_STATION_BANNER','SQ_STATION_BANNER_TITLE','WORK_PLACE_SAFETY_BANNER','WORK_PLACE_SAFETY_BANNER_TITLE'";
$susFieldsSql = "SELECT CS_option,CS_value FROM config_settings WHERE CS_option IN ($fields)";
$susFieldsResult = mysqli_query($con, $susFieldsSql);
if ($susFieldsResult) {
    while ($susFieldsResultRowObj = mysqli_fetch_object($susFieldsResult)) {
        $options[$susFieldsResultRowObj->CS_option] = $susFieldsResultRowObj->CS_value;
    }
} else {
    if (DEBUG) {
        $err = "susFieldsResult error" . mysqli_error($con);
    } else {
        $err = "susFieldsResult query failed";
    }
}
$pageUrlName = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <title>SQ GROUP | Sustainability</title>        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="SQ-Group,KNITWEAR,LINGERIE,SHIRT,RETAIL,Biriqina">
            <meta name="author" content="SQ-Group">
                <meta name="description" content="<?php if (isset($options["SUSTAINABILITY_INTRO_DESCRIPTION"])) {
                                echo html_entity_decode($options["SUSTAINABILITY_INTRO_DESCRIPTION"]);
                            } else {
                                echo '';
                            } ?>">

                    <?php include 'sustainability_header.php'; ?>

                    </head>
                    <body>
                        <div id="topBarContainer"> </div>
                        <div class="mobilemenu" style="display:none">
                            <div class="rmm">
                                <?php include 'sustainability_left_navigation.php'; ?>
                            </div>
                        </div>
                        <div id="wrapper">
                            <div id="leftPanel" class="">
                                <div class="shadowIe"></div>
                                <div class="logo">
                                    <p align="center"><a title="Home" href="<?php echo baseUrl('index.php');?>"> <img width="100" src="<?php echo baseUrl('images/logo.png') ?>" /></a><br />
                                        <span class="sus"><a class="sasLogoLink" title="sustainability" href="<?php echo baseUrl('sustainability/index.php');?>"> Sustainability </a></span> </p>
                                </div>

                                <div class="leftMenu">
                                    <?php include 'sustainability_left_navigation.php'; ?>
                                </div>
                                <div class="hrcenter"></div>
                                <div class="slogan"><p align="center">
                                        <img width="140" src="<?php echo baseUrl('images/slogan.png') ?>" alt="Slogan" />
                                    </p>


                                </div>

                                <div class="hrcenter"></div>
                            </div>
                            <div id="rightPanel" class="rightPanelGap">
                                <div class="sasContainer">
                                    <div class="sasContainerLeft">
                                        <div class="sasSliderContentMain">
                                            <div class="sasSliderContent">
                                                <img alt="sustainability banner" src="<?php echo baseUrl('upload/sustainability/intro/' . $options['SUSTAINABILITY_INTRO_BANNER']); ?>" width="100%"  />
                                            </div>
                                        </div>

                                        <div class="SasDis">
                                                <?php
                                                if (isset($options["SUSTAINABILITY_INTRO_DESCRIPTION"])) {
                                                    echo html_entity_decode($options["SUSTAINABILITY_INTRO_DESCRIPTION"]);
                                                } else {
                                                    echo '';
                                                }
                                                ?>

                                               </div>

                                    </div>

                                    <div class="sasContainerRight">

                                        <?php include 'sustainability_right_navigation.php';?>
                                    </div>

                                    <!--end  containerRight-->
                                </div>    
                                <!-- Container --> 
                            </div>
                            <!--end rightPanel-->
                        </div>   
                        <!-- end wrapper-->

                    </body>
                    </html>