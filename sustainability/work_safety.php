<?php
include '../config/config.php';
$options = array();
$fields = "'WORK_PLACE_SAFETY_BANNER','WORK_PLACE_SAFETY_BANNER_TITLE','WORK_PLACE_SAFETY_DESCRIPTION','WORK_PLACE_SAFETY_QUICKREAD'";
$susFieldsSql = "SELECT CS_option,CS_value FROM config_settings WHERE CS_option IN ($fields)";
$susFieldsResult = mysqli_query($con, $susFieldsSql);
if ($susFieldsResult) {
    while ($susFieldsResultRowObj = mysqli_fetch_object($susFieldsResult)) {
        $options[$susFieldsResultRowObj->CS_option] = $susFieldsResultRowObj->CS_value;
    }
} else {
    if (DEBUG) {
        $err = "susFieldsResult error" . mysqli_error($con);
    } else {
        $err = "susFieldsResult query failed";
    }
}

/* Start: block query */
$blocks = array();
$blockFields = "'WORK_PLACE_SAFTY','WORK_PLACE_SAFTY-FIRE_SAFTY','WORK_PLACE_SAFTY-BUILDING_SAFTY','WORK_PLACE_SAFTY-JOB_SAFTY'";
$blockSql = "SELECT * FROM sustainability_block WHERE sustainability_block_page_name IN ($blockFields) ORDER BY sustainability_block_priority DESC";
$blockSqlResult = mysqli_query($con, $blockSql);

if ($blockSqlResult) {
    if (mysqli_num_rows($blockSqlResult)) {
        while ($blockSqlResultRowObj = mysqli_fetch_object($blockSqlResult)) {
            //sustainability_block_page_name
            if (isset($blocks[$blockSqlResultRowObj->sustainability_block_page_name])) {
                $blocks[$blockSqlResultRowObj->sustainability_block_page_name][] = $blockSqlResultRowObj;
            } else {
                $blocks[$blockSqlResultRowObj->sustainability_block_page_name] = array();
                $blocks[$blockSqlResultRowObj->sustainability_block_page_name][] = $blockSqlResultRowObj;
            }
        }
    }
    mysqli_free_result($blockSqlResult);
} else {
    if (DEBUG) {
        $err = "blockSqlResult error" . mysqli_error($con);
    } else {
        $err = "blockSqlResult query failed";
    }
}
//printDie($blocks);
/* End: block query */
/* Start code for menu active */
$pageUrlName = '';
$pageUrlName = basename(__FILE__, '.php');
$pageUrlName = $pageUrlName . '.php';
/* End code for menu active */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <title>SQ GROUP | Work Place Safety</title>        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="SQ-Group,KNITWEAR,LINGERIE,SHIRT,RETAIL,Biriqina">
            <meta name="author" content="SQ-Group">
                <meta name="description" content="<?php
                if (isset($options["WORK_PLACE_SAFETY_DESCRIPTION"])) {
                    echo html_entity_decode($options["WORK_PLACE_SAFETY_DESCRIPTION"]);
                } else {
                    echo '';
                }
                ?>">

                    <?php include 'sustainability_header.php'; ?>

                    <!-- Load ScrollTo -->
                    <script src="<?php echo baseUrl(); ?>js/jquery.scrollTo-1.4.2-min.js"></script>
                    <!-- Load LocalScroll -->
                    <script src="<?php echo baseUrl(); ?>js/jquery.localscroll-1.2.7-min.js"></script>
                    <script type="text/javascript" src="<?php echo baseUrl(); ?>js/jquery.easing.1.3.js"></script>
                    <script>

                        // When the document is loaded...
                        $(document).ready(function()
                        {
                            // Scroll the whole document 
                            $('#scroll-link').localScroll({
                                target: 'body'
                            });

                        });
                    </script>

                    </head>
                    <body>
                        <div id="topBarContainer"> </div>
                        <div class="mobilemenu" style="display:none">
                            <div class="rmm">
                                <?php include 'sustainability_left_navigation.php'; ?>
                            </div>
                        </div>
                        <div id="wrapper">
                            <div id="leftPanel" class="">

                                <div class="shadowIe"></div>
                                <div class="logo">
                                    <p align="center"><a title="Home" href="<?php echo baseUrl('index.php'); ?>"> <img width="100" src="<?php echo baseUrl('images/logo.png') ?>" /></a><br />
                                        <span class="sus"><a class="sasLogoLink" title="sustainability" href="<?php echo baseUrl('sustainability/index.php'); ?>"> Sustainability </a></span> </p>
                                </div>


                                <div class="leftMenu">
                                    <?php include 'sustainability_left_navigation.php'; ?>
                                </div>
                                <div class="hrcenter"></div>
                                <div class="slogan"><p align="center">
                                        <img width="140" src="<?php echo baseUrl('images/slogan.png') ?>" alt="Slogan" />
                                    </p>


                                </div>

                                <div class="hrcenter"></div>


                            </div>
                            <div id="rightPanel" class="rightInner">

                                <div class="sasContainer">



                                    <div class="sasInnerLeft">
                                        <div class="imgslider">
                                            <img src="<?php echo baseUrl('upload/sustainability/work_place_safety/' . $options["WORK_PLACE_SAFETY_BANNER"]) ?>" width="100%" alt="work place safety" /></div><!--imgslider-->

                                        <div class="sasDescript">
                                            <div class="sasDescriptHeading">
                                                <h2 style="opacity:1 !important;" data-name="workSaftyModal" class="quickReadSus modallink">
                                                    <?php
                                                    if (isset($options["WORK_PLACE_SAFETY_BANNER_TITLE"])) {
                                                        echo $options["WORK_PLACE_SAFETY_BANNER_TITLE"];
                                                    } else {
                                                        echo '';
                                                    }
                                                    ?></h2></div>

                                            <?php
                                            if (isset($options["WORK_PLACE_SAFETY_DESCRIPTION"])) {
                                                echo html_entity_decode($options["WORK_PLACE_SAFETY_DESCRIPTION"]);
                                            } else {
                                                echo '';
                                            }
                                            ?>
                                            <div style="clear:both"></div>
                                            <?php $blocksCounter = count($blocks); ?>
                                            <?php if ($blocksCounter > 0): ?>
                                                <!--                                            FIRE_SAFTY start-->
                                                <?php $fireSaftyCounter = count($blocks['WORK_PLACE_SAFTY-FIRE_SAFTY']); ?>
                                                <?php if ($fireSaftyCounter > 0): ?>


                                                    <h3 class="sasContentTableBeforeTitle" align="left">Fire Safety </h3>
                                                    <div style="clear:both"></div>
                                                    <?php for ($i = 0; $i < $fireSaftyCounter; $i++): ?>
                                                        <div id="block_<?php echo $blocks['WORK_PLACE_SAFTY-FIRE_SAFTY'][$i]->sustainability_block_id; ?>" class="sasContentTable">

                                                            <div class="tdFull">

                                                                <div class="sasnewsboxLeftHighliths">

                                                                    <span class="fullConText"><?php echo $blocks['WORK_PLACE_SAFTY-FIRE_SAFTY'][$i]->sustainability_block_title; ?> </span>
                                                                    <br />


                                                                    <p>


                                                                        <img align="left" class="relative imghalf" src="<?php echo baseUrl('upload/sustainability_block_image/large/' . $blocks['WORK_PLACE_SAFTY-FIRE_SAFTY'][$i]->sustainability_block_image); ?>" width="286" />

                                                                        <?php
                                                                        echo preg_replace('/<p>/', '', html_entity_decode($blocks['WORK_PLACE_SAFTY-FIRE_SAFTY'][$i]->sustainability_block_description), 1);
                                                                        ;
                                                                        ?>


                                                                </div>
                                                            </div>

                                                        </div>
                                                    <?php endfor; /* ($i=0; $i < $fireSaftyCounter; $i++): */ ?>


                                                <?php endif; /* ($fireSaftyCounter > 0): */ ?>

                                                <!--                                            FIRE_SAFTY end -->

                                                <!--                                            BUILDING_SAFTY start-->
                                                <?php $buildingSaftyCounter = count($blocks['WORK_PLACE_SAFTY-BUILDING_SAFTY']); ?>
                                                <?php if ($buildingSaftyCounter > 0): ?>
                                                    <div class="sasContentTable sasContentTableSpecial" >

                                                        <div class="tdFull">
                                                            <p>Similar to fire safety, the following comprehensive measures are taken in securing building's solid foundation and structure and thereby safety of the buildings.  </p>

                                                            <div style="clear:both"></div>
                                                            <h3 class="sasContentTableBeforeTitle" align="left">Building Safety </h3>
                                                            <div style="clear:both"></div>
                                                        </div></div>
                                                    <?php for ($i = 0; $i < $buildingSaftyCounter; $i++): ?>
                                                        <div id="block_<?php echo $blocks['WORK_PLACE_SAFTY-BUILDING_SAFTY'][$i]->sustainability_block_id; ?>" class="sasContentTable">

                                                            <div class="tdFull">

                                                                <div class="sasnewsboxLeftHighliths">

                                                                    <span class="fullConText"><?php echo $blocks['WORK_PLACE_SAFTY-BUILDING_SAFTY'][$i]->sustainability_block_title; ?> </span>
                                                                    <br />
                                                                    <p>
                                                                        <img align="left" class="relative imghalf" src="<?php echo baseUrl('upload/sustainability_block_image/large/' . $blocks['WORK_PLACE_SAFTY-BUILDING_SAFTY'][$i]->sustainability_block_image); ?>" width="286" />

                                                                        <?php
                                                                        echo preg_replace('/<p>/', '', html_entity_decode($blocks['WORK_PLACE_SAFTY-BUILDING_SAFTY'][$i]->sustainability_block_description), 1);
                                                                        ;
                                                                        ?>


                                                                </div>
                                                            </div>

                                                        </div>
                                                    <?php endfor; /* ($i=0; $i < $buildingSaftyCounter; $i++): */ ?>


                                                <?php endif; /* ($buildingSaftyCounter > 0): */ ?>


                                                <!--                                          BUILDING_SAFTY end --> 


                                                <!--                                          JOB_SAFTY start-->
                                                <?php $jobSaftyCounter = count($blocks['WORK_PLACE_SAFTY-JOB_SAFTY']); ?>
                                                <?php if ($jobSaftyCounter > 0): ?>

                                                    <div id="" class="sasContentTableSpecial">
                                                        <h3 class="sasContentTableBeforeTitle" align="left">Job Safety </h3>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <?php for ($i = 0; $i < $jobSaftyCounter; $i++): ?>
                                                        <div id="block_<?php echo $blocks['WORK_PLACE_SAFTY-JOB_SAFTY'][$i]->sustainability_block_id; ?>" class="sasContentTable">

                                                            <div class="tdFull">

                                                                <div class="sasnewsboxLeftHighliths">

                                                                    <span class="fullConText"><?php echo $blocks['WORK_PLACE_SAFTY-JOB_SAFTY'][$i]->sustainability_block_title; ?> </span>
                                                                    <br/>
                                                                    <p>
                                                                        <img align="left" class="relative imghalf" src="<?php echo baseUrl('upload/sustainability_block_image/large/' . $blocks['WORK_PLACE_SAFTY-JOB_SAFTY'][$i]->sustainability_block_image); ?>" width="286" />

                                                                        <?php echo preg_replace('/<p>/', '', html_entity_decode($blocks['WORK_PLACE_SAFTY-JOB_SAFTY'][$i]->sustainability_block_description), 1); ?>


                                                                </div>
                                                            </div>

                                                        </div>
                                                    <?php endfor; /* ($i=0; $i < $jobSaftyCounter; $i++): */ ?>


                                                <?php endif; /* ($jobSaftyCounter > 0): */ ?>


                                                <!--                                        JOB_SAFTY end -->
                                            <?php endif; /* ($blocksCounter > 0): */ ?>




                                        </div><!--sasDescript-->

                                    </div><!--sasInnerLeft-->

                                    <div class="sasInnerRight">
                                        <h6> </h6>
                                        <?php $blocksCounter = count($blocks); ?>
                                        <?php if ($blocksCounter > 0): ?>
                                            <ul class="faiLink" id="scroll-link">
                                                <!--                                            fire safe start-->
                                                <?php $fireCounter = count($blocks['WORK_PLACE_SAFTY-FIRE_SAFTY']); ?>
                                                <?php if ($fireCounter > 0): ?>
                                                    <?php for ($i = 0; $i < $fireCounter; $i++): ?>
                                                        <li><a href="#block_<?php echo $blocks['WORK_PLACE_SAFTY-FIRE_SAFTY'][$i]->sustainability_block_id; ?> "><?php echo $blocks['WORK_PLACE_SAFTY-FIRE_SAFTY'][$i]->sustainability_block_title; ?>  </a></li>
                                                    <?php endfor; /* ($i=0; $i < $fireCounter; $i++): */ ?>
                                                <?php endif; /* ($fireCounter > 0): */ ?>

                                                <!--                                            fire safe end -->

                                                <!--                                            building safe start-->
                                                <?php $buildingCounter = count($blocks['WORK_PLACE_SAFTY-BUILDING_SAFTY']); ?>
                                                <?php if ($buildingCounter > 0): ?>
                                                    <?php for ($i = 0; $i < $buildingCounter; $i++): ?>
                                                        <li><a href="#block_<?php echo $blocks['WORK_PLACE_SAFTY-BUILDING_SAFTY'][$i]->sustainability_block_id; ?> "><?php echo $blocks['WORK_PLACE_SAFTY-BUILDING_SAFTY'][$i]->sustainability_block_title; ?>  </a></li>
                                                    <?php endfor; /* ($i=0; $i < $buildingCounter; $i++): */ ?>
                                                <?php endif; /* ($buildingCounter > 0): */ ?>

                                                <!--                                            building safe end -->

                                                <!--                                            job safe start-->
                                                <?php $jobCounter = count($blocks['WORK_PLACE_SAFTY-JOB_SAFTY']); ?>
                                                <?php if ($jobCounter > 0): ?>
                                                    <?php for ($i = 0; $i < $jobCounter; $i++): ?>
                                                        <li><a href="#block_<?php echo $blocks['WORK_PLACE_SAFTY-JOB_SAFTY'][$i]->sustainability_block_id; ?> "><?php echo $blocks['WORK_PLACE_SAFTY-JOB_SAFTY'][$i]->sustainability_block_title; ?>  </a></li>
                                                    <?php endfor; /* ($i=0; $i < $fireCounter; $i++): */ ?>
                                                <?php endif; /* ($jobCounter > 0): */ ?>

                                                <!--                                            job safe end -->

                                            </ul>

                                        <?php endif; /* ($blocksCounter > 0): */ ?>

                                    </div>

                                </div>

                                <!--sasContainer-->




                                <!-- Container --> 
                            </div>
                            <!--                            end rightInner-->
                        </div>

                        <!--end wrapper-->



                        <div class="sqModal fullW" data-id="workSaftyModal"> <a class="modalclose"> X </a>
                            <div class="modalinner2 modalinner-80">
                                <div id="modal" class="owl-carousel">
                                    <?php
                                    if (isset($options["WORK_PLACE_SAFETY_QUICKREAD"])) {
                                        echo html_entity_decode($options["WORK_PLACE_SAFETY_QUICKREAD"]);
                                    }
                                    ?>

                                </div>


                            </div>
                        </div>


                        <?php
                        include(basePath('quick_reader.php'));
                        ?>
                    </body>
                    </html>