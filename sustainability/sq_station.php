<?php
include '../config/config.php';
$options = array();
$fields = "'SQ_STATION_BANNER','SQ_STATION_BANNER_TITLE','SQ_STATION_DESCRIPTION','SQ_STATION_QUICKREAD'";
$susFieldsSql = "SELECT CS_option,CS_value FROM config_settings WHERE CS_option IN ($fields)";
$susFieldsResult = mysqli_query($con, $susFieldsSql);
if ($susFieldsResult) {
    while ($susFieldsResultRowObj = mysqli_fetch_object($susFieldsResult)) {
        $options[$susFieldsResultRowObj->CS_option] = $susFieldsResultRowObj->CS_value;
    }
} else {
    if (DEBUG) {
        $err = "susFieldsResult error" . mysqli_error($con);
    } else {
        $err = "susFieldsResult query failed";
    }
}

/* Start: block query */
$blocks = array();
$blockFields = "'SQ_STATION'";
$blockSql = "SELECT * FROM sustainability_block WHERE sustainability_block_page_name IN ($blockFields) ORDER BY sustainability_block_priority DESC";
$blockSqlResult = mysqli_query($con, $blockSql);

if ($blockSqlResult) {
    if (mysqli_num_rows($blockSqlResult)) {
        while ($blockSqlResultRowObj = mysqli_fetch_object($blockSqlResult)) {
            //sustainability_block_page_name
            if (isset($blocks[$blockSqlResultRowObj->sustainability_block_page_name])) {
                $blocks[$blockSqlResultRowObj->sustainability_block_page_name][] = $blockSqlResultRowObj;
            } else {
                $blocks[$blockSqlResultRowObj->sustainability_block_page_name] = array();
                $blocks[$blockSqlResultRowObj->sustainability_block_page_name][] = $blockSqlResultRowObj;
            }
        }
    }
    mysqli_free_result($blockSqlResult);
} else {
    if (DEBUG) {
        $err = "blockSqlResult error" . mysqli_error($con);
    } else {
        $err = "blockSqlResult query failed";
    }
}
//printDie($blocks, TRUE);
/* End: block query */
/* Start code for menu active */
$pageUrlName = '';
$pageUrlName = basename(__FILE__, '.php');
$pageUrlName = $pageUrlName . '.php';
/* End code for menu active */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <title>SQ GROUP | SQ Station</title>        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="SQ-Group,KNITWEAR,LINGERIE,SHIRT,RETAIL,Biriqina">
            <meta name="author" content="SQ-Group">
                <meta name="description" content="<?php
                if (isset($options["SQ_STATION_DESCRIPTION"])) {
                    echo html_entity_decode($options["SQ_STATION_DESCRIPTION"]);
                } else {
                    echo '';
                }
                ?>">

                    <?php include 'sustainability_header.php'; ?>

                    <!-- Load ScrollTo -->
                    <script src="<?php echo baseUrl(); ?>js/jquery.scrollTo-1.4.2-min.js"></script>
                    <!-- Load LocalScroll -->
                    <script src="<?php echo baseUrl(); ?>js/jquery.localscroll-1.2.7-min.js"></script>
                    <script type="text/javascript" src="<?php echo baseUrl(); ?>js/jquery.easing.1.3.js"></script>
                    <script>

                        // When the document is loaded...
                        $(document).ready(function()
                        {
                            // Scroll the whole document 
                            $('#scroll-link').localScroll({
                                target: 'body'
                            });

                        });
                    </script>

                    </head>
                    <body>
                        <div id="topBarContainer"> </div>
                        <div class="mobilemenu" style="display:none">
                            <div class="rmm">
                                <?php include 'sustainability_left_navigation.php'; ?>
                            </div>
                        </div>
                        <div id="wrapper">
                            <div id="leftPanel" class="">

                                <div class="shadowIe"></div>
                                <div class="logo">
                                    <p align="center"><a title="Home" href="<?php echo baseUrl('index.php'); ?>"> <img width="100" src="<?php echo baseUrl('images/logo.png') ?>" /></a><br />
                                        <span class="sus"><a class="sasLogoLink" title="sustainability" href="<?php echo baseUrl('sustainability/index.php'); ?>"> Sustainability </a></span> </p>
                                </div>


                                <div class="leftMenu">
                                    <?php include 'sustainability_left_navigation.php'; ?>
                                </div>
                                <div class="hrcenter"></div>
                                <div class="slogan"><p align="center">
                                        <img width="140" src="<?php echo baseUrl('images/slogan.png') ?>" alt="Slogan" />
                                    </p>


                                </div>

                                <div class="hrcenter"></div>


                            </div>
                            <div id="rightPanel" class="rightInner">

                                <div class="sasContainer">



                                    <div class="sasInnerLeft">
                                        <div class="imgslider">
                                            <img src="<?php echo baseUrl('upload/sustainability/sq_station/' . $options["SQ_STATION_BANNER"]) ?>" width="100%" alt="Green Initiative" /></div><!--imgslider-->

                                        <div class="sasDescript">
                                            <div class="sasDescriptHeading">
                                                <h2 style="opacity:1 !important;" data-name="sqStationModal" class="quickReadSus modallink">
                                                    <?php
                                                    if (isset($options["SQ_STATION_BANNER_TITLE"])) {
                                                        echo $options["SQ_STATION_BANNER_TITLE"];
                                                    } else {
                                                        echo '';
                                                    }
                                                    ?></h2></div>

                                            <?php
                                            if (isset($options["SQ_STATION_DESCRIPTION"])) {
                                                echo html_entity_decode($options["SQ_STATION_DESCRIPTION"]);
                                            } else {
                                                echo '';
                                            }
                                            ?>
                                            <div style="clear:both"></div>

                                            <?php $blocksCounter = count($blocks); ?>
                                            <?php if ($blocksCounter > 0): ?>
                                                <!--                                            green start-->
                                                <?php $greenCounter = count($blocks['SQ_STATION']); ?>
                                                <?php if ($greenCounter > 0): ?>
                                                    <?php for ($i = 0; $i < $greenCounter; $i++): ?>
                                                        <div id="block_<?php echo $blocks['SQ_STATION'][$i]->sustainability_block_id; ?>" class="sasContentTable">

                                                            <div class="tdFull">

                                                                <div class="sasnewsboxLeftHighliths">

                                                                    <span class="fullConText"><?php echo $blocks['SQ_STATION'][$i]->sustainability_block_title; ?> </span>
                                                                    <br/>


                                                                    <p>


                                                                        <img align="left" class="relative imghalf" src="<?php echo baseUrl('upload/sustainability_block_image/large/' . $blocks['SQ_STATION'][$i]->sustainability_block_image); ?>" width="286" />

                                                                        <?php echo preg_replace('/<p>/', '', html_entity_decode($blocks['SQ_STATION'][$i]->sustainability_block_description), 1); ?>


                                                                </div>
                                                            </div>

                                                        </div>
                                                    <?php endfor; /* ($i=0; $i < $greenCounter; $i++): */ ?>

                                                    <!--                                            green end -->
                                                <?php endif; /* ($greenCounter > 0): */ ?>

                                            <?php endif; /* ($blocksCounter > 0): */ ?>




                                        </div><!--sasDescript-->

                                    </div><!--sasInnerLeft-->

                                    <div class="sasInnerRight">
                                        <h6> </h6>
                                        <?php $blocksCounter = count($blocks); ?>
                                        <?php if ($blocksCounter > 0): ?>
                                            <ul class="faiLink" id="scroll-link">
                                                <!--                                            green start-->
                                                <?php $greenCounter = count($blocks['SQ_STATION']); ?>
                                                <?php if ($greenCounter > 0): ?>
                                                    <?php for ($i = 0; $i < $greenCounter; $i++): ?>
                                                        <li><a href="#block_<?php echo $blocks['SQ_STATION'][$i]->sustainability_block_id; ?> "><?php echo $blocks['SQ_STATION'][$i]->sustainability_block_title; ?>  </a></li>
                                                    <?php endfor; /* ($i=0; $i < $greenCounter; $i++): */ ?>

                                                    <!--                                            green end -->
                                                </ul>
                                            <?php endif; /* ($greenCounter > 0): */ ?>
                                        <?php endif; /* ($blocksCounter > 0): */ ?>

                                    </div>

                                </div>

                                <!--sasContainer-->




                                <!-- Container --> 
                            </div>
                            <!--                            end rightInner-->
                        </div>

                        <!--end wrapper-->


                        <div class="sqModal fullW" data-id="sqStationModal"> <a class="modalclose"> X </a>
                            <div class="modalinner2 modalinner-80">
                                <div id="modal" class="owl-carousel">
                                    <?php
                                    if (isset($options["SQ_STATION_QUICKREAD"])) {
                                        echo html_entity_decode($options["SQ_STATION_QUICKREAD"]);
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>
                        <?php
                        include(basePath('quick_reader.php'));
                        ?>
                    </body>
                    </html>