<?php
include ('./config/config.php');
include ('./lib/email/mail_helper_functions.php');

$err = "";
$fname = "";
$lname = "";
$email = "";
$dob = "";
$position = "";
$nationality = "";
$phone = "";
$gender = "";
$education = "";
$extension = array();
$extension = array('doc', 'docx', 'pdf'); //valid extensions for cv
//initializing variables
$CircularID = intval($_GET['circular_id']);
$PostApplyFor = '';
$emailsToSendNotification = '';

//getting circular information from database
$SelectCircular = mysqli_query($con, "SELECT * FROM circular WHERE circular_id=$CircularID");
if ($SelectCircular AND mysqli_num_rows($SelectCircular) > 0) {
    $GetCircular = mysqli_fetch_object($SelectCircular);
    $PostApplyFor = $GetCircular->circular_position;
    //splitting comma separated admin emails from database
    $emailsToSendNotification = explode(",", $GetCircular->circular_application_to_email);
} else {
    if (DEBUG) {
        echo 'SelectCircular error: ' . mysqli_error($con);
    }
}


if (isset($_POST['submit'])) {

    extract($_POST);
    $CheckEmail = mysqli_query($con, "SELECT * FROM candidate_db WHERE candidate_email='" . mysqli_real_escape_string($con, $email) . "' AND candidate_circular_id=$CircularID");
    $CountRow = mysqli_num_rows($CheckEmail);

    if ($CountRow > 0) {
        $err = "You already submitted your CV.";
    } else {

        if ($fname == "") {
            $err = "First Name field is required.";
        } elseif ($lname == "") {
            $err = "Last Name field is required.";
        } elseif ($dob == "") {
            $err = "Date of Birth field is required.";
        } elseif ($email == "") {
            $err = "E-mail Address field is required.";
        } elseif ($phone == "") {
            $err = "Phone No. field is required.";
        } elseif ($gender == "") {
            $err = "Gender field is required.";
        } elseif ($nationality == "") {
            $err = "Nationality field is required.";
        } elseif (!ctype_alpha($fname)) {
            $err = "First Name can only be alphabet.";
        } elseif (!ctype_alpha($lname)) {
            $err = "Last Name can only be alphabet.";
        } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $err = "Please provide a valid E-mail Address.";
        } elseif ($_FILES["cv"]["tmp_name"] == "") {
            $err = "Please upload your CV.";
        } else {



            //preparing cv for upload
            $cv_upload = basename($_FILES['cv']['name']);
            $ext = pathinfo($cv_upload, PATHINFO_EXTENSION); /* it will return me like jpeg, gif, pdf, png */
            $cv_name = $fname . '' . $lname . '.' . $ext; /* create custom image name color id will add  */
            $cv_source = $_FILES["cv"]["tmp_name"];

            if (!in_array($ext, $extension)) {
                $err = "Your CV must be in .doc, .docx or .pdf format.";
            } else {

                //creating directory


                if (!is_dir('cv/')) {
                    mkdir('cv/', 0777, TRUE);
                }

                $cv_target = 'cv/' . $cv_name;


                if (!move_uploaded_file($cv_source, $cv_target)) {
                    $err = "File Upload fail.";
                }

                /* Start: save to db and send mail */
                if ($err == "") {
                    $CandidateAdd = '';
                    $CandidateAdd .=' candidate_fname = "' . mysqli_real_escape_string($con, $fname) . '"';
                    $CandidateAdd .=', candidate_lname = "' . mysqli_real_escape_string($con, $lname) . '"';
                    $CandidateAdd .=', candidate_dob = "' . mysqli_real_escape_string($con, $dob) . '"';
                    $CandidateAdd .=', candidate_phone = "' . mysqli_real_escape_string($con, $phone) . '"';
                    $CandidateAdd .=', candidate_email = "' . mysqli_real_escape_string($con, $email) . '"';
                    $CandidateAdd .=', candidate_gender = "' . mysqli_real_escape_string($con, $gender) . '"';
                    $CandidateAdd .=', candidate_circular_id = "' . mysqli_real_escape_string($con, $CircularID) . '"';
                    $CandidateAdd .=', candidate_nationality = "' . mysqli_real_escape_string($con, $nationality) . '"';
                    $CandidateAdd .=', candidate_cv = "' . mysqli_real_escape_string($con, $cv_name) . '"';

                    $CandidateSql = "INSERT INTO candidate_db SET $CandidateAdd";
                    $AddCandidate = mysqli_query($con, $CandidateSql);


                    if ($AddCandidate) {
                        $CandidateID = mysqli_insert_id($con);
                        //sending email to candidate
                        $Subject = "Thank you for your Job Application";
                        $EmailBody = file_get_contents(baseUrl('emails/job_application/applicant_email.php?applicant_id=' . $CandidateID));
                        $sendEmailToApplicant = sendEmailFunction($email, $fname, 'no-reply@sqgc.com', $Subject, $EmailBody);

                        //sending email alerts to admin emails
                        if ($CircularID > 0) {

                            foreach ($emailsToSendNotification as $sendemail) {
                                $SubjectAdmin = "New CV Submission Alert!";
                                $EmailBodyAdmin = file_get_contents(baseUrl('emails/job_application/company_email.php?applicant_id=' . $CandidateID . '&position=' . base64_encode($GetCircular->circular_position) . '&target=' . base64_encode('/' . $cv_target)));
                                $sendEmailToApplicant = sendEmailFunction($sendemail, $sendemail, 'no-reply@sqgc.com', $SubjectAdmin, $EmailBodyAdmin);
                            }
                        }

                        $link = 'thank_you.php';
                        redirect($link);
                    } else {
                        echo 'AddCandidate Error: ' . mysql_error();
                        $err = "Insert Query failed.";
                    }
                }
                /* End: save to db and send mail */
            }
        }
    }
}

/* Start code for menu active*/
$pageUrlName = '';
$pageUrlName = basename(__FILE__, '.php');
$pageUrlName = $pageUrlName . '.php';
/* End code for menu active*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet"  href="css/style.css" type="text/css">
                <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
                <link rel="apple-touch-icon" href="apple-touch-icon.png" />
                <link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png" />
                <link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png" />
                <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png" />
                <link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png" />
                <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png" />
                <link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png" />
                <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png" />
                <link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png" />
                <!-- Custom scrollbars CSS -->
                <title>SQ GROUP</title>
                <!-- Google CDN jQuery with fallback to local -->
                <script src="js/jquery.min.js"></script>
                <link rel="stylesheet" href="css/responsivemobilemenu.css" type="text/css"/>
                <link rel="stylesheet" href="css/forms.css" type="text/css"/>
                <script type="text/javascript" src="js/responsivemobilemenu.js"></script>
                <!-- date picker -->
                <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

                <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
                <script>
                    $(function() {
                        $("#datepicker").datepicker();
                    });
                </script>
                <!-- end date picker -->

                <!--[if IE 6]>
                      <link href="css/ie.css" rel="stylesheet" type="text/css" />
                      <![endif]-->

                <!--[if IE 7]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 8]>
                        <link href="css/ie" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 9]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 10]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                        <![endif]-->

                <!--[if IE 8]>
                        <link href="css/ie.css" rel="stylesheet" type="text/css" />
                <![endif]-->
                <script type="text/javascript">
                    if ($(window).width() < 699) {
                        //small screen, load other JS files
                        $.getScript('js/device.js', function() {
                            //the script has been added to the DOM, you can now use it's code
                        });
                    }


                    window.onorientationchange = function()
                    {
                        window.location.reload();
                    }


                </script>
                </head>
                <body>
                    <div id="topBarContainer"> </div>
                    <div class="mobilemenu" style="display:none">
                        <div class="rmm">
                              <?php include 'home_left_menu.php'; ?>
                        </div>
                    </div>
                    <div id="wrapper">

                        <div id="leftPanel">
                            <div class="shadowIe"></div>
                            <div class="logo">
                                <p align="center">
                                    <a href="index.php"><img width="100" src="images/logo.png"></a>
                                    <br>
                                        <span class="sus">
                                            <a href="career.php" class="sasLogoLink sasLogoLinkExtra">  
                                                Careers   </a>
                                        </span>
                                </p>
                            </div>

                            <div class="leftMenu">
                                 <?php include 'home_left_menu.php'; ?>
                            </div>
                            <div class="hrcenter"></div>
                            <div class="slogan"><p align="center">
                                    <img width="140" src="images/slogan.png">
                                </p>


                            </div>

                            <div class="hrcenter"></div>

                        </div>

                        <div id="rightPanel" class="rightInner ">

                            <div class="careerContainer">
                                <div class="careerTop">
                                <!--<a href="index.html"><img src="images/logo_career.png" height="100" alt="logo" /></a>-->
                                </div><!--careerTop-->
                                <div class="careerInner"
                                     >     <h2>Online Job Application Form</h2>

                                    <div style="width:900px; clear:both; height:40px"></div>
<?php
if ($err != "") {
    ?>    
                                        <div style="width: 400px; display: block; margin: 0px auto; font-size: 18px; color: rgb(204, 0, 51);">
                                            <p align="center" style="display: block; color: rgb(255, 255, 255); background: none repeat scroll 0% 0% rgb(204, 153, 153); padding: 5px; position: relative; border: 1px solid rgb(204, 0, 51);"><?php echo $err; ?></p></div>

                                        <?php
                                    }
                                    ?>

                                    <?php
                                    if ($_GET['circular_id'] != 0) {
                                        ?>    
                                        <div style="display:block; width:900px; height:50px; margin:0 auto; ">
                                            <p class="help_text">You are applying for <strong><?php echo $GetCircular->circular_position; ?></p>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                    <div class="sections">
                                        <fieldset>
                                            <legend title="Application Instruction">Application Instruction</legend>
                                            <ol class="listed">
                                                <li>Fill up the Personal Details form below.</li>
                                                <li>Upload your resume.</li>
                                            </ol>
                                            <p class="help_text">If all the boxes are not filled up, the application will not be accepted.</p>
                                        </fieldset>
                                    </div>

                                    <div style="clear: both;"></div>

                                    <form name="appsubmit" action="apply_form.php?circular_id=<?php echo $_GET['circular_id']; ?>" method="post" enctype="multipart/form-data">

                                        <div class="sections">
                                            <fieldset>
                                                <legend title="Application Instruction">Personal Details</legend>
                                                <div style="display:block; float:left; height:auto; width:100%; line-height:50px">
                                                    <table>
                                                        <tr>
                                                            <td class="col1"><label>First Name:</label></td><td class="col2"><input type="text" name="fname" value="<?php echo $fname; ?>" /></td>
                                                            <td class="col1"><label>Last Name:</label></td><td class="col2"><input type="text" value="<?php echo $lname; ?>" name="lname" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col1"><label>Date of Birth:</label></td><td class="col2"><input type="text" value="<?php echo $dob; ?>" name="dob" id="datepicker"/></td>
                                                            <td class="col1"><label>E-mail Address:</label></td><td class="col2"><input type="text" value="<?php echo $email; ?>" name="email" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col1"><label>Phone No.:</label></td><td class="col2"><input type="text" value="<?php echo $phone; ?>" name="phone" /></td>
                                                            <td class="col1"><label>Nationality:</label></td><td class="col2"><input type="text" value="<?php echo $nationality; ?>" name="nationality" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col1"><label>Gender:</label></td>
                                                            <td class="col2"><select name="gender">
                                                                    <option value=""> -- Select Gender -- </option>
                                                                    <option value="male" <?php if ($gender == 'male') {
                                        echo 'selected';
                                    } ?>>Male</option>
                                                                    <option value="female" <?php if ($gender == 'female') {
                                        echo 'selected';
                                    } ?>>Female</option>
                                                                </select>
                                                            </td>
                                                            <td class="col1"><!--<label>Last Education:</label>--></td>
                                                            <td class="col2"><!--<select name="education">
                                                            <option value=""> -- Select Last Education -- </option>
                                                            <option value="College">College</option>
                                                            <option value="Associate Degree">Associate Degree</option>
                                                            <option value="Bachelor's Degree">Bachelor's Degree</option>
                                                            <option value="Postgraduate">Postgraduate</option>
                                                            <option value="Master's Degree">Master's Degree</option>
                                                            <option value="Ph.D">Ph.D</option>
                                                            </select>--></td>

                                                        </tr>
                                                    </table>



                                                </div>
                                            </fieldset>
                                        </div>

                                        <div style="clear: both;"></div>

                                        <div class="sections">
                                            <fieldset style="margin-top: 10px;">
                                                <legend title="Application Instruction">Upload Resume</legend>
                                                <table>
                                                    <tr>
                                                        <td class="col3"><label>Upload your CV:</</label></td>
                                                        <td class="col4"><input type="file" name="cv" style=" margin-left:40px; width:auto !important;" /></td>
                                                    </tr>
                                                </table>

                                                <p class="help_text">File can be in Word Document (.doc, .docx) or PDF (.pdf) format. Any other format is not allowed.</p>    
                                            </fieldset>
                                        </div>


                                        <div class="sections">
                                            <button type="submit" class="btn" name="submit">Submit Application</button>
                                        </div>

                                    </form>
                                </div>





                            </div><!--careerInner-->

                        </div>






                        <!-- Container --> 
                    </div>
                    </div>



                </body>
                </html>