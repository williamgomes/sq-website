<?php
include '../config/config.php';
$options = array();
$fields = "'SUPPORTING_INDUSTRY_INTRO_BANNER_TITLE','SUPPORTING_INDUSTRY_INTRO_BANNER','SUPPORTING_INDUSTRY_INTRO_DESCRIPTION'";
$suopportingIndustryFieldsSql = "SELECT CS_option,CS_value FROM config_settings WHERE CS_option IN ($fields)";
$suopportingIndustryFieldsResult = mysqli_query($con, $suopportingIndustryFieldsSql);
if ($suopportingIndustryFieldsResult) {
    while ($suopportingIndustryFieldsResultRowObj = mysqli_fetch_object($suopportingIndustryFieldsResult)) {
        $options[$suopportingIndustryFieldsResultRowObj->CS_option] = $suopportingIndustryFieldsResultRowObj->CS_value;
    }
} else {
    if (DEBUG) {
        $err = "suopportingIndustryFieldsResult error" . mysqli_error($con);
    } else {
        $err = "suopportingIndustryFieldsResult query failed";
    }
}
$pageUrlName = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>SQ GROUP | Supporting Industry</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="SQ-Group,KNITWEAR,LINGERIE,SHIRT,RETAIL,Biriqina">
<meta name="author" content="SQ-Group">
<meta name="description" content="SQ visions to build an organization that would thrive on its tribal culture. After all, we all have a common story – SQ. The story of SQ, its attitude and the way it conducts its suopportingIndustry are part of this segment. It's all about what we are, not what we have.">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include 'supporting_industry_header.php';?>
</head>
<body class="productbody">
<div id="topBarContainer"> </div>
<div class="mobilemenu" style="display:none">
              <div class="rmm">
                <?php include 'supporting_industry_left_navigation.php';?>
                </div>
    </div>
	<div id="wrapper">
    <div id="leftPanel">
  <div class="shadowIe"></div>
    <div class="logo">
      <p align="center">
      <a href="<?php echo baseUrl();?>"><img width="100" src="<?php echo baseUrl();?>images/logo.png" /></a>
      <br>
<span class="sus">
    <a class="sasLogoLink sasLogoLinkExtra" href="<?php echo baseUrl("supporting_industry/");?>">  Supporting <br />
Industries  </a>
</span>
      </p>
     </div>
    <div class="leftMenu">
      <?php include 'supporting_industry_left_navigation.php';?>
    </div>
    <div class="hrcenter"></div>
    <div class="slogan"><p align="center">
    <img width="140" src="<?php echo baseUrl();?>images/slogan.png" />
    </p>
     </div>
    <div class="hrcenter"></div>
  </div>
            <div id="rightPanel">
        <div class="contentbox" id="">
        	<div class="prxtop1 supportBg " style="background:url(<?php echo baseUrl('upload/supporting_industry/intro/' . $options['SUPPORTING_INDUSTRY_INTRO_BANNER']); ?>) top  fixed;">
            <h2 class="titleXlarge shadowTxt titleMLarge "> <?php if (isset($options["SUPPORTING_INDUSTRY_INTRO_BANNER_TITLE"])) {
                                echo $options["SUPPORTING_INDUSTRY_INTRO_BANNER_TITLE"];
                            } else {
                                echo '';
                            } ?> </h2>
			<h4 class="titleSub"></h4>
          </div>
          <div class="prxtop2">
        	<div class="proInner proInnerHome">
                   <div class="introContent">
                      <?php
                                if (isset($options["SUPPORTING_INDUSTRY_INTRO_DESCRIPTION"])) {
                                    echo html_entity_decode($options["SUPPORTING_INDUSTRY_INTRO_DESCRIPTION"]);
                                } else {
                                    echo '';
                                }
                                ?>
                     </div>
</div>
            </div>
            <div style="clear:both"></div>
        </div>
        

        </div>

  </div> <!-- wrapper --> 
</div>
       <script type="text/javascript">
        $(function() {

            $('.imgOpa').each(function() {

                $(this).hover(

                    function() {

                        $(this).stop().animate({ opacity: 0.7 }, 800);

                    },

                   function() {

                       $(this).stop().animate({ opacity: 0.9 }, 800);

                   })

                });

        });

    </script>

    

    <script src="<?php echo baseUrl();?>js/jquery.backstretch.js"></script>

    

	<script>

        $("#link1").backstretch("images/suopportingIndustry.jpg");

  </script>

  

  	<script>

		$("#link2").backstretch("images/suopportingIndustry1.jpg");

  </script>

  

  	<script>

		$("#link3").backstretch("images/suopportingIndustry2.jpg");

  </script>

    	<script>

		$("#link4").backstretch("images/suopportingIndustry3.jpg");

  </script>

    

    

       <script type="text/javascript">

     $(document).ready(function(){

			$(window).bind('scroll',function(e){

				parallaxScroll();

			});

		 

			function parallaxScroll(){

				var scrolledY = $(window).scrollTop();

				$('.prxtop1').css('background-position','center -'+((scrolledY*0.2))+'px');

				$('.titleXlarge').css('marginTop','-'+((scrolledY*0.5))+'px');

				$('.fish').css('top','-'+((scrolledY*0.8))+'px');

			}

		 

		});

    </script>

    





<!-- Here we want a DYNAMIC value -->



</body>

</html>