<?php
include '../config/config.php';
$options = array();
$fields = "'PRINT_ELASTIC_BRA_CUP_BANNER','PRINT_ELASTIC_BRA_CUP_BANNER_TITLE','PRINT_ELASTIC_BRA_CUP_DESCRIPTION','PRINT_ELASTIC_BRA_CUP_TITLE'";
$suopportingIndustryFieldsSql = "SELECT CS_option,CS_value FROM config_settings WHERE CS_option IN ($fields)";
$suopportingIndustryFieldsResult = mysqli_query($con, $suopportingIndustryFieldsSql);
if ($suopportingIndustryFieldsResult) {
    while ($suopportingIndustryFieldsResultRowObj = mysqli_fetch_object($suopportingIndustryFieldsResult)) {
        $options[$suopportingIndustryFieldsResultRowObj->CS_option] = $suopportingIndustryFieldsResultRowObj->CS_value;
    }
} else {
    if (DEBUG) {
        $err = "suopportingIndustryFieldsResult error" . mysqli_error($con);
    } else {
        $err = "suopportingIndustryFieldsResult query failed";
    }
}
/* Start code for menu active*/
$pageUrlName = '';
$pageUrlName = basename(__FILE__, '.php');
$pageUrlName = $pageUrlName . '.php';
/* End code for menu active*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <title>SQ GROUP | Supporting Industry : Fabric</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
include("supporting_industry_sub_left_navigation.php");
?>

</head>
<body class="productbody">



<div id="topBarContainer"> </div>
<div class="mobilemenu" style="display:none">
              <div class="rmm">
                <?php include ('supporting_industry_left_navigation.php'); ?>
                </div>
    </div>
	<div id="wrapper">
    	
        <div id="leftPanel">
  <div class="shadowIe"></div>
    <div class="logo">
      <p align="center">
      <a href="<?php echo baseUrl();?>index.php"><img width="100" src="<?php echo baseUrl();?>images/logo.png" /></a>
      <br>
<span class="sus">
<a class="sasLogoLink sasLogoLinkExtra" href="<?php echo baseUrl();?>supporting_industry">  Supporting <br />
Industries  </a>
</span>
      </p>
     </div>
    
    <div class="leftMenu">
       <?php include ('supporting_industry_left_navigation.php'); ?>
    </div>
    <div class="hrcenter"></div>
    <div class="slogan"><p align="center">
    <img width="140" src="<?php echo baseUrl();?>images/slogan.png" />
    </p>
    
    
     </div>
    
    <div class="hrcenter"></div>
    
  </div>
        
   <div id="rightPanel">
   <div id="container">
        <div class="contentbox" id="">
        
        	<div class="prxtop1 prxtopPdyeing" style="background:url(<?php echo baseUrl('upload/supporting_industry/print_elastic_bra_cup/' . $options['PRINT_ELASTIC_BRA_CUP_BANNER']); ?>) center 0px fixed  ;">
            
            <h2 class="titleXlarge titleMLargeNormal" >
                                            <?php
                                            if (isset($options["PRINT_ELASTIC_BRA_CUP_BANNER_TITLE"])) {
                                                echo $options["PRINT_ELASTIC_BRA_CUP_BANNER_TITLE"];
                                            } else {
                                                echo '';
                                            }
                                            ?></h2>
			<h4 class="titleSub"></h4>
            
            </div>
            
            <div class="prxtop2">
            	<div class="proInner">
                    <h2 class="protitle2"><?php
                                            if (isset($options["PRINT_ELASTIC_BRA_CUP_TITLE"])) {
                                                echo $options["PRINT_ELASTIC_BRA_CUP_TITLE"];
                                            } else {
                                                echo '';
                                            }
                                            ?></h2>
                    <?php
                                        if (isset($options["PRINT_ELASTIC_BRA_CUP_DESCRIPTION"])) {
                                        echo html_entity_decode($options["PRINT_ELASTIC_BRA_CUP_DESCRIPTION"]);
                                        } else {
                                        echo '';
                                        }
                                        ?>
                </div>
            
                
            
            </div>
            
            
            
            
             <div style=" clear:both"></div>

  
  

            </div>
            <div style="clear:both"></div>
            
            <!--<div class="prxtop4">
            
            <div class="proInner">
            
                 <p class="prodis prodisT3"></p>
                
               </div>

            
            </div>-->

        
        </div>
        </div>
        </div>
        </div>
        
        
        
 
  </div> <!-- wrapper --> 
 

    
       <script type="text/javascript">
     $(document).ready(function(){
			$(window).bind('scroll',function(e){
				parallaxScroll();
			});
		 
			function parallaxScroll(){
				var scrolledY = $(window).scrollTop();
				$('.prxtop1').css('background-position','center -'+((scrolledY*0.2))+'px');
				$('.titleXlarge').css('marginTop','-'+((scrolledY*0.5))+'px');
				$('.fish').css('top','-'+((scrolledY*0.8))+'px');
			}
		 
		});
    </script>
    


<!-- Here we want a DYNAMIC value -->

</body>
</html>