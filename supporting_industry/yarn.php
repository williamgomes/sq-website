<?php
include '../config/config.php';
$options = array();
$fields = "'YARN_DYEING_BANNER_TITLE','YARN_DYEING_BANNER','YARN_DYEING_DESCRIPTION','YARN_DYEING_TITLE','YARN_DYEING_QUICKREAD','YARN_DYEING_BOTTOM_IMAGE_URL'";
$suopportingIndustryFieldsSql = "SELECT CS_option,CS_value FROM config_settings WHERE CS_option IN ($fields)";
$suopportingIndustryFieldsResult = mysqli_query($con, $suopportingIndustryFieldsSql);
if ($suopportingIndustryFieldsResult) {
    while ($suopportingIndustryFieldsResultRowObj = mysqli_fetch_object($suopportingIndustryFieldsResult)) {
        $options[$suopportingIndustryFieldsResultRowObj->CS_option] = $suopportingIndustryFieldsResultRowObj->CS_value;
    }
} else {
    if (DEBUG) {
        $err = "suopportingIndustryFieldsResult error" . mysqli_error($con);
    } else {
        $err = "suopportingIndustryFieldsResult query failed";
    }
}
$slideArray = array();
$slideSql = "SELECT AI_album_id,AI_image_name FROM album_images WHERE AI_album_id IN (SELECT album_id FROM album WHERE album_keyword='YARN_DYEING')";
$slideResult = mysqli_query($con, $slideSql);
if ($slideResult) {
    while ($slideResultRowObj = mysqli_fetch_object($slideResult)) {
        $slideArray[] = $slideResultRowObj->AI_image_name;
        $slide_album_id = $slideResultRowObj->AI_album_id;
    }
} else {
    if (DEBUG) {
        echo'slideResult Error' . mysqli_error($con);
    } else {
        echo'Slide Result Error';
    }
}
/* Start code for menu active */
$pageUrlName = '';
$pageUrlName = basename(__FILE__, '.php');
$pageUrlName = $pageUrlName . '.php';
/* End code for menu active */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <title>SQ GROUP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <?php
            include("supporting_industry_sub_left_navigation.php");
            ?>
    </head>

    <body class="productbody">

        <div id="topBarContainer"> </div>
        <div class="mobilemenu" style="display:none">
            <div class="rmm">
                <?php include ('supporting_industry_left_navigation.php'); ?>
            </div>
        </div>
        <div id="wrapper">

            <div id="leftPanel">
                <div class="shadowIe"></div>
                <div class="logo">
                    <p align="center">
                        <a href="<?php echo baseUrl(); ?>index.php"><img width="100" src="<?php echo baseUrl(); ?>images/logo.png" /></a>
                        <br>
                            <span class="sus">
                                <a class="sasLogoLink sasLogoLinkExtra" href="<?php echo baseUrl(); ?>supporting_industry">  Supporting <br />
                                    Industries  </a>
                            </span>
                    </p>
                </div>

                <div class="leftMenu">
                    <?php include ('supporting_industry_left_navigation.php'); ?>
                </div>
                <div class="hrcenter"></div>
                <div class="slogan"><p align="center">
                        <img width="140" src="<?php echo baseUrl(); ?>images/slogan.png" />
                    </p>


                </div>

                <div class="hrcenter"></div>

            </div>

            <div id="rightPanel">
                <div id="container">
                    <div class="contentbox">

                        <div class="prxtop1 prxtopYarn" style="background:url(<?php echo baseUrl('upload/supporting_industry/yarn_dyeing/' . $options['YARN_DYEING_BANNER']); ?>) center 0px fixed ;">

                            <h2 class="titleXlarge titleMLargeNormal"><?php
                                if (isset($options["YARN_DYEING_BANNER_TITLE"])) {
                                    echo $options["YARN_DYEING_BANNER_TITLE"];
                                } else {
                                    echo '';
                                }
                                ?></h2>
                            <h4 class="titleSub"></h4>
                        </div>

                        <div class="prxtop2">
                            <div class="proInner">

                                <h2 style="opacity:1 !important;" data-name="yarnModal" class="protitle2 quickRead modallink">
                                    <?php
                                    if (isset($options["YARN_DYEING_TITLE"])) {
                                        echo $options["YARN_DYEING_TITLE"];
                                    } else {
                                        echo '';
                                    }
                                    ?></h2>
                                <?php
                                if (isset($options["YARN_DYEING_DESCRIPTION"])) {
                                    echo html_entity_decode($options["YARN_DYEING_DESCRIPTION"]);
                                } else {
                                    echo '';
                                }
                                ?>
                            </div>

                        </div>

                    </div>



                    <div style="clear:both"></div>
                    <?php
                        if ($options["YARN_DYEING_BOTTOM_IMAGE_URL"] != '') {
                            ?>
                    <div class="sqTables clearfix">
                        <h1 class="text-center"> <img alt="knitwear" src="<?php echo $options["YARN_DYEING_BOTTOM_IMAGE_URL"]; ?>" class="img-responsive"></h1>
                    </div>
                    <?php
                        }
                        ?>

                    <div style="clear:both"></div>
                    <?php
                    $slideArrayCount = count($slideArray);
                    if ($slideArrayCount > 0) {
                        ?>
                        <div class="prokSide">
                            <div class="slidePng">
                                <a class="prev2" href="<?php echo baseUrl(); ?>#"></a>

                                <a class="next2" href="<?php echo baseUrl(); ?>#"></a>  


                            </div>




                            <div style=" clear:both"></div>
                            <div class="prokSideContent">
                                <?php
                                for ($i = 0; $i < $slideArrayCount; $i++) {
                                    ?>
                                    <img src="<?php echo baseUrl(); ?>upload/album/<?php echo$slide_album_id . "/" . $slideArray[$i] ?>" alt="yarn" />
                                    <?php
                                }
                                ?>
                            </div>
                            <div style=" clear:both"></div>
                        </div>

                        <?php
                    }
                    ?>






                </div>
            </div>
        </div>
        <!-- wrapper --> 

        <div class="sqModal fullW" data-id="yarnModal"> <a class="modalclose"> X </a>
            <div class="modalinner2 modalinner-80">
                <div id="modal" class="owl-carousel">
                    <?php
                    if (isset($options["YARN_DYEING_QUICKREAD"])) {
                        echo html_entity_decode($options["YARN_DYEING_QUICKREAD"]);
                    }
                    ?>

                </div>

            </div>
        </div>




        <script type="text/javascript">
            $(document).ready(function() {
                $(window).bind('scroll', function(e) {
                    parallaxScroll();
                });

                function parallaxScroll() {
                    var scrolledY = $(window).scrollTop();
                    $('.prxtop1').css('background-position', 'center -' + ((scrolledY * 0.2)) + 'px');
                    $('.titleXlarge').css('marginTop', '-' + ((scrolledY * 0.5)) + 'px');
                    $('.fish').css('top', '-' + ((scrolledY * 0.8)) + 'px');
                }

            });
        </script>
        <?php
        include(basePath('quick_reader.php'));
        ?>



        <!-- Here we want a DYNAMIC value -->

    </body>
</html>