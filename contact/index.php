<?php
include '../config/config.php';
/** Start: query for contact information * */
$contactArray = array();

$contactArray['BANNER_IMAGE'] = '';
$contactArray['PHONE_CALL'] = '';
$contactArray['EMAIL'] = '';
$contactArray['VISIT'] = '';
$contactArray['SKYPE'] = '';

$contact_sql = mysqli_query($con, "SELECT * FROM contact");
if ($contact_sql) {
    while ($getContact = mysqli_fetch_object($contact_sql)) {
        $contactArray[$getContact->contact_option] = $getContact->contact_value;
    }
}

/** End: query for contact information * */

/* Start code for active side menu */
    $path = dirname($_SERVER['PHP_SELF']);
    $position = strrpos($path, '/') + 1;
    $pageUrlName = substr($path, $position);
/* End code for active side menu*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <title>SQ GROUP | Contact</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <?php
            include("contact_header.php");
            ?>
    </head>
    <body class="ContactPage">
        <div id="topBarContainer"> </div>
        <div class="mobilemenu" style="display:none">
            <div class="rmm">
                <?php
                include '../home_left_menu.php';
                ?>
            </div>
        </div>
        <div id="wrapper">

            <div id="leftPanel">
                <div class="shadowIe"></div>
                <div class="logo">
                    <p align="center">
                        <a href="<?php echo baseUrl(); ?>index.php"><img width="100" src="<?php echo baseUrl(); ?>images/logo.png" /></a>
                        <br />

                        <span class="sus">
                            <a class="sasLogoLink sasLogoLinkExtra " href="<?php echo baseUrl(); ?>contact/">  
                                Contact us   </a>
                        </span>
                    </p>
                </div>

                <div class="leftMenu">
                    <?php
                    include '../home_left_menu.php';
                    ?>
                </div>
                <div class="hrcenter"></div>
                <div class="slogan"><p align="center">
                        <img width="140" src="<?php echo baseUrl(); ?>images/slogan.png" />
                    </p>


                </div>

                <div class="hrcenter"></div>

            </div>

            <div id="rightPanel" class="rightInner ">

                <div class="contactContainer">

                    <?php
                    if (isset($contactArray['BANNER_IMAGE']) && $contactArray['BANNER_IMAGE'] != '') {
                        ?>
                        <div class="contactBanner">
                            <img src="<?php echo baseUrl('upload/contact_banner_image/small/' . $contactArray['BANNER_IMAGE']); ?>" width="100%"/>
                        </div>
                        <?php
                    }
                    ?>

                    <div style="clear:both"></div>

                    <div class="contactInner">
                        <div class="">


                            <h3>Let's talk</h3>
                            <p class="pDot"></p>

                            <div style="clear:both"></div>

                            <div class="contactAddress">
                                <img src="<?php echo baseUrl(); ?>images/phone.png" width="42" height="42" alt="phone" />
                                <div class="contactInfo">
                                    <h6>Call</h6>
                                    <p><?php echo $contactArray['PHONE_CALL']; ?></p>
                                </div><!--contactInfo-->
                            </div><!--contactAddress-->

                            <div class="contactAddress">
                                <img src="<?php echo baseUrl(); ?>images/social_email.png" width="42" height="42" alt="phone" />
                                <div class="contactInfo">
                                    <h6>Email</h6>
                                    <p><?php echo $contactArray['EMAIL']; ?></p>
                                </div><!--contactInfo-->
                            </div><!--contactAddress-->


                            <div style="margin-right:0;" class="contactAddress">
                                <img src="<?php echo baseUrl(); ?>images/skype.png"  height="42" alt="phone" />
                                <div class="contactInfo">
                                    <a href="<?php echo baseUrl(); ?>www.skype.com/sqgroupbd">Skype</a>
                                    <p><?php echo $contactArray['SKYPE']; ?></p>
                                </div><!--contactInfo-->
                            </div><!--contactAddress-->

                            <div style="clear:both"></div>

                            <div class="contactAddress visit">
                                <img src="<?php echo baseUrl(); ?>images/home.png" width="42" height="42" alt="phone" />
                                <div class="contactInfo">
                                    <h6>Visit</h6>

                                    <?php
                                    echo html_entity_decode($contactArray['VISIT']);
                                    ?>
                                </div><!--contactInfo-->

                            </div><!--contactAddress-->

                            <!--contactAddress-->


                        </div><!--padding-->
                    </div><!--contactInner-->

                </div>
                <!-- Container --> 
            </div>
        </div>




    </body>
</html>